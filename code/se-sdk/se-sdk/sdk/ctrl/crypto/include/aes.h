/******************************************************************************
* \file  aes.h
* \brief code AES
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#ifndef __AES_H__
#define __AES_H__

#include <stdint.h>

#define AES_ENCRYPT ( 1 )
#define AES_DECRYPT ( 2 )

uint16_t aes(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode);
uint16_t aes2(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode);

#endif // __AES_H__
