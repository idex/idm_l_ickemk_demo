
#include "main.h"

uint8_t LED_color_flag;
uint8_t LED_mode_flag;
uint8_t led_slash_flag;
uint8_t led_ms_status=0;


void LED_port_initial(void)
{


	//led green

	am_hal_gpio_pinconfig(LEDG_Pin,g_AM_HAL_GPIO_OUTPUT);
	am_hal_gpio_output_clear(LEDG_Pin);
	//led red

	am_hal_gpio_pinconfig(LEDR_Pin,g_AM_HAL_GPIO_OUTPUT);
	am_hal_gpio_output_clear(LEDR_Pin);


	 LED_color_flag=0;
	 LED_mode_flag=0;
	 led_slash_flag=0;


}


void led_control_process(uint8_t led_new_color_flag)
{
	if(LED_color_flag==led_new_color_flag)return;
	LED_color_flag=led_new_color_flag;
	LED_mode_flag=0;
	led_slash_flag=0;
	LED_GREEN_OFF;
	LED_RED_OFF;
	switch (LED_color_flag)
		{
		case 0x01://green always on
		LED_GREEN_ON;
		LED_RED_OFF;
		break;
		case 0x02://red always on
		LED_RED_ON;
		LED_GREEN_OFF;
		break;
		case 0x03://green and red always on
		LED_GREEN_ON;
		LED_RED_ON;
		break;
		case 0x04://yellow on, ignore
		break;
		case 0x10:
		case 0x08://green slash
		LED_mode_flag=0x01;
		led_ms_status=1;
		break;

		case 0x20://red slash
		LED_mode_flag=0x02;
		led_ms_status=1;
		break;
		case 0x40://yellow slash ignore
		break;
		case 0x80://green and red alternating
		LED_mode_flag=0x04;
		led_ms_status=1;

		break;
		case 0x00:
		default:
		LED_RED_OFF;
		LED_GREEN_OFF;
		LED_mode_flag=0;
		led_ms_status=0;
		break;
		}

}


void led_light_process(void)
{
	switch(LED_mode_flag)
	{
	case 0x01://green slash
		{
		if(0==led_slash_flag)
			{
			LED_GREEN_ON;
			}
		else
			{
			LED_GREEN_OFF;
			}
		led_slash_flag++;
		led_slash_flag&=0x01;
		}
	break;
	case 0x02:
		{
		if(0==led_slash_flag)
			{
			LED_RED_ON;
			}
		else
			{
			LED_RED_OFF;
			}
		led_slash_flag++;
		led_slash_flag&=0x01;
		}

	break;
	case 0x03:
		{
		if(0==led_slash_flag)
			{
			LED_RED_ON;
			LED_GREEN_ON;
			}
		else
			{
			LED_RED_OFF;
			LED_GREEN_OFF;
			}
		led_slash_flag++;
		led_slash_flag&=0x01;
		}

	break;
	case 0x04:
			{
		if(0==led_slash_flag)
			{
			LED_RED_OFF;
			LED_GREEN_ON;
			}
		else
			{

			LED_GREEN_OFF;
			LED_RED_ON;

			}
		led_slash_flag++;
		led_slash_flag&=0x01;
		}

	break;
	default:
		break;


	}
}






