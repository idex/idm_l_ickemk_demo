

#include "main.h"

extern uint8_t wtx_sensor_interrupt_flag;

extern int appolo3_spi_transfer( void *p, unsigned char *buffer, int txSize, int rxSize );
extern void apollo3_spi_fast_send(uint8_t *data, uint8_t data_size);


void idxicksuspend_with_wtx(void)
{
	uint8_t data[8];
	data[0]=0x53;
	data[1]=0x53;
	apollo3_spi_fast_send(data,2);

}

void idxickresume_with_wtx(void)
{
	uint8_t data[2];
	data[0]=0x52;
	data[1]=0x52;
	apollo3_spi_fast_send(data,2);
}

void wtx_stage_one_callback(void)
{

	uint8_t data[2];
	if(0x01==wtx_sensor_interrupt_flag)
		{
		idxicksuspend_with_wtx( );
		am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x06;
		while(1)
			{
			data[0]=am_hal_ios_pui8LRAM[wtx_power_reg];
			data[1]=am_hal_ios_pui8LRAM[wtx_power_reg+1];
			if(0x09==data[0])
				{
				if(0x00==data[1])//process wtx
					{
					idxickresume_with_wtx( );
					am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x09;
					break;
					}
				}
			uSleep(100);
			}
		wtx_sensor_interrupt_flag=0;
		}

}


void wtx_stage_two_callback(void)
{

	uint8_t data[2];
	if(0x01==wtx_sensor_interrupt_flag)
		{
		am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x06;
		while(1)
			{
			data[0]=am_hal_ios_pui8LRAM[wtx_power_reg];
			data[1]=am_hal_ios_pui8LRAM[wtx_power_reg+1];
			if(0x09==data[0])
				{
				if(0x00==data[1])//process wtx
					{
					am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x09;
					break;
					}
				}
			uSleep(100);
			}
		wtx_sensor_interrupt_flag=0;
		}
		
}

void wtx_stage_three_callback(void)
{

	uint8_t data[2];
	am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x06;
	while(1)
		{
		data[0]=am_hal_ios_pui8LRAM[wtx_power_reg];
		data[1]=am_hal_ios_pui8LRAM[wtx_power_reg+1];
		if(0x09==data[0])
			{
			if(0x00==data[1])//process wtx
				{
				am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x09;
				break;
				}
			}
		uSleep(100);
		}

}










