/** ***************************************************************************
 * \file   idxHalSystem.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         System configuration. If a new platform is to be supported
 *         then it is expected that the HAL APIs are implemented for that
 *         platform in order to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_SYSTEM_H_
#define _IDX_HAL_SYSTEM_H_

#include <stdint.h>
#include "idxSeEmkDefines.h"
#include "idxSerialInterface.h"
#include "_idxHalSystem.h"
 /**
 * @defgroup hal_system Hal system module
 * hal_system module provides system initialization/sleep/control frequency APIs for implementation
 * @{
 */

/*!
 * @brief Template storage area structure
 */
struct idxHalSystemTemplateArea_s
{
	uint8_t *pTemplateBase; //!< Base of area available for template storage
	uint32_t TemplateSize; //!< Size of area available for template storage
};

/** \brief Define SE power states
 */
typedef enum idxSePowerState {
  idxPowerStateSE,     ///< Power required by the SE (normal operation)
  idxPowerStateBio,    ///< Power required by the biometric MCU and/or Sensor
  idxPowerStateComms,  ///< Shared power usage while host interface communication occurs
  idxPowerStateLow,    ///< Low power state
  idxPowerStateLowest, ///< Lowest power state (after enrol)
} idxSePowerState;

extern const idx_MatcherWorkArea_t idxHalSystemWorkAreas[2];
extern const struct idxHalSystemTemplateArea_s idxHalSystemTemplateArea;
extern const struct idxBioPolicies_s idxHalSystemPolicies;

/** \brief Initialize System HAL module on startup
 *
 * Also initialize the modules of the HAL that are not required on all
 * IDEX systems (typically SPI and/or UART).
 */
void idxHalSystemInit(void);

/**
* Force system to enter sleep mode.
*
* @return     NONE.
*/
void idxHalSystemSleep(void);

/** \brief Wait for an interrupt
 *
 * If *\b flag is initally false then wait for an interrupt
 * ideally in a low power sleep mode.
 * \b mode may be used, for example, to avoid actually waiting for
 * an interrupt (in a low power sleep mode) in MODE_CONTACT
 * perhaps with the aim that certain clocks reamin running.
 * It is assumed that *\b flag will be set in the service routine
 * of one of the possible interrupts. The implementation is required
 * to avoid the inherent race condition in first testing *\b flag
 * then waiting, in between which the interrupt may occur.
 *
 * \param[in] mode System operating mode
 * \param[in] *flag Flag to test before waiting.
 */ 
void idxHalSystemWaitForInt(uint8_t mode, const volatile bool *flag);

void idxHalSystemHminDegradation(void);

/** \brief Set the SE power state
 *
 * \param[in] powerState The required power state
 */
void idxHalSystemSetPowerState(idxSePowerState powerState);

/** @}*/
#endif /* _IDX_HAL_SYSTEM_H_ */
