
#include "main.h"





unsigned short crc16_ccitt( unsigned char *x, int size )
{
	unsigned short crc_new = 0 ;
	int i ;

	for( i=0; i<size; i++ )
	{
		crc_new  = (unsigned char)(crc_new >> 8) | (crc_new << 8);
		crc_new ^= x[i] ;
		crc_new ^= (unsigned char)(crc_new & 0xff) >> 4;
		crc_new ^= crc_new << 12;
		crc_new ^= (crc_new & 0xff) << 5;
	}
	
	return crc_new ;
}

void AddCRC(uint8_t * data, int data_size, uint8_t *data_crc)
{
	uint16_t crc=0;

   crc = crc16_ccitt(data, data_size);   //CRChdr is calculated over fixed 4 bytes
   data_crc[0]=(uint8_t)(crc&0x00ff);
   data_crc[1]=(uint8_t)((crc&0xff00)>>8);
}

int CheckCRC(uint8_t * data, int data_size, uint8_t *data_crc)
{
	uint16_t crc=0;
	uint16_t cmp_crc=0;
	crc = crc16_ccitt(data, data_size);   //CRChdr is calculated over fixed 4 bytes
	cmp_crc= (((((uint16_t)data_crc[1])<<8)&0xff00)+((uint16_t)data_crc[0] &0x00ff));
	if(cmp_crc==crc)
		{return 0;}
	else
		{return -1;}
	
}




