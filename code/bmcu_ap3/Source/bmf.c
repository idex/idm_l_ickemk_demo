

#include "main.h"

extern idx_State state;
extern uint8_t *dataImage;
extern uint8_t epd_show_flag;
extern uint8_t* shared_heap_start_address;
extern uint8_t wtx_sensor_process_flag;




extern const SensorHeapCallbacks emk_heap_callbacks;

extern void Sleep( uint32_t ms );
extern void uSleep( uint32_t us );
extern void wtx_stage_two_callback(void);
extern int idx_idm_listrecord(idx_State *pState);
extern int idx_idm_DeleteRecord(idx_State *pState);
extern int idxFlashEntryUpdate(idx_State *pState, uint16_t handle, uint32_t offset, uint32_t size);
extern int idx_template_data_store( idx_TemplateData_t* pTemplateData, uint32_t availible_address);
extern void AddCRC(uint8_t * data, int data_size, uint8_t *data_crc);
extern int idx_split_prepare_image(idx_State *pState);
extern int sensor_initial_process_with_wtx(idx_State *pState);
extern int idxFlashEntryCount(idx_State *pState);
extern void led_control_process(uint8_t led_new_color_flag);
extern void enable_96mhz (void);
extern void disable_96mhz(void);



void idx_set_reply_info(uint16_t return_code, idx_State *pState)
{
	uint16_t temp_data;
	temp_data=pState->commDataSize +IDX_COMMAND_HEADER_SIZE;
	pState->pCommHeader[0]= (uint8_t)temp_data;
	pState->pCommHeader[1]=(uint8_t)((temp_data>>8)&0xff);
	pState->pCommHeader[2]=(uint8_t)(return_code&0x00ff);
	pState->pCommHeader[3]=(uint8_t)((return_code&0xff00)>>8);
	AddCRC(pState->pCommHeader,4,&pState->pCommHeader[4]);
}


void idx_error_cmd_process(idx_State * pState)
{
	int result;
	wtx_stage_two_callback();

	result=ISO_ERR_BAD_COMMAND;
	pState->commDataSize=0;
	idx_set_reply_info(result,pState);

}

int idx_idm_einkshow()
{
	epd_show_flag=0x01;
	return ISO_SUCCESS;
}


void idx_eink_show_process(idx_State * pState)
{
	int result;
	wtx_stage_two_callback();
	result=idx_idm_einkshow();
	pState->commDataSize=0;
	idx_set_reply_info(result,pState);
}

int idx_idm_getversion(idx_State *pState)
{
	//idx_UnitVersion_t emk_version;

	pState->pCommData[0]=0x00;
	pState->pCommData[1]=0x01;
	pState->pCommData[2]=0x59;
	pState->pCommData[3]=0x25;
	pState->pCommData[4]=0x02;
	pState->pCommData[5]=0x05;
	pState->pCommData[6]=0xb3;
	pState->pCommData[7]=0x01;
	pState->commDataSize=8;
	//idxEmkExGetUnitVersion(&emk_version);
	//printf("v=%x,rv=%x,bd=%x\r\n",emk_version.version,emk_version.revision,emk_version.build);
	return ISO_SUCCESS;

}

void idx_get_version_process(idx_State * pState)
{
	int result;

	wtx_stage_two_callback();

	result= idx_idm_getversion(pState);
	idx_set_reply_info(result,pState);
}

int idx_idm_getimage(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	if(NULL==dataImage)
		{
		ret.idxRet = ISO_ERR_BAD_PARAMETERS;
		return ret.idxRet;
		}
	pState->pSavedData=dataImage;
	pState->savedDataSize=pState->pLoadImage_ob.offset;
	pState->pCommData[2]=0;
	pState->pCommData[3]=0;
	*(uint16_t *)pState->pCommData=pState->pLoadImage_ob.offset;
	pState->commDataSize=4;
	//printf("gisize=%x\r\n",pState->commDataSize);
	return ret.idxRet;
}

void idx_get_image_procss(idx_State *pState)
{
	int result;
	wtx_stage_two_callback();
	result= idx_idm_getimage(pState);
	idx_set_reply_info(result,pState);
}

void idx_get_loaded_image_information(idx_State *pState)
{
	pState->pSavedData=dataImage;
	switch(pState->pLoadImage_ob.offset)
		{
		case 16641:
			{
			pState->imageInfo.width=129;
			pState->imageInfo.height=129;
			}
			break;
		case 17292:
		default:
			{
			pState->imageInfo.width=132;
			pState->imageInfo.height=131;
			}
			break;
		}
	pState->imageInfo.DPI=pState->sensorInfo.DPI;
	pState->imageInfo.depth = 8;	
	pState->imageInfo.ridgeColor = BLACK_RIDGE;   // Supported: 0 = BLACK RIDGE  
}


int idx_idm_loadimage(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	uint16_t data_size;
	
	if(0==pState->pLoadImage_ob.flag)
		{
		pState->pLoadImage_ob.offset=0;

		if(NULL==dataImage)	dataImage=shared_heap_start_address;//to do: get correct image address;
		if(NULL==dataImage)
			{
			ret.idxRet = ISO_ERR_NO_SPACE;
			return ret.idxRet;
			}
		data_size= *(uint16_t *)pState->pCommHeader - IDX_COMMAND_HEADER_SIZE;
		memcpy(dataImage, pState->pCommData,data_size);
		pState->pLoadImage_ob.offset=data_size;
		pState->pLoadImage_ob.flag=0x01;
		}
	else
		{
		data_size= *(uint16_t *)pState->pCommHeader - IDX_COMMAND_HEADER_SIZE;
		if(17292<(data_size+pState->pLoadImage_ob.offset))
			{
			ret.idxRet = ISO_ERR_SIZE;
			return ret.idxRet;
			}
		memcpy(dataImage+pState->pLoadImage_ob.offset, pState->pCommData,data_size);
		pState->pLoadImage_ob.offset +=data_size;
		if(0x80==(0x80&pState->pCommHeader[3]))//last trunk
			{
			pState->mcuState =BIOMETRIC_IMAGE;
			pState->pLoadImage_ob.flag=0;
			idx_get_loaded_image_information(pState);
			}
		}
	pState->commDataSize=0;
	return ret.idxRet;


}


void idx_load_image_procss(idx_State *pState)
{
	int result;
	wtx_stage_two_callback();
	result= idx_idm_loadimage(pState);
	idx_set_reply_info(result,pState);
}


void idx_list_record_procss(idx_State *pState)
{
	int result;
	wtx_stage_two_callback();
	result= idx_idm_listrecord(pState);
	idx_set_reply_info(result,pState);
}

void idx_delete_record_procss(idx_State *pState)
{
	int result;
	wtx_sensor_process_flag=0x02;
	wtx_stage_two_callback();

	result= idx_idm_DeleteRecord(pState);
	idx_set_reply_info(result,pState);
}

int idx_idm_setparameter(idx_State *pState)
{
	int ret;
	switch (pState->pCommData[0])
	{
	case 135:

		led_control_process(pState->pCommData[1]);
		pState->commDataSize=0;
		ret = ISO_SUCCESS;
		break;
	default:
		break;
		}

return ret;
}

void idx_set_parameter_procsss(idx_State *pState)
{
	int result;
	wtx_stage_two_callback();
	result=idx_idm_setparameter(pState);
	idx_set_reply_info(result,pState);
}




int idx_idm_dataflush(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, IDX_SUCCESS };
	idx_MatcherCfg_t pConfig = { 0 };

	// flag is not used yet
	//unsigned short *flag = (unsigned short *)pState->pCommData;
	pState->commDataSize=0;

	ret = idxEmkExUninitialize(pState->pEmkHandle);
	if(IDX_SUCCESS!=ret.idxRet)
		{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
		}
	else
		{
		ret = idxEmkExGetDefaultConfig(&pConfig);	//	Load out configuration with default values.
		pConfig.imageWidth= idxIckGetSizeX();
		pConfig.imageHeight= idxIckGetSizeY();
		pConfig.imageDPI= idxIckGetSensorDPI();
		pConfig.enrollMode = ACCEPT_ALL;
		pState->sensorInfo.width=pConfig.imageWidth;
		pState->sensorInfo.height=pConfig.imageHeight;
		pState->sensorInfo.DPI=pConfig.imageDPI;
		ret=idxEmkExInitialize(&pConfig,&emk_heap_callbacks,&pState->pEmkHandle);
		//observe_data=5;

		if (ret.idxRet != IDX_SUCCESS)
			{
			printf("Failed to initialize the EMK\r\n");
			ret.idxRet= ISO_ERR_STATE;
			}
		else
			{
			ret.idxRet= ISO_SUCCESS;
			
			}
		}
	

/*
	ret = idxEmkExFlush(pState->pEmkHandle);
	pState->commDataSize=0;
	if(IDX_SUCCESS==ret.idxRet)
		{
		ret.idxRet = ISO_SUCCESS;
		}
	else
		{
		ret.idxRet = ISO_ERR_STATE;
		}

	*/
	return ret.idxRet;

}


void idx_data_flush_process(idx_State *pState)
{
	int result;
	wtx_stage_two_callback();
	result=idx_idm_dataflush(pState);
	idx_set_reply_info(result,pState);
}




uint8_t* idxFlashEntryAllocate(idx_State *pState, uint32_t size,
	uint16_t *pHandle)
{
	int32_t count = 0;
	uint8_t* ret = NULL;

	count = idxFlashEntryCount(pState);

	if ((count == FLASH_MAX_ENTRIES)||(size > IDX_FLASH_SIZE))
	{
		return ret;// too many entries stored
	}
	ret= (uint8_t *)(pState->pTemplateStartAddress+ 0x2000*count);
	*pHandle=count;
	return ret;
}




int idx_idm_storeblob(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	uint16_t handle;
	uint8_t* address;
	idx_TemplateData_t templateData;
	//unsigned char *pTemplate;
	uint32_t dataSize;
	//idx_StoreBlobData *pData;
	// check MCU state
	if (BIOMETRIC_TEMPLATE != pState->mcuState)
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}

	// read command data
	//pData = (idx_StoreBlobData *)pState->pCommData;
	//dataSize = pData->size;

	// get template from EMK
	
	idxEmkExGetTemplateToStore(pState->pEmkHandle, &templateData);

	//pTemplate = templateData.data;
	dataSize = templateData.size;


	// allocate space for template
	address = idxFlashEntryAllocate(pState, dataSize, &handle);
	if (address)
	{
		idx_template_data_store(&templateData,(uint32_t)address);
		// if previous store command successfully completed, update header
		ret.idxRet = idxFlashEntryUpdate(pState, handle, (uint32_t)address, dataSize);
		if(IDX_SUCCESS!=ret.idxRet)
			{
			pState->commDataSize=0;
			return ret.idxRet;
			}
		// prepare reply
		memcpy(pState->pCommData, &handle, sizeof(handle));
		pState->commDataSize = sizeof(handle);


		ret.idxRet = ISO_SUCCESS;
	}
	else
	{
		pState->commDataSize=0;
		ret.idxRet = ISO_ERR_NO_SPACE;
	}

	return ret.idxRet;

}

void idx_store_blob_process(idx_State *pState)
{
	int result;
	wtx_sensor_process_flag=0x02;
	wtx_stage_two_callback();

	result=idx_idm_storeblob(pState);
	idx_set_reply_info(result,pState);
}

int idx_idm_readdata(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	idx_ReadData *pData;
	uint32_t size;
	uint32_t offset;
	// check MCU state
	if (BIOMETRIC_UNINITIALIZED == pState->mcuState)
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}

	// read command data
	pData = (idx_ReadData *)pState->pCommData;
	size = pData->size;
	offset = pData->offset;
	//printf("size=%x,offset=%x\r\n",size,offset);
	// check if we are within bounds 
	if (offset + size > pState->savedDataSize)
		ret.idxRet = ISO_ERR_SIZE;
	else
	{
		memcpy(pState->pCommData, &pState->pSavedData[offset], size);
		pState->commDataSize = (uint16_t)size;

		ret.idxRet = ISO_SUCCESS;
	}
	return ret.idxRet;

}

void idx_read_data_process(idx_State *pState)
{
	int result;
	wtx_stage_two_callback();
	result=idx_idm_readdata(pState);
	idx_set_reply_info(result,pState);
}

int idx_idm_matchtemplate(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	int32_t handleToFind;
	int32_t i = 0;
	uint16_t templateCount = 0;
	idx_TemplateData_t inTemplateData;
	idx_TemplateData_t outTemplateData;
	idx_TemplateData_t pEnrollTemplateData;


	// check MCU state
	if (BIOMETRIC_TEMPLATE != pState->mcuState)
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}




	// read command data
	inTemplateData.size = *(uint16_t *)(pState->pCommData + 3);
	inTemplateData.data = pState->pCommData + 5;
	handleToFind = inTemplateData.data[0];


	// the command passes the handle of the template in the data

	

	templateCount=0;
	for(i=0;i<FLASH_MAX_ENTRIES;i++)
		{
		
		if((uint8_t)handleToFind== *(uint8_t *)(pState->pFlashStartAddress + 16*i +8))
			{
			pEnrollTemplateData.data=(uint8_t *)(*(uint32_t *)(pState->pFlashStartAddress + 16*i));
			pEnrollTemplateData.size=*(uint32_t *)(pState->pFlashStartAddress + 16*i +4);
			templateCount=1;
			break;
			}
		}


	if (0 == templateCount)
	{
		ret.idxRet = IDX_ERR_REC_NOT_FOUND;
		pState->commDataSize=0;
	}
	else
	{
		// match template
		ret = idxEmkExMatch(pState->pEmkHandle,
			0,
			0,
			templateCount,
			&pEnrollTemplateData,
			0,
			&outTemplateData,
			0);
		if (IDX_SUCCEEDED(0, ret))
		{
			uint32_t templateSize = outTemplateData.size;
			// store template in safe location,prepare for read
			memcpy(pState->pSavedData, outTemplateData.data, templateSize);
			pState->savedDataSize = (uint16_t)outTemplateData.size;

			// prepare reply
			memcpy(pState->pCommData, &templateSize, sizeof(uint32_t));
			pState->commDataSize = sizeof(uint32_t);
			ret.idxRet=ISO_SUCCESS;
		}
		else
			{
			ret.idxRet=ISO_ERR_UNKNOWN;
			pState->commDataSize=0;
			
			}
		
	}
	return ret.idxRet;

}

void idx_match_template_process(idx_State *pState)
{
	int result;
	wtx_sensor_process_flag=0x02;
	wtx_stage_two_callback();

	result=idx_idm_matchtemplate(pState);
	idx_set_reply_info(result,pState);
}

int idx_idm_mergetemplate(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	idx_MergeInfo_t mergeInfo;
	idx_TemplateData_t outTemplateData;
	//uint8_t flags;
	uint8_t numTemplates;

	// check MCU state
	if (BIOMETRIC_TEMPLATE != pState->mcuState)
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}



	// read command data
	//flags = pState->pCommData[0];
	numTemplates = pState->pCommData[1];
	
	// merge template
	ret = idxEmkExMerge(pState->pEmkHandle,
		0,
		numTemplates,
		0,
		0,
		&mergeInfo,
		&outTemplateData);
	if (IDX_SUCCEEDED(0, ret))
	{
		// store template in safe location,prepare for read
		memcpy(pState->pSavedData, &mergeInfo, sizeof(mergeInfo));
		pState->savedDataSize = sizeof(mergeInfo);
		memcpy(&pState->pSavedData[sizeof(mergeInfo)],
			outTemplateData.data, mergeInfo.templateSize);
		pState->savedDataSize += mergeInfo.templateSize;

		// change MCU state
		pState->mcuState = BIOMETRIC_TEMPLATE;

		// prepare reply
		memcpy(pState->pCommData, &mergeInfo, sizeof(mergeInfo));
		pState->commDataSize = sizeof(mergeInfo);
		ret.idxRet=ISO_SUCCESS;
	}
	else
	{
	ret.idxRet=ISO_ERR_STATE;
	}
	return ret.idxRet;



}

void idx_merge_template_process(idx_State *pState)
{
	int result;
	wtx_sensor_process_flag=0x02;
	wtx_stage_two_callback();

	result=idx_idm_mergetemplate(pState);
	idx_set_reply_info(result,pState);

}

idx_ImageInfo_t emk_imageinfo;
idx_ExtractInfo_t emk_extractInfo;
idx_TemplateData_t emk_templateData;


int idx_idm_gettemplate(idx_State *pState)
{
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	uint8_t flag;
	idx_ImageInfo_t imageData;
	idx_TemplateData_t templateData={NULL,0};
	idx_ExtractInfo_t extractInfo={0};
	idx_basic_ImageInfo_t imageInfo = { 0 };
	idx_MatcherCfg_t config;
	if (BIOMETRIC_IMAGE != pState->mcuState)
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}
	
	emk_imageinfo.data=pState->pSavedData;
	emk_imageinfo.info=pState->imageInfo;

	memset(&emk_extractInfo,0,sizeof(emk_extractInfo));
	emk_templateData.data=NULL;
	emk_templateData.size=0;


	imageInfo.width = pState->imageInfo.width;
	imageInfo.height = pState->imageInfo.height;
	imageInfo.DPI = pState->imageInfo.DPI;
	imageInfo.depth=pState->imageInfo.depth;
	imageData.data = pState->pSavedData;
	imageData.info = imageInfo;

	

	// get current handle's matcher configuration.
	ret = idxEmkExGetMatcherConfig(pState->pEmkHandle, &config);
	if (!IDX_SUCCEEDED(0, ret))
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}

	// read command data
	flag = pState->pCommData[0];
	config.inEnrollPhase = (flag & GET_TEMPLATE_ENROLL_PHASE) ? 1 : 0;

	// set matcher configs to EMK handle.
	ret = idxEmkExSetMatcherConfig(pState->pEmkHandle, &config);
	if (!IDX_SUCCEEDED(0, ret))
	{
		ret.idxRet = ISO_ERR_STATE;
		return ret.idxRet;
	}

	// extract EMK template
	enable_96mhz();
	ret = idxEmkExExtract(pState->pEmkHandle, &imageData, &extractInfo,
		&templateData);
	disable_96mhz();
	if (IDX_SUCCEEDED(0, ret))
	{

	
		// store extracted template info and template data in safe location,prepare for read
		memcpy(pState->pSavedData, &extractInfo, sizeof(extractInfo));
		pState->savedDataSize = sizeof(extractInfo);

		memcpy(&pState->pSavedData[sizeof(extractInfo)],
			templateData.data, extractInfo.templateSize);
		pState->savedDataSize += extractInfo.templateSize;

		// change MCU state
		pState->mcuState = BIOMETRIC_TEMPLATE;

		// prepare reply
		memcpy(pState->pCommData, &extractInfo, sizeof(extractInfo));
		pState->commDataSize = sizeof(extractInfo);
		ret.idxRet=ISO_SUCCESS;
		
		/*
				// store extracted template info and template data in safe location,prepare for read
		memcpy(pState->pSavedData, &emk_extractInfo, sizeof(emk_extractInfo));
		pState->savedDataSize = sizeof(emk_extractInfo);

		memcpy(&pState->pSavedData[sizeof(emk_extractInfo)],
			emk_templateData.data, emk_extractInfo.templateSize);
		pState->savedDataSize += emk_extractInfo.templateSize;
		//printf("ok,s=%x\r\n",emk_extractInfo.templateSize);

		// change MCU state
		pState->mcuState = BIOMETRIC_TEMPLATE;

		// prepare reply
		memcpy(pState->pCommData, &emk_extractInfo, sizeof(emk_extractInfo));
		pState->commDataSize = sizeof(emk_extractInfo);
		ret.idxRet=ISO_SUCCESS;
		*/
	}
	else
		{
		ret.idxRet=ISO_ERR_STATE;
		}

return ret.idxRet;

}

void idx_get_template_process(idx_State *pState)
{
	int result;
	wtx_sensor_process_flag=0x02;
	wtx_stage_two_callback();
	result=idx_idm_gettemplate(pState);
	idx_set_reply_info(result,pState);
}

void idx_prepare_image_process(idx_State *pState)
{
	uint16_t result=0;
	result=idx_split_prepare_image(pState);
	idx_set_reply_info(result,pState);
}

void idx_initialize_process(idx_State *pState)
{
	uint16_t result=0;
	result=sensor_initial_process_with_wtx(pState);
	pState->commDataSize=0;
	idx_set_reply_info(result,pState);
}


void SE_SPI_biometrics_process(idx_State * pstate)
{

	switch (pstate->pCommHeader[2])
		{
		case IDX_HOSTIF_CMD_INITIALIZE:
			idx_initialize_process(pstate);
			break;
		case IDX_HOSTIF_CMD_PREPAREIMAGE:
			{
			idx_prepare_image_process(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_GETTEMPLATE:
			{
			idx_get_template_process(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_MERGETEMPLATES:
			{
			idx_merge_template_process(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_MATCHTEMPLATES:
			{
			idx_match_template_process(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_READ_DATA:
			{
			idx_read_data_process(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_STORE_BLOB:
			{
			idx_store_blob_process(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_BIODATAFLUSH:
			{
			idx_data_flush_process(pstate);
			}
			break;

		case IDX_HOSTIF_CMD_SETPARAMETER:
			{
			idx_set_parameter_procsss(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_DELETERECORD:
			{
			idx_delete_record_procss(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_LISTRECORD:
			{
			idx_list_record_procss(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_LOAD_IMAGE:
			{
			idx_load_image_procss(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_GETIMAGE:
			{
			idx_get_image_procss(pstate);
			}
			break;
		case IDX_HOSTIF_CMD_GETVERSION:
			{
			idx_get_version_process(pstate);
			}
			break;

		case IDX_HOSTIF_CMD_EINK_SHOW:
			{
			idx_eink_show_process(pstate);
			}
			break;
		default:
			{
			idx_error_cmd_process(pstate);
			}
			break;
		}

}




