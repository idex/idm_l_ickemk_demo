/*
 *  Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: cfg.c
 *  SCFID: 
 *  Feature: config array to  make many items convenient to use
 *  Version: V0.1
 * 
 *  History: 
 *    2014-11-18
 *      1. Original version 0.1
 * */
#include "type.h"
#include "com_cfg.h"

unsigned char const g_pbComCfg[COM_CFG_SIZE]  = {
/* PROTOCOL_SUPPORT 1*/     0x0B,
/* CT_CWT 2*/               0x05, 0x00,
/* CT_EGT/TRY 2*/           0x00, 0x06,
/* CT_BGT 1*/               0x10,
/* CT_ATR_PROTOCOL 14*/     0x02, 0x10, 0x96, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* CT_ATR_HISTORY 16*/      0x08, DEMO_VERSION, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* CT_AUTO_CFG_INFO 1*/     0x00,
/* CL3A_ATQA 2*/            0x08, 0x00, 
/* CL3A_SAK 3*/	            0x20, 0x20, 0x20,
/* CL3A_CASA 1*/            0x00,
/* CL3A_UID 4*/             0x14, 0x00, 0x00, 0x51,
/* CL3A_ATS_PROTOCOLINFO 5*/0x04, 0x78, 0x00, 0x71, 0x02,
/* CL3A_ATS_HISTORY 16*/    0x08, DEMO_VERSION, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
/* M1_DATA_ADDR*/           0x0C, 0x05,0x10,0x00,
/* CLKSYSSELOFFS5*/         0x30, 
/* CLKSYSSELOFFS6*/         0x30, 
/* CLKSYSSELOFFS7*/         0x00, 
/* CLKSYSSELOFFS8*/         0x10, 
/* CLKSYSSELOFFS9*/         0x10, 
/* CLKSYSSELOFFS10*/        0x00, 
/* CLKSYSSELOFFS11*/        0x00, 
/* CLKSYSSELOFFS12*/        0x00, 
/* CPU_CT_CLK*/             0x00,	 
/* PKE_RF_CLK*/             0x00,
/* PKE_CT_CLK*/             0x00,
/* M1_MODE*/                0x00, //0: S50, 1: S70
/* CPU_RF_CLK*/  		    0x00,
/* IS_CPUCLK_SPEC*/         0x00,
/* IS_SDPASS */             0x01,
/* CPU_RATS_CLK*/           0x00,
/* NVM_CMP_BEFORE_WRITE*/   0x01,
/* HISTORY_SN_LEN */        0x00,
/* HISTORY_SN_RFA_LEN*/     0x00,
/* RF_TYPA_FDT*/            0x01,
/* RFPOW_LEVEL*/            0x00,
/* RFPOW_DIV_PKE*/          0x16,
/* RFPOW_DIV_CPU*/          0x0B,
/* MINFS*/  				0x00
};
