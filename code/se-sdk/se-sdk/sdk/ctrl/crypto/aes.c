/******************************************************************************
* \file  aes.c
* \brief code AES
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdlib.h>
#include "aes.h"

// bmode = b0-b3   a=encrypt , 5 = decrypt
//         b4-b7   a=AES128 , 5 = AES192, 3 = AES256
//         b8-b15  0x22
//         b16-b20 5
//         b21-b23 5 = sec level normal, other = sec level high
//         b24-b27 5 = no checking compute, other =  checking compute
// return 0xaa success, other = error
#ifdef TH89
uint8_t tmcAES_ECB_Mask(uint8_t *pbKey, uint8_t *pbDataInput, uint8_t *MK, uint8_t *TK, uint8_t *pbDataOutput, uint32_t bmode);


// return 128 bit AES-ECB xor with iv

uint16_t aes(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode){
  uint8_t ret,i;
  uint8_t MK[] = {0,0,0,0,0,0,0,0,0};
  uint8_t TK[] = {0,0,0,0,0,0,0,0,0};
  uint8_t bin[16];
  uint32_t bmode = (mode == AES_ENCRYPT ? 0xa : 0x05) | (0x0a << 4) | (0x22 << 8) | (5 << 16) | (5 << 21 ) | (5 << 24);
  if(iv != NULL)  for(i = 0 ; i < 16 ; i++) bin[i] = in[i] ^ iv[i];
  else            for(i = 0 ; i < 16 ; i++) bin[i] = in[i];
  ret = tmcAES_ECB_Mask(K, bin, MK, TK, out, bmode);
  return (ret == 0xaa ? 0x9000 : 0x6969);
}

uint16_t aes2(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode){
  uint8_t ret,i;
  uint8_t MK[] = {0,0,0,0,0,0,0,0,0};
  uint8_t TK[] = {0,0,0,0,0,0,0,0,0};
  uint32_t bmode = (mode == AES_ENCRYPT ? 0xa : 0x05) | (0x0a << 4) | (0x22 << 8) | (5 << 16) | (5 << 21 ) | (5 << 24);
  ret = tmcAES_ECB_Mask(K, in, MK, TK, out, bmode);
  if (ret != 0xaa ) return 0x6969;
  if(iv != NULL)  for(i = 0 ; i < 16 ; i++) out[i] ^= iv[i];
  return  0x9000;
}
#endif
#ifdef CIU9872B_01
uint16_t aes(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode){
  return  0x9000;
}
uint16_t aes2(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint8_t mode){
  return  0x9000;
}
#endif
