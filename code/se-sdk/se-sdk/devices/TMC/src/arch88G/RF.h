/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName:  rf.h
 * * SCFID: 
 * * Feature: 14443-3 typeA state machine of PICC
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-11-18
 * *     1. Original version 0.1
 * */
#ifndef	__RF_H__
#define	__RF_H__
#pragma pack(push)
#pragma pack(1)

typedef enum {
	RFOK = 0,
	RFERROR,
	RCVDATOK,
	REQAOK
} RFFUNCRET;

#define IS_RF_POW_POS	1
#define IS_RF_RX_FIN	2
#define IS_RF_TX_FIN	4
#define IS_RF_DET_B	8
#define IS_RF_TMR_OVR	0x10
#define IS_RF_ERROR	0x20
#define CLR_RF_POW_POS	0xFE
#define CLR_RF_RX_FIN	0xFD
#define CLR_RF_TX_FIN	0xFB
#define	CLR_RF_DET_B	0xF7
#define CLR_RF_TMR_OVR	0xEF

#define PCB	(RF_BUF[0])
#define CID	(RF_BUF[1])

typedef u8 (*Anti_COMMAND)(void);

typedef enum {
	POWER_OFF = 0,
	IDLE,
	READY,
	REQUESTED,
	DECLARED,
	ACTIVE,
	HALT,
	PROTOCAL
}RFSTATUS;

typedef enum {
	RT_TYPEA = 0,
	RT_TYPEB,
	RT_NO_RF,
	RT_HAVE_RF //RF exist but not know which type(a or b)	
}ST_RFTYPE;

//RF state enum
typedef enum {
	TYPEA_IDLE = 0X00,
#ifdef TYPEA_SOFTANTI
	//Hardware deal with anti-collision process 93 20 --- 9370 9520-9570 9720 - 9770, return SAK.
	//Indicates the level of anti-collision and whether to support 14443-4
	TYPEA_READY,		
	TYPEA_ANTI,			  //state of anti-collision
#endif
	TYPEA_ACTIVE_ATS,	//witing for RATS and send ATS, if error, return IDLE or HALT, enters PPS state after sending ATS.
	TYPEA_ACTIVE_PPS,	//Enter PROTOCOL after get PPS, if the frame is not the correct PPS, can enter PROTOCOL directly.
	TYPEA_HALT,
	
	RF_ACTIVE,			  // Nomatter A of B, already finished anti-collision and activated.
}ST_RFSTATE;

// TYPEB global Variables
typedef struct {
	u8 TimeSlot;		//Slot
	u8 *PUPI;       //PUPI obtained after power 
	u8 *APP;        //APP info of ATQB
	u8 *PROTOCOL;
}ST_TYPEB;

//RFglobal Variables
typedef struct
{
	u8 *pbNeedCID; //wether CIDis in need, used for both A and B
	u8 CID_Value; //value of CID, used for both A and B
	u8 volatile event; //RF event,  used for both A and B
	u8 volatile err;	//RF error,  used for both A and B
	u8 volatile bFWI;	//FWI�� used for both A and B
	u8 DSDR;	//DSDR value,  used for both A and B
	ST_RFSTATE  curRFState; // RF state value
	u16 RF_DataLen;  	//data length that RF received
	ST_TYPEB TypebEnv;
} ST_RFCurState;
#pragma pack(pop)

//public variable declarations
extern ST_RFCurState g_stCurRFEnv;
extern u16 const framesize[];
extern u8 const DI[8];

void StopTimer0(void);
u8 RFRxCmdHeader(void);
void RFTxResponse(ST_COMCMD *stCMD);
u8 RFProc(void);
void RFinit(void);
u8 RFRxCmdHeader(void);
void StartWTXTimer(void);
void RFSendWTX(u8 bWTX);
void RFtxOnly(void);
void RFtxrx(void);
void RFrxOnly(void);
void RFTxResponse(ST_COMCMD *stCMD);

#endif

