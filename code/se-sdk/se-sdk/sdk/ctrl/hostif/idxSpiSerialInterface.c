/** ***************************************************************************
 * \file   idxSpiSerialInterface.c
 * \brief  Communication over SPI-based IDEX serial interface.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2012-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include "idxSerialInterface.h"

#include <stdint.h>
#include <string.h> // For memcpy

#include "idxSerialHostInterface.h"
#include "_idxSerialInterface.h"

#include "idxHalGpio.h"
#include "idxHalSystem.h"
#include "idxHalAlgo.h"
#include "idxHalSpi.h"

#include "idxBaseErrors.h"

#define CRC_SIZE          2
#define HDR_NO_CRC_SIZE   4
#define HDR_WITH_CRC_SIZE (HDR_NO_CRC_SIZE + CRC_SIZE)
#define MAX_PACKET_SIZE   512

#define HOSTIF_ACK 0x06
#define HOSTIF_NAK 0x15
#define CMD_ACK_SIZE 1
#define RSP_ACK_SIZE 2

// GPIO pin definitions/
#define IDEX_GPIO0              0x01	
#define IDEX_GPIO1              0x02	
#define IDEX_GPIO2              0x04
#define IDEX_GPIO3              0x08
#define IDEX_GPIO4              0x10



#define IDEX_SPI_CLK            IDEX_GPIO0	
#define IDEX_SE_HSO             IDEX_GPIO0	
#define IDEX_SPI_MOSI           IDEX_GPIO2		
#define IDEX_SE_HSI             IDEX_GPIO1	
#define IDEX_SPI_MISO           IDEX_GPIO3
#define IDEX_ISO_IO             IDEX_GPIO4	



#define handshake_ready_reg 0x75   //    1 byte, write 0x06 start handshake, change to 0x09, succeed
#define mcu_ready_reg	0x74  //    1byte, write 0x06 start check ready, change to 0x09, ready
#define se_reply_ack_reg	0x72  //  2 byte, example 06,00, 
#define mcu_reply_ack_reg 0x71 // 1byte, example 0x06
#define wtx_power_reg	0x6f  // 2 byte, write 0x06,0x00, read 0x06,0x06,enter wtx, write 0x09,0x00,read 0x09,0x09,quit wtx
#define long_data_receive_readycheck_reg 0x6e //  1 byte, write 0x06 start check, change to 0x09, ready
#define long_data_send_readycheck_reg 0x6d //  1 byte, write 0x06 start check, change to 0x09, ready


extern void TMC_GPIO_Write(unsigned int data);
extern unsigned int TMC_GPIO_Read(void);


#define SE_SPI_CS_H TMC_GPIO_Write((TMC_GPIO_Read() | IDEX_SE_HSI)); 
#define SE_SPI_CS_L TMC_GPIO_Write((TMC_GPIO_Read() & ~IDEX_SE_HSI)); 


extern uint8_t SPI_RxByte(void);
extern uint8_t SPI_TxByte(uint8_t data);
extern void idxHalTimerDelayUsec(uint16_t usec);

#define UNUSED(p) if(p){}

/// Compute A-B ensuring the result is non-negative (overflow)
#define USUB(A,B) (((A) > (B)) ? ((A) - (B)) : 0)

typedef enum {
  MCU_STATE_IDLE,
  MCU_STATE_BUSY,
  MCU_STATE_EXECUTE
} mcuState_t;

typedef enum {
  WTX_NONE,
  WTX_DEFERRED,
  WTX_SIGNALLING
} wtxAction_t;

// Note that mode is deliberately initialized here to avoid
// the possibility that WTX timer events that occur before
// idxSerialInterfaceInit() is called do not cause
// idxProcessWTX() to test uninitialized variables
uint8_t connection_mode = MODE_UNINITIALIZED;

static bool erase;
static bool firstCommand;
static bool useFastSpi;
static mcuState_t mcuState;
uint8_t long_wait_cmd_flag;
static volatile wtxAction_t wtxAction;
static volatile bool hsiIntFlag;

static void hsiCallback(void);




extern void mcu_execute_usleep(uint32_t us);


//--------------------------------------------------------------------

static uint16_t addCRC(const idxBuffer_t *pCommsBuffer, bool addCRCDat, uint16_t *retWSize)
{
  const uint16_t  bufferSize = pCommsBuffer->bufferSize;
  uint8_t * const pBuffer    = pCommsBuffer->pBuffer;
  uint8_t * const pwSize    = &pBuffer[0];
  uint8_t * const pbOptions = &pBuffer[3];
  uint8_t * const pCRCHdr   = &pBuffer[4];
  uint16_t wSize;
  uint16_t CRCHdr;

  // Comments refer to "CRCs" chapter in host interface protocol document
  // 1. Increment wSize by 2
  wSize = RD_U16(pwSize);
  if (wSize < HDR_NO_CRC_SIZE) return IDX_ISO_ERR_BAD_PARAMS;
  wSize += 2;
  if (wSize > bufferSize) return IDX_ISO_ERR_NO_SPACE;
  WR_U16(pwSize, wSize);

  if (addCRCDat) {
    uint16_t CRCDat;

    // 2. Set bOptions[0] = 1
    *pbOptions |= DATA_CRC; 

    // 3. Force CRCHdr = 0 and insert at end of header
    WR_U16(pCRCHdr, 0); 

    // 4. Calculate CRCDat over the whole command (including zero'd CRChdr bytes)
    (void) idxHalAlgoCRC16(pBuffer, wSize, &CRCDat);

    // 5. Insert CRCDat at end of all data
    WR_U16(&pBuffer[wSize], CRCDat);

    // 6. Increment wSize by 2
    wSize += 2; 
    if (wSize > bufferSize) return IDX_ISO_ERR_NO_SPACE;
    WR_U16(pwSize, wSize);
  }
  
  //       No CRCDat: 2. Calculate CRCHdr over the 4-byte header
  // CRCDat included: 7. Calculate CRCHdr over the 4-byte header
  (void) idxHalAlgoCRC16(pCommsBuffer->pBuffer, HDR_NO_CRC_SIZE, &CRCHdr);

  //       No CRCDat: 3. Insert CRCHdr at end of header
  // CRCDat included: 8. Insert CRCHdr at end of header (replacing zero'd bytes)
  WR_U16(pCRCHdr, CRCHdr);

  if (retWSize) *retWSize = wSize;

  return IDX_ISO_SUCCESS;
}

static uint16_t checkCRCHdr(const uint8_t *pBuffer)
{
  const uint8_t * const pCRCHdr = &pBuffer[4];
  uint16_t CRCHdr;
  
  (void) idxHalAlgoCRC16(pBuffer, HDR_NO_CRC_SIZE, &CRCHdr);

  return (CRCHdr == RD_U16(pCRCHdr)) ? IDX_ISO_SUCCESS : IDX_ISO_ERR_COMM;
}

static uint16_t checkCRCDat(const idxBuffer_t *pCommsBuffer, uint16_t *retWSize)
{
  const uint16_t  bufferSize = pCommsBuffer->bufferSize;
  uint8_t * const pBuffer    = pCommsBuffer->pBuffer;
  uint8_t * const pwSize     = &pBuffer[0];
  uint8_t * const pCRCHdr    = &pBuffer[4];
  uint8_t *pCRCDat;
  uint16_t wSize;
  uint16_t CRCDat;

  wSize = RD_U16(pwSize);

  // Refuse to read beyond end of buffer
  if (wSize > bufferSize) return IDX_ISO_ERR_COMM;

  // Calculate pointer to CRCDat
  wSize = USUB(wSize, CRC_SIZE);
  pCRCDat = &pBuffer[wSize];

  // Update wSize
  WR_U16(pwSize, wSize);

  // Zero CRChdr
  WR_U16(pCRCHdr, 0);

  (void) idxHalAlgoCRC16(pBuffer, wSize, &CRCDat);

  if (retWSize) *retWSize = wSize;

  return (CRCDat == RD_U16(pCRCDat)) ? IDX_ISO_SUCCESS : IDX_ISO_ERR_COMM;
}

/** \brief Wait for ready
 *
 * This waits for a RDY indication from the MCU. It waits (in a low
 * power state) for a rising edge on HSI. However, not all such
 * rising edges indicate RDY, since this is also used to acknowledge
 * a suspend request.
 *
 * wtxAction is examined to determine how to proceed.
 *
 * If wtxAction is WTX_SIGNALLING then \ref idxSendWTX() is called to
 * send the WTX and the suspend/resume signalling protocol is completed.
 * Then the routine loops to wait for a subsequent rising edge on HSI.
 *
 * If wtxAction is WTX_DEFFERED then \ref idxSendWTX() is called to
 * send the WTX, but in this case this is a RDY indication so the
 * routine returns.
 *
 * If wtxAction is WTX_NONE then this is a RDY indication, no special
 * action is taken, and the routine returns.
 *
 * mcuState will be set to MCU_STATE_IDLE before this function returns.
 */
static void waitForRdy()
{
  do {
    wtxAction_t savedWtxAction;

    // Wait for a rising edge on HSI
    do {
      idxHalSystemWaitForInt(connection_mode, &hsiIntFlag);
    } while (!hsiIntFlag);
    hsiIntFlag = false;

    if (mcuState == MCU_STATE_BUSY) {
      mcuState = MCU_STATE_IDLE;
      // WTX_DEFERRED cannot now be requested
      // (though it may already have been)
    }

    // Decision point...
    savedWtxAction = wtxAction;

    if (savedWtxAction != WTX_NONE  ) {

      wtxAction = WTX_NONE;
      idxSendWTX();

      if (savedWtxAction == WTX_SIGNALLING) {
        /*
         * Suspend/resume is complete - allow the MCU to proceed
         * with the command.
         */
        idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_HIGH);
        /*
         * On systems that share HSO with a communication function, switch
         * the pin back to be controlled by the hardware comms. block.
         */
        idxHalGpioConfigure(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_HARDWARE, IDX_HAL_GPIO_IGNORE);

        // Note that this rising edge isn't a RDY indication (it acknowledged
        // the suspend request) so the MCU is still busy - mcuState is still
        // MCU_STATE_IDLE, so loop to wait again
      } else {
        mcuState = MCU_STATE_IDLE;
      }
    } else {
      mcuState = MCU_STATE_IDLE;
    }
  } while (mcuState != MCU_STATE_IDLE);

  // Only matters at the end of command-execution where the
  // power state was idxPowerStateBio
  idxHalSystemSetPowerState(idxPowerStateComms);
}

/** \brief Transmit data over SPI
 *
 * Wait for RDY then transmit data over SPI, setting mcuState to
 * MCU_STATE_BUSY just before sending the last byte (after which
 * the MCU will inevitably, in due course, become active to process
 * the data transfer).
 */
static void idxSerialTx(const uint8_t *p, unsigned int size)
{
  waitForRdy();
  if (size > 1) idxHalSpiTx(p, size-1);
  mcuState = MCU_STATE_BUSY;
  if (size > 0) idxHalSpiTx(&p[size-1], 1);
}

/** \brief Receive data over SPI
 *
 * Wait for RDY then recieve data over SPI, setting mcuState to
 * MCU_STATE_BUSY just before starting to clock the last byte in
 * (after which the MCU will inevitably, in due course, become
 * active to process the data transfer).
 */
static void idxSerialRx(uint8_t *p, unsigned int size)
{
  waitForRdy();
  if (size > 1) idxHalSpiRx(p, size-1);
  mcuState = MCU_STATE_BUSY;
  if (size > 0) idxHalSpiRx(&p[size-1], 1);
}

/** \brief Set mcuState to MCU_STATE_EXECUTE
 *
 * When starting the command execution phase (where suspend/resume
 * signalling is supported) mcuState will have been set to
 * MCU_STATE_BUSY at the end of the preceding data transfer.
 *
 * This function sets mcuState to MCU_STATE_EXECUTE, and deals with
 * the case that a WTX interrupt may have occurred. If it has then
 * wtxAction will have been set to WTX_DEFERRED, and HSO will not have
 * been driven low to start suspend/resume signalling so this is done
 * now (and wtxAction corrected to WTX_SIGNALLING).
 *
 * MCU_STATE_EXECUTE is not entered immediately at the end of the
 * preceding data transfer for two reasons:
 *
 * 1. If there is no command data for the current command, then
 *    the preceding transfer will be the reception of the achnowledge
 *    of the header. If the MCU sends a NAK, then the command
 *    execution phase is not entered because the command header must
 *    be resent.
 *
 * 2. The host interface protocol requires that HSI has gone low to
 *    indicate the start of command execution (on the MCU) before
 *    HSO can be driven low to start suspend/resume signalling.
 *    If MCU_STATE_EXECUTE is set before HSI has gone low it would
 *    be necessary to wait for it to go low in the WTX timer
 *    interrupt serive routine, which is clearly unacceptable.
 */
static void mcuStateExecute()
{
  while ((idxHalGpioGet(IDX_HAL_GPIO_HSI) != IDX_HAL_GPIO_LOW) &&
         (!hsiIntFlag) /* i.e. not been low and now high again */) {
    /* Wait */
  }

  mcuState = MCU_STATE_EXECUTE;

  /*
   * Note that there is no race here. A WTX timer interrupt occurring
   * after mcuState is set to MCU_STATE_EXECUTE would cause wtxAction
   * to be set the WTX_SIGNALLING. So if it is WTX_DEFERRED that must
   * have occurred before mcuState changed. Furthermore, if wtxAction
   * is not WTX_NONE then a subsequent WTX timer interrupt cannot
   * occur until idxSendWTX() is called (either below in the exceptional
   * "erase" case, or more usually in \ref waitForRdy()).
   */

  if (wtxAction == WTX_DEFERRED) {
    if (erase) {
      // WTX would have been sent directly by the ISR if it had
      // not mistakenly thought it could be deferred.
      idxSendWTX();
    } else {
      wtxAction = WTX_SIGNALLING;
      idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_LOW);
    }
  }

  idxHalSystemSetPowerState(idxPowerStateBio);
}





void HW_spi_Master_apollo3_normal_send(uint8_t start_addr, uint8_t* tx_data_buf, uint16_t tx_data_length)//SPI_TxByte(pBuf[i]);
{
//normal send only support size < 0x76
	uint32_t i=0;
	uint32_t counter=tx_data_length;
	uint8_t temp=0;
	uint8_t send_byte=0;
	uint8_t *pointer_tx;
	mcuState_t saved_mcuState;
	pointer_tx=tx_data_buf;
	if(119<(start_addr+tx_data_length)) return;//over 119 will cause apollo3 hardfault.
	saved_mcuState=mcuState;
	mcuState = MCU_STATE_BUSY;



	if(counter)
		{
		SE_SPI_CS_L;
		send_byte=(start_addr|0x80);//bit 7=1, write, =0, read
		SPI_TxByte(send_byte);

		for(i=0;i<counter;i++)
			{
			send_byte=(*pointer_tx);
			SPI_TxByte(send_byte);
			pointer_tx++;
			}
		UNUSED(temp);
		}
	SE_SPI_CS_H ;
		mcuState = saved_mcuState;

}

void HW_spi_Master_apollo3_normal_receive(uint8_t start_addr, uint8_t* rx_data_buf, uint16_t rx_data_length )
{
	//normal receive only support size <= 224, 100 in reg, and 124 in FIFO
//	
	uint32_t i=0;
	uint32_t j=0;
	uint32_t counter=rx_data_length;
	uint8_t temp=0;
	uint8_t send_byte=0;
	uint8_t offset=0;
	uint8_t *pointer_rx;
	mcuState_t saved_mcuState;
	pointer_rx=rx_data_buf;
	j=rx_data_length;
//	error state: addr>119 
	if(119<start_addr) return;
	if(224<(start_addr+rx_data_length))return;
	if((100<start_addr)&&(119<(start_addr+rx_data_length)))return;
	saved_mcuState=mcuState;
	mcuState = MCU_STATE_BUSY;
	if(counter)
		{
		
		if(119>=(start_addr+rx_data_length))
			{
			SE_SPI_CS_L
			send_byte=start_addr;
			SPI_TxByte(send_byte);
			//first byte in reading is no use.			
			for(i=0;i<counter;i++)
				{
				temp=SPI_RxByte();
				if(j)
					{
					*pointer_rx=temp;
					pointer_rx++;
					j--;
					}
				}
			SE_SPI_CS_H
			}
		else
			{
			//part one
			SE_SPI_CS_L
			send_byte=start_addr;
			SPI_TxByte(send_byte);
			//first byte in reading is no use.	
			counter=100-start_addr;
			offset=counter;
			for(i=0;i<counter;i++)
				{
				temp=SPI_RxByte();
				if(j)
					{
					*pointer_rx=temp;
					pointer_rx++;
					j--;
					}
				}
			SE_SPI_CS_H

			idxHalTimerDelayUsec(100);
			//part two
			SE_SPI_CS_L 
			send_byte=0x7f;// FIFO interface
			SPI_TxByte(send_byte);
			//first byte in reading is no use.	
			counter=rx_data_length-offset;
			pointer_rx=(rx_data_buf+offset);

			for(i=0;i<counter;i++)
				{
				temp=SPI_RxByte();
				if(j)
					{
					*pointer_rx=temp;
					pointer_rx++;
					j--;
					}
				}
			SE_SPI_CS_H

			}
	}
	mcuState = saved_mcuState;
}


uint16_t wtx_with_BMCU_apollo3(void)
{
	int i;
	uint8_t data[2];
	uint8_t timeout;

	data[0]=0x06;
	data[1]=0x00;
	HW_spi_Master_apollo3_normal_send(wtx_power_reg,data,2);//send shutdown
	timeout=100;
	for(i=0;i<timeout;i++)
		{
		idxHalTimerDelayUsec(100);
		HW_spi_Master_apollo3_normal_receive(wtx_power_reg,data,2);
		if(0x06==data[1])
			{
			break;
			}
		}
	if(i>=99)
		{
		idxSendWTX();
		return IDX_ISO_ERR_TIMEOUT;
		}
	idxSendWTX();
	data[0]=0x09;
	data[1]=0x00;
	HW_spi_Master_apollo3_normal_send(wtx_power_reg,data,2);//send shutdown
	timeout=100;
	for(i=0;i<timeout;i++)
		{
		idxHalTimerDelayUsec(100);
		HW_spi_Master_apollo3_normal_receive(wtx_power_reg,data,2);
		if(0x09==data[1])
			{
			break;
			}
		}
	if(i>=99)return IDX_ISO_ERR_TIMEOUT;
return IDX_ISO_SUCCESS;

}


uint16_t waitForRdy_apollo3(uint32_t timeout)//timeout =38ms
{
	int i;
	uint8_t data;
	uint16_t ret;
	wtxAction_t savedWtxAction;
	data=0x06;// handshake start
	HW_spi_Master_apollo3_normal_send(mcu_ready_reg,&data,1);
	mcuState = MCU_STATE_IDLE;

	for(i=0;i<timeout;i++)
		{
		idxHalTimerDelayUsec(100);
		savedWtxAction=wtxAction;
		if (savedWtxAction != WTX_NONE	) 
			{
			wtxAction = WTX_NONE;

			if (savedWtxAction == WTX_SIGNALLING) //wtx with mcu
				{
				ret=wtx_with_BMCU_apollo3();
				if(IDX_ISO_SUCCESS!=ret)return ret;
		  		} 
			else//savedWtxAction == WTX_DEFERRED
				{
				idxSendWTX();//wtx with card reader
				}
			}
		HW_spi_Master_apollo3_normal_receive(mcu_ready_reg,&data,1);
		if(0x09==data)
			{
			return IDX_ISO_SUCCESS;
			}
		}
	return IDX_ISO_ERR_UNKNOWN;
}



uint16_t apollo3_handshake(void)//handshake_ready_reg
{
	int i;
	uint8_t data;
	data=0x06;// handshake start
	HW_spi_Master_apollo3_normal_send(handshake_ready_reg,&data,1);
	for(i=0;i<100;i++)
		{
		idxHalTimerDelayUsec(100);
		HW_spi_Master_apollo3_normal_receive(handshake_ready_reg,&data,1);
		if(0x09==data)
			{
			return IDX_ISO_SUCCESS;
			}
		}
	return IDX_ISO_ERR_UNKNOWN;
}

uint16_t long_data_send_waitforready_apollo3(void)
{
	uint32_t i;
	uint8_t data;
	data=0x06;// handshake start
	HW_spi_Master_apollo3_normal_send(long_data_receive_readycheck_reg,&data,1);
	for(i=0;i<100000;i++)
		{
		idxHalTimerDelayUsec(100);
		HW_spi_Master_apollo3_normal_receive(long_data_receive_readycheck_reg,&data,1);
		if(0x09==data)
			{
			return IDX_ISO_SUCCESS;
			}
		}
	return IDX_ISO_ERR_UNKNOWN;

}
uint16_t long_data_receive_waitforready_apollo3(void)
{
	uint32_t i;
	uint8_t data;
	data=0x06;// handshake start
	HW_spi_Master_apollo3_normal_send(long_data_send_readycheck_reg,&data,1);
	for(i=0;i<100000;i++)
		{
		idxHalTimerDelayUsec(100);
		HW_spi_Master_apollo3_normal_receive(long_data_send_readycheck_reg,&data,1);
		if(0x09==data)
			{
			return IDX_ISO_SUCCESS;
			}
		}
	return IDX_ISO_ERR_UNKNOWN;

}


uint16_t idxSerialTx_apollo3(uint8_t *data, uint16_t data_size)
{
	uint16_t ret;
	uint16_t offset;
	uint16_t transfer_length;
	mcuState = MCU_STATE_BUSY;
	if(100>=data_size)
		{
		HW_spi_Master_apollo3_normal_send(0,data,data_size);
		}
	else
		{
		offset=0;
		while(offset<data_size)
			{
			if(100<(data_size-offset))
				{transfer_length=100;}
			else
				{
				transfer_length=(data_size-offset);
				}
			HW_spi_Master_apollo3_normal_send(0,data+offset,transfer_length);
			idxHalTimerDelayUsec(30);
			ret=long_data_send_waitforready_apollo3();
			if(IDX_ISO_SUCCESS!=ret)
				{
				//
				return ret;
				}
			offset += transfer_length;
			}
		}
	return IDX_ISO_SUCCESS;

}

uint16_t idxSerialRx_apollo3(uint8_t *data, uint16_t data_size)
{
	uint16_t ret;
	uint16_t offset;
	uint16_t transfer_length;
	mcuState = MCU_STATE_BUSY;
	if(224>=data_size)
		{
		HW_spi_Master_apollo3_normal_receive(0,data,data_size);
		}
	else
		{
		offset=0;
		while(offset<data_size)
			{
			if(224<(data_size-offset))
				{
				transfer_length=224;
				}
			else
				{
				transfer_length=(data_size-offset);
				}
			HW_spi_Master_apollo3_normal_receive(0,data+offset,transfer_length);
			idxHalTimerDelayUsec(30);
			ret=long_data_receive_waitforready_apollo3();
			if(IDX_ISO_SUCCESS!=ret)return ret;
			idxHalTimerDelayUsec(10);
			offset+=transfer_length;
			}

		}
	return IDX_ISO_SUCCESS;

}



uint16_t mcuStateExecute_apollo3(void)
{
	uint32_t i;
	uint8_t data;
	int ret;
	wtxAction_t savedWtxAction;
	mcuState = MCU_STATE_EXECUTE;
	data=0x06;//
	HW_spi_Master_apollo3_normal_send(mcu_ready_reg,&data,1);
	for(i=0;i<10000;i++)
		{
		if(0x01==long_wait_cmd_flag)
			{
			//mcu_execute_usleep(1000);
			idxHalTimerDelayUsec(1000);
			}
		else
			{
			//mcu_execute_usleep(100);
			idxHalTimerDelayUsec(100);
			}
		

		savedWtxAction=wtxAction;
		if (savedWtxAction != WTX_NONE	) 
			{
			wtxAction = WTX_NONE;

			if (savedWtxAction == WTX_SIGNALLING) //wtx with mcu
				{
				ret=wtx_with_BMCU_apollo3();
				if(IDX_ISO_SUCCESS!=ret)return ret;
		  		} 
			else//savedWtxAction == WTX_DEFERRED
				{
				idxSendWTX();//wtx with card reader
				}
			}
		
		HW_spi_Master_apollo3_normal_receive(mcu_ready_reg,&data,1);
		if(0x09==data)
			{
			return IDX_ISO_SUCCESS;
			}
		}
	
	return IDX_ISO_ERR_TIMEOUT;

}

void check_long_wait(uint8_t cmd)
{
	switch (cmd)
		{
		case IDX_HOSTIF_CMD_PREPAREIMAGE:
		case IDX_HOSTIF_CMD_GETTEMPLATE:
			long_wait_cmd_flag=1;
			break;
		default:
			long_wait_cmd_flag=0;
			break;
		}
}

uint16_t idxBioDrvComms_apollo3(const idxBuffer_t *pCommsBuffer)
{
	uint8_t * const pBuffer = pCommsBuffer->pBuffer;
	bool addCRCDat = false; // TODO: Should come from a policy
	uint8_t bOrdinal = pBuffer[2];
	uint8_t bOptions;
	uint16_t wSize;
	uint16_t ret = IDX_ISO_ERR_UNKNOWN;
	uint8_t ack[RSP_ACK_SIZE] = {0};
	uint16_t dataSize;
	// Set flag indicating if the MCU is erasing flash in this command
	erase = (bOrdinal == IDX_HOSTIF_CMD_DELETERECORD);
	check_long_wait(bOrdinal);

	if ( (ret = addCRC(pCommsBuffer, addCRCDat, &wSize)) != IDX_ISO_SUCCESS ) {
	  // PowerState unchanged - OK to return
	  return ret;
	}
	bOptions = pBuffer[3];
	// Set to BUSY before heartbeat, do it here so that WTX timer ISR
	// will not call idxSendWTX() while already in that routine
	mcuState = MCU_STATE_BUSY;
	idxHalSystemSetPowerState(idxPowerStateComms);
//---------------------------------------------start handshake
	if(IDX_ISO_SUCCESS!=apollo3_handshake()) return IDX_ISO_ERR_UNKNOWN;
//-------------------------------------------------------------
	dataSize = USUB(wSize, HDR_WITH_CRC_SIZE);//datasize now is the length of data part

//--------------------------------------------------step 1, send header
	idxHalTimerDelayUsec(10);
	ret=waitForRdy_apollo3(380);
	if(IDX_ISO_SUCCESS!=ret)return ret;
	idxHalTimerDelayUsec(10);
	idxSerialTx_apollo3(pBuffer, HDR_WITH_CRC_SIZE);
//---------------------------------------------------step 2,receive ack
	idxHalTimerDelayUsec(10);
	ret=waitForRdy_apollo3(380);
	if(IDX_ISO_SUCCESS!=ret)return ret;
	idxHalTimerDelayUsec(10);

	HW_spi_Master_apollo3_normal_receive(mcu_reply_ack_reg,ack,CMD_ACK_SIZE);
	idxHalTimerDelayUsec(10);
//---------------------------------------------------step 3, send data if there were
	if (dataSize != 0) 
		{
		ret=waitForRdy_apollo3(380);
		if(IDX_ISO_SUCCESS!=ret)return ret;
		idxHalTimerDelayUsec(10);
		ret=idxSerialTx_apollo3(&pBuffer[HDR_WITH_CRC_SIZE], dataSize);
		if(IDX_ISO_SUCCESS!=ret)return ret;
		idxHalTimerDelayUsec(30);
		}
//-------------------------------------------------step4, wait for mcu working
	idxHalTimerDelayUsec(10);

	ret=mcuStateExecute_apollo3();
	mcuState = MCU_STATE_IDLE;
	if(IDX_ISO_SUCCESS!=ret)return ret;
	idxHalSystemSetPowerState(idxPowerStateComms);
//--------------------------------------------------step 5, read reply header
	idxHalTimerDelayUsec(10);

	idxSerialRx_apollo3(pBuffer, HDR_WITH_CRC_SIZE);
	ack[0] = ((checkCRCHdr(pBuffer) == IDX_ISO_SUCCESS) ?  HOSTIF_ACK : HOSTIF_NAK);
//-------------------------------------------------step 6, reply ack to mcu


	ret=waitForRdy_apollo3(380);
	if(IDX_ISO_SUCCESS!=ret)return ret;
	idxHalTimerDelayUsec(10);

	HW_spi_Master_apollo3_normal_send(se_reply_ack_reg,ack, RSP_ACK_SIZE);
	idxHalTimerDelayUsec(10);

//------------------------------------------------step 7, receive data if there were
	wSize = RD_U16(pBuffer);
	if (wSize > pCommsBuffer->bufferSize) 
		{
	  	ret = IDX_ISO_ERR_NO_SPACE;
		} 
	else 
		{
	  	dataSize = USUB(wSize, HDR_WITH_CRC_SIZE);
	  	if (dataSize != 0) 
			{
			ret=waitForRdy_apollo3(380);
			if(IDX_ISO_SUCCESS!=ret)return ret;
			idxHalTimerDelayUsec(10);
			idxSerialRx_apollo3(&pBuffer[HDR_WITH_CRC_SIZE], dataSize);
			idxHalTimerDelayUsec(10);
			ret=waitForRdy_apollo3(380);
	  		}
		else
			{
			ret=waitForRdy_apollo3(380);
			if(IDX_ISO_SUCCESS!=ret)return ret;
			}
		ret = IDX_ISO_SUCCESS;
		}
//----------------------------------------------step 8, check CRC data if enable
	if ((ret == IDX_ISO_SUCCESS) && ((bOptions & DATA_CRC) != 0)) {
	  ret = checkCRCDat(pCommsBuffer, &wSize);
	}
	// Update returned wSize to reflect the size of CRCHdr
	wSize = USUB(wSize, CRC_SIZE);
	WR_U16(pCommsBuffer->pBuffer, wSize);

	return ret;
}



uint16_t idxBioDrvComms(const idxBuffer_t *pCommsBuffer)
{
  uint8_t * const pBuffer = pCommsBuffer->pBuffer;
  bool addCRCDat = false; // TODO: Should come from a policy
  uint8_t bOrdinal = pBuffer[2];
  uint8_t bOptions;
  uint16_t wSize;
  uint16_t ret = IDX_ISO_ERR_UNKNOWN;

  uint8_t ack[RSP_ACK_SIZE] = {0};
  uint16_t dataSize;
  
  idxHalSpiSpeedType spiSpeed = ((useFastSpi) ? IDX_HAL_SPI_SPEED_HIGH :
                                                IDX_HAL_SPI_SPEED_LOW );

  // Set flag indicating if the MCU is erasing flash in this command
  erase = (bOrdinal == IDX_HOSTIF_CMD_DELETERECORD);

  if ( (ret = addCRC(pCommsBuffer, addCRCDat, &wSize)) != IDX_ISO_SUCCESS ) {
    // PowerState unchanged - OK to return
    return ret;
  }
  bOptions = pBuffer[3];
  
  // This implementation does not deal with the large packets (which
  // are never used on a Secure Element) but which are theoretically
  // supported by the host interface protocol
  if (wSize > MAX_PACKET_SIZE) {
    // PowerState unchanged - OK to return
    return IDX_ISO_ERR_BAD_PARAMS;
  }

  // Set to BUSY before heartbeat, do it here so that WTX timer ISR
  // will not call idxSendWTX() while already in that routine
  mcuState = MCU_STATE_BUSY;

  if (firstCommand) {
    // The first command to the MCU causes it to complete its boot
    // process and this process is unnterruptible, so make sure that
    // a WTX timer interrupt cannot occur until it has completed booting.
    if (connection_mode == MODE_CONTACTLESS) {
      idxSendWTX();
    }
    firstCommand = false;
  }

  idxHalSystemSetPowerState(idxPowerStateComms);
  idxHalGpioInitHandshake();

  idxHalSpiSetSpeed(spiSpeed);

  idxHalGpioRegisterListener(hsiCallback);
  idxHalGpioSetIRQ(IDX_HAL_GPIO_HSI, IDX_HAL_GPIO_IRQ_RISING_EDGE);

  hsiIntFlag = false;

  // End of the heartbeat handshake - tell the MCU we're ready to go
  idxHalSpiConfigure(); // Which also drives HSO high

  dataSize = USUB(wSize, HDR_WITH_CRC_SIZE);

  do {
    idxSerialTx(pBuffer, HDR_WITH_CRC_SIZE);

    idxSerialRx(ack, CMD_ACK_SIZE);

  } while (ack[0] != HOSTIF_ACK);

  if (dataSize != 0) {
    idxSerialTx(&pBuffer[HDR_WITH_CRC_SIZE], dataSize);
  }

  mcuStateExecute();

  do {
    idxSerialRx(pBuffer, HDR_WITH_CRC_SIZE);

    ack[0] = ((checkCRCHdr(pBuffer) == IDX_ISO_SUCCESS) ?
              HOSTIF_ACK : HOSTIF_NAK);
    idxSerialTx(ack, RSP_ACK_SIZE);

  } while (ack[0] != HOSTIF_ACK);

  wSize = RD_U16(pBuffer);
  if (wSize > pCommsBuffer->bufferSize) {
    ret = IDX_ISO_ERR_NO_SPACE;
  } else {
    dataSize = USUB(wSize, HDR_WITH_CRC_SIZE);

    if (dataSize != 0) {
      idxSerialRx(&pBuffer[HDR_WITH_CRC_SIZE], dataSize);
    }

    waitForRdy();
    idxHalGpioSetIRQ(IDX_HAL_GPIO_HSI, IDX_HAL_GPIO_IRQ_DISABLE);

    if (bOrdinal == IDX_HOSTIF_CMD_INITIALIZE) {
      // Once a successful Initialize has occurred it is OK to use
      // a faster SPI clock.
      useFastSpi = true;
    } else if (bOrdinal == IDX_HOSTIF_CMD_UNINITIALIZE) {
      firstCommand = true;
      useFastSpi = false;
    }
    ret = IDX_ISO_SUCCESS;
  }

  idxHalSystemSetPowerState((useFastSpi) ? idxPowerStateSE :
                            idxPowerStateLow);

  if ((ret == IDX_ISO_SUCCESS) && ((bOptions & DATA_CRC) != 0)) {
    ret = checkCRCDat(pCommsBuffer, &wSize);
  }

  // Update returned wSize to reflect the size of CRCHdr
  wSize = USUB(wSize, CRC_SIZE);
  WR_U16(pCommsBuffer->pBuffer, wSize);

  return ret;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static void hsiCallback()
{
  hsiIntFlag = true;
}

void idxProcessWTX()
	{
	if (
		( 						connection_mode 	!= MODE_CONTACTLESS  ) ||
		( 						mcuState == MCU_STATE_IDLE	  ) ||
		((						mcuState == MCU_STATE_EXECUTE) &&(erase))
	) {

		idxSendWTX();//wtx with card reader
	  } else // wtx with mcu
	  {
		if (mcuState == MCU_STATE_EXECUTE) 
			{

			  wtxAction = WTX_SIGNALLING;
			} 
		else 
		{

		  wtxAction = WTX_DEFERRED;
		}
	  }
	}


/*
void idxProcessWTX()
{
  if ((                         mode     != MODE_CONTACTLESS  ) ||
      (                         mcuState == MCU_STATE_IDLE    ) ||
      ((                        mcuState == MCU_STATE_EXECUTE) &&
       ((idxHalGpioGet(IDX_HAL_GPIO_HSI) != IDX_HAL_GPIO_LOW) ||
        (                       erase                       )))) {
   //
     // Either not in contactless mode (for example, because
     // idxSetConnectionMode() hasn't been called yet) or the
     // MCU is idle, or the MCU is in command execution but HSI
     // has already gone high at the end of command execution or
     // (an exceptional case) the MCU is processing a command but
     // can't deal with WTX suspend/resume (because it's erasing NVM),
     // so just send the WTX request (and get its response) without any
     // special handling.
     //
    idxSendWTX();
  } else {
    //
    // At this point the MCU is active doing something as a result
     // of the data transfer that just occurred. The WTX is not
     // immediately sent, but is deferred until the following
    // rising edge on HSI.
     //
     // Set a state to modify the behaviour in response to the
    // rising edge on HSI (which may either be an acknowledgement
    // of the suspend request or a normal RDY indication).
    //

    if (mcuState == MCU_STATE_EXECUTE) {
      //
     // The MCU is active doing a biometric command, so
      // suspend/resume signalling is needed to get it to a low-
      // power state before the NFC communications for the WTX
       // request/response can occur. Drive HSO low to start the
     // suspend/resume signalling.
       //
       // Note that MCU_STATE_EXECUTE is not entered until HSI
       // has gone low, so it is valid to drive HSO low.
      //
      wtxAction = WTX_SIGNALLING;
      idxHalGpioSet(IDX_HAL_GPIO_HSO, IDX_HAL_GPIO_LOW);
    } else {
     //
     //Simply wait until the RDY indication (which is expected
   //to occur soon) with no suspend/resume signalling.
    //
      wtxAction = WTX_DEFERRED;
    }
  }
}

*/


void idxSetConnectionMode( uint8_t newMode )
{
  connection_mode = newMode;
}

uint8_t idxGetConnectionMode()
{
  return connection_mode;
}

/*****************************************************************************
 * @brief Define the Communication IF 
 */

void idxSerialInterfaceInit()
{
  connection_mode         = MODE_UNINITIALIZED;
  erase        = false;
  firstCommand = true;
  useFastSpi   = false;
  mcuState     = MCU_STATE_IDLE;
  wtxAction    = WTX_NONE;
  hsiIntFlag   = false;
}
