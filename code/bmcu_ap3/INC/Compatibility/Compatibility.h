/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __COMPATIBILITY_H__
#define __COMPATIBILITY_H__

#include "IDEXWarnings.h"
#include <stdint.h>

typedef struct 
{
    uint32_t seconds;
    uint32_t millis;
} IDEX_Time;

void IDEX_GetSystemTime( IDEX_Time *time ) ;

#ifdef LINUX
#define PATH_SEPARATOR	      '/'
#define PATH_SEPARATOR_STRING "/"
#define LIBRARY_EXT		      "*.so"
#else
#define PATH_SEPARATOR	      '\\'
#define PATH_SEPARATOR_STRING "\\"
#define LIBRARY_EXT		      "*.dll"
#endif

#if defined(WIN32) || defined(_WIN32_WCE)

#include <windows.h> // Use windows
#include <assert.h>
#include <mmsystem.h> // Use multimedia system

#define __func__ __FUNCTION__
#ifdef _DEBUG
#ifndef DEBUG
#define DEBUG
#endif
#endif

int Win2Errno( DWORD error ) ;

#else
#include <stdio.h>
#include <stddef.h> //NULL is defined there
#include <stdint.h>

#ifndef IDEX_TZ
#include <assert.h>
#endif


#ifdef LINUX
#ifdef  __cplusplus
extern "C" int _kbhit();
extern "C" int _getch();
#else
int _kbhit();
int _getch();
#endif
#endif

typedef uint32_t UINT;
typedef uint16_t WORD;
//typedef uint8_t BYTE;
typedef unsigned long DWORD ;

typedef uint32_t LONG;
typedef uint64_t LONGLONG;
#define ZeroMemory(Destination, Length)  memset((Destination),0,(Length))
#define __max(a, b) ( ((a) > (b)) ? (a) : (b) )
#define __min(a, b) ( ((a) < (b)) ? (a) : (b) )

typedef union _LARGE_INTEGER {
	struct {
		DWORD LowPart;
		LONG HighPart;
	} u1;
	struct {
		DWORD LowPart;
		LONG HighPart;
	}   u2;
	LONGLONG QuadPart;
} LARGE_INTEGER, *PLARGE_INTEGER;

#define _PACKED  __attribute__ ((__packed__))

typedef unsigned long HRESULT ;
typedef void *LPVOID ;
typedef unsigned long ULONG ;
typedef char * LPTSTR ;
typedef char *LPSTR;
typedef unsigned char BYTE ;
typedef BYTE *LPBYTE ;
typedef FILE *HMMIO;
typedef char *HPSTR;          /* a huge version of LPSTR */
typedef DWORD           FOURCC;         /* a four character code */
typedef void * HANDLE ;
typedef HANDLE HTASK;
typedef void * HWND ;
typedef void * HKEY ;
typedef void * HINSTANCE ;
typedef UINT WPARAM ;
//typedef UINT LPARAM ;
typedef UINT        MMRESULT;   /* error return code, 0 means no error */
typedef DWORD LRESULT ;
typedef DWORD LPARAM ;
typedef char TCHAR ;
typedef char CHAR ;
typedef unsigned char UCHAR ;
typedef UINT BOOL ;
typedef BYTE BOOLEAN ;
typedef unsigned short USHORT ;
typedef DWORD *LPDWORD ;         /* a four character code */
typedef void *PVOID ;
//typedef unsigned short WORD ;
typedef WORD *LPWORD ;
typedef void *LPOVERLAPPED ;
typedef unsigned char *PUCHAR ;
typedef char *PCHAR ;
typedef unsigned long *PULONG ;
typedef long *LPLONG ;
typedef const TCHAR *LPCTSTR ;
typedef void *LPSECURITY_ATTRIBUTES ;

#define GUID_NULL { 0, 0, 0, { 0, 0, 0, 0, 0, 0, 0, 0 } }

typedef struct _RECT { 
	LONG left; 
	LONG top; 
	LONG right; 
	LONG bottom; 
} RECT, *PRECT; 

typedef struct _PACKED tagBITMAPFILEHEADER { 
	WORD    bfType; 
	DWORD   bfSize; 
	WORD    bfReserved1; 
	WORD    bfReserved2; 
	DWORD   bfOffBits; 
} BITMAPFILEHEADER, *PBITMAPFILEHEADER; 

typedef struct _PACKED tagBITMAPINFOHEADER{
	DWORD  biSize; 
	LONG   biWidth; 
	LONG   biHeight; 
	WORD   biPlanes; 
	WORD   biBitCount; 
	DWORD  biCompression; 
	DWORD  biSizeImage; 
	LONG   biXPelsPerMeter; 
	LONG   biYPelsPerMeter; 
	DWORD  biClrUsed; 
	DWORD  biClrImportant; 
} BITMAPINFOHEADER, *PBITMAPINFOHEADER; 

typedef struct _PACKED tagRGBQUAD {
	BYTE    rgbBlue; 
	BYTE    rgbGreen; 
	BYTE    rgbRed; 
	BYTE    rgbReserved; 
} RGBQUAD; 

#define _O_RDWR O_RDWR
#define _O_BINARY 0
#define _O_CREAT O_CREAT
#define _O_EXCL O_EXCL
#define _S_IWRITE S_IWRITE
#define _S_IREAD S_IREAD
#define _unlink unlink
#define _close close 
#define S_OK 0
#define S_FALSE (-1)
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
//#define NULL ((void *)0)
#define INFINITE 0xFFFFFFFF
#define INVALID_HANDLE_VALUE HANDLE(-1)
#define CALLBACK
#define WINAPI
#define MAX_PATH	256
#define STD_OUTPUT_HANDLE 0
#define EXCEPTION_EXECUTE_HANDLER 0
#define BI_RGB 0

#define WAIT_OBJECT_0			0
#define WAIT_TIMEOUT			258L
#define WAIT_ABANDONED			0x00000080L
#define WAIT_FAILED				-1


#define RGB(r, g ,b)  ((DWORD) (((BYTE) (r) | \
    ((WORD) (g) << 8)) | \
    (((DWORD) (BYTE) (b)) << 16)))
#define GetRValue(r) (r)
 
#define __try try
#define __except(a) catch(...)
 
void SetEvent( HANDLE hEvent ) ;

void ResetEvent( HANDLE hEvent ) ;

void CloseHandle( HANDLE hEvent ) ;

HANDLE CreateEvent( int, int, int, int ) ;

void WaitForSingleObject( HANDLE hEvent, UINT timeout ) ;

//#include <algorithm>
#ifdef __cplusplus
using namespace std ;
#endif

#define HEAP_ZERO_MEMORY                0x00000008      
void *HeapAlloc( void *hHeap, DWORD dwFlags, DWORD dwBytes ) ;
void *GetProcessHeap(void) ;
void HeapFree( void *heap, DWORD dwFlags, void *ptr ) ;
HANDLE GetStdHandle( int hnd );
unsigned char *_mbscpy( unsigned char *strDestination, const unsigned char *strSource ); // C++ only _mbscpy strcpy
int _mbscmp( const unsigned char *string1, const unsigned char *string2 );
//int CoCreateGuid( GUID * pguid ) ;

void MessageBox(void *p, char *text, char *title, BOOL but);
#define MB_OK 1

void GetCurrentDirectory(size_t size, char *buf);
void SetCurrentDirectory(char *buf);

typedef LRESULT (CALLBACK MMIOPROC)(LPSTR lpmmioinfo, UINT uMsg,
            LPARAM lParam1, LPARAM lParam2);
typedef MMIOPROC *LPMMIOPROC;


#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |   \
                ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))
#define mmioFOURCC(ch0, ch1, ch2, ch3)  MAKEFOURCC(ch0, ch1, ch2, ch3)

typedef struct tWAVEFORMATEX
{
    WORD        wFormatTag;         /* format type */
    WORD        nChannels;          /* number of channels (i.e. mono, stereo...) */
    DWORD       nSamplesPerSec;     /* sample rate */
    DWORD       nAvgBytesPerSec;    /* for buffer estimation */
    WORD        nBlockAlign;        /* block size of data */
    WORD        wBitsPerSample;     /* number of bits per sample of mono data */
    WORD        cbSize;             /* the count in bytes of the size of */
                                    /* extra information (after cbSize) */
} __attribute__ ((__packed__)) WAVEFORMATEX, *PWAVEFORMATEX ;

typedef struct _MMCKINFO
{
        FOURCC          ckid;           /* chunk ID */
        DWORD           cksize;         /* chunk size */
        FOURCC          fccType;        /* form type or list type */
        DWORD           dwDataOffset;   /* offset of data portion of chunk */
        DWORD           dwFlags;        /* flags used by MMIO functions */
} MMCKINFO, *PMMCKINFO, *LPMMCKINFO ;
typedef const MMCKINFO *LPCMMCKINFO;

typedef struct _MMIOINFO
{
        /* general fields */
        DWORD           dwFlags;        /* general status flags */
        FOURCC          fccIOProc;      /* pointer to I/O procedure */
        LPMMIOPROC      pIOProc;        /* pointer to I/O procedure */
        UINT            wErrorRet;      /* place for error to be returned */
        HTASK           htask;          /* alternate local task */

        /* fields maintained by MMIO functions during buffered I/O */
        LONG            cchBuffer;      /* size of I/O buffer (or 0L) */
        HPSTR           pchBuffer;      /* start of I/O buffer (or NULL) */
        HPSTR           pchNext;        /* pointer to next byte to read/write */
        HPSTR           pchEndRead;     /* pointer to last valid byte to read */
        HPSTR           pchEndWrite;    /* pointer to last byte to write */
        LONG            lBufOffset;     /* disk offset of start of buffer */

        /* fields maintained by I/O procedure */
        LONG            lDiskOffset;    /* disk offset of next read or write */
        DWORD           adwInfo[3];     /* data specific to type of MMIOPROC */

        /* other fields maintained by MMIO */
        DWORD           dwReserved1;    /* reserved for MMIO use */
        DWORD           dwReserved2;    /* reserved for MMIO use */
        HMMIO           hmmio;          /* handle to open file */
} MMIOINFO, *PMMIOINFO, *NPMMIOINFO, *LPMMIOINFO;
typedef const MMIOINFO *LPCMMIOINFO;

#define MMIO_READ       0x00000000      /* open file for reading only */
#define MMIO_WRITE      0x00000001      /* open file for writing only */
#define MMIO_READWRITE  0x00000002      /* open file for reading and writing */
#define MMIO_ALLOCBUF   0x00010000      /* mmioOpen() should allocate a buffer */


#define MMSYSERR_NOERROR      0                    /* no error */
#define MMSYSERR_BASE          0
#define WAVERR_BASE            32
#define MIDIERR_BASE           64
#define TIMERR_BASE            96
#define JOYERR_BASE            160
#define MCIERR_BASE            256
#define MIXERR_BASE            1024
#define MMSYSERR_ERROR        (MMSYSERR_BASE + 1)  /* unspecified error */
#define MMSYSERR_BADDEVICEID  (MMSYSERR_BASE + 2)  /* device ID out of range */
#define MMSYSERR_NOTENABLED   (MMSYSERR_BASE + 3)  /* driver failed enable */
#define MMSYSERR_ALLOCATED    (MMSYSERR_BASE + 4)  /* device already allocated */
#define MMSYSERR_INVALHANDLE  (MMSYSERR_BASE + 5)  /* device handle is invalid */
#define MMSYSERR_NODRIVER     (MMSYSERR_BASE + 6)  /* no device driver present */
#define MMSYSERR_NOMEM        (MMSYSERR_BASE + 7)  /* memory allocation error */
#define MMSYSERR_NOTSUPPORTED (MMSYSERR_BASE + 8)  /* function isn't supported */
#define MMSYSERR_BADERRNUM    (MMSYSERR_BASE + 9)  /* error value out of range */
#define MMSYSERR_INVALFLAG    (MMSYSERR_BASE + 10) /* invalid flag passed */
#define MMSYSERR_INVALPARAM   (MMSYSERR_BASE + 11) /* invalid parameter passed */
#define MMSYSERR_HANDLEBUSY   (MMSYSERR_BASE + 12) /* handle being used */
                                                   /* simultaneously on another */
                                                   /* thread (eg callback) */
#define MMSYSERR_INVALIDALIAS (MMSYSERR_BASE + 13) /* specified alias not found */
#define MMSYSERR_BADDB        (MMSYSERR_BASE + 14) /* bad registry database */
#define MMSYSERR_KEYNOTFOUND  (MMSYSERR_BASE + 15) /* registry key not found */
#define MMSYSERR_READERROR    (MMSYSERR_BASE + 16) /* registry read error */
#define MMSYSERR_WRITEERROR   (MMSYSERR_BASE + 17) /* registry write error */
#define MMSYSERR_DELETEERROR  (MMSYSERR_BASE + 18) /* registry delete error */
#define MMSYSERR_VALNOTFOUND  (MMSYSERR_BASE + 19) /* registry value not found */
#define MMSYSERR_NODRIVERCB   (MMSYSERR_BASE + 20) /* driver does not call DriverCallback */
#define MMSYSERR_MOREDATA     (MMSYSERR_BASE + 21) /* more data to be returned */
#define MMSYSERR_LASTERROR    (MMSYSERR_BASE + 21) /* last error in range */


/* various MMIO flags */
#define MMIO_FHOPEN             0x0010  /* mmioClose: keep file handle open */
#define MMIO_EMPTYBUF           0x0010  /* mmioFlush: empty the I/O buffer */
#define MMIO_TOUPPER            0x0010  /* mmioStringToFOURCC: to u-case */
#define MMIO_INSTALLPROC    0x00010000  /* mmioInstallIOProc: install MMIOProc */
#define MMIO_GLOBALPROC     0x10000000  /* mmioInstallIOProc: install globally */
#define MMIO_REMOVEPROC     0x00020000  /* mmioInstallIOProc: remove MMIOProc */
#define MMIO_UNICODEPROC    0x01000000  /* mmioInstallIOProc: Unicode MMIOProc */
#define MMIO_FINDPROC       0x00040000  /* mmioInstallIOProc: find an MMIOProc */
#define MMIO_FINDCHUNK          0x0010  /* mmioDescend: find a chunk by ID */
#define MMIO_FINDRIFF           0x0020  /* mmioDescend: find a LIST chunk */
#define MMIO_FINDLIST           0x0040  /* mmioDescend: find a RIFF chunk */
#define MMIO_CREATERIFF         0x0020  /* mmioCreateChunk: make a LIST chunk */
#define MMIO_CREATELIST         0x0040  /* mmioCreateChunk: make a RIFF chunk */


/* message numbers for MMIOPROC I/O procedure functions */
#define MMIOM_READ      MMIO_READ       /* read */
#define MMIOM_WRITE    MMIO_WRITE       /* write */
#define MMIOM_SEEK              2       /* seek to a new position in file */
#define MMIOM_OPEN              3       /* open file */
#define MMIOM_CLOSE             4       /* close file */
#define MMIOM_WRITEFLUSH        5       /* write and flush */


#ifndef NULL
#define NULL (0)
#endif

#define WINAPI
#define FAR

HMMIO WINAPI mmioOpen(LPTSTR pszFileName, LPMMIOINFO pmmioinfo, DWORD fdwOpen) ;
MMRESULT WINAPI mmioClose(HMMIO hmmio, UINT fuClose) ;
LONG WINAPI mmioRead(HMMIO hmmio, HPSTR pch, LONG cch) ;
LONG WINAPI mmioWrite(HMMIO hmmio, const char * pch, LONG cch) ;
MMRESULT WINAPI mmioDescend(HMMIO hmmio, LPMMCKINFO pmmcki,const MMCKINFO FAR* pmmckiParent, UINT fuDescend) ;
MMRESULT WINAPI mmioAscend(HMMIO hmmio, LPMMCKINFO pmmcki, UINT fuAscend) ;
MMRESULT WINAPI mmioCreateChunk(HMMIO hmmio, LPMMCKINFO pmmcki, UINT fuCreate) ;

#define WAVE_FORMAT_PCM     1
#ifdef EMBEDDED_IDXSERIAL
#if !defined (_countof)
#if !defined (__cplusplus)
#define _countof(_Array) (sizeof(_Array) / sizeof(_Array[0]))
#endif
#endif
//#define printf_s RtPrintf
#define printf_s( ... )
#else
#ifdef IDEX_TZ

#ifndef DEBUG
#define DEBUG
#endif

#ifdef DEBUG
#define printf_s __DbgLog
#ifdef OP_TEE
// in optee __DbgLog calls printf
#else
#define printf __DbgLog
#endif
#else
#define printf_s
#define printf
#define SLog
#endif

#else
#define printf_s printf
#endif

#define sprintf_s sprintf
#endif // EMBEDDED_IDXSERIAL
#endif // WIN32

#include "idxSleep.h"

#endif // __COMPATIBILITY_H__

