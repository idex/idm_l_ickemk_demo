/*
 * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: m1lib.h
 * SCFID: 
 * Feature: M1 header file of M1Lib
 */
#ifndef CRYPTO_EE_H
#define CRYPTO_EE_H

typedef unsigned char BOOL;
typedef enum {
	CR1SECRefuse = 0,
	CR1RIGTHTKEYA,
	CR1RIGTHTKEYB,
	CR1RIGTHTKEYAB
}CR1RIGHT;

typedef enum {
	CR1RetOK=0,
	CR1RetErr,
	CR1Ret40
}CR1RETVALUE;

typedef struct {
	u8 KEYA[6];
	u8 ACS[4];
	u8 KEYB[6];
}stTrailerSector ;

typedef struct {
	u8 block[16];
}stBlock;

typedef struct {
	u8 bLgTrailerBlockNum;		//current block's trailer logic block num
	u8 bCurBlockACS;
	u8 bCurSectorACS;
	u8 bAuthTrailerBlockNum;	//pass authentication trailer Logic block num
	
	stTrailerSector *pTrailerAddr;	//current sectortrailer addree
	u8 *pbValidBlockAddr;	    //Logic block's valid physical address
	u8 *pbInvalidBlockAddr;	   //Logic block's invalid physical address
	CR1RIGHT bCurRight;		       //current pass authentication sector's access right
}stCurCR1Control;

//trailer access list
typedef struct {
	u8 KEYA_RD;
	u8 KEYA_WR;
	u8 ACS_RD;
	u8 ACS_WR;
	u8 KEYB_RD;
	u8 KEYB_WR;
}stTrailerRight;

//data access list
typedef struct {
	u8 DATA_RD;
	u8 DATA_WR;
	u8 INCREMENT;
	u8 DEC_TRAN_RESTORE;
}stDataRight;

//varible
extern u8 g_cfgM1;
extern u8 g_getRandTimes;
extern stCurCR1Control CurCR1Ctl;
extern volatile u8 g_M1Err;			
extern volatile u8 g_M1State;		

//Crypto1
/*
* Function: u8 mainCrypto1(u8 mode, u32 ulNVMAddr)
* Description: handle MIFARE command.        
* Input: Input: mode - 0: s50 configure, 1: s70 configure, ulNVMAddr - M1DAT_ADDR_BEGIN
* Output: -
* Return: RFOK - 0
*	      RFERROR - 1		  
* Other: Does not support interactions with the contact interface.
*        Unless the non-contact leave, otherwise the function will not exit.
*/
u8 mainCrypto1(u8 mode, u32 ulNVMAddr);

// Crypto1 API
/*
* Function: u8 cfgCrypto1(u8 mode)	
* Description: M1 s50 or s70 configure.
* Input: mode - 0: s50 configure, 1: s70 configure
* Output: -
* Return: OK - 0
*         ERR - 1 	
* Other: -
*/
u8 cfgCrypto1Init(u8 mode,u32 ulNVMAddr);
/*
* Function: u8 readWriteCrypto1(u8 mode, u8* data, u16 offset, u16 block)	
* Description: The API can read or write data from the Mifare memory.
* Input: mode - only define 1(PASSWORD_READ) or 0(PASSWORD_WRITE)
*        data - 12 bytes key pair and 16 bytes data
*		 offset	- data offset in *data
*		 block - Mifare block to be read or written. S70,the value is 0~255.
* Output: -
* Return: OK - 0
*         ERR - 1	
* Other: 12 bytes key pair is keyA and keyB.
*/
u8 readWriteCrypto1(u8 mode,u8* data,u16 offset,u16 block);
/*
* Function: u8 readWriteCrypto1_purePwd(u8 mode, u8* data, u16 offset, u16 block)	
* Description: The API can read or write data from the Mifare memory.
* Input: mode - only define 1(PASSWORD_READ) or 0(PASSWORD_WRITE)
*        data - 8 bytes MF_password and 16 bytes data
*		 offset	- data offset in *data
*		 block - Mifare block to be read or written. S70,the value is 0~255.
* Output: -
* Return: OK - 0
*         ERR - 1	
* Other: 8 bytes key is MF_password.
*/
u8 readWriteCrypto1_purePwd(u8 mode,u8* data,u16 offset,u16 block);
/*
* Function: u8 restoreDefaults(u16 secterNumber)	
* Description: The API can restore the sector to default value.
* Input: secterNumber - To restore the sector number. S70,the value is 0~39.
* Output: -
* Return: OK - 0
*         ERR - 1	
* Other: NOTE:Execution of the function will be lost all data (except block0), restore to default value.
*/
u8 restoreDefaults(u16 secterNumber);

/*
* Function: u8 Crypto1GetVersion(u8 *bBuffer)
* Description: Get the version number of Crypto1 lib
* Input: -
* Output: bBuffer - the version number, be not less than 2 bytes length. Such as "110" means 1.1.0
* Return: -
*/
u8 Crypto1GetVersion(u8 *bBuffer);
	
#define MFinit(void)\
{					\
	g_getRandTimes = 0x01;	\
	CurCR1Ctl.bCurRight = CR1SECRefuse;	\
	CurCR1Ctl.bAuthTrailerBlockNum = 2;	\
	CurCR1Ctl.bCurSectorACS = 0; \
	CurCR1Ctl.bCurBlockACS = 0;\
}
 
#endif
