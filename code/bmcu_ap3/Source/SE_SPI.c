
#include "main.h"





uint8_t ios_long_data_transfer_flag=0;
uint8_t se_spi_porcess_flag=0;

am_hal_ios_buffer_t g_sSRAMBuffer;
uint32_t gAmHalResetStatus = 0;


am_hal_ios_state_t g_IOShandles[AM_REG_IOSLAVE_NUM_MODULES];
 void *g_pIOSHandle;
volatile uint8_t * const am_hal_ios_pui8LRAM = (uint8_t *)REG_IOSLAVE_BASEADDR;



extern uint8_t wtx_sensor_process_flag;
extern uint8_t wtx_sensor_interrupt_flag;

extern idx_State state;

extern uint8_t se_spi_common_buffer[2560];
extern int CheckCRC(uint8_t * data, int data_size, uint8_t *data_crc);
extern void AddCRC(uint8_t * data, int data_size, uint8_t *data_crc);

extern void SE_SPI_biometrics_process(idx_State * pstate);
extern int appolo3_spi_transfer( void *p, unsigned char *buffer, int txSize, int rxSize );
extern void wtx_stage_three_callback(void);
extern void wtx_stage_two_callback(void);

const struct
	{    uint32_t      ui32PeriphEnable;    uint32_t      ui32PeriphStatus;    uint32_t      ui32PeriphEvent;}
am_hal_pwrctrl_peripheral_control[AM_HAL_PWRCTRL_PERIPH_MAX] =
	{    
	{0, 0, 0},                                  //  AM_HAL_PWRCTRL_PERIPH_NONE    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOS, PWRCTRL_DEVPWREN_PWRIOS_EN),     PWRCTRL_DEVPWRSTATUS_HCPA_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPAEVEN, PWRCTRL_DEVPWREVENTEN_HCPAEVEN_EN)},  // AM_HAL_PWRCTRL_PERIPH_IOS    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOM0, PWRCTRL_DEVPWREN_PWRIOM0_EN),     PWRCTRL_DEVPWRSTATUS_HCPB_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPBEVEN, PWRCTRL_DEVPWREVENTEN_HCPBEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_IOM0    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOM1, PWRCTRL_DEVPWREN_PWRIOM1_EN),     PWRCTRL_DEVPWRSTATUS_HCPB_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPBEVEN, PWRCTRL_DEVPWREVENTEN_HCPBEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_IOM1    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOM2, PWRCTRL_DEVPWREN_PWRIOM2_EN),     PWRCTRL_DEVPWRSTATUS_HCPB_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPBEVEN, PWRCTRL_DEVPWREVENTEN_HCPBEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_IOM2    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOM3, PWRCTRL_DEVPWREN_PWRIOM3_EN),     PWRCTRL_DEVPWRSTATUS_HCPC_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPCEVEN, PWRCTRL_DEVPWREVENTEN_HCPCEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_IOM3   
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOM4, PWRCTRL_DEVPWREN_PWRIOM4_EN),     PWRCTRL_DEVPWRSTATUS_HCPC_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPCEVEN, PWRCTRL_DEVPWREVENTEN_HCPCEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_IOM4    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRIOM5, PWRCTRL_DEVPWREN_PWRIOM5_EN),     PWRCTRL_DEVPWRSTATUS_HCPC_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPCEVEN, PWRCTRL_DEVPWREVENTEN_HCPCEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_IOM5   
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRUART0, PWRCTRL_DEVPWREN_PWRUART0_EN),     PWRCTRL_DEVPWRSTATUS_HCPA_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPAEVEN, PWRCTRL_DEVPWREVENTEN_HCPAEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_UART0    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRUART1, PWRCTRL_DEVPWREN_PWRUART1_EN),     PWRCTRL_DEVPWRSTATUS_HCPA_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPAEVEN, PWRCTRL_DEVPWREVENTEN_HCPAEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_UART1    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRADC, PWRCTRL_DEVPWREN_PWRADC_EN),     PWRCTRL_DEVPWRSTATUS_PWRADC_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_ADCEVEN, PWRCTRL_DEVPWREVENTEN_ADCEVEN_EN)},    //  AM_HAL_PWRCTRL_PERIPH_ADC   
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRSCARD, PWRCTRL_DEVPWREN_PWRSCARD_EN),     PWRCTRL_DEVPWRSTATUS_HCPA_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_HCPAEVEN, PWRCTRL_DEVPWREVENTEN_HCPAEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_SCARD    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRMSPI, PWRCTRL_DEVPWREN_PWRMSPI_EN),     PWRCTRL_DEVPWRSTATUS_PWRMSPI_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_MSPIEVEN, PWRCTRL_DEVPWREVENTEN_MSPIEVEN_EN)},  //  AM_HAL_PWRCTRL_PERIPH_MSPI    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRPDM, PWRCTRL_DEVPWREN_PWRPDM_EN),     PWRCTRL_DEVPWRSTATUS_PWRPDM_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_PDMEVEN, PWRCTRL_DEVPWREVENTEN_PDMEVEN_EN)},    //  AM_HAL_PWRCTRL_PERIPH_PDM    
	{_VAL2FLD(PWRCTRL_DEVPWREN_PWRBLEL, PWRCTRL_DEVPWREN_PWRBLEL_EN),     PWRCTRL_DEVPWRSTATUS_BLEL_Msk,     _VAL2FLD(PWRCTRL_DEVPWREVENTEN_BLELEVEN, PWRCTRL_DEVPWREVENTEN_BLELEVEN_EN)}   //  AM_HAL_PWRCTRL_PERIPH_BLEL
	};


const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOS_SCK =
	{    .uFuncSel            = AM_HAL_PIN_0_SLSCK,    
	.eGPInput            = AM_HAL_GPIO_PIN_INPUT_ENABLE
	};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOS_SCL =
	{    .uFuncSel            = AM_HAL_PIN_0_SLSCL,    
	.eGPInput            = AM_HAL_GPIO_PIN_INPUT_ENABLE
	};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOS_SDA =
	{    .uFuncSel            = AM_HAL_PIN_1_SLSDAWIR3,    
	.ePullup             = AM_HAL_GPIO_PIN_PULLUP_1_5K,    
	.eGPOutcfg           = AM_HAL_GPIO_PIN_OUTCFG_OPENDRAIN
	};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOS_MOSI =
	{    .uFuncSel            = AM_HAL_PIN_1_SLMOSI,    
	.eGPInput            = AM_HAL_GPIO_PIN_INPUT_ENABLE
	};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOS_MISO =
	{    .uFuncSel            = AM_HAL_PIN_2_SLMISO,    
	.eDriveStrength      = AM_HAL_GPIO_PIN_DRIVESTRENGTH_12MA
	};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOS_CE =
	{    .uFuncSel            = AM_HAL_PIN_3_SLnCE,    
	.eGPInput            = AM_HAL_GPIO_PIN_INPUT_ENABLE,    
	.uNCE                = 0,    
	.eCEpol              = AM_HAL_GPIO_PIN_CEPOL_ACTIVELOW
	};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_pin3_input =
	{    .uFuncSel            = AM_HAL_PIN_3_GPIO,    
	.ePullup =	AM_HAL_GPIO_PIN_PULLUP_WEAK,
	.eGPInput            = AM_HAL_GPIO_PIN_INPUT_ENABLE,    
	.uNCE                = 0,    
	.eIntDir =AM_HAL_GPIO_PIN_INTDIR_HI2LO,
	.eCEpol              = AM_HAL_GPIO_PIN_CEPOL_ACTIVELOW
	};





void am_ioslave_acc_isr(void)
{
	NVIC_DisableIRQ(IOSLAVEACC_IRQn);

	IOSLAVE->REGACCINTCLR|=0xffffffff;
	NVIC_EnableIRQ(IOSLAVEACC_IRQn);

}
/*
void wtx_with_SE(void)
{
	uint8_t data[2];
	data[0]=am_hal_ios_pui8LRAM[wtx_power_reg];
	data[1]=am_hal_ios_pui8LRAM[wtx_power_reg+1];
	if(0x06==data[0])
		{
		if(0x00==data[1])//process wtx
			{
			am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x06;
			}
		}
	if(0x09==data[0])
		{
		if(0x00==data[1])//process wtx
			{
			am_hal_ios_pui8LRAM[wtx_power_reg+1]=0x09;
			}
		}

}
*/

void wtx_with_SE(void)
{
	uint8_t data[2];
	data[0]=am_hal_ios_pui8LRAM[wtx_power_reg];
	data[1]=am_hal_ios_pui8LRAM[wtx_power_reg+1];
	if(0x06==data[0])
		{
		if(0x00==data[1])//process wtx
			{
			if(0x02==wtx_sensor_process_flag)//only in Lib process, the wtx issues in ISR
				{
				wtx_stage_three_callback();
				}
			else
				{
				wtx_sensor_interrupt_flag=0x01;
				}
			}
		}

}


void am_ioslave_ios_isr(void)
{
	uint32_t data;
	NVIC_DisableIRQ(IOSLAVE_IRQn);
	data=IOSLAVE->INTSTAT;
	IOSLAVE->INTCLR|=0x000003ff;
	if(0x40==(0x40&data))
		{
		IOSLAVE->FUPD=0x01;
		IOSLAVE->FIFOPTR=0x7f80;
		IOSLAVE->FUPD=0x00;
		
		IOSLAVE->FIFOCTR=0x0000007f;//avoid FIFO read to hardfault
		IOSLAVE->INTCLR|=0x000003ff;

		}
	if(0x200==(0x200&data))//reg write
		{
		wtx_with_SE();
		}
	IOSLAVE->INTCLR|=0x000003ff;

	NVIC_EnableIRQ(IOSLAVE_IRQn);

}





void SE_SPI_PORT_INIT(void)
{
	am_hal_gpio_pinconfig(SE_SPI_SCK,	g_AM_BSP_GPIO_IOS_SCK);
	am_hal_gpio_pinconfig(SE_SPI_MISO, g_AM_BSP_GPIO_IOS_MISO);
	am_hal_gpio_pinconfig(SE_SPI_MOSI, g_AM_BSP_GPIO_IOS_MOSI);
	am_hal_gpio_pinconfig(SE_SPI_CS,	g_AM_BSP_GPIO_IOS_CE);

	PWRCTRL->DEVPWREN |=PWRCTRL_DEVPWREN_PWRIOS_Msk;

	IOSLAVE->FIFOCFG=0x0f002010;
	IOSLAVE->FIFOTHR=0x0000007f;
	IOSLAVE->CFG=0x80000001;
	IOSLAVE->INTCLR |=0x000003ff;

	IOSLAVE->FUPD=0x01;
	IOSLAVE->FIFOPTR=0x7f80;
	IOSLAVE->FUPD=0x00;

	IOSLAVE->FIFOCTR=0x0000007f;//avoid FIFO read to hardfault

	
	IOSLAVE->INTEN=0x000003fe;
	NVIC_EnableIRQ(IOSLAVE_IRQn);



}









void se_read_data_init(void)
{

	memset((uint8_t *)am_hal_ios_pui8LRAM,0,100);
	memset((uint8_t *)(am_hal_ios_pui8LRAM+0x80),0,118);
}






int check_data_ready_set_stage(uint8_t flag)
{
	uint32_t time_out_counter=0;
	time_out_counter=1000;
	while(0<time_out_counter)
		{
		if (0x06==am_hal_ios_pui8LRAM[mcu_ready_reg])
			{
			se_spi_porcess_flag=flag +1;//step 1, wait for spi data, 6 byte header, when data transfered, SE will check ready,
			break;
			}
		time_out_counter--;
		wtx_stage_two_callback();
		if(10>time_out_counter)
			{
			se_spi_porcess_flag=0;
			memset((uint8_t *)(am_hal_ios_pui8LRAM+long_data_send_readycheck_reg),0x00,9);
			return -1;
			}
		uSleep(10);
		}
return 0;

}




int check_long_data_receive_ready(void)
{
	uint32_t time_out_counter=0;
	time_out_counter=1000;
	while(0<time_out_counter)
		{
		if (0x06==am_hal_ios_pui8LRAM[long_data_receive_readycheck_reg])
			{
			break;
			}
		wtx_stage_two_callback();
		time_out_counter--;
		if(10>time_out_counter)
			{
			se_spi_porcess_flag=0;
			memset((uint8_t *)(am_hal_ios_pui8LRAM+long_data_send_readycheck_reg),0x00,9);
			return -1;
			}
		uSleep(10);
		}
return 0;
}

int se_pull_data_in(idx_State * pstate)
{
	uint16_t data_size=0;
	uint16_t transfer_length=0;
	uint16_t offset=0;
	int ret;
	data_size=pstate->commDataSize;
	//printf("s=%x\r\n",pstate->commDataSize);
	if(data_size<100)
		{
		SET_MCU_READY;
		ret=check_data_ready_set_stage(se_spi_porcess_flag);
		if(0!=ret)return ret;
		memcpy(pstate->pCommData,(uint8_t *)am_hal_ios_pui8LRAM,pstate->commDataSize);

		}
	else
		{
		offset=0;
		SET_MCU_READY;
		while(data_size>offset)
			{
			ret=check_long_data_receive_ready();
			if(100<(data_size-offset))
				{
				transfer_length=100;

				}
			else
				{
				transfer_length=(data_size-offset);
				}
			wtx_stage_two_callback();
			memcpy((uint8_t *)(pstate->pCommData+offset),(uint8_t *)am_hal_ios_pui8LRAM,transfer_length);
			offset +=transfer_length;
			SET_LONG_SE_SEND_READY;
			}
		wtx_stage_two_callback();
		}
return 0;
	

}


int check_long_data_send_ready(void)
{
	uint32_t time_out_counter=0;
	time_out_counter=1000;
	while(0<time_out_counter)
		{
		if (0x06==am_hal_ios_pui8LRAM[long_data_send_readycheck_reg])
			{
			break;
			}
		wtx_stage_two_callback();
		time_out_counter--;
		if(10>time_out_counter)
			{
			se_spi_porcess_flag=0;
			memset((uint8_t *)(am_hal_ios_pui8LRAM+long_data_send_readycheck_reg),0x00,9);
			return -1;
			}
		uSleep(10);
		}
return 0;
}

int se_pull_data_out(idx_State * pstate)
{
	uint16_t transfer_length=0;
	uint16_t offset=0;
	int ret;

	ret=check_data_ready_set_stage(se_spi_porcess_flag);
	if(0!=ret)return ret;

	memcpy((uint8_t *)am_hal_ios_pui8LRAM,pstate->pCommData,100);
	wtx_stage_two_callback();
	memcpy((uint8_t *)(am_hal_ios_pui8LRAM+0x80),pstate->pCommData+100,124);
	wtx_stage_two_callback();
	IOSLAVE->INTCLR |=0x000003ff;
	IOSLAVE->FUPD=0x01;
	IOSLAVE->FIFOPTR=0x7f80;
	IOSLAVE->FUPD=0x00;
	IOSLAVE->FIFOCTR=0x0000007f;//avoid FIFO read to hardfault
	IOSLAVE->INTCLR |=0x000003ff;
	uSleep(10);
	//printf("ds=%x\r\n",pstate->commDataSize);
	SET_MCU_READY;
	if(pstate->commDataSize>224)
		{
		offset=224;
		while(pstate->commDataSize>offset)
			{
			ret=check_long_data_send_ready();
			if(0!=ret)return ret;
			memcpy((uint8_t *)am_hal_ios_pui8LRAM,pstate->pCommData+offset,100);
			memcpy((uint8_t *)(am_hal_ios_pui8LRAM+0x80),pstate->pCommData+100+offset,124);
			IOSLAVE->INTCLR |=0x000003ff;
			IOSLAVE->FUPD=0x01;
			IOSLAVE->FIFOPTR=0x7f80;
			IOSLAVE->FUPD=0x00;
			IOSLAVE->FIFOCTR=0x0000007f;//avoid FIFO read to hardfault
			IOSLAVE->INTCLR |=0x000003ff;
			if(224<(pstate->commDataSize-offset))
				{
				transfer_length=224;

				}
			else
				{
				transfer_length=(pstate->commDataSize-offset);
				}
			wtx_stage_two_callback();
			offset +=transfer_length;
			SET_LONG_SE_RECEIVE_READY;

			}

		ret=check_long_data_send_ready();
		if(0!=ret)return ret;
		SET_LONG_SE_RECEIVE_READY;
		ret=check_data_ready_set_stage(se_spi_porcess_flag);
		if(0!=ret)return ret;
		SET_MCU_READY;
		}
	else
		{
		ret=check_data_ready_set_stage(se_spi_porcess_flag);
		if(0!=ret)return ret;
		SET_MCU_READY;
		}
return 0;
}



void SE_handshake_process(idx_State * pstate)
{
	int ret=0;
	uint16_t data_size;
	if(0x06!=am_hal_ios_pui8LRAM[handshake_ready_reg])return; // no handshake
	se_spi_porcess_flag=0x01;
	wtx_sensor_process_flag=0;
	memset((uint8_t *)(am_hal_ios_pui8LRAM+long_data_send_readycheck_reg),0x00,9);
	SET_HANDSHAKE_READY;

	ret=check_data_ready_set_stage(se_spi_porcess_flag);//check ready for data input
	if(0!=ret)return;

	//------------------------------------------step 1, ready for input header
	SET_MCU_READY;
	ret=check_data_ready_set_stage(se_spi_porcess_flag);//check ready for data input
	if(0!=ret)return;


	memcpy(pstate->pCommHeader,(uint8_t *)am_hal_ios_pui8LRAM,6);

	//----------------------------------------step2, reply ack

	am_hal_ios_pui8LRAM[mcu_reply_ack_reg]=0x06;
	SET_MCU_READY;
	ret=check_data_ready_set_stage(se_spi_porcess_flag);
	if(0!=ret)
		{
		printf("mcu ack timeout\r\n");
		return;}

	//----------------------------------------step3, receive data;
	data_size=pstate->pCommHeader[1];
	data_size<<=8;
	data_size=(data_size&0xff00)+pstate->pCommHeader[0];
	if(0x06<data_size)
		{
		pstate->commDataSize =data_size -6;
		ret=se_pull_data_in(pstate);
		if(0!=ret){
		printf("se pull in data time out\r\n");
		return;}
		}
	//printf("pull data ok and ready\r\n");
	//----------------------------------------step4,process data;
	ret=CheckCRC(pstate->pCommHeader,4,&pstate->pCommHeader[4]);
	if(0!=ret)
		{
		pstate->pCommHeader[0]=0x06;
		pstate->pCommHeader[1]=0x00;
		pstate->pCommHeader[2]=0x00;
		pstate->pCommHeader[3]=0x66;
		AddCRC(pstate->pCommHeader,4,&pstate->pCommHeader[4]);
		}
	else
		{
		//printf("cmd=%x\r\n",pstate->pCommHeader[2]);
		SE_SPI_biometrics_process(pstate);
		wtx_sensor_process_flag=0x01;
		}
	wtx_stage_two_callback();
	ret=check_data_ready_set_stage(se_spi_porcess_flag);
	if(0!=ret)		{
		printf("process timeout\r\n");
		return;}
	//----------------------------------------step5,prepare response
	memcpy((uint8_t *)am_hal_ios_pui8LRAM,pstate->pCommHeader,6);
	SET_MCU_READY;
	ret=check_data_ready_set_stage(se_spi_porcess_flag);
	if(0!=ret)		{
		printf("res header timeout\r\n");
		return;}
	//----------------------------------------step6, se ack
	SET_MCU_READY;
	//------------------------------------------ack received
	ret=check_data_ready_set_stage(se_spi_porcess_flag);
	if(0!=ret)		{
		printf("process timeout\r\n");
		return;}


	//----------------------------------------step7, se receive data
	if(0<pstate->commDataSize)
		{
		ret=se_pull_data_out(pstate);
		if(0!=ret)		{
		printf("res data timeout\r\n");
		return;}
		}
	else
		{
		ret=check_data_ready_set_stage(se_spi_porcess_flag);
		if(0!=ret)return ;
		SET_MCU_READY;
		}


}




