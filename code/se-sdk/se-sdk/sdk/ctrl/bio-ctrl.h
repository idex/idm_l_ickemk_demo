#ifndef BIO_CTRL_H
#define BIO_CTRL_H

#include "idxSeEmkAPI.h"
#include "idxSeSdkTypes.h"

#define LED_COLOR_GREEN ( 0x01 )
#define LED_COLOR_RED 	( 0x02 )

/* Data declarations */
extern uint8_t ctrlErr;

void idxBioCtrlInit(void);
			  
/** \brief Send a command and get its response.
 *
 * This function sends a command and gets its response. Any encryption and
 * signing required by the bOptions byte will be dealt with.
 *
 * This is the interface that would be called from within idxSeBioApi.c (and
 * some calls from within bio-ctrl.c) replacing the current mcuComms().
 *
 * If command data is to be sent it must be located at
 * pBuffer[IDX_BIO_BUFFER_DATA_OFFSET] which allows, for those implementations
 * that are able to take advantage of this, the host interface header to be
 * placed in front of the data in the buffer.  The amount of additional command
 * data to send, if any, after the header is *\ref pDataSize. (Note that this is
 * not \a wSize and \ref pDataSize should be zero if there is no command data to
 * send.)
 *
 * Similarly, any response data will be located at
 * \ref pBuffer[\ref IDX_BIO_BUFFER_DATA_OFFSET], and the number of bytes of
 * response data returned will be the (updated value of) *\ref pDataSize.
 *
 * \param[in]    bOrdinal     Specifies the host interface command to send.
 * \param[in]    bOptions     Options for the host interface command.
 * \param[inout] pDataSize    Pointer to a value initially indicating the number
 *                            of command data bytes to send, and updated to
 *                            indicate the number of response data bytes that were
 *                            returned. (If NULL, the number of command data bytes
 *                            is assumed to be zero, and the number of response
 *                            data bytes is not returned.)
 * \param[in]    pCommsBuffer Pointer to a structure giving the size and location
 *                            of the communication buffer. Any command data is
 *                            passed in this buffer (starting at
 *                            pCommsBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET])
 *                            and, if pRespDataBuf is NULL, any response data is
 *                            returned in the same location.
 * \param[out]   pwCode       Pointer to where the response wCode is returned, may
 *                            be NULL if the value of wCode is not required in which
 *                            case a returned wCode that indicates an error will be
 *                            reflected in the function's return value.
 * \param[in]    pRespDataBuf Normally NULL, in which case any response data is
 *                            returned in pCommsBuffer. However, to avoid copying
 *                            returned data to another location, a non-NULL pointer
 *                            may be given which points to a structure giving the
 *                            size and location of an alternative buffer into which
 *                            the response data may directly be written. Note that
 *                            in this case the response data will be placed at the
 *                            start of the response data buffer.
 *
 * \return Either IDX_SUCCESS, or an error code.
 */
uint16_t idxMcuCommsFull(uint8_t            bOrdinal,
                         uint8_t            bOptions,
                         uint16_t          *pDataSize,
                         const idxBuffer_t *pCommsBuffer,
                         uint16_t          *pwCode,
                         const idxBuffer_t *pRespDataBuf);
				  
/*
 * This is a trivial wrapper around idxMcuCommsFull() setting both
 * pwCode and pRespDataBuf to NULL. It is expected that most calls to
 * send a command to the MCU will use this simplified interface.
 */
uint16_t idxMcuComms(uint8_t            bOrdinal,
                     uint8_t            bOptions,
                     uint16_t          *pDataSize,
                     const idxBuffer_t *pCommsBuffer);

/*  Debug Function Declarations */
void setReplyData(uint8_t *buf, uint8_t bsize, const idxBuffer_t *pCommBuff);
void setErrorData(uint8_t *buf, uint8_t bsize, const idxBuffer_t *pCommBuff);
void addReplyData(uint8_t *buf, uint8_t bsize, const idxBuffer_t *pCommBuff);                  

#endif // BIO_CTRL_H
