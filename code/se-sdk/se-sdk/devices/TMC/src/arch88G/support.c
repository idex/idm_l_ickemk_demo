/*
 * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: support.c
 * SCFID: 
 * Feature: Big endian transform into little endian or little endian transform into big endian.
 * Version: V0.1
 *
 * History: 
 *   2014-11-19 
 *     1. Original version 0.1
 */
#include "halglobal.h"

/*
* Function: void TransBigLittleBW(u8 *pbIn, u32 *pwOut)
* Description: big endian transform into little endian or little endian transform into big endian. input: byte,output: word.
* Input: pbIn - ready to transform data	(byte)
* Output: pwOut - the transform results	(word)
* Return: - 	
* Other: -
*/
void TransBigLittleBW(u8 *pbIn, u32 *pwOut)
{
	u16 i;
	u8 tmp[4];
	for (i=0; i<4; i++)
	{
		tmp[i] = pbIn[3-i];
	}		
	*(pwOut) = *(u32 *)tmp;	
}

/*
* Function: void TransBigLittleBB(u8 *pbIn, u8 *pbOut)
* Description: big endian transform into little endian or little endian transform into big endian. input: byte,output: byte.
* Input: pbIn - ready to transform data (byte)
* Output: pbOut - the transform results	(byte)
* Return: -	
* Other: - 
*/
void TransBigLittleBB(u8 *pbIn, u8 *pbOut)
{
	u16 i;
	for (i=0; i<4; i++)
		*(pbOut+i) = *(pbIn+3-i);
}

/*
* Function: void TransBigLittleWB(u32 ulIn, u8 *pbOut)
* Description: big endian transform into little endian or little endian transform into big endian. input: word,output: byte.
* Input: wIn - ready to transform data (word)
* Output: pbOut - the transform results	(byte)
* Return: - 	
* Other: -
*/
void TransBigLittleWB(u32 ulIn, u8 *pbOut)
{
	u8 i;
	u8 tmp[4];
	*(u32 *)tmp = ulIn;
	for (i=0; i<4; i++)
		*(pbOut+i) = tmp[3-i];			
}

