'''
Created on 14 Sep 2018

@author: jl241
'''
#import time
#
from struct import *

from tkinter import *
from tkinter import ttk
#from PIL import Image, ImageTk
import base64, zlib, os
#import tkFont
#
#from smartcard.System import readers
from smartcard.util import toHexString
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest

from smartcard.CardMonitoring import CardMonitor, CardObserver

from time import sleep

#import pyautogui

####################################### Constants #################################

IDEX= b'eJyt0ltsU3UcB/Cv+5923VbGbt0FRNoBZWRjUBwrVFo2ydxK+/e/+WB08GA00fBgshmioPHBxH\
hBs2wPRonwQCJEHtQNwQdjBAaEKeDsXLhlDJFx6S6ujtGtu/D32zGWhhie/J9+e8759Xx+/9P/OcBj3PLz\
wW8LOsxALoAihiWsxv36o8atoyW4Hc+xlYwLt9vXInxiHcInPYwX4VNPo/90Ffo7qjHQ4cfAL89i4Nc6DM\
Zz9nkMnK3H4Ll6DP22FUOdL2Ho91cxFNqGv/94nXkDw93xbMfwhXcROf8WIhfeYXh8kbn0HvM+Ipc/xkjP\
Jxi50sS0YKT3M4xc3Y07f+7G6LUvcefaXu734syJ/Tzeh9G/9uFu3wHma9y9+Q3u3mpjDiN6+xDGwkcQDf\
+IaP9PGBs8jvHBdowPnUJs+AxikXNMCLGR84jduYTJ0cuYGO3FZPQ6ppjJ6E1MjfVhcrwfU+NhTMWGMD3x\
D6YmRjE9FcX0dAz37t1DSF+F1npm/ZJWAUYtYG4ALM1AaiuQ1gmkRwC7zoJXu7BF12GnbsQXugVHdBu6dW\
jO59AX0q+m99IH6V+kf41+h87ELvo99N/SH6Pvou9L8E76Cvp6+u30TfQH6dvpQ/QX6Xvpr9OH6Yfpowl+\
lROorQQatgDN24HWJqDzIBA5AeiuTOiLLugrddB9jdD9LdCRNuhogs+hL6Tny9bso5f07BXZRr+Tfhf9Xv\
rv6I/Td9HfSPCC3qDnu9ucQp9Kb6VPp7dnQfvot9K/Tf85/Q/03Qke6ahFHhrgQDOK0YpydKICEfihM+uh\
XQ3QdR9AN+6BbjkE3dYBHbqa4O305fRB+pfpd9A30X9F/z39z/Sn6TlnSw/9DfrInC9hhwA7bGOHD9lhPz\
ucZIdr7BDJ1BhxaYzWaUQbNcZaNGJtGpMhPecX0q+k30j/HP0r9G/Sf0S/m/4A/WH6dvpO+h76gQRvmV0B\
G/1Cege9k76E3kf/An0j/af0B+iP0l9O8P811MyQ3BKGlHJ2/1D1QcFvf5JHFYU1D11flSqyfcptZPgTrv\
c4K5W/yGR2q2KxOKGTU4i8CrnRarilfYH0LJ4pKxmUapHJXCqrUizVPLOm3b+RJfMdnDEryak8xgKlvGKF\
8vpZX5NlGE9sDtgMtyowKlWZ8CjTErqAVAXCFqyxzJNPiSK1WihlLJcy18hVcllSKWf3Sottpi5KlfQ8bj\
ZtCMyzyhKe5aTdr7vi9+8z50qHubrGXKxyU2frStrtKj+Zs62RNcGAJVstE8qdVM7+ybI0aZ1KS3YHqvJE\
WXC+NbjMqFCyVKxX5oygN1WYhCiUa4Uz/pOUm0w2uUIUBPyr7Es3qPVmq98tnPF1cIgyuSgpwx3g33eYLD\
6/NfmZ+AL5rWaPLLUIIy2Z67cpYOMtzKyzL8XkCta4FucVOL1SZYuls+svK9JF9uxTdphMxXLuOW5ebhGm\
eTkZbGbzqgf1+Me/1p6TlZVXvFHOPiz5qHfo/xj/As0gO1E='

msg = {
 "init":              [0x00,0x54,0x00,0x01,0x01,0x02],
 "uninit":            [0x00,0x54,0x01,0x01,0x00],
 "get version" :      [0x00,0x54,0x30,0x01,0x00],
 "get fw version":    [0x00,0x54,0x36,0x01,0x00],
 "prepare image":     [0x00,0x54,0x02,0x01,0x01,0x03],
 "get image":         [0x00,0x54,0x04,0x11,0x01,0x00],
 "load image":        [0x00,0x54,0x52,0x01],
 "read data":         [0x00,0x54,0x50,0x01,0x04],
 "match templates":   [0x00,0x54,0x07,0x01],
 "merge templates":   [0x00,0x54,0x06,0x01],
 "get template" :     [0x00,0x54,0x05,0x11,0x01,0x03],
 "get full template": [0x00,0x54,0x7f,0x01,0x01,0x03],     # special code for tests
 "list records":      [0x00,0x54,0x12,0x01,0x03,0x00,0xff,0xff],
# "get records" :    [0x00,0x54,0x16,0x01],
 "delete record" :    [0x00,0x54,0x11,0x01,0x02,0xff,0xff],
 "store record" :     [0x00,0x54,0x10],
 "set parameter 32":  [0x00,0x54,0x09,0x01,0x03,0x21,0x8c,0x0a],
 "get parameter 32":  [0x00,0x54,0x08,0x01,0x01,0x21],
 "store params":      [0x00,0x54,0x0c,0x01,0x00],
 "enroll":            [0x00,0x54,0x17,0x01],
 # update commands
 "store blob":      [0x00,0x54,0x18,0x01],
 "update start":    [0x00,0x54,0x31,0x01,0x00],
 "update details":  [0x00,0x54,0x32,0x01],
 "update data":     [0x00,0x54,0x33],
 "update hash":     [0x00,0x54,0x34,0x01],
 "update end":      [0x00,0x54,0x35,0x01,0x00],
 "SE EMK enroll":   [0x00,0x54,0x87,0x00],
 "SE EMK enroll1":  [0x00,0x54,0x8b,0x01],
 "SE EMK enroll2":  [0x00,0x54,0x8b,0x02],
 "SE EMK match":    [0x00,0x54,0x88,0x00,0x00],
 "SE EMK info":     [0x00,0x54,0x89,0x00,0x00],
 "SE EMK debug":    [0x00,0x54,0x8c,0x00,0x00]
}

errCodes ={
    0x6300: 'templates do not match',
    0x6741: 'communication problem (EIO)',
    0x6742: 'interrupt problem (ETIME)',
    0x6743: 'wrong calibration data (EAGAIN)',
    0x6745: 'image too small',
    0x6746: 'acquisition aborted',
    0x6747: 'quality is too bad',
    0x6748: 'user timeout',
    0x6969: 'UNKNOWN ERROR',
    0x6985: 'Conditions of use not satisfied',
    0x6986: 'Command is not allowed',
    0x6A80: 'Bad parameters',
    0x6A81: 'function not supported',
    0x6A83: 'Record not found',
    0x6A84: 'Not enough space left on device',
    0x6A87: 'Size error',
    0x6985: 'wrong state',
    0x6A89: 'keys are already created',
    0x6D00: 'command is not valid or not implemented'
}

#define IDX_SUCCESS            0x9000

####################################### Functions #################################
def transmit(apdu):
    return cardobserver.transmit(apdu)

def transmit2(apdu):
    #print 'sending ' + toHexString(apdu)
    try :
        response, sw1, sw2 = root.cardservice.connection.transmit( apdu )
    except :
        response, sw1, sw2 = cardservice.connection.transmit( apdu )
    #print 'response: ',  [hex(x)[2:] for x in response] , ' status words: ', "%x %x" % (sw1, sw2)
    print ('response: ',  response , ' status words: ', "%x %x" % (sw1, sw2))
    if (response[2] != 144) and (response[3] != 0):
        if (response[2]==0x6a)  and (response[3]==0x83):    # Record not found
            print ('Record not found')
            return response
        elif (response[2]==0x6a)  and (response[3]==0x81):    # function not supported
            print ('Function not supported')
            return response
        print ('MCU reported error: '+ str(hex(response[2]))+' '+ str(hex(response[3])))
        return []
    elif (sw1 != 0x90) and (sw2 != 0):
        print ('APDU reported error: '+ str(hex(response[2]))+' '+ str(hex(response[3])))
        return []
    else:
        return response

def transmit3(apdu):
    print (apdu)
    

####################################   Frame #########################
class TextRedirector(object):
    def __init__(self, widget, tag="stdout"):
        self.widget = widget
        self.tag = tag
 
    def write(self, str):
        self.widget.configure(state="normal")
        self.widget.insert("end", str, (self.tag,))
        self.widget.configure(state="disabled")
        self.widget.see(END)
        self.widget.winfo_toplevel().update()

class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.init = 0
        self.widgets = {
            #'init':    {'row':0,'cmd':{'':self.cmdInit},   'text':'MCU Init (HOST/IF call)'},
            #'extract': {'row':1,'cmd':{'':self.cmdExtract},'text':'SE-EMK Extract (EMK internal call)'},        
            #'merge':   {'row':2,'cmd':{'':self.cmdMerge},  'text':'SE-EMK Merge (EMK internal call)'},        
            'match1':  {'row':0,'cmd':{'':self.cmdMatch1}, 'text':'Erase-OS'},#'SE-EMK Match1 (EMK internal call)'},
            'match2':  {'row':1,'cmd':{'':self.cmdMatch2}, 'text':'Download-OS'},#'SE-EMK Match2 (EMK internal call)'},      
            #'info':    {'row':5,'cmd':{'':self.cmdInfo},   'text':'SE-EMK-API GetInfo'},
            #'enroll':  {'row':6,'cmd':{'Finger1':self.cmdEnroll,'Finger2':self.cmdEnroll2}, 'text':'SE-EMK-API Enroll'},      
            #'verify':  {'row':7,'cmd':{'x1':self.cmdVerify, 'x10':self.cmdVerify10},'text':'SE-EMK-API Verify'}
        }
        self.NUM_ROWS = 3
        self.NUM_COLS = 1
        self.grid(ipadx=10, ipady = 100, sticky='nsew')
        self.createWidgets()
        
        
    def createWidgets(self):
        top=self.winfo_toplevel()
        top.rowconfigure(0, weight=1)
        top.columnconfigure(0, weight=1)
        for row in range(self.NUM_ROWS): self.rowconfigure(row, weight=1)
        for col in range(self.NUM_COLS): self.columnconfigure(col, weight=1,uniform='fred')
        
        #self.s        = ttk.Style()
        
        # ~~~~~~  Define Label Frames  ~~~~~~~~~~
        self.frames = { k : {'frame':LabelFrame(self,text=v['text'],borderwidth=2,relief ='ridge',foreground='blue'),'cmd':v['cmd'],'row':v['row']} for k,v in self.widgets.items()}
        
        # ~~~~~~  Positioning Frames on Grid ~~~~~~~~~~
        for key in self.frames.keys():
            self.frames[key]['frame'].grid( row = self.frames[key]['row'], column=0,columnspan=1, sticky = 'wens')
        
        # ~~~~~~  Define Buttons inside Frames ~~~~~~~~~~
        self.buttons={}
        for k,v in self.frames.items():
            for k1,v1 in v['cmd'].items(): 
                self.buttons[k+k1] = ttk.Button(v['frame'],text=k1,command=v1, state='disabled') 
        
        # ~~~~~~  Positioning Buttons inside Frames ~~~~~~~~~~
        for k,v in self.buttons.items() :
            v.pack(side=LEFT)
        
        # ~~~~~~  Define Labels used for Comments ~~~~~~~~~~
       # my_font = tkFont.Font(family='Consolas', size=10, weight = 'bold')        
        self.comments = { k : {'label':ttk.Label(self,text='',borderwidth=1,relief ='groove',foreground='blue', width = 60),'row':v['row']} for k,v in self.widgets.items() }
        
        # ~~~~~~  Positioning Comment Labels on Grid ~~~~~~~~~~
        for k in self.comments.keys():
            self.comments[k]['label'].grid(row = self.comments[k]['row'] ,column=1,columnspan=1, sticky = 'wens', padx=(0,0))
        
        # ~~~~~~  Add Picture to Match Label ~~~~~~~~~~
#        self.iconPath = 'idxmatch.gif'
#        self.icon = ImageTk.PhotoImage(Image.open(self.iconPath))
#        self.comments['verify']['label'].image = self.icon  # <== this is were we anchor the img object
#        self.comments['verify']['label'].configure(image=self.icon)
#        self.comments['verify']['label'].configure(anchor='center')
        
        # ~~~~~~  Define Labels used for Alerts ~~~~~~~~~~
        self.alerts = { k : {'label':ttk.Label(self,text='',borderwidth=1,relief ='groove',foreground='blue'),'row':v['row']} for k,v in self.widgets.items() }

        # ~~~~~~  Positioning Alest Labels on Grid ~~~~~~~~~~
        for k in self.alerts.keys():
            self.alerts[k]['label'].grid(row = self.alerts[k]['row'] ,column=2,columnspan=1, sticky = 'wens')


#        self.original = Image.open('nomatch.gif')
#        resized = self.original.resize((275,183),Image.ANTIALIAS)
#        self.comments[1]['image']=ImageTk.PhotoImage(resized)
#        del(self.original)
        
        self.text   = Text(self,height=12, wrap='word')    
        self.text.grid(row=self.NUM_ROWS-1,column=0,rowspan=1, columnspan=3, sticky = 'we')
        self.text.tag_configure("stderr", foreground="#b22222")
        sys.stdout = TextRedirector(self.text, "stdout")
        sys.stderr = TextRedirector(self.text, "stderr")
#     def disableButtons(self,buttons):
#         for b in buttons:
#             b.config(state = DISABLED)
#     def enableButtons(self,buttons):
#         for b in buttons:
#             b.config(state = NORMAL)
#

#        self.buttons['enrollFinger1']['state'] = 'normal' 
#        self.buttons['verifyx1']['state'] = 'normal' 
#        self.buttons['init']['state'] = 'normal' 
#        self.buttons['info']['state'] = 'normal' 
        self.buttons['match1']['state'] = 'normal' 
        self.buttons['match2']['state'] = 'normal' 

    def saveText(self):
        with open("filepy.txt", "wt") as outf:
            outf.write(self.text.get("1.0",'end-1c'))
        pic = pyautogui.screenshot()
        pic.save('screenshot.png')

    def showError(self):
        self.iconPath = 'idxerror.gif'
        self.showImage()
    def showIdx(self):
        self.iconPath = 'idxmatch.gif'
        self.showImage()
    def showRed(self):
        self.iconPath = 'nomatch.gif'
        self.showImage()
    def showGreen(self):
        self.iconPath = 'match.gif'
        self.showImage()
    def showImage(self):
        self.icon = ImageTk.PhotoImage(Image.open(self.iconPath))
        self.comments['verify']['label'].image = self.icon  # <== this is were we anchor the img object
        self.comments['verify']['label'].configure(image=self.icon)

    def wrBlankAlert(self,s):
        self.alerts[s]['label'].configure(background = '#f2f2f2')
        self.alerts[s]['label']['text'] = ''
    def wrAlert(self,s):
        self.alerts[s]['label'].configure(anchor     = 'center')
        #self.alerts[s]['label'].configure(foreground = 'forestgreen')
        self.alerts[s]['label'].configure(background = 'lightyellow')
        self.alerts[s]['label']['text'] = 'Please touch sensor'
    def wrActions(self,s):
        self.alerts[s]['label'].configure(anchor     = 'nw')
        self.alerts[s]['label'].configure(background = 'lightyellow')
        self.alerts[s]['label']['text'] = \
'Please save context by pressing Context->Save buttons.\n \
If create Jira, please attach .txt and .png context files.\n \
To continue, please reset the board and the card reader.'
    def wrText(self,s,text):
        my_font = tkFont.Font(family='Consolas', size=40, weight = 'bold')
        self.alerts[s]['label']['font'] = my_font         
        self.alerts[s]['label'].configure(anchor     = 'nw')
        self.alerts[s]['label'].configure(background = 'lightyellow')
        self.alerts[s]['label']['text'] = text


    def wrBlank(self,s):
        self.comments[s]['label'].configure(background = '#f2f2f2')
        self.comments[s]['label']['text'] = ''
    def wrSuccess(self,s):
        self.comments[s]['label'].configure(anchor     = 'center')
        self.comments[s]['label'].configure(foreground = 'forestgreen')
        self.comments[s]['label'].configure(background = 'lightyellow')
        self.comments[s]['label']['text'] = '  SUCCESS  '
    def wrFailure(self,s,err):
        self.comments[s]['label'].configure(anchor     = 'center')
        self.comments[s]['label'].configure(foreground = 'red')
        self.comments[s]['label'].configure(background = 'lightyellow')
        if err : txt = '  FAILURE : ' + err 
        else:    txt = '  FAILURE  '
        self.comments[s]['label']['text'] = txt
        self.wrActions(s)
        
    def wrInfo(self,data):
        s = ''   
        if data[6] == 1 : s +='EMK Type: IDEX SECURE         ' 
        else : s += 'EMK Type: PB SECURE           '
        s += 'Version: '+str(data[7])+'.'+str(data[8])+'.'+str(data[9]+data[10]*256) +'\n' 
        s += 'Max Touches Per Finger: '+ str(data[11])+'     '  
        s += 'Max Enrolled Fingers: '+ str(data[12])  + '\n'
        s += 'Min Scrach pad RAM [KB]: '+ str(data[13])+'   '
        s += 'Min Template Flash [KB]: '+ str(data[14])
        my_font = tkFont.Font(family='Consolas', size=9, weight = 'bold')
        self.comments['info']['label'].configure(anchor = 'w')
        self.comments['info']['label'].configure(justify = LEFT)
        self.comments['info']['label'].configure(foreground = 'forestgreen')
        self.comments['info']['label'].configure(background = 'lightyellow')
        self.comments['info']['label'].configure(font=my_font)
        self.comments['info']['label']['text'] = s

    def checkForError(self,data,s):
        return 0
        if data :
            if data[2] == 144 : 
                self.wrSuccess(s)
                self.wrBlankAlert(s)
                return 0
            else :
                errCode = data[2]*256+data[3]
                err =  '0x{:04x}'.format(errCode) +'  ' + errCodes[errCode]
                print (err) 
                self.wrFailure(s,err)
        else : self.wrFailure(s,'')
        return 1

    def cmdInit(self):
        if self.init == 0 : 
            self.wrBlank('init')
            data = transmit(msg['init'])
            sleep(0.5)
            errCode = self.checkForError(data, 'init')
            if not errCode : self.init = 1

    def cmdExtract(self):   # label-1
        self.wrBlank('extract')
        if self.init == 0: self.cmdInit()
        self.wrAlert('extract')
        data = extract()
        self.checkForError(data, 'extract')

    def cmdMerge(self):
        self.wrBlank('merge')
        self.wrBlankAlert('merge')
        if self.init == 0: self.cmdInit()
        #data = merge_hack()
        data = merge()
        self.checkForError(data, 'merge')

    def cmdMatch1(self):
        f = open("DEMO_ReturnToBL.txt","r")
        s = f.readlines()
        f.close()
        for line in s:
            m = re.findall('(..)',line)
            apdu = [int(x,16) for x in m]
            #print apdu
            data = transmit(apdu)

    def cmdMatch2(self):
        f = open("..\\..\\output\\thd89\\bin\\se-sdk-idm_l-thd89.txt","r")
        s = f.readlines()
        f.close()
        for line in s:
            if len(line) <  3 : continue
            m = re.findall('(..)',line)
            apdu = [int(x,16) for x in m]
            #print apdu
            data = transmit(apdu)
        
    def cmdMatch1_(self):
        self.wrBlank('match1')
        self.wrBlankAlert('match1')
        if self.init == 0: self.cmdInit()
        #data = matchPhase1_hack()
        data = matchPhase1()
        self.checkForError(data, 'match1')
        
    def cmdMatch2_(self):
        self.wrBlank('match2')
        self.wrBlankAlert('match2')
        if self.init == 0: self.cmdInit()
        data = matchPhase2()
        self.checkForError(data, 'match2')
        
    def cmdInfo_(self):
        self.wrBlank('info')
        if self.init == 0: self.cmdInit()
        data = transmit(msg['SE EMK info'])
        if data :
            if data[2] == 144 :
                if data[6] == 1 : print ('EMK Type: IDEX SECURE' )
                else : print ('EMK Type: PB SECURE')
                self.wrInfo(data)   
                print ('Version: ',data[7],'.',data[8],'.',str(data[9]+data[10]*256) )
                print ('Max Touches Per Finger: ', data[11] ) 
                print ('Max Enrolled Fingers: ', data[12] )
                print ('Min Scrach pad RAM [KB]:  ', data[13])  
                print ('Min Template Flash [KB]: ', data[14])
            else: self.wrFailure('info')
        else : self.wrFailure('info')

    def cmdInfo(self):    # label-6
        self.wrBlank('info')
        data = transmit(msg['SE EMK debug'])

    def cmdEnroll(self):    # label-6
        self.wrBlank('enroll')
        self.wrAlert('enroll')
        #root.update()   
        if self.init == 0: self.cmdInit()
        data = transmit(msg['SE EMK enroll'])
        errCode = self.checkForError(data, 'enroll')
        return errCode 
        
    def cmdEnroll1(self):    # label-6
        self.wrBlank('enroll')
        self.wrAlert('enroll')
        #root.update()   
        print ('Please touch the sensor')
        if self.init == 0: self.cmdInit()
        data = transmit(msg['SE EMK enroll'])
        errCode = self.checkForError(data, 'enroll')
        return errCode 
        
    def cmdEnroll2(self):    # label-6
        self.wrBlank('enroll')
        self.wrAlert('enroll')
        #root.update()   
        print ('Please touch the sensor')
        if self.init == 0: self.cmdInit()
        data = transmit(msg['SE EMK enroll2'])
        errCode = self.checkForError(data, 'enroll')
        self.buttons['enrollFinger2']['state'] = 'disabled' 
        return errCode 

    def cmdEnroll10(self):
        for i in range(5):
            print (i, )
            errCode = self.cmdEnroll()
            if errCode > 0 : return 
            sleep(1)


    def cmdVerify(self):
        print ('Please touch the sensor')
#        self.showIdx()
        self.wrAlert('verify')
        root.update()
        if self.init == 0: self.cmdInit()
        data = transmit(msg['SE EMK match'])
        if data :
            if data[2] == 144 : 
                self.wrBlankAlert('verify')
                self.showGreen()
                return 0
            else : 
                self.showRed()
                self.wrActions('verify')
        else : 
            self.showError()
            self.wrActions('verify')
        return 1

    def cmdVerify10(self):
        for i in range(5):
            print (i, )
            errCode = self.cmdVerify()
            if errCode > 0 : return
            sleep(1)

    def cmdVerify2(self):
        print ('Please touch the sensor')
#       self.showIdx()
        self.wrAlert('verify')
        root.update()
        #if self.init == 0: self.cmdInit()
        cmd = [0x00,0x54,0x8a,0x00,0x00]
        cmd = [0x00,0x54,0x81,0x00,0x00]
        #data = transmit(msg['SE EMK match'])
        data = transmit(cmd)
        if data :
            if data[2] == 144 : 
                self.wrBlankAlert('verify')
                self.showGreen()
            else : 
                self.showRed()
                self.wrActions('verify')
        else : 
            self.showError()
            self.wrActions('verify')
#        cmd = [0x00,0x54,0x8c,0x00]
#        res = transmit(cmd)
#        t1 = int((res[4]*256 + res[5] ) * 64 / 1000)
#        s = str(t1)+'ms '
#        print (s)
                
    def cmdMatchtemplate(self):
        a = 0
        print ('Please touch the sensor')
#        self.showIdx()
        self.wrAlert('verify')
        root.update()
        if self.init == 0: 
            self.cmdInit()
            #for _i in range(10000): a += 1
            sleep(.5)
        transmit(msg["prepare image"])
        #for _i in range(1000000): a += 1
        #sleep(0.7)
        cmd = [x for x in msg["get template"]]
        cmd[3] = 1      # bOptions
        cmd[5] = 0      # template remains local 
        transmit(cmd)
        #sleep(0.2)
        try: 
            cmd = msg["match templates"]+[3,6,0,0] # dataSize, {flags,mask-len,mask,num-templates}
            data = transmit(cmd)
            if data :
                if data[2] == 144 : 
                    self.wrBlankAlert('verify')
                    self.showGreen()
                    #return 0
                else : 
                    self.showRed()
                    self.wrActions('verify')
            else : 
                self.showError()
                self.wrActions('verify')        
        except Exception (e):
            print (str('Match: '+e+a))
        
        cmd = [0x00,0x54,0x8c,0x00]
        res = transmit(cmd)
        t1 = int((res[4]*256 + res[5] ) * 16 / 1000)
        t2 = int((res[6]*256 + res[7] ) * 16 / 1000)
        t3 = int((res[8]*256 + res[9] ) * 16 / 1000)
        t4 = int((res[10]*256 + res[11] ) * 16 / 1000)
        s = str(t1)+'ms '+str(t2)+'ms '+str(t3)+'ms '+str(t4)+'ms '
        print (s)
        pass
##########################    Extract  ########################
def extract():
#     print 'Please touch the sensor'
#     cmd    = [x for x in msg["prepare image"]]
#     cmd[3] = 0  # bOptions = 0    
#     #print 'cmd:',cmd
#     data = transmit(cmd)
#     if data == [] : return -1
#     sleep(1.0)

    loadimage()
    
    cmd    = [x for x in msg["get template"]]
    cmd[3] = 0x10  # bOptions = RETURN SIZE ONLY
    cmd[5] = 0x01  # flags = Secure Matcher
    #print 'cmd:',cmd    
    data = transmit(cmd)
    print (data)
    if not data : return data
    if len(data) < 8 : return data 
    sleep(0.2)

    tSize = data[6] + data[7]*256  # "return size" actually return reply.wSize - headerSize
    
    tInfo = []
    cmd = [] + msg["read data"] + [0,0,8,0] # read data (it needs offset and size)
    cmd[3] = 0
    #print 'cmd:',cmd
    data = transmit(cmd)    
    tInfo.append(data)
    tSize -= 8  #
        
    MAX_SE_TRANSFER_SZ = 224
    rdOff = 8;
    tData = []
    while tSize > MAX_SE_TRANSFER_SZ :        
        cmd = [] + msg["read data"] + wrU16(rdOff) + [MAX_SE_TRANSFER_SZ,0]
        #print 'cmd: ', cmd
        data = transmit(cmd)
        tData.append(data)
        tSize -= MAX_SE_TRANSFER_SZ
        rdOff += MAX_SE_TRANSFER_SZ
    cmd = [] + msg["read data"] + wrU16(rdOff) + [tSize,0]
    data = transmit(cmd)
    tData.append(data)
    return data

def wrU16(u16):
    return [u16 & 0xff, ( u16 >> 8 ) & 0xff]    

def merge_hack():
    Data_3_Size = 200
    cmd    = [x for x in msg["merge templates"]]
    #cmd[3] = 0x40  # bOptions = CHUNKS
    cmd += [4 + Data_3_Size, 2, 1] + wrU16(Data_3_Size) + list(range(255,255-Data_3_Size,-1)) # data[4] = { flags = SECURE-MATCHER, num_templates, template-size }
    print ('cmd: ', cmd)
    data = transmit(cmd)
    sleep(1.0)
    return data


def merge():
#    cmd  = [x for x in msg["store blob"]]
#    cmd[3] = 0
#    cmd += [3,6,0,0] # data[3] = { flags = SE_TEMPLATE, size[2] = {0,0} }
#    data = transmit(cmd)    
#    return data

    Data_3_Size = 3584
    cmd    = [x for x in msg["merge templates"]]
    cmd[3] = 0x40  # bOptions = CHUNKS
    cmd += [4,2,1] + wrU16(Data_3_Size)  # data[4] = { flags = SECURE-MATCHER, num_templates, template-size }
    print ('cmd: ', cmd)
    transmit(cmd)
    sleep(1.0)

    MAX_SE_TRANSFER_SZ = 224
    tData = 16 * list(range(255,-1,-1))
    tSize = Data_3_Size
    wrOff = 0
    while tSize > MAX_SE_TRANSFER_SZ :
        cmd    = [x for x in msg["merge templates"]]
        cmd[3] = 0x40  # bOptions = CHUNKS
        cmd += [MAX_SE_TRANSFER_SZ] + tData[wrOff: wrOff + MAX_SE_TRANSFER_SZ] 
        transmit(cmd)
        tSize -= MAX_SE_TRANSFER_SZ
        wrOff += MAX_SE_TRANSFER_SZ
    cmd    = [x for x in msg["merge templates"]]
    cmd[3] = 0xc0  # bOptions = CHUNKS | LAST
    cmd += [tSize] + tData[wrOff: wrOff + tSize] 
    data = transmit(cmd)

    sleep(0.1)
    cmd  = [x for x in msg["store blob"]]
    cmd += [3,6,0,0] # data[3] = { flags = SE_TEMPLATE, size[2] = {0,0} }
    data = transmit(cmd)    
    return data

def matchPhase1_hack():
    Data_3_Size = 150
    cmd    = [x for x in msg["match templates"]]
    cmd[3] = 0  # bOptions = CHUNKS
    cmd += [5 + Data_3_Size , 0, 0, 1] + wrU16(Data_3_Size) + list(range(255,255-Data_3_Size,-1)) 
    print ('cmd: ', cmd)
    data = transmit(cmd)
    sleep(1.0)
    return data

def matchPhase1():
    Data_3_Size = 3584
    cmd    = [x for x in msg["match templates"]]
    cmd[3] = 0x40  # bOptions = CHUNKS
    cmd += [5,0,0,1] + wrU16(Data_3_Size)
    print ('cmd: ', cmd)
    transmit(cmd)
    sleep(1.0)

    MAX_SE_TRANSFER_SZ = 224
    tData = 16 * list(range(255,-1,-1))
    tSize = Data_3_Size
    wrOff = 0
    while tSize > MAX_SE_TRANSFER_SZ :
        cmd    = [x for x in msg["match templates"]]
        cmd[3] = 0x40  # bOptions = CHUNKS
        cmd += [MAX_SE_TRANSFER_SZ] + tData[wrOff: wrOff + MAX_SE_TRANSFER_SZ] 
        transmit(cmd)
        tSize -= MAX_SE_TRANSFER_SZ
        wrOff += MAX_SE_TRANSFER_SZ
    cmd    = [x for x in msg["match templates"]]
    cmd[3] = 0xc0  # bOptions = CHUNKS | LAST
    cmd += [tSize] + tData[wrOff: wrOff + tSize] 
    data = transmit(cmd)
    return data
    
def matchPhase2():
    MAX_FINGERS = 1
    MAX_TOUCHES = 1
    info = [1,1,1,1]
    data = []
    for finger in range(MAX_FINGERS):
        for touch in range(MAX_TOUCHES):
            cmd    = [x for x in msg["match templates"]]
            cmd[3] = 0x10  # bOptions = RETURN SIZE
            cmd   += [5+len(info),0x10,0,1] +wrU16(len(info))+info
            print ('cmd: ', cmd)
            data = transmit(cmd)
            tSize = data[6]+data[7]*256
            MAX_SE_TRANSFER_SZ = 224
            rdOff = 8;
            tData = []
            while tSize > MAX_SE_TRANSFER_SZ :        
                cmd = [] + msg["read data"] + wrU16(rdOff) + [MAX_SE_TRANSFER_SZ,0]
                data = transmit(cmd)
                tData.append(data)
                tSize -= MAX_SE_TRANSFER_SZ
                rdOff += MAX_SE_TRANSFER_SZ
            cmd = [] + msg["read data"] + wrU16(rdOff) + [tSize,0]
            data = transmit(cmd)
            tData.append(data)
    return data

def loadimage():
    MAX_SE_TRANSFER_SZ = 128
    fd = open("image.bmp","rb")
    data1 = fd.read()
    fd.close()
    data2 = unpack('B'*len(data1),data1)
    allbytes = [x for x in data2]    
    imgbytes = allbytes[1078:]   
    if len(imgbytes) != 17292 :
        exit(0)
    cmd    = [x for x in msg["load image"]]
    cmd[3] = 0x40  # bOptions = CHUNKS
    cmd += wrU16(MAX_SE_TRANSFER_SZ) + imgbytes[0:MAX_SE_TRANSFER_SZ]
    print ('cmd: ', cmd)
    transmit(cmd)
    sleep(1.0)
    tSize = 17292-MAX_SE_TRANSFER_SZ
    wrOff = MAX_SE_TRANSFER_SZ
    while tSize > MAX_SE_TRANSFER_SZ :
        print (wrOff,'  ',)
        if wrOff == 16352 :
            print ("before crash" )
        cmd    = [x for x in msg["load image"]]
        cmd[3] = 0x40  # bOptions = CHUNKS
        cmd += [MAX_SE_TRANSFER_SZ] + imgbytes[wrOff: wrOff + MAX_SE_TRANSFER_SZ] 
        transmit(cmd)
        tSize -= MAX_SE_TRANSFER_SZ
        wrOff += MAX_SE_TRANSFER_SZ
    cmd    = [x for x in msg["load image"]]
    cmd[3] = 0xc0  # bOptions = CHUNKS | LAST
    cmd += [tSize] + imgbytes[wrOff: wrOff + tSize] 
    data = transmit(cmd)
    return data

def nop():    
    return
    
#########################################  Main ######################################

########################################  Tests  ####################################

#exit(0)
#########################  Card Reader Connection 
# cardtype = AnyCardType()
# cardrequest = CardRequest( timeout=1, cardType=cardtype )
# cardservice = cardrequest.waitforcard()
#        
# cardservice.connection.connect()
# print toHexString( cardservice.connection.getATR() )
# print cardservice.connection.getReader()
   
#getSpiBuffer()
#getTemplate2()
 
# print 'cmd: ',msg["init"]
# transmit(msg["init"])
# sleep(0.1)
# print 'cmd: ',msg["get fw version"]
# transmit(msg["get fw version"])
# sleep(0.1)
 
# for i in range(10):
#     transmit(msg["secure matcher enroll"])
#     sleep(0.5)
#     transmit(msg["secure matcher match"])
#     sleep(0.5)
#  
# exit(0);
#  
# for i in range(2):
#     extract()
#     merge_hack()
 
# extract()
# 
# merge()
# 
# # matchPhase1_hack()
# matchPhase1()
# matchPhase2()
#    
# exit(0)

# cmd = [x for x in msg["init"]]
# cmd[5] = 0x02
# transmit(cmd)
# transmit(msg["get parameter 32"])
# transmit(msg["set parameter 32"])
# transmit(msg["store params"])
#  
# exit(0)
# ######################## Update commands
# fname = 'idx-update-0.4.182.bin'
# loadUpdateBin(fname)
# fname = 'eclipse_debug.bin'
# loadNewFw(fname)
# exit(0)




# a simple card observer that prints inserted/removed cards
class PrintObserver(CardObserver):
    """A simple card observer that is notified
    when cards are inserted/removed from the system and
    prints the list of cards
    """
    
    def connect(self):
        cardtype = AnyCardType()
        cardrequest = CardRequest( timeout=1, cardType=cardtype )
        self.cardservice = cardrequest.waitforcard()
  
        self.cardservice.connection.connect()
        print (toHexString( self.cardservice.connection.getATR() ))
        print (self.cardservice.connection.getReader())


    def update(self, observable, actions):
        (addedcards, removedcards) = actions
        for card in addedcards:
            print("+Inserted: ", toHexString(card.atr))
            self.connection = card.createConnection()
            self.connection.connect()
            app.init = 0
            #self.connect()
            #app.showIdx()
###########   Please comment out the next block for Enroll 
#            try:
#                self.match2()
#            except Exception, e:
#                print str('Update '+e)
#                #app.showError()
#                cardmonitor.deleteObserver(cardobserver)
###############################################################                
        for card in removedcards:
            print("-Removed: ", toHexString(card.atr))

    def match(self):
        try: 
            app.cmdVerify()
        except Exception (e):
            
            print (str('Match: '+e))
            #app.showError()
            #cardmonitor.deleteObserver(cardobserver)

    def match2(self):
        try: 
            app.cmdVerify2()
            #app.cmdMatchtemplate()
        except Exception (e):
            print (str('Match: '+e))
            #app.showError()
            #cardmonitor.deleteObserver(cardobserver)
        
#     def match(self):
#         for _ in range(5) : 
#             res = self.transmit(msg['init'])
#             time.sleep(.5)
#             if ( res[2] == 144) and (res[3] == 0) : break               
#         self.transmit(msg["prepare image"])
#         time.sleep(1.0)
#         cmd = [x for x in msg["get template"]]
#         cmd[3] = 1      # bOptions
#         cmd[5] = 0      # template remains local 
#         self.transmit(cmd)
#         time.sleep(0.2)
#         cmd = msg["match templates"]+[3,6,0,0] # dataSize, {flags,mask-len,mask,num-templates}
#         res = self.transmit(cmd)
#         if(res==[]): return 
#         elif( res[2] ==  99 ): 
#             print 'NO match !'
#             app.showRed()
#         elif( res[2] == 144): 
#             print 'MATCH !'
#             app.showGreen()
#         else: 
#             print 'ERROR !'
#             app.showError()

    def test(self):
        app.iconPath = 'match.gif'
        app.icon = ImageTk.PhotoImage(Image.open(app.iconPath))
        app.comments[1].image = app.icon  # <== this is were we anchor the img object
        app.comments[1].configure(image=app.icon)
        


    def transmit(self,apdu):
        #print 'sending'
        #response, sw1, sw2 = self.connection.transmit( apdu )
        #print 'response: ',  response , ' status words: ', "%x %x" % (sw1, sw2)
        #return response
        try:
            response, sw1, sw2 = self.connection.transmit( apdu )
            #print 'response: ',
            #for i in range(len(response)): print "%02x" % response[i],
            s = 'response: '
            for i in range(len(response)): s += "%02x " % response[i]
            print (s,)
            print (' status words: ', "%02x %02x" % (sw1, sw2))
            return response
        except Exception (e):
            print ('Transmit ' + str(e))
            #app.showError()


########################  GUI ( main starts here)  ###############################        
root = Tk()
root.title('Ridge Matcher Test Tool')
with open('tree.ico', 'wb') as f:
    f.write(zlib.decompress(base64.b64decode(IDEX)))
root.iconbitmap('tree.ico')
os.remove('tree.ico')

app = Application(master=root)


print("Insert or remove a smartcard in the system.")
print("This program will exit in 10 seconds")
print("")
cardmonitor = CardMonitor()
cardobserver = PrintObserver()
cardmonitor.addObserver(cardobserver)

#app = Application(master=root)

#cardobserver.test()
#root.mainloop()

menubar = Menu(root)
# create a pulldown menu, and add it to the menu bar
filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Unimplemented", command=nop)
filemenu.add_command(label="Unimplemented", command=nop)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=root.quit)
menubar.add_cascade(label="File", menu=filemenu)

# create more pulldown menus
editmenu = Menu(menubar, tearoff=0)
editmenu.add_command(label="Save", command=app.saveText)
menubar.add_cascade(label="Context", menu=editmenu)

helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="About", command=nop)
menubar.add_cascade(label="Help", menu=helpmenu)

changemenu = Menu(menubar, tearoff=0)
changemenu.add_command(label="Change", command=nop)
menubar.add_cascade(label="Mode Contact    ", menu=changemenu)
            
# display the menu
root.config(menu=menubar)

root.mainloop()


#sleep(30)

# don't forget to remove observer, or the
# monitor will poll forever...
cardmonitor.deleteObserver(cardobserver)

exit(0)
