/**
  ******************************************************************************
  * @file    THD89/SPI.h
  * @author  GaoY
  * @version V1.0
  * @date    16/05/2017
  * @brief   
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, TMC SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 TMC</center></h2>
  */

/** @
  * @{
  */

#ifndef __SPI_H__
#define __SPI_H__ 

///* Exported macros ------------------------------------------------------- */
extern u8 SPIMasterEn;
void SPI_MasterInit(uint8_t speed, uint8_t clkMode);
void SPI_SlaveInit(uint8_t clkMode); 
void SPI_DmaRx(uint8_t * buf,uint16_t len);
void SPI_DmaTx(uint8_t * buf,uint16_t len);
void SPI_TxByte(uint8_t data);
u8 SPI_RxByte(void);
void SPI_RxNBytes(uint8_t * buf,uint16_t len);
void SPI_TxNBytes(uint8_t * buf,uint16_t len);

#endif




