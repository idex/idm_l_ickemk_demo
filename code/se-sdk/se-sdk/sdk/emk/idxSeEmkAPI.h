/******************************************************************************
* \file  idxSeEmkAPI.h
* \brief header for SE Enroll/Match Kit API
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#ifndef __IDX_SE_EMK_API_H__
#define __IDX_SE_EMK_API_H__

#include "idxSeEmkDefines.h"
#include "idxBaseDefines.h"
#include "idxSeSdkTypes.h"

/**
* @brief Identify the point where an error occurred.
*
* This is a debugging aid. It is set to a value when an error occurs (that causes
* a function to return a value other than \ref IDX_ISO_SUCCESS) that allows the point
* of the failure to be determined.
*/
extern uint8_t errPoint;

// API FUNCTION DEFINITIONS

/**
* @brief Initialise the SE-EMK module
*
* @return IDX_ISO_SUCCESS
*/
uint16_t idxSeEmkInit(void);

/**
* @brief Generate the SE-EMK version and Fuze ID
*
* @return 32 byte version and fuze ID
*/

void idxSeEmkGetUnitVersion(idx_UnitFuzeVersion_t* pEmkVersion);

#ifndef IMM_ONLY

/**
* @brief Function used to perform enroll
*
*
* @param[in] pConfig is a pointer includes needed informations.
*
* @return IDX_ISO_SUCCESS if successful,
*         or another error code if not successful
*/
uint16_t idxSeEmkEnroll(idx_SeEmkConfig_t *pConfig);

uint16_t idxSeEmkSleeveEnroll(idx_SeEmkConfig_t *pConfig);

uint16_t idxSeEmkSingleEnroll(idx_SeEmkConfig_t *pConfig,
                              uint8_t  touch,
                              uint8_t  finger);

/**
* @brief Function used to perform match
*
*
* @param[in] pConfig is a pointer includes needed informations.
*
* @return IDX_ISO_SUCCESS if successful,
*         IDX_ISO_ERR_NO_MATCH if no match
*/
uint16_t idxSeEmkMatch(idx_SeEmkConfig_t *pConfig,
                       uint8_t  imgsrc,
                       uint16_t mwfTimeBudget);

/**
* @brief Function used to delete enrollment of verification templates from the flash
*
*
* @param[in] pConfig is a pointer includes needed informations.
* @param[in] finger to remove from flash
* @param[in] mode indicates the delete mode (enrollment or verification template)
*
* @return IDX_ISO_SUCCESS if successful,
*         IDX_ISO_ERR_NOT_FOUND if no record found
*/
uint16_t idxSeEmkDeleteFinger(idx_SeEmkConfig_t *pConfig,
                              uint8_t finger,
                              int8_t mode);
#else

uint16_t idxSeEmkMcuEnroll(void);
uint16_t idxSeEmkImmMatch(void);
uint16_t idxSeEmkImmEnroll(void);
uint16_t idxSeEmkImmSleeveEnroll(void);

#endif

                               
#endif //__IDX_SE_EMK_API_H__
