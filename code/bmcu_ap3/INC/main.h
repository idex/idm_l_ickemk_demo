

#ifndef __MAIN_H__
#define __MAIN_H__


#include "idxBaseTypes.h"
#include "idxBaseDefines.h"
#include "idxSensorDefines.h"
#include "idxHeapAPI.h"
#include "idxSensorDriverAPI.h"
#include "idxIck.h"
#include "Compatibility.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "am_mcu_apollo.h"
#include "am_bsp.h"
#include "am_util.h"


#include "idxEmkExBaseDefines.h"
#include "idxEmkExAPI.h"
#include "idxIsoErrors.h"


#define AM_HAL_PWRCTRL_MAX_WFE  20
#define LONG_TIMEOUT 10000


#define ETIME 62

#define CS_pin				13
#define READY_pin			11
#define SCK_pin				5
#define MISO_pin			6
#define MOSI_pin			7
#define RESET_pin			12




#define SE_SPI_CS              3
#define SE_SPI_MISO            2
#define SE_SPI_MOSI            1
#define SE_SPI_SCK             0

#define Vol_detect_Pin 16


#define LEDG_Pin 17
#define LEDR_Pin 19

#define EPD_BUSY_PIN 25//support pull_up
#define EPD_NRES_PIN 28
#define EPD_NDC_PIN 27
#define EPD_NCS_PIN 29

#define EPD_NSCK_PIN 5
#define EPD_NSDA_PIN 7


#define PIC_NONE		0
#define PIC_WHITE		20	
#define PIC_BLACK		21
#define PIC_Source_Line     	22  		// Source?
#define PIC_Gate_Line      	23  		// Gate?
#define PIC_Dot     		24  		// ??????
#define PIC_Name     		25  		// ???

#define image_bias   120



#define EPD_CS_H am_hal_gpio_output_set(EPD_NCS_PIN);
#define EPD_CS_L am_hal_gpio_output_clear(EPD_NCS_PIN);
#define EPD_RST_H am_hal_gpio_output_set(EPD_NRES_PIN);
#define EPD_RST_L am_hal_gpio_output_clear(EPD_NRES_PIN);
#define EPD_DC_H am_hal_gpio_output_set(EPD_NDC_PIN);
#define EPD_DC_L am_hal_gpio_output_clear(EPD_NDC_PIN);

#define EPD_SCK_H am_hal_gpio_output_set(EPD_NSCK_PIN);
#define EPD_SCK_L am_hal_gpio_output_clear(EPD_NSCK_PIN);

#define EPD_SI_H am_hal_gpio_output_set(EPD_NSDA_PIN);
#define EPD_SI_L am_hal_gpio_output_clear(EPD_NSDA_PIN);

#define LED_RED_ON am_hal_gpio_output_set(LEDR_Pin);
#define LED_GREEN_ON am_hal_gpio_output_set(LEDG_Pin);
#define LED_RED_OFF am_hal_gpio_output_clear(LEDR_Pin);
#define LED_GREEN_OFF am_hal_gpio_output_clear(LEDG_Pin);



#define EALREADY		120
#define BMCU_NUM_FINGERS_TO_ENROLL  (2)
#define BMCU_MAX_TEMPLATES_IN_MCU (16)

#define BMCU_NUM_ENROLL_IMAGES_PER_FINGER  (8)
#define SE_NUM_FINGERS_TO_ENROLL  (2)
#define SE_NUM_ENROLL_IMAGES_PER_FINGER  (8)



#define MAX_SHARED_MEM_ALLOCATION (43612)
#define MAX_DEDICATED_MEM_ALLOCATION (5376)
#define MEM_ALIGNMENT_MASK 0xfffffffc
#define MEM_ALIGNMENT_UNIT 4


#define INDEX_TABLE_ADDR 0x00040000//256k
#define INDEX_TABLE_PAGE 32  //32*8=256k

#define TEMPLATE_START_ADDRESS 0x00042000
#define TEMPLATE_START_PAGE 33

#define WAKE_INTERVAL           XT_PERIOD * WAKE_INTERVAL_IN_MS * 1e-3
#define WAKE_INTERVAL_IN_MS     1000
#define XT_PERIOD               32768


#define IDX_COMMAND_HEADER_SIZE 6

#define IDX_BIOMETRICS_TOTAL_PAGE 13
#define FLASH_MAX_ENTRIES           12
#define FLASH_RESET_OFFSET_VALUE    0xFFFFFFFF
#define IDX_ERR_REC_NOT_FOUND  0x6A83   // Record not found
#define GET_TEMPLATE_ENROLL_PHASE	   0x80


#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t



typedef struct PACKED_ATTRIBUTE __idx_ReadData
{
	unsigned short offset ;
	unsigned short size ;
} idx_ReadData ;


typedef struct _extended_heap_entry
{
	uint32_t offset;
	uint32_t size;
	uint8_t handle;

} idx_FlashEntry;

#define IDX_FLASH_SIZE                  (FLASH_MAX_ENTRIES * (8192 + sizeof(idx_FlashEntry) + 0xFF))


#define handshake_ready_reg 0x75   //    1 byte, write 0x06 start handshake, change to 0x09, succeed
#define mcu_ready_reg	0x74  //    1byte, write 0x06 start check ready, change to 0x09, ready
#define se_reply_ack_reg	0x72  //  2 byte, example 06,00, 
#define mcu_reply_ack_reg 0x71 // 1byte, example 0x06
#define wtx_power_reg	0x6f  // 2 byte, write 0x06,0x00, read 0x06,0x06,enter wtx, write 0x09,0x00,read 0x09,0x09,quit wtx
#define long_data_receive_readycheck_reg 0x6e //  1 byte, write 0x06 start check, change to 0x09, ready
#define long_data_send_readycheck_reg 0x6d //  1 byte, write 0x06 start check, change to 0x09, ready


typedef struct PACKED_ATTRIBUTE __idx_StoreBlobData
{
	unsigned char type;
	unsigned int size;

} idx_StoreBlobData;


typedef struct {

uint32_t index;
uint32_t template_size;
uint32_t template_addr;

}BMCU_index_table;

typedef struct {

uint32_t index;
uint32_t template_size;
uint32_t template_addr;

}SE_index_table;

typedef enum
{
	ENROLL_NEXT=0,
	ENROLL_LAST,
	MATCH
} extract_option;


typedef enum
{
	BMCU_INITIAL=0,
	BMCU_GET_VERSION,
	BMCU_EM_INITIAL,
	BMCU_EM_UNINITIAL,
	BMCU_ENROLL_NEXT,
	BMCU_ENROLL_LAST,
	BMCU_MAKE_TEMPLATE_FOR_MATCH,
	BMCU_MATCH_AND_GET_UPDTEMPLATE,
	BMCU_READ_TEMPLATE,
	BMCU_READ_UPDATE_TEMPLATE,
	BMCU_LIST_RECORD,
	BMCU_DELETE_RECORD
} SE_CMD_TYPE;




typedef enum
{
  GPIO_PIN_RESET = 0,
  GPIO_PIN_SET
}GPIO_PinState;

typedef enum
{
	MATCHING = 0,     //0 = MATCHING
	ENROLLMENT				// 1 = ENROLLMENT 
}ENROLL_MATCH_OPTION;

typedef struct _idx_load_image_object
{
	uint8_t flag;
	uint16_t offset;
}idx_load_image_object;



typedef struct _idx_State
{
	void *pEmkHandle;
	idx_basic_ImageInfo_t imageInfo;
	idx_basic_ImageInfo_t sensorInfo;
	uint8_t *pSavedData;
	uint16_t savedDataSize;
	uint8_t *pCommData;
	uint8_t *pCommHeader;
	uint16_t commDataSize;
	uint8_t mcuState;
	uint8_t connection_mode;
	uint8_t current_command;
	idx_load_image_object pLoadImage_ob;
	uint32_t pFlashStartAddress;
	uint32_t pTemplateStartAddress;
} idx_State;



typedef struct
{
    bool        bValid;
    uint32_t    regFIFOCFG;
    uint32_t    regFIFOTHR;
    uint32_t    regCFG;
    uint32_t    regINTEN;
    uint32_t    regACCINTEN;
} am_hal_ios_register_state_t;


typedef struct
{
    am_hal_handle_prefix_t  prefix;
    //
    // Physical module number.
    //
    uint32_t                ui32Module;

    am_hal_ios_register_state_t registerState;

    uint8_t *pui8FIFOBase;
    uint8_t *pui8FIFOEnd;
    uint8_t *pui8FIFOPtr;
    uint8_t ui32HwFifoSize;
    uint32_t ui32FifoBaseOffset;
} am_hal_ios_state_t;


#define IDX_HOSTIF_CMD_BIOMETRIC	0x00
#define IDX_HOSTIF_CMD_DATABASE		0x10
#define IDX_HOSTIF_CMD_ENCRYPTION	0x20
#define IDX_HOSTIF_CMD_UPDATE		0x30
#define IDX_HOSTIF_CMD_EAGLE		0x40
#define IDX_HOSTIF_CMD_MANAGEMENT	0x50
#define IDX_HOSTIF_CMD_DEBUG		0x60

//////////////////////////////////////////////////////
//
// sensor commands
//
enum
{
	IDX_HOSTIF_CMD_INITIALIZE = IDX_HOSTIF_CMD_BIOMETRIC,
	IDX_HOSTIF_CMD_UNINITIALIZE,
	IDX_HOSTIF_CMD_PREPAREIMAGE,
	IDX_HOSTIF_CMD_ACQUIRE = IDX_HOSTIF_CMD_PREPAREIMAGE,
	IDX_HOSTIF_CMD_ABORT,
	IDX_HOSTIF_CMD_GETIMAGE,
	IDX_HOSTIF_CMD_GETTEMPLATE,
	IDX_HOSTIF_CMD_MERGETEMPLATES,
	IDX_HOSTIF_CMD_MATCHTEMPLATES,
	IDX_HOSTIF_CMD_GETPARAMETER,
	IDX_HOSTIF_CMD_SETPARAMETER,
	IDX_HOSTIF_CMD_GETRAWDATA,						// GETBASELINE_CMD
	IDX_HOSTIF_CMD_RESTOREPARAM,
	IDX_HOSTIF_CMD_STOREPARAM,
	IDX_HOSTIF_CMD_TEST,
	IDX_HOSTIF_CMD_GETSENSORINFO,
	IDX_HOSTIF_CMD_BIOMETRICSCRIPT
} ;

//////////////////////////////////////////////////////
//
// storage commands
//
enum
{
	IDX_HOSTIF_CMD_STORERECORD = IDX_HOSTIF_CMD_DATABASE,
	IDX_HOSTIF_CMD_DELETERECORD,
	IDX_HOSTIF_CMD_LISTRECORD,
	IDX_HOSTIF_CMD_GETRECORDDETAILS,
	IDX_HOSTIF_CMD_SAVEDB,
	IDX_HOSTIF_CMD_RESTOREDB,
	IDX_HOSTIF_CMD_GETRECORDTEMPLATE,
	IDX_HOSTIF_CMD_ENROLL,
	IDX_HOSTIF_CMD_STORE_BLOB
} ;

//////////////////////////////////////////////////////
//
// encryption commands
//
enum
{
	IDX_HOSTIF_CMD_CREATEKEYS = IDX_HOSTIF_CMD_ENCRYPTION,
	IDX_HOSTIF_CMD_RESET,
	IDX_HOSTIF_CMD_OPENCHANNEL,
	IDX_HOSTIF_CMD_CLOSECHANNEL,
	IDX_HOSTIF_CMD_GETUID,
	IDX_HOSTIF_CMD_INITIALIZE_UPDATE,
	IDX_HOSTIF_CMD_EXTERNAL_AUTHENTICATE,
	IDX_HOSTIF_CMD_PUTKEY,
	IDX_HOSTIF_CMD_FLASHUPDATE,
	IDX_HOSTIF_CMD_GET_RANDOM,
	IDX_HOSTIF_CMD_LOCK,
	IDX_HOSTIF_CMD_AESENC,
	IDX_HOSTIF_CMD_AESDEC
} ;

//////////////////////////////////////////////////////
//
// update commands
//
enum
{
	IDX_HOSTIF_CMD_GETVERSION = IDX_HOSTIF_CMD_UPDATE,
	IDX_HOSTIF_CMD_UPDATESTART,
	IDX_HOSTIF_CMD_UPDATEDETAILS,
	IDX_HOSTIF_CMD_UPDATEDATA,
	IDX_HOSTIF_CMD_UPDATEHASH,
	IDX_HOSTIF_CMD_UPDATEEND,
	IDX_HOSTIF_CMD_GETFWVERSION,
	IDX_HOSTIF_CMD_UPDATEBIST,
	IDX_HOSTIF_CMD_GETCOMPONENTVERSION
} ;

enum {
	UPDATE_FLAGS_ENCRYPTED = ( 1 << 0 ),
	UPDATE_FLAGS_MCU_FIRMWARE = ( 0 << 1 ),
	UPDATE_FLAGS_SENSOR_RAM_FIRMWARE = ( 1 << 1 ),
	UPDATE_FLAGS_SENSOR_OTP_FIRMWARE = ( 1 << 2 ),
	UPDATE_FLAGS_SENSOR_PRIMARY = ( 0 << 3 ),
	UPDATE_FLAGS_SENSOR_SSBL = ( 1 << 3 ),
};

//////////////////////////////////////////////////////
//
// low-level communication commands
//
enum
{
	IDX_HOSTIF_CMD_EAGLE_WRITE = IDX_HOSTIF_CMD_EAGLE,
	IDX_HOSTIF_CMD_EAGLE_READ,
	IDX_HOSTIF_CMD_EAGLE_RESET,
	IDX_HOSTIF_CMD_EAGLE_GETREADYPIN,
	IDX_HOSTIF_CMD_EAGLE_WAITFORREADYSTATE,
	IDX_HOSTIF_CMD_EAGLE_CALIBRATE,
	IDX_HOSTIF_CMD_EAGLE_TRANSFER,
	IDX_HOSTIF_CMD_EAGLE_PRODTEST = IDX_HOSTIF_CMD_EAGLE_TRANSFER,
	IDX_HOSTIF_CMD_SENSOR_COMMAND
} ;

enum
{
	IDX_HOSTIF_CMD_READ_DATA = IDX_HOSTIF_CMD_MANAGEMENT,
	IDX_HOSTIF_CMD_READ_FLASH,
	IDX_HOSTIF_CMD_LOAD_IMAGE,
	IDX_HOSTIF_CMD_LOAD_CPU,
	IDX_HOSTIF_CMD_RESET_CPU,
	IDX_HOSTIF_CMD_SENSOR_STACK_TEST = IDX_HOSTIF_CMD_LOAD_CPU,
	IDX_HOSTIF_CMD_EMK_TEST = IDX_HOSTIF_CMD_RESET_CPU,
	IDX_HOSTIF_CMD_BIODATAFLUSH, // from idx_biodataflush(const idx_CommandOptions managementCommands[], after idx_resetcpu) of "kestrel-sw\biomcu\firmware\idx-serial\idx-commands-eclipse.c"
	IDX_HOSTIF_CMD_IPL_TEST = IDX_HOSTIF_CMD_BIODATAFLUSH,
	IDX_HOSTIF_CMD_ICK_TEST,
	IDX_HOSTIF_CMD_COMMON_TEST,
} ;

enum
{
	IDX_HOSTIF_CMD_BRIDGE_SET_WTX = IDX_HOSTIF_CMD_DEBUG,
	IDX_HOSTIF_CMD_RMA_DEBUG_COMMAND,
	IDX_HOSTIF_CMD_BRIDGE_BMCU_COMM_CONFIG, 
	IDX_HOSTIF_CMD_EINK_SHOW,
};

enum
{
	BIOMETRIC_UNINITIALIZED=0,
	BIOMETRIC_IDLE,
	BIOMETRIC_IMAGE,
	BIOMETRIC_TEMPLATE,
	BIOMETRIC_MATCH,
	BIOMETRIC_REJECTED,
};

enum
{
	BIOMETRIC_MODE_CONTACTLESS=0,
	BIOMETRIC_MODE_CONTACT
};





#define AM_HAL_IOS_MAX_SW_FIFO_SIZE 1023
#define AM_HAL_MAGIC_IOS            0x123456
#define AM_HAL_IOS_CHK_HANDLE(h)    ((h) && ((am_hal_handle_prefix_t *)(h))->s.bInit && (((am_hal_handle_prefix_t *)(h))->s.magic == AM_HAL_MAGIC_IOS))
#define AM_HAL_IOS_INT_ERR  (AM_HAL_IOS_INT_FOVFL | AM_HAL_IOS_INT_FUNDFL | AM_HAL_IOS_INT_FRDERR)

#define AM_HAL_IOS_XCMP_INT (AM_HAL_IOS_INT_XCMPWR | AM_HAL_IOS_INT_XCMPWF | AM_HAL_IOS_INT_XCMPRR | AM_HAL_IOS_INT_XCMPRF)



#define SET_HANDSHAKE_READY 	am_hal_ios_pui8LRAM[handshake_ready_reg]=0x09;//claim MCU handshake ready
#define SET_MCU_READY 	am_hal_ios_pui8LRAM[mcu_ready_reg]=0x09;//claim MCU handshake ready
#define SET_LONG_SE_SEND_READY 	am_hal_ios_pui8LRAM[long_data_receive_readycheck_reg]=0x09;//claim MCU handshake ready
#define SET_LONG_SE_RECEIVE_READY 	am_hal_ios_pui8LRAM[long_data_send_readycheck_reg]=0x09;//claim MCU handshake ready


#endif
