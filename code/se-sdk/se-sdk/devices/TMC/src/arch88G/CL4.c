/*
 * * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: cl4.c
 * * SCFID: 
 * * Feature: 14443-4 chaining process of PICC
 * * Version: V0.1
 * *
 * * History: 
 * *   2012-01-06 by Wangql
 * *     1. Original version 0.1
 * */

#include "halglobal.h"
#include "RF.h"
#include "CL4.h"

//by PCB,14443-4 can obtain the frame information of I,R,S
#define CL_I_ID		0X00
#define CL_S_ID		0XC0
#define CL_R_ID		0X80
 
extern RF_SendData(u8 len);
extern RF_Deselect(void);
extern void SetNeedCID( u8 *pbNeedCID );
stCL_4 gCL;




// set max frame size
void CL4_SetMaxFrameSize(u16 usMaxFrameSize)
{
	gCL.usMaxFrameSize = usMaxFrameSize;
}
void CL4_SetCID(u8 bcid)
{
	gCL.bCID = bcid;
}

void CL4_Init(u8 *pbCL4Buf, u8 *pbRFBuf)
{
	gCL.CurBlockNumber = 1;
	gCL.bCL4_State = CL4_IDLE;
	gCL.bNeedCID = 0x08;
					    
	gCL.pbCL4_FrameBuffer = pbCL4Buf;
	gCL.wCL4_FrameLen = 0;
	
	gCL.pbCL4_WinTop = gCL.pbCL4_FrameBuffer;
	gCL.pbCL4_WinBottom = gCL.pbCL4_FrameBuffer;
	gCL.usMaxFrameSize = 256;	//

	gCL.pbRFBlockBuffer = pbRFBuf;//GetHalRFBufPointer();
}

void CL4_Reset(void)
{
	gCL.CurBlockNumber = 1;
	gCL.bCL4_State = CL4_IDLE;
	gCL.wCL4_FrameLen = 0;
	gCL.pbCL4_WinTop = gCL.pbCL4_FrameBuffer;
	gCL.pbCL4_WinBottom = gCL.pbCL4_FrameBuffer;
}
/*
* Function    : CL4_SendData 
* Description : send data this function is to send out the content in the sending buffer  
* input :
* 	wSendLength : the data length this time send out 
* 	wRemainedLen: the data length next time send out
* output : -
* return : -
*/
void CL4_SendData(u16 wSendLength, u16 wRemainedLen)
{
	u8 HaveCidOrNot;
	
	gCL.RemainedLen = wRemainedLen;

	gCL.wCL4_FrameLen = wSendLength;
	gCL.pbCL4_WinTop = gCL.pbCL4_FrameBuffer;
	gCL.pbCL4_WinBottom = gCL.pbCL4_FrameBuffer;
	*(gCL.pbRFBlockBuffer + 1) = gCL.bCID;
	gCL.bCL4_State = CL4_TX_ING;	//get into transfer state

	if ( gCL.bNeedCID != 0)
		HaveCidOrNot = 4;
	else 
		HaveCidOrNot = 3;

	if (wSendLength > (gCL.usMaxFrameSize - HaveCidOrNot))		//whether need to construct chaining. must to reserve two bytes for PCB and CID
	{
		//construct chaining
		*(gCL.pbRFBlockBuffer) = 0X12 | gCL.CurBlockNumber | gCL.bNeedCID;		//PCB
		gCL.bPCB = *(gCL.pbRFBlockBuffer);

		_HalRamCopy((gCL.pbRFBlockBuffer + HaveCidOrNot - 2), gCL.pbCL4_WinBottom, gCL.usMaxFrameSize - HaveCidOrNot);
		gCL.RemainedLen = wSendLength - (gCL.usMaxFrameSize - HaveCidOrNot) ;
		gCL.pbCL4_WinTop += (gCL.usMaxFrameSize - HaveCidOrNot);

	    RF_SendData(gCL.usMaxFrameSize-2);	//no CRC, so need to minus 2 bytes
	}
	else
	{
		if (gCL.RemainedLen == 0)
		{
			//no need to construct chaining
			*(gCL.pbRFBlockBuffer) = (0X02 | gCL.CurBlockNumber | gCL.bNeedCID);		//PCB
			
		}
		else
			//need to construct chaining
			*(gCL.pbRFBlockBuffer) = (0X12 | gCL.CurBlockNumber | gCL.bNeedCID);		//PCB
		
		gCL.bPCB = *(gCL.pbRFBlockBuffer);

		_HalRamCopy((gCL.pbRFBlockBuffer + HaveCidOrNot - 2), gCL.pbCL4_WinBottom, wSendLength);
		RF_SendData(wSendLength + HaveCidOrNot - 2);	//finished sending out all at one time but crc
		gCL.bCL4_State = CL4_TX_FIN;

		gCL.pbCL4_WinTop += wSendLength;
		
	}
	
	return;
}

/*
* Function    : CL4_ContinueSend 
* Description : obatin data of next section to be sent
* input : -
* output : -
* return : -
*/
void CL4_ContinueSend()
{
	u16 LeavingLength;
	u16 wSendLen;
	u8 	HaveCidOrNot;

	if (gCL.bCL4_State == CL4_TX_FIN)
	{
		if (gCL.RemainedLen != 0)
		{
			//gCL.RemainedLen = GetNextBlock(gCL.pbCL4_FrameBuffer, gCL.RemainedLen,0x0);
			gCL.wCL4_FrameLen = gCL.RemainedLen;	   

			gCL.pbCL4_WinTop = gCL.pbCL4_FrameBuffer;
			gCL.pbCL4_WinBottom = gCL.pbCL4_FrameBuffer;
			*(gCL.pbRFBlockBuffer + 1) = gCL.bCID;
			gCL.bCL4_State = CL4_TX_ING; //get into transfer state
		}
	}
	if (gCL.bCL4_State != CL4_TX_ING)  //must be in transfer state, it can transfer
	{
		RF_SendData(0);
		return;
	}
	//whether finishing sending out or not? yes: and need another frame,the card must be wrong. no: do not deal this condition
	if (gCL.pbCL4_WinTop != gCL.pbCL4_FrameBuffer + gCL.wCL4_FrameLen)  
	{
		gCL.pbCL4_WinBottom = gCL.pbCL4_WinTop; //adjust the bottom of the window

		*(gCL.pbRFBlockBuffer + 1) = gCL.bCID;
	
		LeavingLength = gCL.RemainedLen; //remaining length
		

		if ( gCL.bNeedCID != 0)
			HaveCidOrNot = 4;
		else 
			HaveCidOrNot = 3;
		
		if (LeavingLength > (gCL.usMaxFrameSize - HaveCidOrNot))  //whether to construct chaining
		{
			gCL.CurBlockNumber ^= 0X01;//block number turn over
			//need to construct chaining
			*(gCL.pbRFBlockBuffer) = (0X12 | gCL.CurBlockNumber | gCL.bNeedCID);  //PCB
			gCL.bPCB = *(gCL.pbRFBlockBuffer);

			_HalRamCopy(gCL.pbRFBlockBuffer + HaveCidOrNot - 2, gCL.pbCL4_WinBottom, gCL.usMaxFrameSize - HaveCidOrNot);
			
			gCL.RemainedLen -= gCL.usMaxFrameSize-HaveCidOrNot;
			wSendLen = gCL.usMaxFrameSize-2;
										  
			RF_SendData(wSendLen);
					
			gCL.pbCL4_WinTop += gCL.usMaxFrameSize - HaveCidOrNot;	 
			if(gCL.RemainedLen == 0 )  //the last frame
			{
				gCL.bCL4_State = CL4_TX_FIN;
			}
			
	
		}
		else  //the last frame this time
		{
			gCL.CurBlockNumber ^= 0X01; //block number turn over

			//no need to consturct chaining, this is the last block which the size is not bigger than gCL.usMaxFrameSize - 4
			*(gCL.pbRFBlockBuffer) = (0X02 | gCL.CurBlockNumber | gCL.bNeedCID);  //PCB
			
			gCL.bPCB = *(gCL.pbRFBlockBuffer);

			_HalRamCopy(gCL.pbRFBlockBuffer + HaveCidOrNot-2, gCL.pbCL4_WinBottom, LeavingLength);
		 	RF_SendData(LeavingLength + HaveCidOrNot-2); //finfish send one time
		
			gCL.pbCL4_WinTop += LeavingLength;
			gCL.bCL4_State = CL4_TX_FIN;   //data transfering is finished
		}
	}
}

/*
* Function    : CL4_SendACK 
* Description : send ACK
* inout  : -
* output : -
* return : - 
*/
void CL4_SendACK()
{
	if(gCL.bNeedCID != 0)  //need CID
	{
		gCL.pbRFBlockBuffer[0] = 0xaa | gCL.CurBlockNumber;
		gCL.pbRFBlockBuffer[1] = gCL.bCID;
		RF_SendData(2);
	}
	else
	{
		gCL.pbRFBlockBuffer[0] = 0xa2 | gCL.CurBlockNumber;
		RF_SendData(1);
	}
}

/*
* Function    : CL4_SendRepeat 
* Description : resend the last frame
* Input  : -
* Output : -
* Return : -
*/
void CL4_SendRepeat()
{
	*(gCL.pbRFBlockBuffer) = gCL.bPCB;		//PCB
	if((gCL.bPCB & 0x08) != 0)  //need CID
	{
		*(gCL.pbRFBlockBuffer + 1) = gCL.bCID;
		_HalRamCopy(gCL.pbRFBlockBuffer + 2, gCL.pbCL4_WinBottom, gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom);
		RF_SendData(gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom + 2);
	}
	else
	{
		_HalRamCopy(gCL.pbRFBlockBuffer + 1, gCL.pbCL4_WinBottom, gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom);
		RF_SendData(gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom + 1);
	}
}
void CL4_SendEmptyResponse(void)
{
	gCL.pbCL4_WinBottom = gCL.pbCL4_FrameBuffer;
	gCL.pbCL4_WinTop = gCL.pbCL4_FrameBuffer+2;
	*gCL.pbCL4_FrameBuffer = 0x67;
	*(gCL.pbCL4_FrameBuffer+1) = 0x00;
	gCL.bCL4_State = CL4_TX_FIN;
	gCL.bPCB = *(gCL.pbRFBlockBuffer);
	if(gCL.bNeedCID != 0)  //need CID
	{
		RF_BUF[2] = 0x67;
		RF_BUF[3] = 0x00;
		RF_SendData(4);
	}
	else
	{
		RF_BUF[1] = 0x67;
		RF_BUF[2] = 0x00;	
		RF_SendData(3);
	}
}

/*
* Function    : CL4_RcvData 
* Description : receive and analyze data in active state
* Input  : -
* Output : -
* Return : -
*/
CL_RCV_STATE CL4_RcvData(void)
{
	CL_RCV_STATE statereturn;
	// if receive length is bigger than FSC, mute.  
	if((g_stCurRFEnv.RF_DataLen + 2) > framesize[g_pbComCfg[CL3A_ATS_PROTOCOLINFO + 1]&0x0f])
	{
		return	RCV_NOPROCESING; 
	}	
	//the data that receiveed 
	if(*(gCL.pbRFBlockBuffer)&0x04)//NAD
	{
		return	RCV_NOPROCESING;
	}
	if (gCL.bCID == 0)
    {
	   gCL.bNeedCID = (*gCL.pbRFBlockBuffer) & 0x08;
    }
	else
    {
	   gCL.bNeedCID = 0x08;
    }
	   
	if (gCL.bNeedCID != 0)
	{
	   if( *(gCL.pbRFBlockBuffer+1) !=  gCL.bCID)
	   {
			RFRSCL = 0;
			RFRSCH = 0;
			return	RCV_NOPROCESING;
		}
	}
	SetNeedCID(&gCL.bNeedCID);
	switch ((*gCL.pbRFBlockBuffer) & 0xc0)
	{
	   case CL_I_ID:
   		
	   	// check PCD blocknum
	   	if ((gCL.CurBlockNumber & 0x01) ==  ((*gCL.pbRFBlockBuffer) & 0x01))
		{
			return RCV_NOPROCESING;
		}
	   	if (gCL.bCL4_State != CL4_RX_ING)
	   	{
	   		gCL.bCL4_State = CL4_RX_ING;
	   		gCL.pbCL4_WinTop = gCL.pbCL4_FrameBuffer;
	   		gCL.pbCL4_WinBottom= gCL.pbCL4_FrameBuffer;
			
            //clear buffer
            //if not do this, when 4 bytes are sended in Case1 commend, 5 bytes are copied, remained Lc will be copied
            _HalRamSet(gCL.pbCL4_FrameBuffer, 0x00, APDU_BUFFER_SIZE);
	   	}
	   	if(gCL.bNeedCID != 0) //CID?
	   	{
				// the following code is used to judge whether the data length received is bigger than APDU_BUFFER_SIZE��
				// if the length is bigger than APDU_BUFFER_SIZE, the data outof the size will not be reveived
				u16 nextLen = APDU_BUFFER_SIZE - (gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom);
				nextLen = nextLen > g_stCurRFEnv.RF_DataLen-2 ? g_stCurRFEnv.RF_DataLen-2 : nextLen;
				_HalRamCopy(gCL.pbCL4_WinTop, gCL.pbRFBlockBuffer + 2, nextLen);
				gCL.pbCL4_WinTop += nextLen;
	   	}
	   	else
	   	{
				//  the following code is used to judge whether the data length received is bigger than APDU_BUFFER_SIZE��
				//  if the length is bigger than APDU_BUFFER_SIZE, the data outof the size will not be reveived
				u16 nextLen = APDU_BUFFER_SIZE - (gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom);
				nextLen = nextLen > g_stCurRFEnv.RF_DataLen-1 ? g_stCurRFEnv.RF_DataLen-1 : nextLen;
				_HalRamCopy(gCL.pbCL4_WinTop, gCL.pbRFBlockBuffer + 1, nextLen);
				gCL.pbCL4_WinTop += nextLen;
	   	}
	   	
	   	gCL.CurBlockNumber ^= 0x01;//block number turn over
	   	if ((*gCL.pbRFBlockBuffer) & 0x10) //chaining
	   	{
	   		statereturn = RCV_SEND_ACK;
	   	}
	   	else
	   	{
	   		gCL.wCL4_FrameLen = gCL.pbCL4_WinTop - gCL.pbCL4_WinBottom; 
	   		gCL.bCL4_State = CL4_RX_FIN;
			if(gCL.wCL4_FrameLen)
			{
	   			statereturn = RCV_RCV1FRAME;//no chaining	
			}
			else
			{
	   			statereturn = RCV_RESPONSECMD;//no chaining	
			}
	   	}
	   	break;
	   case CL_R_ID:
	   	if ( 0xB0 == ((*gCL.pbRFBlockBuffer) & 0xF4) )
	   	{
			//NAK, if block number the same, we must see whether it is in a receive condition,
			//if in receive condition then send ACK,ro else resend the last frame
	   		if(((*gCL.pbRFBlockBuffer) & 0X01) == gCL.CurBlockNumber)
	   		{
				if ((gCL.bCL4_State == CL4_TX_ING) || (gCL.bCL4_State == CL4_TX_FIN))
	   			{
	   				statereturn = RCV_REPEAT;		//block number the same and in send state,resend the last frame
						break;
	   			}
	   		}
	   		statereturn = RCV_SEND_ACK;	// block is not the same,send ACK
	   	}
	   	
	   	else if ( 0xa0 == ((*gCL.pbRFBlockBuffer) & 0xF4) )	 //
	   	{
	   		//ACK  
	   		if (gCL.bCL4_State != CL4_RX_ING)//non contact,we compare blocknum or resend or send next frame
				{
	   			if(((*gCL.pbRFBlockBuffer) & 0X01) == gCL.CurBlockNumber)
	   			{
	   				statereturn = RCV_REPEAT;	//block the same, resend the last frame
						break;
	   			}
	   			else
	   				statereturn = RCV_SEND_NEXTBLCOK;		//block number not the same,send the next block
					}
	   		else
	   			statereturn = RCV_NOPROCESING;	//receive state,ignore the ACK

	   	}
		else  //not the R frame of ACK/NAK,no response
		{
			statereturn = RCV_NOPROCESING;
		}
	   	break;
	   case CL_S_ID:
	   	
	   	if(((*gCL.pbRFBlockBuffer) & 0xf0)  == 0xc0)	//DESELCET
	   	{
	   		statereturn = RCV_DESELECT;
	   	}
	   	else 
	   		statereturn = RCV_NOPROCESING;
	   	break;
	   default:
	   	statereturn = RCV_NOPROCESING;
	   	break;
	   }
	
	return statereturn;
}

/*
* Function    : CL4_Receive 
* Description : circle detection receive RF data
* Input  : -
* Output : -
* Return : -
*/
u8 CL4_Receive(void)
{
	CL_RCV_STATE receivestate;
	
	receivestate = CL4_RcvData();	//RF receive data
	
		//receive analize then determine the next action
		switch(receivestate)
		{
			case RCV_NOPROCESING:
				RF_SendData(0);
				break;
			case RCV_SEND_NEXTBLCOK:
				CL4_ContinueSend();
				break;
			case RCV_SEND_ACK:
				CL4_SendACK();
				break;
			case RCV_RCV1FRAME:
				return 1;
			case RCV_REPEAT:
				CL4_SendRepeat();
				break;
			case RCV_DESELECT:
				RF_Deselect();
				RF_SendData(0);
				break;
			case RCV_RESPONSECMD://do nothing
				CL4_SendEmptyResponse();
				break;
			default:
				RF_SendData(0);
				break;
		}
	return 0;
}

