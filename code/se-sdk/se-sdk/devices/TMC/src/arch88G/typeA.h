/*
 * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: typeA.h
 * SCFID: 
 * Feature: 14443-3 typeA state machine of PICC
 * Version: V0.1
 *
 * History: 
 *   2011-12-31 by Luqian
 *     1. Original version 0.1
 */
#ifndef TYPEA_H
#define TYPEA_H

//function
u8 TypeAInit(void);	   
u8 ActiveA(void);	
u8 ProtocolA(void);		
u8 RATS(void);		   
u8 PPSA(void);	
u8 HLTA(void);	   

//TYPEA FUNCTABLE
typedef struct {
	u8 (*pfTypeAInit) (void);
	u8 (*pfActiveA) (void);
	u8 (*pfProtocolA) (void);
	u8 (*_pfRATS) (void);
	u8 (*_pfPPSA) (void);
	u8 (*_pfHLTA) (void);
}ST_CL3ACMDTABLE;

#endif
