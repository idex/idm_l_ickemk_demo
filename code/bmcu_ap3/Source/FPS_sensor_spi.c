
#include "main.h"

//extern uint32_t log_timer_data[1000];
//extern uint16_t log_timer_counter;
//extern uint32_t log_data_in[1000];
//extern uint16_t log_data_in_counter;
//extern uint32_t log_data_out[1000];
//extern uint16_t log_data_out_counter;
extern uint8_t wtx_sensor_process_flag;



extern void wtx_stage_one_callback(void);
extern void wtx_stage_two_callback(void);
extern void Sleep_with_wtx( uint32_t ms);



int appolo3_spi_reset( void *p, int do_warm_reset )
{

	am_hal_gpio_output_clear(RESET_pin);
	if ( do_warm_reset )
	{uSleep( 2 );}
	else
	{Sleep( 2 );}
	Sleep( 2 );
	am_hal_gpio_output_set(RESET_pin);
	Sleep( 5 );
	return 0;
}


void* appolo3_spi_open( void* arg )
{
	void* ret = (void *)1; // 0 is considered bad
	
	return ret;
}
const am_hal_gpio_pincfg_t g_AM_HAL_GPIO_INPUT_pullup_24k =
{
	3,//.uFuncSel		=
	0,//	ePowerSw
	AM_HAL_GPIO_PIN_PULLUP_24K,//	ePullup
	AM_HAL_GPIO_PIN_DRIVESTRENGTH_2MA,//.eDriveStrength = 
	AM_HAL_GPIO_PIN_OUTCFG_DISABLE,//.eGPOutcfg	 =
	AM_HAL_GPIO_PIN_INPUT_ENABLE,// eGPInput
	0,// eIntDir
	AM_HAL_GPIO_PIN_RDZERO_READPIN,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22

};


const am_hal_gpio_pincfg_t g_AM_HAL_GPIO_INPUT_pullup_weak =
{
	3,//.uFuncSel		=
	0,//	ePowerSw
	AM_HAL_GPIO_PIN_PULLUP_WEAK,//	ePullup
	0,//.eDriveStrength = 
	AM_HAL_GPIO_PIN_OUTCFG_DISABLE,//.eGPOutcfg  =
	AM_HAL_GPIO_PIN_INPUT_ENABLE,// eGPInput
	0,// eIntDir
	AM_HAL_GPIO_PIN_RDZERO_READPIN,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22

};




const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOM0_SCK =
{
	AM_HAL_PIN_5_M0SCK,//.uFuncSel		=
	0,//	ePowerSw
	0,//	ePullup
	AM_HAL_GPIO_PIN_DRIVESTRENGTH_12MA,//.eDriveStrength = 
	0,//.eGPOutcfg  =
	0,// eGPInput
	0,// eIntDir
	0,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22
};

const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOM0_MISO =
{
	AM_HAL_PIN_6_M0MISO,//.uFuncSel		=
	0,//	ePowerSw
	0,//	ePullup
	0,//.eDriveStrength = 
	0,//.eGPOutcfg  =
	0,// eGPInput
	0,// eIntDir
	0,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22
};
const am_hal_gpio_pincfg_t g_AM_BSP_GPIO_IOM0_MOSI =
{
	AM_HAL_PIN_7_M0MOSI,//.uFuncSel		=
	0,//	ePowerSw
	0,//	ePullup
	AM_HAL_GPIO_PIN_DRIVESTRENGTH_12MA,//.eDriveStrength = 
	0,//.eGPOutcfg  =
	0,// eGPInput
	0,// eIntDir
	0,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22
};


void appolo3_spi_init( void )
{

	//cs pin
	am_hal_gpio_pinconfig(CS_pin,g_AM_HAL_GPIO_OUTPUT);
	am_hal_gpio_output_set(CS_pin);
	//reset pin
	am_hal_gpio_pinconfig(RESET_pin,g_AM_HAL_GPIO_OUTPUT);
	am_hal_gpio_output_set(RESET_pin);


	//ready
	am_hal_gpio_pinconfig(READY_pin,g_AM_HAL_GPIO_INPUT_pullup_weak);
	
	//SCLK
	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_SCK,  g_AM_BSP_GPIO_IOM0_SCK);
	//MISO
	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_MISO, g_AM_BSP_GPIO_IOM0_MISO);
	//MOSI
	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_MOSI, g_AM_BSP_GPIO_IOM0_MOSI);
	//am_hal_mcuctrl_control(AM_HAL_MCUCTRL_CONTROL_FAULT_CAPTURE_ENABLE, 0);
	MCUCTRL->FAULTCAPTUREEN_b.FAULTCAPTUREEN = 1;
	PWRCTRL->DEVPWREN |=PWRCTRL_DEVPWREN_PWRIOM0_Msk;
	while(!(PWRCTRL->DEVPWREN&PWRCTRL_DEVPWREN_PWRIOM0_Msk));

	IOM0->CLKCFG 	= 0x00000401;//b=4Mhz,a=8Mhz,4=6mhz
	IOM0->SUBMODCTRL =IOM0_SUBMODCTRL_SMOD0EN_Msk;
	IOM0->MSPICFG	|= IOM0_MSPICFG_FULLDUP_Msk|IOM0_MSPICFG_SPHA_Msk|IOM0_MSPICFG_SPOL_Msk;

}


void appolo3_spi_close( void* p )
{

}


unsigned char spi_exchange_byte( unsigned char data)
{
return data;
}


int appolo3_spi_getreadypin( void *p, unsigned char *val )
{
	int ret = 0 ;

	*val=((GPIO->RDA & (1<<READY_pin))>0)? 1:0;
	return ret;
}



int appolo3_spi_waitforreadystate( void* p, int timeout_ms, int pin_state )
{
	unsigned char readvalue = ( unsigned char ) ~pin_state;
	uint8_t is_long_time_out;
	uint8_t i;
	
	timeout_ms = ( timeout_ms <= 0 ) ? 30 : timeout_ms;
	pin_state = pin_state != 0 ? 1 : 0;

	is_long_time_out=0;
	if(LONG_TIMEOUT<timeout_ms)
		{
		is_long_time_out=1;
		wtx_sensor_process_flag=0x01;
		}

	//log_timer_data[log_timer_counter]=timeout_ms;
	//log_timer_counter++;

	if(timeout_ms<200)
		{
		timeout_ms<<=4;
		}


	while( timeout_ms )
	{
		appolo3_spi_getreadypin( NULL, &readvalue );
		if(1==is_long_time_out)
			{
			for(i=0;i<10;i++)
				{
				wtx_stage_one_callback();
				uSleep(100);
				}
			}
		else
			{
			uSleep(60);
			}
		timeout_ms--;
		if( readvalue == pin_state )
		break;
	}
	//log_timer_data[log_timer_counter]=timeout_ms;
	//log_timer_counter++;
	wtx_sensor_process_flag=0;
	return ( readvalue == pin_state ) ? 0 : -ETIME;
}


void apollo3_spi_epd_send(uint8_t data, uint8_t dc)
{
	uint32_t temp_data;
	uint32_t watch_data;
if(0==dc)
{
	EPD_DC_L;

}
else
{
	EPD_DC_H;
}
	am_hal_gpio_output_clear(EPD_NCS_PIN);
	temp_data=data;
	
	IOM0->FIFOPUSH=(temp_data&0x000000ff);
	IOM0->INTCLR|=AM_HAL_IOM_INT_ALL;
	temp_data=(0x00000001+(1<<8));
	IOM0->CMD=temp_data;
	watch_data=IOM0->INTSTAT;
	while(0x00000000==(0x00000001&watch_data))
		{
		watch_data=IOM0->INTSTAT;
		}
	
	temp_data=IOM0->FIFOPOP;//read and clear fifo
	IOM0->FIFOCTRL&=~IOM0_FIFOCTRL_FIFORSTN_Msk;
	uSleep(5);
	IOM0->FIFOLOC=0;
	IOM0->FIFOCTRL|=IOM0_FIFOCTRL_FIFORSTN_Msk;
	am_hal_gpio_output_set(EPD_NCS_PIN);
}



void apollo3_spi_fast_send(uint8_t *data, uint8_t data_size)
{
	uint32_t temp_data;
	uint32_t watch_data;
	temp_data= *(uint32_t *)data;
	data_size&=0x03;
	IOM0->FIFOPUSH=temp_data;
	IOM0->INTCLR|=AM_HAL_IOM_INT_ALL;
	temp_data=(0x00000001+(data_size<<8));
	IOM0->CMD=temp_data;
	watch_data=IOM0->INTSTAT;
	while(0x00000000==(0x00000001&watch_data))
		{
		watch_data=IOM0->INTSTAT;
		}
	temp_data=IOM0->FIFOPOP;//read and clear fifo
	IOM0->FIFOCTRL&=~IOM0_FIFOCTRL_FIFORSTN_Msk;
	uSleep(5);
	IOM0->FIFOLOC=0;
	IOM0->FIFOCTRL|=IOM0_FIFOCTRL_FIFORSTN_Msk;

}


int appolo3_spi_transfer( void *p, unsigned char *buffer, int txSize, int rxSize )
{
	int ret = 0;
	int k;
	uint32_t *tp;
	uint32_t *point;
	uint32_t temp_data,temp_datab,temp_datad;
	uint32_t count;
	uint8_t input_buffer[516];

	uint16_t log_count_temp=txSize;
	if(rxSize>txSize)log_count_temp=rxSize;
	//memcpy(input_buffer,buffer,log_count_temp);

	k=0;
	point=(uint32_t *)&buffer[0];
	tp=(uint32_t *)&input_buffer[0];
	wtx_stage_two_callback();
	am_hal_gpio_output_clear(CS_pin);
	while(k<log_count_temp)
		{
		wtx_stage_two_callback();

		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		IOM0->FIFOPUSH=*point;
		point++;
		temp_datab=(32<=(txSize-k))? 32:(txSize-k);

		IOM0->INTCLR|=AM_HAL_IOM_INT_ALL;
		temp_data=(0x00000001+(temp_datab<<8));
		IOM0->CMD=temp_data;
		while(0x00000000==(0x00000001&IOM0->INTSTAT));
		count=0;
		while(count<temp_datab)
			{
			temp_datad=IOM0->FIFOPOP;
			*tp=temp_datad;
			tp++;
			count+=4;
			}
		k+=temp_datab;
		}
	IOM0->FIFOCTRL&=~IOM0_FIFOCTRL_FIFORSTN_Msk;
	uSleep(5);
	IOM0->FIFOLOC=0;
	IOM0->FIFOCTRL|=IOM0_FIFOCTRL_FIFORSTN_Msk;
	//printf("PTR=%x\r\n",IOM0->FIFOPTR);
	memcpy(buffer,input_buffer,rxSize);
	
	am_hal_gpio_output_set(CS_pin);
	wtx_stage_two_callback();
	return ret ;
}


/*

int appolo3_spi_transfer( void *p, unsigned char *buffer, int txSize, int rxSize )
{
	int ret = 0;
	int i,j,k;
	uint8_t *tp;
	uint32_t *point;
	uint32_t temp_data,temp_datab,temp_datac,temp_datad,temp_datae;
	uint32_t count;
	uint8_t input_buffer[512];

	uint16_t log_count_temp=txSize;
	if(rxSize>txSize)log_count_temp=rxSize;
	memset(input_buffer,0,log_count_temp);

	j=0;
	k=0;
	point=(uint32_t *)&buffer[0];
	wtx_stage_two_callback();
	am_hal_gpio_output_clear(CS_pin);
	while(k<txSize)
		{
		wtx_stage_two_callback();
		if(k<txSize)
			{
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			temp_datab=(32<=(txSize-k))? 32:(txSize-k);
			}
		IOM0->INTCLR|=AM_HAL_IOM_INT_ALL;
		temp_data=(0x00000001+(temp_datab<<8));
		IOM0->CMD=temp_data;
		while(0x00000000==(0x00000001&IOM0->INTSTAT));
		count=0;
		while(count<temp_datab)
			{
			temp_data=IOM0->FIFOPTR;
			temp_datac=((temp_datab-count)>=4)? 4:(temp_datab-count);
			if(((temp_data&0x00ff0000)>>16)>=temp_datac)
				{					
				temp_datad=IOM0->FIFOPOP;//read and clear fifo
				if(j<txSize)
					{
					tp=(uint8_t *)&temp_datad;
					temp_datae=(temp_datac<=(rxSize-j))? temp_datac:(rxSize-j);
					for(i=0;i<temp_datae;i++)
						{
						input_buffer[j++] = (*tp++);
						}
					}
				k+=temp_datac;
				count+=temp_datac;
				}
			}
		}
	IOM0->FIFOCTRL&=~IOM0_FIFOCTRL_FIFORSTN_Msk;
	uSleep(5);
	IOM0->FIFOLOC=0;
	IOM0->FIFOCTRL|=IOM0_FIFOCTRL_FIFORSTN_Msk;
	//printf("PTR=%x\r\n",IOM0->FIFOPTR);
	memcpy(buffer,input_buffer,txSize);
	
	am_hal_gpio_output_set(CS_pin);
	wtx_stage_two_callback();
	return ret ;
}


*/



