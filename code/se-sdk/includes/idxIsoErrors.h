 /** ***************************************************************************
 * \file   idxIsoErrors.h
 * \brief  Declaration of ISO7816-style error codes used by IDEX
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *
 * This file defines the various error and warning codes that may be returned
 * by the SDK that "look like" codes that may be returned (in SW1/SW2) by an
 * APDU conforming to ISO/IEC 7816-4. They are generally in the numerical
 * range 0x6000 to 0x6fff with the exception that success (the absence of an
 * error or warning) is indicated by the code 0x9000. In many cases the
 * value returned is the same as one of the codes defined by ISO/IEC 7816-4,
 * in which case the symbol is defined using the appropriate symbol defined
 * in \ref idxIsoIec7814-4.h.
 *
 * All symbols in this file are prefixed "IDX_ISO_ERR_". It should be noted
 * that some IDEX code, generally that which may run in a non-embedded
 * environment, uses similarly named symbols that are prefixed "IDX_ERR_"
 * which are in the numeric range 0x0000 (indicating success) to 0x3fff. See
 * \ref idxBaseErrors.h.
******************************************************************************/
#ifndef _IDX_ISO_ERRORS_H_
#define _IDX_ISO_ERRORS_H_

#include "idxIsoIec7816-4.h"

#define IDX_ISO_ERR_MATCHER_EXTRACT_INIT    0x6301                  //!< Error while initializing extractor
#define IDX_ISO_ERR_MATCHER_EXTRACT_PROCESS 0x6302                  //!< Error during extractor processing
#define IDX_ISO_ERR_NO_KEYS      0x6301                             //!< No keys have (ever) been installed
#define IDX_ISO_ERR_INCONSISTENT 0x6302                             //!< Returned data is inconsistent
#define IDX_ISO_ERR_NO_MATCH     (IDX_ISO_WARNING_NVM_CHANGED)      //!< Templates do not match
#define IDX_ISO_ERR_FLASH_ERASE  0x6501                             //!< Erase failed
#define IDX_ISO_ERR_FLASH_WRITE  0x6502                             //!< Write failed
#define IDX_ISO_ERR_NO_PADDING   0x6600                             //!< No padding on encrypted message
#define IDX_ISO_ERR_COMM         0x6741                             //!< Communication problem (EIO)
#define IDX_ISO_ERR_IT           0x6742                             //!< Interrupt problem (ETIME)
#define IDX_ISO_ERR_CALIB        0x6743                             //!< Wrong calibration data (EAGAIN)
#define IDX_ISO_ERR_TOO_SMALL    0x6745                             //!< Image too small
#define IDX_ISO_ERR_ABORTED      0x6746                             //!< Acquisition aborted
#define IDX_ISO_ERR_BAD_QUALITY  0x6747                             //!< Quality is too bad
#define IDX_ISO_ERR_TIMEOUT      0x6748                             //!< User timeout
#define IDX_ISO_ERR_VERSION      (IDX_ISO_ERROR_CLA_FUNC_NOT_SUPP)  //!< Version mismatch
#define IDX_ISO_ERR_UNKNOWN      0x6969                             //!< Unknown error
#define IDX_ISO_ERR_CONDITIONS   (IDX_ISO_CONDITIONS_NOT_SATISFIED) //!< Conditions of use not satisfied
#define IDX_ISO_ERR_ILLEGAL_CMD  (IDX_ISO_CMND_NOT_ALLOWED_NO_EF)   //!< Command not allowed
#define IDX_ISO_ERR_SNS_POWER    (IDX_ISO_SEC_MSG_EXPCT_DO_MISSING) //!< Not enough power for sensor
#define IDX_ISO_ERR_BIO_POWER    (IDX_ISO_INCORRECT_SEC_MSG_DO)     //!< Not enough power
#define IDX_ISO_ERR_BAD_PARAMS   (IDX_ISO_BAD_PARAM_CMD_DATA)       //!< Bad parameters
#define IDX_ISO_ERR_NOT_SUPPTD   (IDX_ISO_FUNCTION_NOT_SUPPORTED)   //!< Function not supported/implemented
#define IDX_ISO_ERR_NOT_FOUND    (IDX_ISO_RECORD_NOT_FOUND)         //!< Record not found
#define IDX_ISO_ERR_NO_SPACE     (IDX_ISO_NOT_ENOUGH_MEM_IN_FILE)   //!< Not enough space left on device
#define IDX_ISO_ERR_DATA_SIZE    (IDX_ISO_NC_INCONSISTENT_P1P2)     //!< Wrong data size for command
#define IDX_ISO_ERR_HAVE_KEYS    (IDX_ISO_FILE_EXISTS)              //!< Keys are already created
#define IDX_ISO_ERR_BAD_COMMAND  (IDX_ISO_INSTR_CODE_NOT_SUPP)      //!< Command not recognized (or not available)
#define IDX_ISO_ERR_BAD_RMAC     0x6F01                             //!< Computed R-MAC does not match

#endif /* _IDX_ISO_ERRORS_H_ */
