

#include "main.h"
//pagesize =8k
// 1m flash =128 page
extern idx_State state;
extern uint8_t se_hso_int_flag;


const g_am_hal_flash_t g_am_hal_flash =
{
         ((int  (*)(uint32_t, uint32_t)) 0x0800004d),                                  // flash_mass_erase
         ((int  (*)(uint32_t, uint32_t, uint32_t))  0x08000051),                       // flash_page_erase
         ((int  (*)(uint32_t, uint32_t *, uint32_t *, uint32_t))  0x08000055),         // flash_program_main
         ((int  (*)(uint32_t, uint32_t, uint32_t *, uint32_t, uint32_t))  0x08000059), // flash_program_info_area
         ((int  (*)(uint32_t, uint32_t)) 0x0800006d),                                  // flash_mass_erase_nb
         ((int  (*)(uint32_t, uint32_t, uint32_t)) 0x08000071),                        // flash_page_erase_nb
         ((int  (*)( uint32_t, uint32_t))  0x08000095),                                // flash_page_erase2_nb
         ((bool (*)(void)) 0x0800007d),                                                // flash_nb_operation_complete
         ((uint32_t (*)(uint32_t *)) 0x08000075),                                      // flash_util_read_word
         ((void (*)( uint32_t *, uint32_t)) 0x08000079),                               // flash_util_write_word
         ((void (*)(uint32_t ))            0x0800009D),                                // bootrom_delay_cycles
         ((int  (*)( uint32_t, uint32_t))  0x08000081),                                // flash_info_erase
         ((int  (*)( uint32_t, uint32_t))  0x08000089),                                // flash_info_plus_main_erase
         ((int  (*)(uint32_t))             0x08000091),                                // flash_info_plus_main_erase_both
         ((int  (*)( uint32_t ))           0x08000099),                                // flash_recovery
         ((void (*)(void))  0x0800005d),                                               // flash_program_main_from_sram
         ((void (*)(void))  0x08000061),                                               // flash_program_info_area_from_sram
         ((void (*)(void))  0x08000065),                                               // flash_erase_main_pages_from_sram
         ((void (*)(void))  0x08000069),                                               // flash_mass_erase_from_sram
         ((void (*)(void))                 0x08000085),                                // flash_info_erase_from_sram
         ((void (*)(void))                 0x0800008D),                                // flash_info_plus_main_erase_from_sram
         ((void (*)(void))                 0x080000A1),                                // flash_nb_operation_complete_from_sram
         ((void (*)(void))                 0x080000A5),                                // flash_page_erase2_nb_from_sram
         ((void (*)(void))                 0x080000A9)                                 // flash_recovery_from_sram
};

const uint32_t ui32SramMaxAddr = 0x10060000;


extern void wtx_stage_two_callback(void);


int am_hal_flash_page_erase(uint32_t ui32ProgramKey, uint32_t ui32FlashInst,uint32_t ui32PageNum)
{
    return g_am_hal_flash.flash_page_erase(ui32ProgramKey,
                                           ui32FlashInst,
                                           ui32PageNum);
} // am_hal_flash_page_erase()

int am_hal_flash_program_main(uint32_t ui32ProgramKey, uint32_t *pui32Src, uint32_t *pui32Dst, uint32_t ui32NumWords)
{
    uint32_t ui32MaxSrcAddr = (uint32_t)pui32Src + (ui32NumWords << 2);

    // workround, the last word of SRAM cannot be the source
    // of programming by BootRom, check to see if it is the last
    if (ui32MaxSrcAddr == ui32SramMaxAddr)
    {
        uint32_t ui32Temp;
        int iRetVal;
        
        // program the other words using the boot-rom function
        if (ui32NumWords > 1) 
        {
            iRetVal = g_am_hal_flash.flash_program_main(
                        ui32ProgramKey,
                        pui32Src,
                        pui32Dst,
                        ui32NumWords - 1);
            // return if anything wrong
            if (iRetVal != 0)
            {
                return iRetVal;
            }
        }
        // program the last word of the pSrc from a local 
        // variable if it is the last word of SRAM
        ui32Temp = *(uint32_t *)(ui32MaxSrcAddr - 4);
        return g_am_hal_flash.flash_program_main(
                        ui32ProgramKey, 
                        &ui32Temp,
                        pui32Dst + ui32NumWords - 1, 
                        1);
    }
    return g_am_hal_flash.flash_program_main(ui32ProgramKey, pui32Src,
                                             pui32Dst, ui32NumWords);
} // am_hal_flash_program_main()


int idxFlashEntryCount(idx_State *pState)
{
	idx_FlashEntry idex_entry_table[FLASH_MAX_ENTRIES];

	int i=0;
	uint32_t p;
	p=pState->pFlashStartAddress;
	for(i=0;i<FLASH_MAX_ENTRIES;i++)
		{
		idex_entry_table[i].offset=*(uint32_t*)p;
		idex_entry_table[i].size=*(uint32_t*)(p+4);
		idex_entry_table[i].handle=*(uint8_t*)(p+8);
		p+=16;
		if(FLASH_RESET_OFFSET_VALUE==idex_entry_table[i].offset)return i;
		}
	return FLASH_MAX_ENTRIES;

}


int idxFlashEntryUpdate(idx_State *pState, uint16_t handle, uint32_t offset, uint32_t size)
{
	int32_t count;
	u8 tempa_buffer[16];
	u8 *p=0;
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };

	memset(tempa_buffer,0xff,16);
	p=tempa_buffer;
	*(uint32_t*)p =offset;
	p+=4;
	*(uint32_t*)p=size;
	p+=4;
	*(uint8_t*)p=(uint8_t)handle;

	count = idxFlashEntryCount(pState);
	if(FLASH_MAX_ENTRIES==count)return ISO_ERR_NO_SPACE;
	ret.idxRet	= am_hal_flash_program_main(AM_HAL_FLASH_PROGRAM_KEY,(uint32_t *)tempa_buffer,(uint32_t *)(pState->pFlashStartAddress + count*16),4);
	return ret.idxRet;
}

int idxFlashEntryDelete(idx_State *pState, uint16_t pHandle)
	{
		idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
		idx_FlashEntry template_table[FLASH_MAX_ENTRIES];
		uint32_t i;
		u8 *p=0;
		u8 tempa_buffer[16];
		if(FLASH_MAX_ENTRIES<=pHandle)
			{
			ret.idxRet = IDX_ERR_REC_NOT_FOUND;
			return ret.idxRet;
			}
	
		for(i=0;i<FLASH_MAX_ENTRIES;i++)
			{
			template_table[i].offset=(uint32_t )(pState->pFlashStartAddress + 16*i );
			template_table[i].size= (uint32_t )(pState->pFlashStartAddress + 16*i +4);
			template_table[i].handle=(uint8_t )(pState->pFlashStartAddress + 16*i +8);
			}
		template_table[pHandle].offset =0xffffffff;
		template_table[pHandle].size =0xffffffff;
		template_table[pHandle].handle = 0xff;

		if(0!=(am_hal_flash_page_erase(AM_HAL_FLASH_PROGRAM_KEY, 0,INDEX_TABLE_PAGE)))
			{
			printf("record delete failed \r\n");
			return ISO_ERR_UNKNOWN;
			}

		memset(tempa_buffer,0xff,16);
		for(i=0;i<FLASH_MAX_ENTRIES;i++)
			{
			p=tempa_buffer;
			*(uint32_t*)p =template_table[i].offset;
			p+=4;
			*(uint32_t*)p=template_table[i].size;
			p+=4;
			*(uint8_t*)p=template_table[i].handle;
			ret.idxRet  = am_hal_flash_program_main(AM_HAL_FLASH_PROGRAM_KEY,(uint32_t *)tempa_buffer,(uint32_t *)(pState->pFlashStartAddress + i*16),4);
			if(0!=ret.idxRet)
				{
				printf("record program failed \r\n");
				return ISO_ERR_UNKNOWN;
				}
			}
		return ISO_SUCCESS;
}


int idxFlashTemplateDelete(idx_State *pState, uint16_t pHandle)
	{
		idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
		if(FLASH_MAX_ENTRIES<=pHandle)
			{
			ret.idxRet = IDX_ERR_REC_NOT_FOUND;
			return ret.idxRet;
			}
		if(0!=(am_hal_flash_page_erase(AM_HAL_FLASH_PROGRAM_KEY, 0,TEMPLATE_START_PAGE+pHandle)))
			{
			printf("record delete failed \r\n");
			return ISO_ERR_UNKNOWN;
			}
		return ISO_SUCCESS;
	}


int flash_erase_block_with_wtx(void)
{
	int i;
	for(i=0;i<IDX_BIOMETRICS_TOTAL_PAGE;i++)
		{
		if(0!=(am_hal_flash_page_erase(AM_HAL_FLASH_PROGRAM_KEY, 0,32+i)))
			{
			printf("page %d erase error.\r\n",(32+i));
			printf("record delete failed \r\n");
			return -1;
			}
		}
	return 0;
}

int idx_idm_listrecord(idx_State *pState)
	{
		idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
	
		uint8_t numRecords = 0;
		uint8_t i = 0;
		uint16_t handles[FLASH_MAX_ENTRIES+1];
		
		// read all handles from flash
		for(i=0;i<FLASH_MAX_ENTRIES;i++)
			{
			if(FLASH_RESET_OFFSET_VALUE!= *(uint32_t *)((pState->pFlashStartAddress)+ 16*i))
				{
				handles[numRecords+1]=*(uint8_t *)((pState->pFlashStartAddress)+ 16*i +8);
				numRecords++;
				}
			}
		handles[0]=numRecords;
	
		if (numRecords)
		{
			// prepare reply
			memcpy(pState->pCommData, handles, (1 + numRecords)*sizeof(uint16_t));
			pState->commDataSize = (1 + numRecords)*sizeof(uint16_t);
			ret.idxRet = ISO_SUCCESS;
		}
		else
			ret.idxRet = IDX_ERR_REC_NOT_FOUND;
	
		return ret.idxRet;
	
	}




int idx_idm_DeleteRecord(idx_State *pState)
	{
		
		idx_ReturnCode_t ret = { 0, ISO_SUCCESS };
		idx_FlashEntry template_table[FLASH_MAX_ENTRIES];
		uint32_t template_to_delete;
		uint32_t i;
		// read command data
		uint16_t pHandle = *(uint16_t*)pState->pCommData;
		pState->commDataSize=0;
			if(0xffff==pHandle)//all delete
				{
				if(0==flash_erase_block_with_wtx())
					{
					//printf("record delete done \r\n");
					ret.idxRet=ISO_SUCCESS;
					}
				else
					{
					printf("record delete failed\r\n");
					ret.idxRet=ISO_ERR_STATE;
					}
				return ret.idxRet;
		
				}
			else//template delete
				{
				template_to_delete=FLASH_MAX_ENTRIES;
				for(i=0;i<FLASH_MAX_ENTRIES;i++)
					{
					template_table[i].handle=(uint8_t )(pState->pFlashStartAddress + 16*i +8);
					if((uint8_t)pHandle==template_table[i].handle)
						{
						template_to_delete=i;
						break;
						}
					}
				if(FLASH_MAX_ENTRIES==template_to_delete)
					{
					ret.idxRet = IDX_ERR_REC_NOT_FOUND;
					return ret.idxRet;
					}
				ret.idxRet =idxFlashTemplateDelete(pState,template_to_delete);
				if(0==ret.idxRet)
					{
					ret.idxRet=idxFlashEntryDelete(pState, template_to_delete);
					}
				}
			return ret.idxRet;
			
		}



int idx_template_data_store( idx_TemplateData_t* pTemplateData, uint32_t availible_address)
{
	u8 *p=0;
	u16 offset,one_page;
	uint32_t size;
	idx_ReturnCode_t ret = { 0, ISO_SUCCESS };



	if(BIOMETRIC_MODE_CONTACTLESS==state.connection_mode)
	{
	p=pTemplateData->data;
	offset=0;

	while(offset!=pTemplateData->size)
	{
		one_page=((u16)(pTemplateData->size) -offset);
		size=(one_page>>2)+1;
		if(256<one_page)
			{one_page=256;
			size=64;}
		ret.idxRet	= am_hal_flash_program_main(AM_HAL_FLASH_PROGRAM_KEY,(uint32_t *)(p+offset),(uint32_t *)(availible_address+offset),size);
		if(0!=ret.idxRet)
			{
			printf("record program failed \r\n");
			return ISO_ERR_UNKNOWN;
			}
		offset+=one_page;
		wtx_stage_two_callback();

	}


	}
	else
	{
		if(pTemplateData->size &0x00000003)
			{
			size=(pTemplateData->size >>2)+1;//get size in words
			}
		else
			{
			size=(pTemplateData->size >>2);
			}
		ret.idxRet	= am_hal_flash_program_main(AM_HAL_FLASH_PROGRAM_KEY,(uint32_t *)pTemplateData->data,(uint32_t *)availible_address,size);
		if(0!=ret.idxRet)
			{
			printf("record program failed \r\n");
			return ISO_ERR_UNKNOWN;
			}
	}
	wtx_stage_two_callback();
	return ISO_SUCCESS;
}

void flash_test(void)
{
	int i;
	int ret;
	uint8_t test[100];
	for(i=0;i<100;i++)
		{
		test[i]=i;
		}
	ret = am_hal_flash_program_main(AM_HAL_FLASH_PROGRAM_KEY,(uint32_t *)test,(uint32_t *)INDEX_TABLE_ADDR,4);
		if(0!=ret)
			{
			printf("record program failed,ret=%x \r\n",ret);
			}
}



