/******************************************************************************
* \file  idxEmkExDefines.h
* \brief Defines for the extended matcher library
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright 2014-2019 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR / FRR performance.
******************************************************************************/

#ifndef __IDX_EMK_EX_DEFINES_H__
#define __IDX_EMK_EX_DEFINES_H__

#include <stdint.h>
#include "idxBaseDefines.h"
#include "idxEmkExBaseDefines.h"

#ifndef __GNUC__
#pragma pack(push)
#pragma pack(1)
#endif

typedef struct PACKED_ATTRIBUTE __idx_ExtractInfo_
{
    uint16_t imageQuality;
    uint16_t templateQuality;
	uint16_t numFeatures;
    uint16_t templateSize;
} 
idx_ExtractInfo_t;

typedef struct PACKED_ATTRIBUTE __idx_MergeInfo_
{
    uint8_t numMerged;
    uint8_t isTemplateFull;
    uint16_t templateQuality;
    uint16_t templateSize;
} 
idx_MergeInfo_t;

enum
{
    MATCHER_TYPE_IDEX = 0,
    MATCHER_TYPE_IDEX_SECURE = 1,
};

enum
{
    MATCHER_CAN_VERIFY = 1,
    MATCHER_CAN_ENROL  = 2,
};

typedef struct PACKED_ATTRIBUTE __idx_MatcherInfo
{
    uint8_t			  type;
    idx_UnitVersion_t version;
    uint8_t			  capabilities;
} 
idx_MatcherInfo_t;

typedef struct PACKED_ATTRIBUTE __idx_MatcherCfg
{
    uint16_t enrollMode;
    uint16_t matchMode;
    uint16_t minEnrollImages;
    uint16_t maxEnrollImages;
    uint16_t imageWidth;
    uint16_t imageHeight;
    uint16_t imageDPI;
    uint16_t matcherSubtype;
    uint16_t showRidges;
    uint8_t isUpdateTemplate;
    uint8_t isEnableSyntheticImage;
	uint16_t securityLevel;
	uint8_t inEnrollPhase;
}
idx_MatcherCfg_t;

#ifndef __GNUC__
#pragma pack(pop)
#endif

#endif //__IDX_EMK_EX_DEFINES_H__
