#include "main.h"

extern idx_State state;
extern unsigned char wtx_sensor_init_status;
extern unsigned char wtx_sensor_process_flag;
extern unsigned char* dataImage;


extern uint8_t LED_color_flag;
extern uint8_t LED_mode_flag;
extern uint8_t led_slash_flag;
extern uint8_t led_ms_status;



extern SensorHeapCallbacks ick_heap_callbacks;
extern SensorHeapCallbacks emk_heap_callbacks;


extern int appolo3_spi_reset( void *p, int do_warm_reset );
extern void* appolo3_spi_open( void* arg );
extern int appolo3_spi_waitforreadystate( void* p, int timeout_ms, int pin_state );
extern int appolo3_spi_getreadypin( void *p, unsigned char *val );
extern void appolo3_spi_close( void* p );
extern int appolo3_spi_transfer( void *p, unsigned char *buffer, int txSize, int rxSize );
extern void fps_image_to_eink(void);
extern void Sleep_with_wtx( uint32_t ms);
extern void clear_idx_heap(void);




const SensorDriverCallbacks df = 
{
	appolo3_spi_open,
	appolo3_spi_close,
	appolo3_spi_transfer,
	appolo3_spi_reset,
	appolo3_spi_getreadypin,
	appolo3_spi_waitforreadystate,
	//optional interfaces
	0,
	0,
	0,
	0
};


int sensor_initial_process_with_wtx(idx_State *pState)
{
	//uint8_t i;
	uint16_t result;
	idx_ReturnCode_t ret;
	idx_MatcherCfg_t pConfig = { 0 };

	if(0x02==pState->pCommData[0])state.connection_mode=BIOMETRIC_MODE_CONTACT;

	am_hal_gpio_output_set(CS_pin);
	am_hal_gpio_output_clear(RESET_pin);
	Sleep_with_wtx(5);
	am_hal_gpio_output_set(RESET_pin);
	Sleep_with_wtx(25);
	led_ms_status=0;
	LED_mode_flag=0;
	LED_color_flag=0;
	led_slash_flag=0;


	idxEmkExUninitialize(pState->pEmkHandle);
	idxIckUninitialize();
	clear_idx_heap();

	am_hal_gpio_output_set(CS_pin);
	ret = idxIckInit(&df, &ick_heap_callbacks, 0);
	am_hal_gpio_output_set(CS_pin);

	if (IDX_SUCCESS != ret.idxRet)
		{
		idxIckUninitialize();
		printf("Failed to initialize the ICK, ret=%x\r\n",ret.idxRet);
		result=IDX_ISO_ERR_UNKNOWN;
		}
	else
		{
		wtx_sensor_init_status=1;
		result=IDX_ISO_SUCCESS;
		//observe_data=3;

		int status;
		bool forceRecal = false;//TRUE
		bool storeCalibration = false;
		status = idxIckInternalFuncs( IFID_CalibrateSensor, ( void* ) forceRecal, ( void* ) storeCalibration );

		if ( 0 == status )
			{
			//printf("Sensor successfully  calibrated.\r\n" );
			}
		else if ( -EALREADY == status )
			{
			//printf("Calibrated sensor detected.\r\n" );
			}

		idxIckSetParameter(WTX_MODE,1);


		//if(BIOMETRIC_UNINITIALIZED==pState->mcuState)
			{
			ret = idxEmkExGetDefaultConfig(&pConfig);	//	Load out configuration with default values.
			pConfig.imageWidth= idxIckGetSizeX();
			pConfig.imageHeight= idxIckGetSizeY();
			pConfig.imageDPI= idxIckGetSensorDPI();
			pConfig.enrollMode = ACCEPT_ALL;
			pState->sensorInfo.width=pConfig.imageWidth;
			pState->sensorInfo.height=pConfig.imageHeight;
			pState->sensorInfo.DPI=pConfig.imageDPI;
			ret=idxEmkExInitialize(&pConfig,&emk_heap_callbacks,&pState->pEmkHandle);
			//observe_data=5;

			//ret = createEmkContext(&emk_heap_callbacks, imm_image_height, imm_image_width, imm_image_DPI, &pState->pEmkHandle);
			if (ret.idxRet != IDX_SUCCESS)
				{
				printf("Failed to initialize the EMK\r\n");
				return IDX_ISO_ERR_UNKNOWN;
				}
			pState->mcuState = BIOMETRIC_IDLE;
			}

		}
	/*
for(i=0;i<wait_for_ready_count;i++)
{
	printf("No%d,P=%x,T=%x\r\n",i,waitforredy_log[i],waitforready_timeout[i]);
}
*/
return result;
}


void idx_get_image_information(idx_State *pState)
{
	pState->pSavedData=dataImage;
	pState->imageInfo.width=pState->sensorInfo.width;
	pState->imageInfo.height=pState->sensorInfo.height;
	pState->imageInfo.DPI=pState->sensorInfo.DPI;
	pState->imageInfo.depth = 8;		// Only 8 bit supported
	pState->imageInfo.ridgeColor = BLACK_RIDGE;   // Supported: 0 = BLACK RIDGE  
	pState->mcuState=BIOMETRIC_IMAGE;
	pState->pLoadImage_ob.offset= (pState->imageInfo.width * pState->imageInfo.height);
}


int idx_split_prepare_image(idx_State *pState)
	{

		idx_ReturnCode_t ret;
		uint8_t enroll_match_mode;
		uint16_t fingerCoverage;
		PREP_IMAG_OPTIONS acquireflag;
		int fingerdetectTimeout = 30000; //timeout in ms
		int buffer_size;
		if(0==(0xf0 & pState->pCommData[0]))
			{
			acquireflag = PREPIMG_FINGERDETECT;//PREPIMG_LIVE;//PREPIMG_FINGERDETECT;//PREPIMG_NEWFINGERDETECT;
			enroll_match_mode=MATCHING;
			}
		else
			{
			acquireflag = PREPIMG_NEWFINGERDETECT;
			enroll_match_mode=ENROLLMENT;
			}


		if(BIOMETRIC_UNINITIALIZED==pState->mcuState)
			{
			ret.idxRet=ISO_ERR_STATE;
			pState->commDataSize=0;
			return ret.idxRet;
			}

		//idxIckSetParameter(WTX_MODE,1);
		ret = idxIckPrepareImage(0, fingerdetectTimeout, acquireflag);//cost 390ms:100ms settle time+ 290ms scan time
			if (IDX_SUCCESS == ret.idxRet)
				{
				
				//printf( "image captured.\r\n" );
				//idxIckSetParameter(WTX_MODE,0);
				ret = idxIckTransferImage();	//cost 44ms at SPI is 8Mhz
				
				if (IDX_SUCCESS == ret.idxRet)
					{
					wtx_sensor_process_flag=0;

					//printf( "image transferred.\r\n" );
					if(MATCHING==enroll_match_mode)
						{
						am_hal_gpio_output_clear(RESET_pin);
						Sleep(2);
						}
					wtx_sensor_process_flag=0x02;
					ret = idxIckProcessImage(&buffer_size, &dataImage);
					if (IDX_SUCCESS == ret.idxRet)
						{
						//printf( "image processed.\r\n" );
						idx_get_image_information(pState);
						//state.pSavedData=dataImage;
						//state.mcuState=BIOMETRIC_IMAGE;
						fps_image_to_eink();
						idxIckGetParameter(LAST_SCANNED_IMAGE_COVERAGE, &fingerCoverage);


						*(uint16_t *) pState->pCommData=fingerCoverage;
						pState->commDataSize=2;
						ret.idxRet=IDX_ISO_SUCCESS;

						}
					else
						{
						ret.idxRet=IDX_ISO_ERR_UNKNOWN;
						pState->commDataSize=0;
						printf("Image processing failed.\r\n");
						}
					}
				else
					{
					ret.idxRet=IDX_ISO_ERR_COMM;
					pState->commDataSize=0;
					printf("Image transfer failed, ret.idxRet= %d \r\n",ret.idxRet);
					}
				}
			else
				{
				ret.idxRet=IDX_ISO_ERR_UNKNOWN;
				pState->commDataSize=0;
				printf("Image capture failed, err=%x\r\n",ret.idxRet);
				}
		return ret.idxRet;
	}




void ad_strip()
{
	df.drvcomm_Open(0);
}


/*************************** End of file ****************************/
