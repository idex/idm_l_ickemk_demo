/*
 * * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: nvm.c
 * * SCFID:
 * * Feature: nvm function
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-11-18 
 * *     1. Original version 0.1
 * */
#include "halglobal.h"
volatile unsigned char dataBUF[0x200] __attribute__((at(0x20002400)));
//NVM buffer write
#define PROG_BUF_WORD_WRITE			0x00010000
#define ERASE_BUF_WORD_WRITE		0x00030000
//NVM erase
#define FAST_ERASE					0x00000010
#define VERIFY_ERASE				0x00000040
#define VREAD_ERASE					0x00000080
#define SECTOR_ERASE				0x00000005
#define CHIP_ERASE					0x0000000a
//NVM program
#define VERIFY_PROG					0x00004000
#define VREAD_PROG					0x00008000
#define WORD_PROG					0x00000100
#define PAGEROW_WORD_PROG			0x00000400
//NVM hardware verify
#define HVTYP						0x10000000
#define EVTYP						0x00000080
#define WORD_VERIFY					0x30000000
#define PAGE_RANDOM_WORD_VERIFY		0x20000000
#define SECTOR_VERIFY				0x10000000
#define PAGE_VERIFY					0x00000000

/*
* Function: u8 singleWordEraseCheck( u8 *pbSrc )
* Description: To check whether the address in nvm is erased, check length is 4 bytes.
* Input: pbSrc  - source address
* Output: -
* Return: 
* 		 0:represent that the address is erased(the last operation is erase)
*		 1:represent that the address is not erased��the last operation is write��
*/
u8 doubleWordEraseCheck( u8 *pbSrc )
{
	u8 flag = 0;
	u32 ulIRQ;
	if ((u32)pbSrc & 0x07) 
	{
		return 1;
	}
	ulIRQ = disableIRQ();
	NVMCON = WORD_VERIFY;
	NVMSDP1 = 0xaa;
	NVMSDP2 = 0xaa;
	*((u32 *)(pbSrc)) = 0x00;

	//event mode
	EVTMEMCON |= 0x01;
	while (!(NVMSTS & 0x01)) 
	{
		__SEV();
		__WFE();
		__WFE();
	}
	EVTMEMCON &= ~0x01;

	if ((NVMSTS & 0xfe) || NVMSTS1)
	{
		flag = 1;
	}
	NVMSTS = 0xff;
	NVMSTS1 = 0xff;
	restoreIRQ(ulIRQ);
	return flag;
}

/*
* Function: u8 NvmEraseCheck( u8 *pbSrc, u16 usLen )
* Description: To check whether nvm is erased.
* Input: pbSrc  - source address
*        wLen - data length
* Output: -
* Return: 
* 		 0:represent that the address is erased(the last operation is erase)
*		 1:represent that the address is not erased��the last operation is write��
*/
u8 NvmEraseCheck( u8 *pbSrc, u16 usLen )
{
	if (usLen & 0x07)
		return 1;
	while (usLen)
	{
		if (doubleWordEraseCheck(pbSrc))
		{
			return 1;
		}
		pbSrc += 8;
		usLen -=8;
	}
	return 0;
}


#define EESECTORSIZE 0X100
#define EEPAGESIZE 0x80
#define EEPAGETRIM (EEPAGESIZE-1)
#define EEADDRMASK (0XFFFFFF80|EEPAGESIZE)
#define WORDTRIM 0x03


/*
* Function: u8 NvmProgramWords(u8 *pbDest, u8 *pbSrc, u16 usLen)
* Description: program nvm 
* Input: pbDest - destination address for programming
*        pbSrc - source address
*		 len - data lengh
* Output: -
* Return: 
*		0:successful execution
* 		1:error execution
*/
// #define CLKSYSSEL				*((volatile unsigned long *)(CLK_BASE_ADDR +0x000))
// u32 nvmLowPowerConfigBegin(void)
// {
// 	u32 bakCLKSYSSEL;
// 	bakCLKSYSSEL = CLKSYSSEL;
// 	if(((*(unsigned long *)0x42900040)<0x14)&&(!(IOSTS0&0x10)))
// 	{
// 		if((bakCLKSYSSEL&0x3f0)<0x2f0)
// 		{
// 			CLKSYSSEL = 0x2F0;
// 		}	
// 	}
// 	return bakCLKSYSSEL;
// }
// void nvmLowPowerConfigEnd(u32 config)
// {
// 	CLKSYSSEL = config;
// }
u8  NvmProgramWords(u8 *pbDest, u8 *pbSrc, u16 usLen)
{
	u8 flag = 0;
	u32 ulIRQ;
	u32 bakCLK;
	if ((usLen & 0x07) || ((u32)pbDest & 0x07))
	{
		return 1;
	}

	ulIRQ = disableIRQ();
	NVMSTS1 = 0xff;
	NvmBufferWrite((u32)pbDest, (u32)pbSrc, usLen, PROG_BUF_WORD_WRITE);
	NVMCON = 0xff00 & PAGEROW_WORD_PROG;
	NVMCON |= 0x4000;            
	NVMSDP1 = 0xaa;
	NVMSDP2 = 0x55;
	*((u32 *)(pbDest)) =  B2L((u8 *)pbSrc);
	//event mode
	EVTMEMCON |= 0x01;
	bakCLK = nvmLowPowerConfigBegin();
	while (!(NVMSTS & 0x01)) 
	{
		__SEV();
		__WFE();
		__WFE();
	}
	EVTMEMCON &= ~0x01;
	nvmLowPowerConfigEnd(bakCLK);
	if ((NVMSTS & 0xfe) || NVMSTS1)
	{
		flag = 1;
	}
	NVMSTS = 0xff;
	NVMSTS1 = 0xff;
	restoreIRQ(ulIRQ);
	return flag;
}


/*
* Function: u8 NvmFastEraseSector(u8 *pbDest)
* Description: nvm sector erase
* Input: dest - destination address for erase
* Output: -
* Return: 
*		0:successful execution
* 		1:error execution
*/		
		
u8 NvmFastEraseSector(u8 *pbDest)
{
	u8 flag = 0;
	u8 trycount = 4;
	u32 ulbakNVMNRSADR,ulbakNVMNRLEN;
	u32 ulIRQ;
	u32 bakCLK;
	if ((u32)pbDest&0x03) 
    {
		return 1;
    }
	ulIRQ = disableIRQ();
	ulbakNVMNRSADR =  NVMNRSADR;
	ulbakNVMNRLEN = NVMNRLEN; 
    NVMNRSADR = (u32)pbDest;
    NVMNRLEN  = 0x200;
	NVMSTS1 = 0xff;

	NVMCON |= 0x10;
	NVMCON |= 0xff & SECTOR_ERASE;
	NVMCON |= 0x00c0;            // erase verify
	EVTMEMCON |= 0x01;
RETRY:
	NVMSDP1 = 0x55;
	NVMSDP2 = 0xaa;
	*((u32 *)pbDest) = 0xffffffff;
	bakCLK = nvmLowPowerConfigBegin();
	//event mode
	while (!(NVMSTS & 0x01)) 
	{
		__SEV();
		__WFE();
		__WFE();
	}
	nvmLowPowerConfigEnd(bakCLK);
	if(NVMSTS1&0x04)
	{				
		trycount--;
		if(trycount)
		{
			NVMCON = (NVMCON&0xffcfffff) | ((4 - trycount)<<20);
			if(trycount == 0x01)
			{
				NVMCON &= 0xffffff7f;	
			}
			NVMSTS1 &= 0x05;
			NVMSTS = 0xff;
			goto RETRY;
		}
				
	}

	NVMCON &= 0xffffffef;
	EVTMEMCON &= ~0x01;
	
	if ((NVMSTS & 0xfe) || NVMSTS1)
	{
		flag = 1;
	}
	NVMSTS = 0xff;
	NVMSTS1 = 0xff;
	NVMNRSADR = ulbakNVMNRSADR; 
	NVMNRLEN = ulbakNVMNRLEN;
	restoreIRQ(ulIRQ);
	return flag;
	
}

void NvmBufferWrite(u32 ulDstAddr, u32 ulSrcAddr, u32 ulLen, u32 ulSetNVMCON)
{
	u32 index=0; 
    u8 Len,HeadLen,EndLen;
    u8 TempBUF[4];
    u32 tempLen;
    u32 ulDestaddr1,ulDestaddr2;

    NVMCON = 0x0f0000 & ulSetNVMCON;
    NVMSDP1 = 0x33;
    NVMSDP2 = 0xcc;
    HeadLen = ulDstAddr & 0x03; 
    ulDestaddr1 = ulDstAddr - HeadLen;

    if (HeadLen)
    {
		for(Len=0; Len<4;Len ++)
		{
			if (0x010000 == (ulSetNVMCON & 0x0f0000))
		    	TempBUF[Len] = *(u8*)(ulDestaddr1 + Len) ;
		    else
		    	TempBUF[Len] =0xff;
		}        
		tempLen = HeadLen+ulLen;
		if(tempLen>4 )
		 	tempLen = 4;
		for(Len=HeadLen;Len<tempLen;Len++)
		{
			TempBUF[Len] = *(u8*)(ulSrcAddr + Len-HeadLen);
		}
//		NVMSTS1 = 0x01;      //Clear redundant read waring;
		*(u32*)(ulDestaddr1) = B2L(TempBUF);
		index +=4;         
	}

    EndLen  =  (ulDstAddr+ulLen)&0x03;
    ulDestaddr2 = (ulDstAddr+ulLen)-EndLen; 
    if(EndLen<ulLen)
    {
		for (index=index; index < ulLen-EndLen; index += 4)
        {
    		if (0x010000 == (ulSetNVMCON & 0x0f0000))
         	*((u32 *)(ulDestaddr1 + index)) = B2L((u8 *)(ulSrcAddr + index -HeadLen));
           	else
				*((u32 *)(ulDestaddr1 + index)) = 0xff;
        }
    }
    
	if(EndLen)
    {
		if((ulDestaddr1!=ulDestaddr2)||(!HeadLen))
		{
	        for(Len=0;Len<4;Len++)
	        {
	        	if (0x010000 == (ulSetNVMCON & 0x0f0000))
	            	TempBUF[Len] = *(u8*)(ulDestaddr2 + Len );
	       		else
	        		TempBUF[Len] = 0xff;
	        }
			for(Len=0;Len<EndLen; Len++)
	        {
	       		TempBUF[Len] = *(u8*)(ulSrcAddr + ulLen - EndLen + Len);
	        }
		 	*(u32*)(ulDestaddr2) = B2L(TempBUF);		
		}			
			    
	}

}

/*
* Function: u32 NvmHalReadStrict(u32 ulSrc, u8 bMode)
* Description: read strict setting for NVM
* Input: wSrc - address for read strict,must align to 4 byte
*		 mode - read mode
				0: strict read 0
				1: strict read 1
* Output: -
* Return: strict read result 
*/		
u32 NvmHalReadStrict(u32 ulSrc, u8 bMode)
{
	u32 result;
	u32 ulIRQ;
	u32 ulbakNVMNRSADR,ulbakNVMNRLEN;

	if (ulSrc & 0x03)
	{
		while(1);
	}

	ulIRQ = disableIRQ();	
	ulbakNVMNRSADR =  NVMNRSADR;
	ulbakNVMNRLEN = NVMNRLEN; 
    NVMNRSADR = ulSrc;
    NVMNRLEN  = 4;
	NVMSRSADR = ulSrc; 
	NVMSRLEN = 4;

	switch (bMode)
	{
		case 0:
			NVMSRCON = 0x01; 	//set strict read 0 enable
		 	break;
		case 1:			
			NVMSRCON = 0x03; 	//set strict read 1 enable
			break;
	}
	result = *(u32 *)(ulSrc);
	NVMSTS1 = 0xff;

	NVMNRSADR = ulbakNVMNRSADR; 
	NVMNRLEN = ulbakNVMNRLEN;
	NVMSRSADR = 0; 
	NVMSRLEN = 0;
	NVMSRCON = 0;
	restoreIRQ(ulIRQ);
	return  result;
}


/*
* Function: u8 NvmReadTolerant(u8 *ulDest, u32 *ulSrc, u32 ulByteLen)
* Description: Nvm tolerant read. 
* Input: ulSrc - address to read, must align to 4 byte.
*        ulByteLen - tolerant read length
* Output: ulDest - address of data that read from ulSrc
* Return: 0: the address that is written
*		  1: the address is not written  
*/	
u8 NvmReadTolerant(u8 *ulDest, u32 *ulSrc, u32 ulByteLen)
{
	u32 i;
	u8 bFlag = 0;
	u32 ulIRQ;
	u32 ulbakNVMNRSADR,ulbakNVMNRLEN;

	if (((u32)ulSrc) & 0x03)
	{
		while(1);
	}

	ulIRQ = disableIRQ();	
	ulbakNVMNRSADR =  NVMNRSADR;
	ulbakNVMNRLEN = NVMNRLEN; 
    NVMNRSADR = (u32)ulSrc;
    NVMNRLEN  = ulByteLen;

	//read memory
	for ( i = 0; i < ulByteLen; i++ )
	{
		*(u8 *)(ulDest + i) =*(u8 *)((u8 *)ulSrc + i);
	}	
	if(NVMSTS1&0x01)
	{
		bFlag = 1;		
	}
	NVMSTS1 = 0xff;
	NVMNRSADR = ulbakNVMNRSADR; 
	NVMNRLEN = ulbakNVMNRLEN;
	restoreIRQ(ulIRQ);
	return bFlag;	
}
/*
* Function: u8 NvmUpdateSector(u8 *pbDest, u8 *pbSrc, u16 usLen)
* Description: erase and program nvm 
* Input: dest - destination address for erase and program
*        src - source address
*		 len - erase and program lengh
* Output: -
* Return: 
*		0:successful execution
* 		1:error execution
*/
u8 NvmUpdateWords(u8 *pbDest, u8 *pbSrc, u16 usLen)
{
	u32 i;
	u8 nFlag = 0;
	u32 desAddr;
	u32 tarAddr;
	u32 ulbakNVMNRSADR, ulbakNVMNRLEN;
	ulbakNVMNRSADR =  NVMNRSADR;
	ulbakNVMNRLEN = NVMNRLEN; 
	desAddr = (u32)pbDest;

    NVMNRSADR = (((u32)pbDest)&0xfffffe00);
    NVMNRLEN  = 0x200;
	tarAddr = desAddr - (desAddr&0x1ff);
	for(i=0; i<512; i++)
	{
		dataBUF[i] = *(u8 *)(tarAddr + i);
	}
	for (i =0; i<usLen; i++)
	{
		dataBUF[(desAddr&0x1ff)+ i] = *((u8 *)pbSrc + i);	
	}
	NVMNRSADR = ulbakNVMNRSADR; 
	NVMNRLEN = ulbakNVMNRLEN;
	nFlag = NvmFastEraseSector((u8 *)(tarAddr));
	nFlag = NvmProgramWords((u8 *)(tarAddr) , (u8 *)dataBUF, 0x100);
	nFlag = NvmProgramWords((u8 *)(tarAddr+0x100) , (u8 *)(dataBUF+0x100), 0x100);
	return nFlag;

}		


u8 NvmUpdate(u8 *pbDest, u8 *pbSrc, u16 usLen)
{
	u32 destAddr;
	u8 ulFlag;
	u16 headLen;
	u32 dataLen  = 	usLen;
	destAddr = (u32)pbDest; 
	while(dataLen)
	{
		if((((u32)destAddr&0x1ff) + dataLen) > 0x200 )
		{
			headLen =  0x200 - 	((u32)destAddr&0x1ff);
		}
		else
		{
			headLen = dataLen;
		}
		dataLen -= headLen;
		ulFlag = NvmUpdateWords((u8 *)destAddr, pbSrc, headLen);
		if(ulFlag)
		{
			return 1;
		}
		destAddr += headLen;
		pbSrc += headLen;
	}
	return 0;	
}


