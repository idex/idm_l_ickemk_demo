/******************************************************************************
 Copyright 2013-2020 IDEX Biometrics ASA. All Rights Reserved. 
 www.idexbiometrics.com

 IDEX Biometrics ASA is the owner of this software and all intellectual 
 property rights in and to the software. The software may only be used together 
 with IDEX fingerprint sensors, unless otherwise permitted by IDEX Biometrics 
 ASA in writing.

 This copyright notice must not be altered or removed from the software.

 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 Biometrics ASA has no obligation to support this software, and the software is 
 provided "AS IS", with no express or implied warranties of any kind, and IDEX 
 Biometrics ASA is not to be liable for any damages, any relief, or for any 
 claim by any third party, arising from use of this software.

 Image capture and processing logic is defined and controlled by IDEX 
 Biometrics ASA in order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __IDX_SE_APDU_INTERFACE_H__
#define __IDX_SE_APDU_INTERFACE_H__

#define IDX_APDU_CMD_INITIALIZE          	0x00
#define IDX_APDU_CMD_UNINITIALIZE        	0x01
#define IDX_APDU_CMD_ACQUIRE             	0x02
#define IDX_APDU_CMD_GETIMAGE            	0x04
#define IDX_APDU_CMD_MATCHTEMPLATES      	0x07
#define IDX_APDU_CMD_GETPARAM            	0x08
#define IDX_APDU_CMD_SETPARAM            	0x09
#define IDX_APDU_CMD_STOREPARAMS         	0x0C
#define IDX_APDU_CMD_GETSENSORINFO       	0x0E
#define IDX_APDU_CMD_DELETERECORD        	0x11
#define IDX_APDU_CMD_LIST                	0x12
#define IDX_APDU_CMD_ENROLL              	0x17
#define IDX_APDU_CMD_STOREBLOB           	0x18		
#define IDX_APDU_CMD_RESET               	0x21
#define IDX_APDU_CMD_INITUPDATE          	0x25
#define IDX_APDU_CMD_EXTERNALAUTH        	0x26
#define IDX_APDU_CMD_PUTKEYHEADER        	0x27
#define IDX_APDU_CMD_LOCK                	0x2A
#define IDX_APDU_CMD_GETVERSIONS         	0x30
#define IDX_APDU_CMD_UPDATESTART         	0x31
#define IDX_APDU_CMD_UPDATEDETAILS       	0x32
#define IDX_APDU_CMD_UPDATEDATA          	0x33
#define IDX_APDU_CMD_UPDATEHASH          	0x34
#define IDX_APDU_CMD_UPDATEEND           	0x35
#define IDX_APDU_CMD_GETSENSORFWVERSION  	0x36
#define IDX_APDU_CMD_GETCOMPONENTVERSION     	0x38
#define IDX_APDU_CMD_CALIBRATE           	0x45
#define IDX_APDU_CMD_READDATA            	0x50
#define IDX_APDU_CMD_LOADIMAGE           	0x52

#define IDX_APDU_CMD_IMM_ENROLL          	0x80
#define IDX_APDU_CMD_POS_VERIFY          	0x81
#define IDX_APDU_CMD_IMM_SLEEVE_ENROLL   	0x82
#define IDX_APDU_CMD_IMM_VERIFY          	0x83
#define IDX_APDU_CMD_GET_INTERNAL_BUFFER 	0x85
#define IDX_APDU_CMD_SET_INTERNAL_BUFFER 	0x86
#define IDX_APDU_CMD_IDM_ENROLL          	0x87
#define IDX_APDU_CMD_IDM_VERIFY          	0x88
#define IDX_APDU_CMD_IDM_GETINFO         	0x89
#define IDX_APDU_CMD_IDM_SINGLE_ENROLL   	0x8a
#define IDX_APDU_CMD_SLEEVE_ENROLL       	0x8b
#define IDX_APDU_CMD_READ_TIMES_BUFFER   	0x8d
#define IDX_APDU_CMD_GETSEVERSION        	0x8e
#define IDX_APDU_CMD_DELETE_FINGER       	0x8f

#define IDX_APDU_TD_POLICY              	0x90 // set/get policies

#define IDX_APDU_DBG_SET_NVM_KEYS        	0xf0 // 0xe0
#define IDX_APDU_DBG_GEN_RN              	0xe1
#define IDX_APDU_DBG_TIMER2              	0xe2
#define IDX_APDU_DBG_ERASE_NVM           	0xe3
#define IDX_APDU_DBG_GET_BUF             	0xe4
#define IDX_APDU_DBG_GET_REG             	0xe5
#define IDX_APDU_DBG_WRITE_NVM           	0xe6
#define IDX_APDU_DBG_AES                 	0xe7
#define IDX_APDU_DBG_CRC                 	0xe8

#define IDX_APDU_DBG_SET_FACTORY_KEYS    	0xe0
#define IDX_APDU_DBG_GET_NEXT_KEYS       	0xea
#define IDX_APDU_DBG_SET_NEXT_KEYS       	0xeb
#define IDX_APDU_DBG_OPEN_SCP03          	0xdd

#endif // __IDX_SE_APDU_INTERFACE_H__
