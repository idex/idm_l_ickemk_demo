/******************************************************************************
* @file  kdf.h
* @brief Key Derivation Function head file.
* @author IDEX ASA
* @version 1.0.0
*
*******************************************************************************
 Copyright 2014-2017 IDEX ASA. All Rights Reserved. www.idex.no


 IDEX ASA is the owner of this software and all intellectual property rights
 in and to the software. The software may only be used together with IDEX
 fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.


 This copyright notice must not be altered or removed from the software.


 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 ASA has no obligation to support this software, and the software is provided
 "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 not to be liable for any damages, any relief, or for any claim by any third
 party, arising from use of this software.


 Image capture and processing logic is defined and controlled by IDEX ASA in
 order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __KDF_H__
#define __KDF_H__

#include <stdint.h>
#include "crypto.h"

enum KDF_CONST {
  KDF_CONST_CARD_CRYPTOGRAM = 0x00,
  KDF_CONST_HOST_CRYPTOGRAM = 0x01,
  KDF_CONST_CARD_CHALLENGE  = 0x02,
  KDF_CONST_SENC            = 0x04,
  KDF_CONST_SMAC            = 0x06,
  KDF_CONST_SRMAC           = 0x07,
};
void genkeySENC(uint8_t *kenc, session_t *session);
void genkeySMAC(uint8_t *kmac, session_t *session);
void genkeySRMAC(uint8_t *kmac, session_t *session);
void genCardCryptogram(uint8_t *cryptogram, session_t *session);
void genHostCryptogram(uint8_t *cryptogram, session_t *session);

#endif // __KDF_H__
