

#include "main.h"



uint8_t epd_show_flag=0;

extern uint8_t * dataImage;
extern unsigned char cmd_string_size,cmd_ready;
extern idx_State state;;

extern uint8_t  string_temp[100];

extern uint8_t PIC_Orientation[4000];



uint8_t cmd_to_be_sent=0;
const uint8_t cmd_enroll[]={"enroll"};
const  uint8_t cmd_delete[]={"delete"};
const  uint8_t cmd_match[]={"match"};
const  uint8_t cmd_list[]={"list"};
const  uint8_t cmd_init[]={"init"};
const  uint8_t cmd_calibration[]={"calibration"};
const  uint8_t cmd_getversion[]={"getversion"};
const  uint8_t cmd_getsensorfwversion[]={"getsfwv"};
const  uint8_t cmd_i2cinit[]={"i2cinit"};
const  uint8_t cmd_getimage[]={"getimage"};
const char cmd_capture[]={"capture"};
const char cmd_show[]={"show"};

const char cmd_force_recal_and_store[]={"frcalandstore"};
const char cmd_force_recal_and_no_store[]={"frcalnostore"};




extern void enable_96mhz (void);
extern void disable_96mhz(void);
extern unsigned char wait_cmd(uint8_t *s_buffer, unsigned char *s_count);
extern void uart_ouput_data(unsigned char *str,unsigned int data_lenth);
extern int idx_split_prepare_image(idx_State *pState);
extern void EpdDisplayPic_wf0154_test(unsigned char num);
extern void EpdDisplayPic(unsigned char num);
extern int sensor_initial_process_with_wtx(idx_State *pState);
extern int mcu_system_init(void);
extern void SE_handshake_process(idx_State *pState);
extern int idx_template_data_store( idx_TemplateData_t* pTemplateData, uint32_t availible_address);




uint32_t watch_data[30];
uint32_t watch_data_2;

extern uint8_t stimer_flag;
extern void se_test_read(void);
extern int Vol_detect_process(void);




int main(void) 
{
	uint16_t result=0;
	uint16_t image_size=0;
	idx_TemplateData_t test_template;


	mcu_system_init();

	cmd_string_size=0;

	while(1)
		{
		SE_handshake_process(&state);
		if(1==stimer_flag)
			{
			stimer_flag=0;
			//se_test_read();
			}
		if(1==epd_show_flag)
			{
			epd_show_flag=0;
			
			//EpdDisplayPic_wf0102(PIC_Name);
			EpdDisplayPic_wf0154_test(PIC_Name);
			//EpdDisplayPic(PIC_Name);
			
			}
		cmd_ready=wait_cmd(string_temp,&cmd_string_size);
		if(1==cmd_ready)
			{
			cmd_to_be_sent=0;
			if((memcmp(cmd_enroll,string_temp,6)==0)&&(6==cmd_string_size))
				{
				cmd_to_be_sent=1;
				}
			if((memcmp(cmd_force_recal_and_no_store,string_temp,cmd_string_size)==0)&&(12==cmd_string_size))
				{
				printf("store test.\r\n");
				for(image_size=0;image_size<4000;image_size++)
					{
					PIC_Orientation[image_size]=(image_size&0x00ff);
					}
				test_template.data=PIC_Orientation;
				test_template.size=4000;
				idx_template_data_store(&test_template,INDEX_TABLE_ADDR);
				printf("store test done.\r\n");

				cmd_to_be_sent=7;
				}
			if((memcmp(cmd_force_recal_and_store,string_temp,cmd_string_size)==0)&&(13==cmd_string_size))
				{
				cmd_to_be_sent=11;
				printf("force recalibration and store is in processing.\r\n");
				//force_recal_and_store_data();
				//please be careful, the sensor OTP support store data only 3 times .
				}

			if((memcmp(cmd_getimage,string_temp,cmd_string_size)==0)&&(8==cmd_string_size))
				{
				cmd_to_be_sent=10;
				image_size=state.pLoadImage_ob.offset;
				string_temp[0]=(image_size-(image_size%256))>>8;
				string_temp[1]=image_size%256;
				string_temp[2]=state.imageInfo.height;
				string_temp[3]=state.imageInfo.width;
				uart_ouput_data(string_temp,4);
				uart_ouput_data(dataImage,image_size);
				}
			if((memcmp(cmd_delete,string_temp,cmd_string_size)==0)&&(6==cmd_string_size))
				{
				cmd_to_be_sent=2;
				//Vol_detect_process();
				epd_show_flag=0x01;

				}
			if((memcmp(cmd_init,string_temp,cmd_string_size)==0)&&(4==cmd_string_size))
				{
				cmd_to_be_sent=5;
				result=sensor_initial_process_with_wtx(&state);
				printf( "initial result=%x.\r\n" ,result);
				
				}
			if((memcmp(cmd_list,string_temp,cmd_string_size)==0)&&(4==cmd_string_size))
				{
				cmd_to_be_sent=6;
				
				}
			if((memcmp(cmd_capture,string_temp,cmd_string_size)==0)&&(7==cmd_string_size))
				{
				cmd_to_be_sent=8;
				result=idx_split_prepare_image(&state);
				printf( "initial result=%x.\r\n" ,result);
				}//cmd_show
			if((memcmp(cmd_show,string_temp,cmd_string_size)==0)&&(4==cmd_string_size))
				{
				cmd_to_be_sent=9;
				epd_show_flag=0x01;
				}

			
			if((memcmp(cmd_match,string_temp,cmd_string_size)==0)&&(5==cmd_string_size))
				{
				cmd_to_be_sent=3;

				}

			if((memcmp(cmd_calibration,string_temp,cmd_string_size)==0)&&(11==cmd_string_size))
				{
				cmd_to_be_sent=4;


				}
			if(0==cmd_to_be_sent)
				{
				printf("was invalid command!\r\n");
				printf("\r\n");
				}
			else
				{
				//printf("cmd_num=	%d\r\n",cmd_to_be_sent);
				}
			cmd_string_size=0;
			}
	}
}

/*************************** End of file ****************************/
