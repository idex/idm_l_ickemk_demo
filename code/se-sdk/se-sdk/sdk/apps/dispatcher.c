/** ***************************************************************************
 * \file   dispatcher.c
 * \brief  Code for testing SE EMK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2013-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "idxSerialHostInterface.h"
#include "idxSeAPDUInterface.h"

#include "idxSeBioAPI.h"
#include "bio-ctrl.h"
#include "dispatcher.h"
#include "idxSerialInterface.h"
#include "idxHalAlgo.h"
#include "idxHalSpi.h"
#include "idxHalUart.h"
#include "idxBioLed.h" // TEMPORARY THERE!
#if __C251__
// Shut up warning in "IDEX_M_Interface.h"
#define _MSC_VER 0
#endif
#include "idxAppDefines.h"
#include "idxPolicies.h"

#ifdef SLCx2
// For debug of ISO7816 host interface only - not intended to remain
// Note that ISO_HOST_DEBUG defined in the following header...
#include "idxIso7816HostIf2WireHal.h"
#else
#define ISO_HOST_DEBUG 0
#endif

#include "crypto.h"

#ifdef TH89
#include "THD89_Hal.h"
#include "idxHalTimer.h"
#include "cmac.h"
#include "kdf.h"
#include "crypto.h"
#endif

#ifdef CIU9872B_01
#include "declare.h"

// These two functions were added temporarily to avoid conflicts until idxHalAlgoAES128encrypt and idxHalAlgoCRC16 are deployed to CIU9872B_01 SE.
uint16_t AES128encrypt(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv);
uint16_t AES128decrypt(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv);
uint16_t CRC16_CCITT(uint8_t *iv, uint8_t *buf, uint32_t len);
#endif

#define UNUSED(p) if(p){}

const struct idxBioSystemConfig_s SystemConfig = 
{ 
  (const struct idxBioPolicies_s *)           &idxHalSystemPolicies,     // Policies
  (const struct idxHalSystemTemplateArea_s *) &idxHalSystemTemplateArea, // TemplateArea
  (struct idxBioEnrolFingerInfo_s *)          NULL,                      // EnrolFingerInfo
  (idxBioEnrollUi_t *)                        NULL                       // idxBioEnrollUi
};

const struct idxBioSystemConfig_s *get_SystemConfig(void)
{
  return &SystemConfig;
}



static uint8_t apdup2;
static uint8_t apdulc;
#define MEM_SIZE 2576
extern uint8_t *matcherBuffer;

extern uint16_t idxBioReadImageFromNvm( uint16_t BufferSize, uint8_t *pBuffer, uint16_t *pOutputDataSize );

uint8_t get_p2()
{
  return apdup2;
}

void set_p2(uint8_t n)
{
  apdup2 = n;
}

uint8_t get_lc()
{
  return apdulc;
}

static uint32_t get_update_address()
{
  return 0x08000000;
}

static uint32_t get_update_size()
{
  return 256;
}

static uint8_t * get_iv()
{
  unsigned int i;
  static uint8_t buff[32];
  for (i=0; i<32; i++) {
    buff[i] = 0;
  }
  return buff;
}

static uint8_t * get_hash()
{
  unsigned int i;
  static uint8_t hash[8];
  for (i=0; i<8; i++) {
    hash[i] = 0;
  }
    return hash;
}

/*****************************************************************************
 * DEBUG 
 *
 *****************************************************************************/
static void updateReplyInfo( uint16_t errCode, uint8_t *info, uint8_t isize, const idxBuffer_t *pCommBuff )
{
  uint8_t wSize;
  uint8_t i;
  uint8_t *pBuffer = pCommBuff->pBuffer;
  uint16_t bufferSize = pCommBuff->bufferSize;
  
  wSize = (uint8_t)RD_U16(&pBuffer[0]);
  
  if ((wSize + isize) > bufferSize)
    return; // TODO, add error code later
  
  WR_U16(&pBuffer[2], errCode);
  pBuffer[4] = errPoint;
  pBuffer[5] = 0x00;
  
  for(i = 0 ; i < isize ;i++){ 
    pBuffer[wSize + i] = info[i];  
  }
  if(isize)
    WR_U16(&pBuffer[0], wSize +isize);
}

static void setReplyInfo( uint16_t errCode, uint8_t *info, uint8_t isize, const idxBuffer_t *pCommBuff )
{
  uint8_t *pBuffer = pCommBuff->pBuffer;
  uint16_t bufferSize = pCommBuff->bufferSize;

  if ((HDR_SIZE + isize) > bufferSize)
    return; // TODO, add error code later
  
  WR_U16(&pBuffer[2], errCode);
  pBuffer[4] = errPoint;
  pBuffer[5] = 0x00;
  if(isize){
    int i; 
    WR_U16(&pBuffer[0], HDR_SIZE + isize);
    for(i = 0 ; i < isize ; i++)
      pBuffer[HDR_SIZE + i] = info[i];     
  }
  else{
    WR_U16(&pBuffer[0], HDR_SIZE);
  }
}

/*****************************************************************************
 * APDU-HANDLER - main function for dispatch the execution flow based on input
* Note: one exception is POS-VERIFY which should execute 2 commands  
 *****************************************************************************/
uint8_t *apdu_handler( const idxBuffer_t *pCommsBuffer )
{
  uint16_t ret = IDX_ISO_ERR_UNKNOWN;

  uint8_t *pHdrAPDU  = &pCommsBuffer->pBuffer[0];
  uint8_t *pDataAPDU = &pCommsBuffer->pBuffer[5];
  uint8_t *pDataBuf  = &pCommsBuffer->pBuffer[6];

  apdup2 = pHdrAPDU[3];
  apdulc = pHdrAPDU[4];

  switch( pHdrAPDU[2] )
  {

	case 0x72://read image from NVM
		{
		uint16_t inSize = ((uint16_t)get_lc() &0x00ff);
			uint16_t output_size;
		memcpy( matcherBuffer, pDataAPDU, inSize );
		ret = idxBioReadImageFromNvm( MEM_SIZE, matcherBuffer, &output_size );
		// setReplyInfo( ret, NULL, 0, pCommsBuffer );
		setReplyInfo( ret, matcherBuffer + (MEM_SIZE >> 1), output_size, pCommsBuffer);
	
	}
	break;




  
    case IDX_APDU_CMD_INITIALIZE:
    {
      enum idxBioInitializeOptions_e options;
      bool desiredSensorInit;
      
      {
        //void Init_SystemConfig();
        //Init_SystemConfig();
      }
      
      idxSetConnectionMode( pDataAPDU[0] & 0x05 );  // extract the mode from flags
      
      switch( pDataAPDU[0] & 0x05)
      {
        case 0x00:
        {
          options = IDX_BIO_INITIALIZE_CONTACT;
          break;
        }
        case 0x01:
        {
          options = IDX_BIO_INITIALIZE_CONTACTLESS;
          break;
        }
        default:
        {
          options = IDX_BIO_INITIALIZE_BATTERY;
          break;
        }          
      }
      
      desiredSensorInit = (( pDataAPDU[0] & 0x02 ) != 0);

      if (desiredSensorInit != SystemConfig.Policies->sensorinit ) {
        ret = IDX_ISO_ERR_NOT_SUPPTD;
      } else {
        ret = idxBioInitialize( options, &SystemConfig, pCommsBuffer );
      }
// For debugging ISO7816 host interface - not intended to remain...
#if ISO_HOST_DEBUG
      if (ret != (signed) 0x9000) {
        *((uint16_t *) &isoHostDebug[ISO_HOST_DEBUG_SIZE-2]) = ret;

        ret = 0x9000; // Else IFX throws it away!
        
        setReplyInfo( ret, isoHostDebug, ISO_HOST_DEBUG_SIZE, pCommsBuffer );
      } else {
        setReplyInfo( ret, NULL, 0, pCommsBuffer );
      }
#else
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
#endif
      break;
    }
    case IDX_APDU_CMD_UNINITIALIZE:
    {
      ret = idxBioUninitialize( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_GETIMAGE:
    {
      uint16_t inSize = (uint16_t)get_lc();
      uint16_t output_size;
      // Put apdu data in pBuffer
      memmove( pDataBuf, &pDataAPDU[0], inSize );
      ret = idxBioGetImage( pCommsBuffer, &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_ACQUIRE:
    {
      uint16_t output_size;
      ret = idxBioAcquire( pCommsBuffer, pDataAPDU[0], &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_LIST:
    {
      uint16_t output_size;
      memmove( pDataBuf, &pDataAPDU[0], 3 ); // List command has 3 bytes of data
      ret = idxBioListTemplates( pCommsBuffer, &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_GETPARAM:
    {
      uint8_t id;
      uint16_t val;
      uint8_t leBuff[2];
      id = pDataAPDU[0];
      ret = idxBioGetParameter( id, &val, pCommsBuffer );
      // setReplyInfo needs data in little endian order only, as it does byte copy only
      // note that val is native endianness
      WR_U16(leBuff, val);
      setReplyInfo( ret, leBuff, sizeof(leBuff), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_SETPARAM:
    {
      uint8_t id = pDataAPDU[0];
      uint16_t val = 0;
      val |= pDataAPDU[1] & 0xFF;
      val |= ((uint16_t)pDataAPDU[2] << 8 ) & 0xFF00; 
      ret = idxBioSetParameter( id, val, pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_STOREPARAMS:
    {
      ret = idxBioStoreParams( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_DELETERECORD:
    {
      uint16_t index = 0;
      index |= pDataAPDU[0] & 0xFF;
      index |= ((uint16_t)pDataAPDU[1] << 8 ) & 0xFF00;
      ret = idxBioDeleteRecord( index, pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
#ifdef IMM_ONLY		
    case IDX_APDU_CMD_IMM_ENROLL:{
      ret = idxBioOnCardEnroll( NULL, NULL );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_IMM_SLEEVE_ENROLL:  // Sleeve enrolment for SLE78
    {
      ret = idxBioSleeveEnroll( NULL, NULL );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_IMM_VERIFY:  // Verify for IMM ( CIU9872B_01 )
    {
      ret = idxBioVerify( NULL, NULL, NULL, 0, 0 );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_POS_VERIFY:
    {
      ret = IDX_ISO_ERR_NOT_SUPPTD;
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
#else
    case IDX_APDU_CMD_IDM_ENROLL:
    {
      ret = idxBioOnCardEnroll( idxHalSystemWorkAreas, pCommsBuffer );

// For debugging ISO7816 host interface - not intended to remain...
#if ISO_HOST_DEBUG
      if (ret != (signed) 0x9000) {
        *((uint16_t *) &isoHostDebug[ISO_HOST_DEBUG_SIZE-2]) = ret;

        ret = 0x9000; // Else IFX throws it away!
        
        setReplyInfo( ret, isoHostDebug, ISO_HOST_DEBUG_SIZE, pCommsBuffer );
      } else {
        setReplyInfo( ret, NULL, 0, pCommsBuffer );
      }
#else
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
#endif
      break;
    }
    case IDX_APDU_CMD_IDM_SINGLE_ENROLL:
    {
      ret = idxBioSingleEnroll( idxHalSystemWorkAreas, pCommsBuffer );
// For debugging ISO7816 host interface - not intended to remain...
#if ISO_HOST_DEBUG
      if (ret != (signed) 0x9000) {
        *((uint16_t *) &isoHostDebug[ISO_HOST_DEBUG_SIZE-2]) = ret;

        ret = 0x9000; // Else IFX throws it away!
        
        setReplyInfo( ret, isoHostDebug, ISO_HOST_DEBUG_SIZE, pCommsBuffer );
      } else {
        setReplyInfo( ret, NULL, 0, pCommsBuffer );
      }
#else
      setReplyInfo( ret, NULL, 0, pCommsBuffer);
#endif
      break;
    }
    case IDX_APDU_CMD_SLEEVE_ENROLL:  // Sleeve enrolment for SLE78
    {
      ret = idxBioSleeveEnroll( idxHalSystemWorkAreas, pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_IDM_VERIFY:
    {
      uint16_t score;
      uint16_t mwfTime;
      uint8_t imgsrc = pDataAPDU[0];

      mwfTime  = pDataAPDU[1] & 0xFF;
      mwfTime |= ((uint16_t)pDataAPDU[2] << 8 ) & 0xFF00;

      ret = idxBioVerify( idxHalSystemWorkAreas, pCommsBuffer, &score, imgsrc, mwfTime );
// For debugging ISO7816 host interface - not intended to remain...
#if ISO_HOST_DEBUG
      if (ret != (signed) 0x9000) {
        *((uint16_t *) &isoHostDebug[ISO_HOST_DEBUG_SIZE-2]) = ret;

        ret = 0x9000; // Else IFX throws it away!
        
        setReplyInfo( ret, isoHostDebug, ISO_HOST_DEBUG_SIZE, pCommsBuffer );
      } else {
        updateReplyInfo( ret, NULL, 0, pCommsBuffer ); // We already set ReplyInfo the EMK/bio-ctrl
      }
#else
      updateReplyInfo( ret, NULL, 0, pCommsBuffer ); // We already set ReplyInfo the EMK/bio-ctrl
#endif
      break;
    }
    case IDX_APDU_CMD_POS_VERIFY:
    {
      uint16_t score;
      uint8_t imgsrc = 1;
			
      ret = idxBioPosVerify( idxHalSystemWorkAreas, pCommsBuffer, &score, imgsrc, (uint16_t)IDEX_MWF_DEFAULT );
      updateReplyInfo( ret, NULL, 0, pCommsBuffer ); // We already set ReplyInfo the EMK/bio-ctrl
      break;
    }
    case IDX_APDU_CMD_DELETE_FINGER:
    {    
      ret = idxBioDeleteFinger( pCommsBuffer, pDataAPDU[0] );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }   
#endif		
    case IDX_APDU_CMD_STOREBLOB:
    {
      uint16_t inSize = (uint16_t)get_lc();
      uint16_t output_size;
      memmove( pDataBuf, &pDataAPDU[0], inSize ); // pDataAPDU contains "flags"
      ret = idxBioStoreBlob( pCommsBuffer, &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_LOCK:
    {
      uint16_t inSize = (uint16_t)get_lc();
      memmove( pDataBuf, &pDataAPDU[0], inSize ); // pDataAPDU contains "flags"
      ret = idxBioLock( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_GETCOMPONENTVERSION:
    {
      uint16_t output_size = 0;
      uint16_t inSize = (uint16_t)get_lc();
      memmove( pDataBuf, &pDataAPDU[0], inSize ); // pDataAPDU[0] contains "component"
      ret = idxBioGetComponentVersion( pCommsBuffer, &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_UPDATESTART:
    {
      ret = idxBioUpdateStart( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer);
      break;
    }
    case IDX_APDU_CMD_UPDATEDETAILS:
    {
      uint16_t inSize =(uint16_t)get_lc();
      
      // Put apdu data in pBuffer
      uint32_t address = get_update_address(); 
      uint32_t size = get_update_size();
      uint8_t * iv = get_iv();
      BioUpdateOptions options = (BioUpdateOptions)pDataAPDU[0];
      memmove( pDataBuf, &pDataAPDU[0], inSize );
      ret = idxBioUpdateDetails( options, address, size, iv, pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_UPDATEDATA:
    {
      uint16_t inSize = (uint16_t)get_lc();
      
      // Put apdu data in pBuffer
      memmove( pDataBuf, &pDataAPDU[0], inSize );
      ret = idxBioUpdateChunk( pCommsBuffer, 0 );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_UPDATEHASH:
    {
      //uint8_t Hash[8];
      uint8_t *pHash = get_hash();
      uint16_t inSize = (uint16_t)get_lc();
      
      // Put apdu data in pBuffer
      memcpy( pDataBuf, &pDataAPDU[0], inSize );
      ret = idxBioUpdateHash( pHash, pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_UPDATEEND:
    {
      ret = idxBioUpdateEnd( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_CALIBRATE:
    {
      uint8_t options = pDataAPDU[0];
      uint16_t output_size;
      ret = idxBioCalibrate( options, pCommsBuffer, &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_READDATA:
    {
      uint16_t inSize = (uint16_t)get_lc();
      uint16_t output_size;
      
      // Put apdu data in RAM0, pBuffer has no enough size for decryption
	  // TODO, check this message!!!
      memmove( pDataBuf, &pDataAPDU[0], inSize );
      ret = idxBioReadData( pCommsBuffer, &output_size );
      setReplyInfo( ret, pDataBuf, ((output_size > 255) ? 255 : output_size), pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_LOADIMAGE:
    {
      uint16_t inSize = (uint16_t)get_lc();
      // Put apdu data in pBuffer
      memmove( pDataBuf, &pDataAPDU[0], inSize );
      ret = idxBioLoadImage( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
/*********************************************************************
 *                Secure Channel Commands
 * Note : implemented as "debug" calls until a complete design for
 *        secure channel "use case" will be in place
 */    
    case IDX_APDU_CMD_RESET:
    {
      ret = idxBioReset( pCommsBuffer );
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_CMD_INITUPDATE:
    {
#if 0
      ret = idxBioInitializeUpdate()
#endif
      break;
    }
    case IDX_APDU_CMD_EXTERNALAUTH:
    {
#if 0
      ret = idxBioExternalAuthenticate()
#endif
      break;
    }
    case IDX_APDU_CMD_PUTKEYHEADER:
    {
#if 0
      ret = idxBioPutKeyHeader()
#endif
      break;
    }
#if 0
    case 0x??:
    {

      idxBioFinalizeKeys( 256, buffer );
      break;
    }
#endif

#ifdef TH89
    case IDX_APDU_DBG_GEN_RN:
    {
      uint8_t buf[16];
      memset(buf,0,16);
      ret = idxHalAlgoGenRandNamber(16,buf);
      setReplyInfo( ret, buf, 16, pCommsBuffer );
    break;
    }
    case IDX_APDU_DBG_TIMER2:
    {
      uint8_t buf[16];
      uint32_t cnt;
      uint16_t i;
      ret = IDX_ISO_SUCCESS;
      idxHalTimerFreeRun();
      for(i = 0 ; i < 1000; i++)
         idxHalAlgoGenRandNamber(16,buf);          
      cnt = idxHalTimerGetTicks();
      setReplyInfo( ret, (uint8_t *)&cnt, 4, pCommsBuffer );
      for(i = 0 ; i < 1000; i++)
         idxHalAlgoGenRandNamber(16,buf);
      cnt = idxHalTimerGetTicks();
      updateReplyInfo( ret, (uint8_t *)&cnt, 4, pCommsBuffer );
      for(i = 0 ; i < 1000; i++)
         idxHalAlgoGenRandNamber(16,buf);
      cnt = idxHalTimerGetTicks();
      updateReplyInfo( ret, (uint8_t *)&cnt, 4, pCommsBuffer );
      for(i = 0 ; i < 1000; i++)
         idxHalAlgoGenRandNamber(16,buf);
      cnt = idxHalTimerGetTicks();
      updateReplyInfo( ret, (uint8_t *)&cnt, 4, pCommsBuffer );
      for(i = 0 ; i < 1000; i++)
         idxHalAlgoGenRandNamber(16,buf);
      cnt = idxHalTimerGetTicks();
      updateReplyInfo( ret, (uint8_t *)&cnt, 4, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_ERASE_NVM:
    {
      #define pFLASH  ((void *)0x0c051000)    // 0x1000 bytes used by demo app
      uint8_t NvmFastEraseSector(uint8_t *pbDest);
  	
      NvmFastEraseSector((uint8_t*)pFLASH);

      ret = IDX_ISO_SUCCESS;
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_GET_BUF:
    {
      #define pFLASH2  ((void *)0x0c060000)    // 0x1000 bytes used by demo app
      ret = IDX_ISO_SUCCESS;
      setReplyInfo( ret, (uint8_t*)pFLASH2, 82, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_SET_FACTORY_KEYS :
    {
      ret = setFactoryKeys(pCommsBuffer);
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_GET_NEXT_KEYS :
    {
#ifdef DBG_CRYPTO      
      setTearingPoint(apdup2);
#endif
      ret = getNextKeys(pCommsBuffer);
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;      
    }      
    case IDX_APDU_DBG_SET_NEXT_KEYS :
    {
#ifdef DBG_CRYPTO
      setTearingPoint(apdup2);
#endif
      ret = setNextKeys(idxHalSystemWorkAreas[0].pointer, idxHalSystemWorkAreas[0].size, 0);    // putKey + flashUpdate
      if(ret != IDX_ISO_SUCCESS) {
        setReplyInfo( ret, NULL, 0, pCommsBuffer );
      }
      break;
    }
    case IDX_APDU_DBG_OPEN_SCP03:
    {
      uint16_t err = 0xffff;
      do{
        ret = getNextKeys(pCommsBuffer);
        if(ret != IDX_ISO_SUCCESS){ err = 0x01; break;} 
        ret = getExtAuthData();    // extAuthorize
        if(ret != IDX_ISO_SUCCESS){ err = 0x02; break;}
        // THD89 doesn't allow encryption into COMBUF, use matcher buffer instead, as putkey needs big buffer!!!
        ret = setNextKeys(idxHalSystemWorkAreas[0].pointer, idxHalSystemWorkAreas[0].size, apdup2);    // putKey + flashUpdate
        if(ret != IDX_ISO_SUCCESS){ err = 0x03; break;}
      }while(0);
      setReplyInfo( ret, (uint8_t*)&err, 2, pCommsBuffer );
      break;      
    }        
   
#endif
#ifdef CIU9872B_01		
    case IDX_APDU_DBG_WRITE_NVM :
    {
      ret = IDX_ISO_SUCCESS;
      setReplyInfo( ret, NULL, 0, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_AES :
    {
      uint8_t iv[]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
      uint8_t key[] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
      uint8_t in[]  = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff};
      uint8_t out1[16];// = {0x69,0xc4,0xe0,0xd8,0x6a,0x7b,0x04,0x30,0xd8,0xcd,0xb7,0x80,0x70,0xb4,0xc5,0x5a};
      uint8_t out2[16]; 
      ret = IDX_ISO_SUCCESS;
      AES128encrypt(out1, key, in, iv);
      setReplyInfo( ret, out1, 16, pCommsBuffer );
      AES128decrypt(out2, key, out1, iv);
      updateReplyInfo( ret, out2, 16, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_CRC :
    {
			uint16_t crc;
			uint8_t buf[] = {0x06,0x00,0x00,0x90};
      ret = idxHalAlgoCRC16(buf, 4, &crc);
      setReplyInfo( ret, (uint8_t *)&crc, 2, pCommsBuffer );
      break;
    }
    case IDX_APDU_DBG_GET_REG :
    {
      volatile uint32_t reg = *(volatile uint32_t *)0x50007180;
      ret = IDX_ISO_SUCCESS;
      setReplyInfo( ret, (uint8_t *)reg, 4, pCommsBuffer );
      break;
    }
#endif		
    case IDX_APDU_TD_POLICY:
    { // set/get policies
      //apdulc value:
      //0x01 Simply read the indexed policy
      //0x02 Read the indexed policy and write the one-byte data
      //0x03 Read the indexed policy and write the two-byte data

      uint32_t policyValResponse;
      uint8_t policySizeResponse;
      ret = idxPolicy(pDataAPDU, get_lc(), // input data
                      &policyValResponse, &policySizeResponse); // output data
      setReplyInfo(ret, (uint8_t*)&policyValResponse, policySizeResponse, pCommsBuffer);
      break;
    }
    case IDX_APDU_CMD_GETVERSIONS:            // Not implemented, only need the version from the MCU, so need to go by default.
    case IDX_APDU_CMD_GETSENSORINFO:          // Not implemented, only need the version from the MCU, so need to go by default.       
    case IDX_APDU_CMD_GETSENSORFWVERSION:     // Not implemented, only need the version from the MCU, so need to go by default.
    default:
    {
      uint8_t command = pHdrAPDU[2];
      uint8_t bOptions = pHdrAPDU[3];
      uint16_t data_size = (uint16_t)get_lc();

      memmove( pDataBuf, &pDataAPDU[0], data_size );
  
      ret = idxMcuComms( command, bOptions, &data_size, pCommsBuffer );
      setReplyInfo( ret, pDataBuf, ((data_size > 255) ? 255 : data_size), pCommsBuffer );
      break;
    }
  }

  //return ret;
  //return (uint8_t*)idxCom.state->ReplyHeader;
  return pCommsBuffer->pBuffer;
}

//------------------------------   end of file  ------------------------------------------------------
