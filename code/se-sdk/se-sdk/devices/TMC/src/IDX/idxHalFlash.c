/** ***************************************************************************
 * \file   idxHalFlash.c
 * \brief  Implementation of Flash HAL API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#include "idxHalFlash.h"

#include "type.h"
#include "NVM.h"

uint16_t idxHalFlashEraseCheck(uint8_t *pAddr, 
                               uint16_t bytesToCheck)
{
  u8 ret = NvmEraseCheck( pAddr, bytesToCheck );

  return (ret == 0) ? IDX_ISO_SUCCESS : IDX_ISO_ERR_FLASH_WRITE;
}	

uint16_t idxHalFlashErase(uint8_t *pAddr,
                          uint32_t bytesToErase)
{
	const uint32_t SECTOR_SIZE = 512;
	const uint32_t SECTOR_MASK = SECTOR_SIZE - 1;
	
  int ret = 0;
  uint8_t offsetAddr = 0;

  if( ((((uint32_t) pAddr) & SECTOR_MASK) != 0) || ((bytesToErase & SECTOR_MASK) != 0) )
  {
    // The address or size is not 512-byte aligned here
		return IDX_ISO_ERR_BAD_PARAMS;
  }

  while( (bytesToErase != 0) && (ret == 0) ) {
    ret = NvmFastEraseSector( pAddr + offsetAddr );
    offsetAddr += SECTOR_SIZE;
		
		if (bytesToErase <= SECTOR_SIZE) {
			// Not strictly necessary due to alignment check above, but to guard
			// against a loop that can run forever...
			bytesToErase = 0;
		} else {
			bytesToErase -= SECTOR_SIZE;
		}
  }

  return (ret == 0) ? IDX_ISO_SUCCESS : IDX_ISO_ERR_FLASH_ERASE;
}
  
uint16_t idxHalFlashWrite(uint8_t *pAddr, 
                          const uint8_t *pData,
						  uint16_t bytesToWrite)
{
  u8 ret = NvmUpdate(pAddr, ((u8 *) pData), bytesToWrite);

	return (ret == 0) ? IDX_ISO_SUCCESS : IDX_ISO_ERR_FLASH_WRITE;
}
