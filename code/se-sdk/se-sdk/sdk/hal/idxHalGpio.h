/** ***************************************************************************
 * \file   idxHalGpio.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         software control of GPIO pins. If a new platform is to be supported
 *         then it is expected that the HAL APIs are implemented for that
 *         platform in order to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_GPIO_H_
#define _IDX_HAL_GPIO_H_

#include <stdint.h>
#include <stdbool.h>

/*!
 * @defgroup idxHalGpio GPIO HAL module
 * The idxHalGpio module provides an API to initialize and control GPIO pins.
 * @{
 */
 
/*!
 * @brief GPIO pin selection
 *
 * These values select the GPIO pin that is to be operated on.
 *
 * \note In some IDEX systems several of the values here may map to
 * the same physical pin. For example on CS4, HSO uses the ISO_IO pin.
 */
typedef enum idxHalGpioPinType
{
	IDX_HAL_GPIO_HSO,   //!< Handshake Out (from SE to MCU)
	IDX_HAL_GPIO_HSI,   //!< Handshake In  (from MCU to SE)
	IDX_HAL_GPIO_ISO_IO //!< I/O pin - for example, to control enroll sleeve LED
} idxHalGpioPinType;

/*!
 * @brief GPIO IRQ edge
 *
 * \note Note all SEs have the hardware to implement all of these.
 */
typedef enum {
	IDX_HAL_GPIO_IRQ_DISABLE,      //!< Disable the interrupt
	IDX_HAL_GPIO_IRQ_RISING_EDGE,  //!< Interrupt on low to high transition
	IDX_HAL_GPIO_IRQ_FALLING_EDGE, //!< Interrupt on high to low transition
	IDX_HAL_GPIO_IRQ_BOTH          //!< Interrupt on any transition
} idxHalGpioIrqEdgeType;

/*!
 * @brief GPIO state
 *
 * Indicates the current state of an input pin, or the desired state of
 * an output pin.
 */
typedef enum idxHalGpioStateType
{
	IDX_HAL_GPIO_LOW,   //!< Drive low, or input is low
	IDX_HAL_GPIO_HIGH,  //!< Drive high, or input is high
  IDX_HAL_GPIO_FLOAT, //!< Output only - release the pin to float
  IDX_HAL_GPIO_IGNORE //!< May be used when the value will be ignored by the callee
} idxHalGpioStateType;

/*!
 * @brief GPIO config type
 *
 * Determine whether the pins is to be associated with a hardware block
 * (for example, SPI or UART) or used as a software controlled I/O pin.
 */
typedef enum idxHalGpioConfigType
{
  IDX_HAL_GPIO_HARDWARE,  //!< Pin connected to SE hardware block (not S/W GPIO)
  IDX_HAL_GPIO_SWIO       //!< Select SWIO appropriate to the pin
} idxHalGpioConfigType;

/** @brief Type of listener function callback pointer
 */
typedef void (*idxHalGpioListenerFuncType)(void);

/** @brief Initialize GPIO module on startup.
*/
void idxHalGpioInit(void);

/**
* @brief Initialize GPIO pins associated with handshaking.
*
* Typically the following three steps are required:
* 1. Reconfigure any pins used by the handshake for S/W I/O.
* 2. Perform the start of the heartbeat handshake
* 3. Do any initialization required for hardware communication block.
*
* @return     NONE.
*/
void idxHalGpioInitHandshake(void);

/*!
* @brief Set specific GPIO pin to required state.
*
* @param[in]  pin   Specify which GPIO pin to control.
* @param[in]  state Desired state of the pin.
*/
void idxHalGpioSet(idxHalGpioPinType pin, idxHalGpioStateType state);

/*!
* @brief Get specific GPIO pin's state.
*
* @param[in]  pin    Specify which GPIO pin to interrogate.
* @return The current state of the pin.
*/
idxHalGpioStateType idxHalGpioGet(idxHalGpioPinType pin);

/**
* @brief Set GPIO IRQ edge (or disable)
*
* @param[in]  pin    Specify which GPIO pin to control.
* @param[in]  edge   Edge of irq (or disable)
*
* @return     NONE.
*/
void idxHalGpioSetIRQ(idxHalGpioPinType pin, idxHalGpioIrqEdgeType edge);

/*!
* @brief Configure a GPIO pin
*
* Several of the IDEX systems multiplex several functions onto the same
* pin. For example, for 4-wire SPI communication HSO and SPI_CLK use same pin.
* Similarly for CS4 (ISO7816) HSO and ISO_I/O share a pin, and HSI and
* ISO_CLK share another.
*
* This function allows a pin to be reconfigured to switch between functions.
* This is not intended to provide a general purpose interface to the GPIO
* capabilities the SE. The exact way in which a pin is configured is "known"
* inside the implementation of idxHalGpio (for a given SE) which will only
* implement the capabilities required by the system(s) that are supported on
* that SE.
*
* @param[in]  pin   Selects the pin to be configured.
* @param[in]  cfg   Selects the required configuration type.
* @param[in]  state Specifies an initial value for the output in SWIO mode.
*
* If \b cfg has the value \ref IDX_HAL_GPIO_HARDWARE then the pin is used by
* an SE hardware block (typically the SPI block, or ISO7816 UART), and the
* \b state parameter is ignored (the value \ref IDX_HAL_GPIO_IGNORE may be
* used).
*
* If \b cfg has the value \ref IDX_HAL_GPIO_SWIO the the pin is reconfigured
* to operated under software control. If this involves the selected \b pin
* driving an output onto the pin then the \b val argument indicates the
* initial value to be driven (to avoid a potential glitch due to the time
* between calling this function and idxHalGpioSet() to drive the required
* value).
*
* \note Implementations will ignore calls that are inappropriate to the
* particular IDEX system in use. For example, the serial interface code will
* call this function to switch the HSO pin to select the hardware block
* in order that it becomes driven by the SPI clock on 4-wire SPI systems.
* On 5-wire SPI systems, where there is a dedicated HSO pin, this call
* should be ignored.
*/
void idxHalGpioConfigure(idxHalGpioPinType pin,
  idxHalGpioConfigType cfg, idxHalGpioStateType state);

/*!
* @brief Register a GPIO callback
*
* @param[in]  listener  Callback to register.
*/
void idxHalGpioRegisterListener(idxHalGpioListenerFuncType listener);

/** @}*/
#endif /* _IDX_HAL_GPIO_H_ */
