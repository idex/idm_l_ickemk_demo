/*
 * * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: com_api.h
 * * SCFID: 
 * * Feature: api header file of commnunication
 * * Version: V0.1
 * *
 * * History: 
 * *   2012-01-03 by Luqian
 * *     1. Original version 0.1
 * */
#ifndef COM_API_DEF_H
#define COM_API_DEF_H

#ifndef NULL
	#define NULL ((void*)0)
#endif

void rtnBackup(void);
void IdleMode(void);

//external function
void InitCom(u8 *pbCL4Buf, u8 *pbCTBuf);
void CLKInit(void);
void InitHardware(void);
u8 RxCmdHead(ST_COMCMD *pstCMD);
void RxCmdBody(ST_COMCMD *pstCMD);
void TxResponse (ST_COMCMD *pstCMD);

#define apduGetPipeNum() (g_stCMD.bPipeNum)  //obtain current physical channel number
extern u32 g_dwSNAddr;//UID address
#endif
