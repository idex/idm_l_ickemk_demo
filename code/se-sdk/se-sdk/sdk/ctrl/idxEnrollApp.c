/** ***************************************************************************
 * \file   idxEnrollApp.c
 * \brief  Example enrollment application
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
 
/*  includes
****************************************************************/
#include "idxEnrollApp.h"
#include "idxHalTimer.h"
#include "idxHalSystem.h"
#include "idxSeBioApi.h"
#include "dispatcher.h" // For set_p2() & get_SystemConfig()
#include "idxSeEmkApi.h"

/*  pragmas
****************************************************************/

/*  defines
****************************************************************/

/*  private prototypes
****************************************************************/

/*  constants
****************************************************************/

/*  variables
****************************************************************/

/*  functions
****************************************************************/

// Use symbols from dispatcher.c, accessed via set_p2() &
// get_SystemConfig() - this is messy.

void idxEnrollApp(const idxBuffer_t *pComBuffer)
{
  uint16_t ret;
  
  idxHalTimerDelayMsec(2000); // Wait for the BMCU to sort itself out

  ret = idxBioInitialize(IDX_BIO_INITIALIZE_BATTERY,
    get_SystemConfig(), pComBuffer);
  
  if (ret == IDX_ISO_SUCCESS) {
    
    // TODO - see (from a policy?) whether the secure channel
    // needs to be opened and if so then open it.
    
    // Do the Enroll...
  
    // Currently idxBioOnCardEnroll() has a kludge where it expects
    // to have been called from the apdu_handler() and expects
    // apdup2 to have been set to control the number of fingers
    // and touches.
    //
    // TODO: FIX THIS!
  
    set_p2(0x26); // Two fingers 6 touches
    (void) idxBioOnCardEnroll(idxHalSystemWorkAreas, pComBuffer );
  }
  
  (void) idxBioUninitialize( pComBuffer );
  
  while (true) {
    idxHalSystemSleep();
  }
}