/******************************************************************************
 Copyright 2013-2020 IDEX Biometrics ASA. All Rights Reserved. 
 www.idexbiometrics.com

 IDEX Biometrics ASA is the owner of this software and all intellectual 
 property rights in and to the software. The software may only be used together 
 with IDEX fingerprint sensors, unless otherwise permitted by IDEX Biometrics 
 ASA in writing.

 This copyright notice must not be altered or removed from the software.

 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 Biometrics ASA has no obligation to support this software, and the software is 
 provided "AS IS", with no express or implied warranties of any kind, and IDEX 
 Biometrics ASA is not to be liable for any damages, any relief, or for any 
 claim by any third party, arising from use of this software.

 Image capture and processing logic is defined and controlled by IDEX 
 Biometrics ASA in order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef _IDX_APP_DEFINES_
#define _IDX_APP_DEFINES_

#include "IDEX_M_Interface.h"

//----------------- MWF DEFAULT ~~~~~~~~~~~~~~~~~~
#define IDEX_MWF_DEFAULT 	MWF_UNINITIALIZED_VALUE

#endif	//_IDX_APP_DEFINES_
