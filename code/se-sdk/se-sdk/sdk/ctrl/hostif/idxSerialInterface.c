/** ***************************************************************************
 * \file   idxSerialInterface.c
 * \brief  Code common between UART/SPI, etc. host interfaces.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2012-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include "idxSerialInterface.h"
#include "_idxSerialInterface.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
