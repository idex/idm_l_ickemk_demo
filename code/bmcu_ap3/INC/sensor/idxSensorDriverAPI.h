/**************************************************************************//*
* \file  idxSensorDriverAPI.h
* \brief header for object for Common SPI communication
* \author IDEX ASA
* \version 0.0.0 IX63SW4.x SSP PC stack
******************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/
#ifndef __SENSORDRIVERAPI_H__
#define __SENSORDRIVERAPI_H__

#include "idxBaseTypes.h"

/**
* idxspi_dbgCounters - structure to pass to IDEX_EAGLE_IOCTL_RW_SPI_MESSAGE command
*/
#define COUNTERS_MAX_NUM        10
#define dbg_cntr_ERR_SPI_IO     0
#define dbg_cntr_ERR_NOACK      1
#define dbg_cntr_ERR_TIMEOUT    2
#define dbg_cntr_ERR_NOTREADY   3
#define dbg_cntr_ERR_CRC        4
#define dbg_cntr_BAD_DATA       5
#define dbg_cntr_WRITE_BYTES    6
#define dbg_cntr_READ_BYTES     7
#define dbg_cntr_READY_INTR     8
#define dbg_cntr_LAST_ERR       9
typedef struct idx_dbgCounters {
    unsigned int counters[COUNTERS_MAX_NUM];
} idx_dbgCounters_t;

#ifdef  __cplusplus
extern "C" {
#endif

#define SENSORDRIVERCALLAPI

/**
* @brief Function pointer defining callback for opening the device.
*
* @param[in] void *arg: Environment specific context.
*
* @return void* Returns created device handle.
*/
typedef void*( SENSORDRIVERCALLAPI *fn_DRV_COMM_Open )(void* arg);

/**
* @brief Function pointer defining callback for closing the device.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
*
* @return void
*/
typedef void(SENSORDRIVERCALLAPI *fn_DRV_COMM_Close)(void* pDev);

/**
* @brief Function pointer defining callback for transferring/receiving data.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[in/out] unsigned char *buffer: Buffer to hold transferred/received data.
* @param[in] int tx_size: Bytes to transfer.
* @param[in] int rx_size: Data size to be copied to the buffer from the received data.
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int(SENSORDRIVERCALLAPI *fn_DRV_COMM_Transfer)(void *pDev, unsigned char *buffer, int tx_size, int rx_size);
#define ICP_WRITE_MAX_PAYLOAD_SIZE  (536)
#define ICP_READ_MAX_PAYLOAD_SIZE   (36*1024)

/**
* @brief Function pointer defining callback for resetting the device.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[in] int do_warm_reset: 0 - cold reset, non zero - warm reset.
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int( SENSORDRIVERCALLAPI *fn_DRV_COMM_HWReset )( void *pDev, int do_warm_reset );
#define COLD_RESET  (0)
#define WARM_RESET  (1)

/**
* @brief Function pointer defining callback for getting ready pin.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[out] unsigned char *byte: The byte which indicates ready level (0-low/1-high).
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int(SENSORDRIVERCALLAPI *fn_DRV_COMM_GetReadyPin)(void *pDev, unsigned char *byte);

/**
* @brief Function pointer defining callback for waiting till sensor switches to the desired pin state or timeout.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[in] int timeout_ms: The timeout in milliseconds.
* @param[in] int pin_state: The pin state for which the function will wait for.
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int(SENSORDRIVERCALLAPI *fn_DRV_COMM_WaitForReadyState)(void *pDev, int timeout_ms, int pin_state);

//PARAMETERS
#define DRV_PARAMETER_BAUDRATE  (1)             /**< DRV_PARAMETER_BAUDRATE - Parameter ID for the baudrate. */

#define DRV_PARAMETER_PROTO     (2)             /**< DRV_PARAMETER_PROTO - Parameter ID for the protocol. */
#define DRV_PARAMETER_PROTO_SPI     (0)         /**< DRV_PARAMETER_PROTO_SPI - SPI protocol. */
#define DRV_PARAMETER_PROTO_FTDI    (1)         /**< DRV_PARAMETER_PROTO_FTDI - FTDI protocol. */

#define DRV_PARAMETER_SENSORSTATE  (3)          /**< DRV_PARAMETER_SENSORSTATE - Parameter ID for the sensor state. */
#define DRV_PARAMETER_SENSORSTATE_SYNC   (0)    /**< DRV_PARAMETER_SENSORSTATE_SYNC - Sensor synchronous state. */
#define DRV_PARAMETER_SENSORSTATE_ASYNC  (1)    /**< DRV_PARAMETER_SENSORSTATE_ASYNC - Sensor asynchronous state. */

#define DRV_PARAMETER_READYLINEPOLARITY  (4)    /**< DRV_PARAMETER_READYLINEPOLARITY - Parameter ID for the ready line polarity. */

#define DRV_PARAMETER_PM_MODE  (5)              /**< DRV_PARAMETER_PM_MODE - Parameter ID for power management modes. */
#define DRV_PARAMETER_PM_MODE_NONE    (0)       /**< DRV_PARAMETER_PM_MODE_NONE - unspecified/invalid mode. */
#define DRV_PARAMETER_PM_MODE_LOW     (1)       /**< DRV_PARAMETER_PM_MODE_LOW - low power for WFF mode - SPI disabled, CPU(400 kHz). */
#define DRV_PARAMETER_PM_MODE_MEDIUM  (2)       /**< DRV_PARAMETER_PM_MODE_FAST - medium power mode for image transfer, SPI (12 MHz), CPU (24 MHz). */
#define DRV_PARAMETER_PM_MODE_HIGH    (3)       /**< DRV_PARAMETER_PM_MODE_HIGH - high power for image processing, matching and crypto - SPI disabled / CPU (64 MHz). */
#define DRV_PARAMETER_PM_MODE_MAX     (255)     /**< DRV_PARAMETER_PM_MODE_MAX - Maximum acceptable mode number */
                                                /**<      range (1-3)   LOW/MEDIUM/HIGH modes that could be set from sensorStack/ICK modules for specifying ongoing actions (wff/transfer/processing). */
                                                /**<      range (4-255) could be used for specific PM modes that may used directly from customer application. */

#define DRV_PARAMETER_TRANSACTION_STATE  (6)   /**< DRV_PARAMETER_TRANSACTION_STATE - Parameter ID for the transaction state. */
#define DRV_PARAMETER_ATOMIC_TRANSACTION_BEGIN  (0)   /**< Begin transaction - need to disabled interrupt. */
#define DRV_PARAMETER_ATOMIC_TRANSACTION_END    (1)   /**< End transaction - need to restore interrupts. */

#define DRV_PARAMETER_SENSOR_SIGNALS               (7)   /**< DRV_PARAMETER_SENSOR_SIGNALS - Parameter ID for generating suspend/resume/abort signals. */
#define DRV_PARAMETER_SENSOR_SIGNAL_ABORT          (0)   /**< abort. */
#define DRV_PARAMETER_SENSOR_SIGNAL_SUSPEND_RESUME (1)   /**< suspend/resume. */

#define DRV_PARAMETER_SENSOR_INIT_MODE               (8)   /**< DRV_PARAMETER_SENSOR_INIT_MODE - Parameter ID for reading MCU init mode (contact vs contactless). */
#define DRV_PARAMETER_SENSOR_INIT_MODE_UNINITIALIZED (0)   /**< none. */
#define DRV_PARAMETER_SENSOR_INIT_MODE_CONTACT       (1)   /**< contact. */
#define DRV_PARAMETER_SENSOR_INIT_MODE_CONTACTLESS   (2)   /**< contactless. */

/**
* @brief Optional function pointer defining callback for getting parameter.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[in] short id: The parameter ID (one from the DRV_PARAMETER_*).
* @param[out] int* value: The parameter value.
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int(SENSORDRIVERCALLAPI *fn_DRV_COMM_GetParameter)(void *pDev, short id, int* value);

/**
* @brief Optional function pointer defining callback for setting parameter.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[in] short id: The parameter ID (one from the DRV_PARAMETER_*).
* @param[in] int value: The parameter value.
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int( SENSORDRIVERCALLAPI *fn_DRV_COMM_SetParameter )( void *pDev, short id, int value);

/**
* @brief Optional function pointer defining callback for getting debug counters.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
* @param[out] int* counters: The debug counters.
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int(SENSORDRIVERCALLAPI *fn_DRV_COMM_GetDebugCounters)(void *pDev, int* counters);

/**
* @brief Optional function pointer defining callback for resetting debug counters.
*
* @param[in] void *pDev: The device handle (returned by fn_DRV_COMM_Open).
*
* @return int
*   0  - success
*   <0 - system errors from errno.h
*/
typedef int( SENSORDRIVERCALLAPI *fn_DRV_COMM_ResetDebugCounters )( void * pDev);

/**
* @brief Function pointer defining callback for retrieving security parameters.
*
*
* @param[in] void *pSecCtx: Pointer to the security context.
* @param[in] unsigned short id: Id of parameter to be returned. Current set of
* parameters are:
* #define DRV_SEC_PARAMETER_KEY_LENGTH           (0)
* #define DRV_SEC_PARAMETER_AUTHTAG_LENGTH       (1)
* #define DRV_SEC_PARAMETER_KEY_STATUS           (2)
* where the key status values are defined as:
* #define DRV_SEC_PARAMETER_KEY_STATUS_UNINTIALIZED     (0)
* #define DRV_SEC_PARAMETER_KEY_STATUS_INITIALIZING     (1)
* #define DRV_SEC_PARAMETER_KEY_STATUS_INITIALIZED      (2)
* @param[out] int *value: Should contain the value of requested parameter.
*
* @return int
*    0 - success
*   <0 - error
*/
typedef int( *fn_SEC_DRV_GetSecParameter )( void* pSecCtx, unsigned short id, int* value );

//Secure PARAMETERS
#define DRV_SEC_PARAMETER_KEY_LENGTH      (0)

#define DRV_SEC_PARAMETER_AUTHTAG_LENGTH  (1)

#define DRV_SEC_PARAMETER_KEY_STATUS      (2)
#define DRV_SEC_PARAMETER_KEY_STATUS_UNINTIALIZED (0)
#define DRV_SEC_PARAMETER_KEY_STATUS_INITIALIZING (1)
#define DRV_SEC_PARAMETER_KEY_STATUS_INITIALIZED  (2)


/**
* @brief Function pointer defining callback for encrypting/padding data.
*
*
* @param[in] void *pSecCtx: Pointer to the security context.
* @param[in/out] unsigned short *sz: Size of data to be encrypted on input. Size of
* encrypted data including padding on output.
* @param[in/out] int *data: Data to be encrypted on input. Encrypted data on output.
* The buffer pointed to by data should be large enough to include any padding added.
*
* @return int
* 0 - success
* <0 - error
*/
typedef int( *fn_SEC_DRV_Encrypt )( void* pSecCtx, unsigned short *sz, unsigned char *data );

/**
* @brief Function pointer defining callback for decrypting/unpadding data.
*
*
* @param[in] void *pSecCtx: Pointer to the security context.
* @param[in/out] unsigned short *sz: Size of data to be decrypted on input. Size of
* decrypted data after removing padding on output.
* @param[in/out] int *data: Data to be decrypted on input. Decrypted data on output.
*
* @return int
* 0 - success
* <0 - error
*/
typedef int( *fn_SEC_DRV_Decrypt )( void* pSecCtx, unsigned short *sz, unsigned char *data );

/**
* @brief Function pointer defining callback for generation authenication tag.
*
*
* @param[in] void *pSecCtx: Pointer to the security context.
* @param[in/out] unsigned short *sz: Size of data on which to generate authentication tag on input.
* Size of encrypted including authentication tag on output.
* @param[in/out] int *data: Data on which to generate authentication tag on input.
* Data with authentication tag appended on output. The buffer pointed to by data
* should be large enough to include the authentication tag.
*
* @return int
* 0 - success
* <0 - error
*/
typedef int( *fn_SEC_DRV_GenerateAuthTag )( void* pSecCtx, unsigned short *sz, unsigned char *data );

/**
* @brief Function pointer defining callback for authentication tag.
*
*
* @param[in] void *pSecCtx: Pointer to the security context.
* @param[in/out] unsigned short *sz: Size of data to be authenticated on input. Size of data
* after removing authentication tag on output.
* @param[in] int *data: Data to be authenticated.
*
* @return int
* 0 - authentication success
* <0 - error
*/
typedef int( *fn_SEC_DRV_VerifyAuthTag )( void* pSecCtx, unsigned short *sz, unsigned char *data );

typedef struct SensorDriverCallbacks_t
{
    fn_DRV_COMM_Open                drvcomm_Open;
    fn_DRV_COMM_Close               drvcomm_Close;
    fn_DRV_COMM_Transfer            drvcomm_Transfer;
    fn_DRV_COMM_HWReset             drvcomm_HWReset;
    fn_DRV_COMM_GetReadyPin         drvcomm_GetReadyPin;
    fn_DRV_COMM_WaitForReadyState   drvcomm_WaitForReadyState;

    //optional interfaces
    fn_DRV_COMM_GetParameter        drvcomm_GetParameter;
    fn_DRV_COMM_SetParameter        drvcomm_SetParameter;
    fn_DRV_COMM_GetDebugCounters    drvcomm_GetDebugCounters;
    fn_DRV_COMM_ResetDebugCounters  drvcomm_ResetDebugCounters;

    //optional secure interfaces
    fn_SEC_DRV_GetSecParameter      drvsec_GetParameter;
    fn_SEC_DRV_Encrypt              drvsec_Encrypt;
    fn_SEC_DRV_Decrypt              drvsec_Decrypt;
    fn_SEC_DRV_GenerateAuthTag      drvsec_GenerateAuthTag;
    fn_SEC_DRV_VerifyAuthTag        drvsec_VerifyAuthTag;
    fn_SEC_DRV_Decrypt              drvsec_ImageDecrypt;
    fn_SEC_DRV_VerifyAuthTag        drvsec_ImageVerifyAuthTag;
} SensorDriverCallbacks, *SensorDriverCallbacks_ptr;

CFUNC_DECLSPEC void CDECL_DECLSPEC FillDrvFuncList( SensorDriverCallbacks* f, void* env );

#ifdef  __cplusplus
}
#endif

#endif // __SENSORDRIVERAPI_H__
