/*
 *  Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: cl4.c
 *  SCFID: 
 *  Feature: main procedure of the cos
 *  Version: V0.1
 * 
 *  History: 
 *    2012-01-06 
 *      1. Original version 0.1
 * */
#include "halglobal.h"
#include "idxSDK.h"

u8  g_pbCTBUF[APDU_BUFFER_SIZE];

extern void demoMain(void);
extern void DemoEmvInit(void);
int main (void)
{
	clkInit(g_pbComCfg[CPU_CT_CLK],g_pbComCfg[MINFS]);
	__disable_irq();
	g_stCMD.pbBuf = g_pbCTBUF;
	ioInit(g_pbCTBUF, g_pbCTBUF);
	DemoEmvInit();
	BackupPC();
	__enable_irq();

	//init ram1 by hardware
	initRam1();
	_halInitAllSecurity();	
	idxSdkInit();
	while(1)
	{	
		if(ioRxCmdHead(0))
		{
			demoMain();
			ioTxResponse();
		}
	}
}
