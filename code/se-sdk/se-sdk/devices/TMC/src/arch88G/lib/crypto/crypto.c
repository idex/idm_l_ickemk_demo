/*
* Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
* All rights reserved.
*
* FileName: crypto.c
* SCFID:
* Feature: CRC algorithm
* Version: V0.1
*
* History: 
*   2014-11-20
*     1. Original version 0.1
*/
#include "halglobal.h"
#include "support.h"

/*
* Function: void CRC(u8 * pDataOut, u8 * pDataIn, u32 len, u32 initValue, u8 mode)
* Description: CRC Calculation.
* Input:  pDataIn - Source data.
*         len - Length of source data.
*         initValue - Init value.
*         mode - config of CRCCON1
*    						bit5-4 CRC type
*    							1x:	CRC32
*    							00:	CRC16-CCITT
*    						bit3 result out order
*    							1: high byte out first
*    							0: low byte out first
*    						bit2	result byte endian
*    							1:	change
*    							0: 	not
*    						bit1	input byte endian
*    							1:	change
*    							0: 	not
* Output: dataout - Output random data.
* Return: -
* Other:
*/
void CRC(u8 * pDataOut, u8 * pDataIn, u32 len, u32 initValue, u8 mode)
{
	CLKALGCON |= 0x01; // enable CRC clk
	
	if (mode & 0x20)
	{
		CRCCON1 = 0x20;
	}
	else
	{
		CRCCON1 = 0x00;
	}
	
	CRCCON1 = mode;
	CRCCON = initValue;

	if (mode & 0x20)
	{
		while (len--)
			*(u8 *)(&CRCDAT) = *pDataIn++;
		*(u32 *)pDataOut = CRCDAT; 
	}
	else
	{
		while (len--)
		{
			CRCDAT = *pDataIn++;
		}
		
		*pDataOut = CRCDAT & 0xff;
		*(pDataOut+1) = CRCDAT & 0xff;
	}
	CLKALGCON &= ~0x01; // disable CRC clk
}

/*
* Function: void CRC16(u8 *iv, u8 *buf, u32 len)
* Description: CRC Calculation.
* Input:  buf - Source data.
*         len - Source data length.
*         iv - Initialization vector. 16 bits length.
* Output: iv - Output CRC. 16 bits length.
* Return: -
* Other:
*/
void CRC16(u8 *iv, u8 *buf, u32 len)
{
	u32 initValue = (u16)(iv[0]<<8) | iv[1];
	CLKALGCON |= 0x01;		// enable CRC clk
	CRCCON1 = 0x00; // Fisrt set to 0, then set to the using mode.
	CRCCON1 = 0x08;
	CRCCON = initValue;

	while (len--)
	{
		CRCDAT = *buf++;
	}
	
	*iv = CRCDAT & 0xff;
	*(iv+1) = CRCDAT & 0xff;

	CLKALGCON &= ~0x01; // disable CRC clk
}

/*
* Function: void CRC32(u8 *iv, u8 *buf, u32 len)
* Description: CRC Calculation.
* Input:  buf - Source data.
*         len - Source data length.
*         iv - Initialization vector. 32 bits length.
* Output: iv - Output CRC. 32 bits length.
* Return: -
* Other:
*/
void CRC32(u8 *iv, u8 *buf, u32 len)
{
	u32 initValue = B2LT(iv);
	u32 temp;
	u8 i;

	// initValue reflection
	temp = 0;
	for (i = 0; i < 16; i++)
	{
		if (initValue & ((u32)0x01 << i))
		{
			temp |= (u32)0x80000000 >> i;
		}

		if (initValue & ((u32)0x80000000 >> i))
		{
			temp |= (u32)0x01 << i;
		}
	}
	initValue = temp;

	CLKALGCON |= 0x01; // enable CRC clk
	CRCCON1 = 0x20; // Fisrt set to 0, then set to the using mode.
	CRCCON1 = 0x26;
	CRCCON = initValue;

	while (len--)
	{
		*(u8 *)(&CRCDAT) = *buf++;
	}
	
	temp = CRCDAT;
	
	*(iv) = temp & 0xff;
	*(iv+1) = (temp >> 8) & 0xff;
	*(iv+2) = (temp >> 16) & 0xff;
	*(iv+3) = (temp >> 24) & 0xff;

	CLKALGCON &= ~0x01; // disable CRC clk
}
