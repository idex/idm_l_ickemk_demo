/*******************************************************************************
* File Name: idxVersion.h
*
*  Description:
*   This is the definitive include file for IDEX SW Version
*
*  Revision History:
*

**************************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __IDX_SW_VERSION_H__
#define __IDX_SW_VERSION_H__

#define IDX_VERSION			2
#define IDX_REVISION		6
#define IDX_BUILD			537
#define IDX_FUZE_ID			"200605-318-537"

#endif // __IDX_SW_VERSION_H__
