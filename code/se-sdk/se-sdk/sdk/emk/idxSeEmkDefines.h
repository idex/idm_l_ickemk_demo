/******************************************************************************
* \file  idxSeEmkDefines.h
* \brief Defines for the SE matcher library
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#ifndef __IDX_SE_EMK_DEFINES_H__
#define __IDX_SE_EMK_DEFINES_H__

#include <stdint.h>
#include <stdbool.h>
#include "idxIsoErrors.h"
#include "idxSeSdkTypes.h"

#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

//~~~~~~~~~~~~~~~ HOST I/F related definition ~~~~~~~~~~~~~~~
#define GETVERSION_CMD     0x30
#define DELETERECORD_CMD   0x11
#define READ_DATA_CMD      0x50
#define PREPAREIMAGE_CMD   0x02
#define GETTEMPLATE_CMD    0x05
#define MERGETEMPLATES_CMD 0x06
#define MATCHTEMPLATES_CMD 0x07
#define STOREBLOB_CMD      0x18
#define LISTRECORDS_CMD    0x12
#define SETPARAMETER_CMD   0x09
#define GETPARAMETER_CMD   0x08

#define GETVERSION_CMD_DATA_SIZE     (0)
#define DELETERECORD_CMD_DATA_SIZE   (2)
#define READ_DATA_CMD_DATA_SIZE      (4)
#define PREPAREIMAGE_CMD_DATA_SIZE   (1)
#define GETTEMPLATE_CMD_DATA_SIZE    (1)
#define MERGETEMPLATES_CMD_DATA_SIZE (4)
#define MATCHTEMPLATES_CMD_DATA_SIZE (6)
#define STOREBLOB_CMD_DATA_SIZE      (5)

#define HDR_SIZE           ( 6 )   // including CRC
#define TEMPL_SZ_OFF       ( 0 )
#ifndef MESSAGE_CHUNK
#define MESSAGE_CHUNK      ( 0x40 )
#endif
#ifndef MESSAGE_LAST
#define MESSAGE_LAST       ( 0x80 )
#endif
#ifndef RETURN_SIZE
#define RETURN_SIZE        ( 0x10 )
#endif
#define SE_MESSAGE_MASK    ( 0x40 )

#define FLAGS_PHASE_1      ( 0x00 )
#define FLAGS_PHASE_2      ( 0x10 )
#define MASK_LEN           ( 0 )
#define NUM_TEMPLATES      ( 1 )

// Template Info Definitions
#define IMG_QUAL_OFF       ( 0 )
#define TEMPL_QUAL_OFF     ( 2 )
#define NUM_FEAT_OFF       ( 4 )
#define TEMPL_SIZE_OFF     ( 6 )

// From idxBaseDefines.h
typedef enum __idex_SeType
{
    SE_SLE78 = 1,
    SE_THD89,
    SE_CIU9872B_01,        
    SE_SLC52BML
}
idex_SeType_t;

typedef struct __idex_FlashTemplateEntry
{
	unsigned char finger;
	unsigned char touch;
	unsigned char blobIndex;
} idex_FlashTemplateEntry_t;

typedef struct __idx_MatcherWorkArea
{
	uint8_t *pointer; // Pointer to start of area
	uint16_t size; // Size of the area (in bytes)
} idx_MatcherWorkArea_t;

// This defines the maximum transfer size allowed in the mcuComms callback for send or receive - must be a multiple of 8 bytes
#define MAX_SE_TRANSFER_SZ     ( 224 )

typedef enum __idx_SeEmkSecurityLevel
{
    SE_EMK_SECURITY_LVL_4  = 4,
    SE_EMK_SECURITY_LVL_5  = 5,
    SE_EMK_SECURITY_LVL_6  = 6,
    SE_EMK_SECURITY_LVL_7  = 7,
    SE_EMK_SECURITY_LVL_8  = 8,
    SE_EMK_SECURITY_LVL_9  = 9,
    SE_EMK_SECURITY_LVL_10 = 10,
    SE_EMK_SECURITY_LVL_12 = 12,
    SE_EMK_SECURITY_LVL_15 = 15,
    SE_EMK_SECURITY_LVL_20 = 20,
}
idx_SeEmkSecurityLevel_t;

typedef enum __idx_SeEmkDeleteFingerMode
{
	SE_EMK_DELETE_ENROLLMENT_TEMPLATE = 0,
	SE_EMK_DELETE_VERIFICATION_TEMPLATE = 1,
}
idx_SeEmkDeleteFingerMode_t;

typedef struct __idx_SeEmkConfig
{
    const idxBuffer_t *pIdxBuffer;
    const idx_MatcherWorkArea_t  *pWorkAreas;   // pointer to the externally allocated (contiguous) RAM sector for use by the matcher
	idx_SeEmkSecurityLevel_t securityLevel;     // match security level
	uint8_t maxTouchesPerFinger;                // max number of touches per finger
	uint8_t maxEnrolledFingers;                 // max number of enrolled fingers
	uint8_t *pTemplateFlashPtr;                 // template flash pointer
	uint32_t templateFlashSize;                 // template flash size in bytes
}
idx_SeEmkConfig_t;

#endif //__IDX_SE_EMK_DEFINES_H__
