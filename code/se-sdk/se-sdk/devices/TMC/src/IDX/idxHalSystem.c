/** ***************************************************************************
 * \file   idxHalSystem.c
 * \brief  Implementation of System HAL API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

/**********************************************************************************
 *  HAL - Implementation
 *
 ***********************************************************************************/
#include "idxHalSystem.h"
#include "idxHalSpi.h"
#include "idxHalAlgo.h"

#include "halglobal.h"
#include "idxSeBioAPI.h"

/*  defines
****************************************************************/

//  ****  Enrollment Parameters  ****
#define kSE_Integrator_maxEnrollFingerCount        (2)                  //  maximum number of fingers to be enrolled
#define kSE_Integrator_maxEnrollTouchCount         (8)                  //  maximum number of touches per finger enrolled
#define kSE_Integrator_maxPerTouchTemplateSize     2304                 //  maximum space allocated for flash storage per enroll touch template
//  maximum space allocated for flash storage of enroll templates
#define kSE_Integrator_maxFlashTemplateSize        (kSE_Integrator_maxEnrollFingerCount * kSE_Integrator_maxEnrollTouchCount * kSE_Integrator_maxPerTouchTemplateSize)

/* THD89 Memory Map
 * RFRAM[256 KB ] 0x20200000-0x202000ff
 * RAM1[   4 KB ] 0x20100000-0x20100fff
 * RAM0[ 2.5 KB ] 0x200017f0-0x200021FF
 * OTP[  0.5 KB ] 0x14000000-0x140001ff
 * NVM[   96 KB ] 0x0C050000-0x0C067fff
 * UserROM[320KB ]0x0C000000-0x0C00ffff
 * LIBROM[ 64KB ] 0x04000000-0x00100fff
 *
 * NVM usage:
 * COS      : 0x0c050000 - 0x0c050fff (4KB)
 * IDM-L    : 0x0c051000 - 0x0c05b1ff (40.5KB) 40KB for 16 templates + 512 bytes for enrolment table
 * GAP      : 0x0c05b200 - 0x0c05ffff (19.5KB)
 * SC(keys) : 0x0c060000 - 0x0c0603ff (1 KB) 2 NVM sectors
 * Policies : 0x0c060400 - 0x0c0605ff (0.5 KB) 1 NVM sector
 * DE state : dynamic enrolment state [TBD]
 */
#define pTemplateFlash                             ((void *)0x0c051000) //  0x1000 bytes used by demo app

#define kSE_Integrator_RAM0_Size                   2576
#define kSE_Integrator_RAM1_Size                   (4L << 10)

static uint8_t RAM0[kSE_Integrator_RAM0_Size] __attribute__((at(0x200017f0)));
uint8_t capture_buffer[0x120];

uint8_t *matcherBuffer;

#define RAM1                                       ((uint8_t*)0x20100000) // RAM1   4KB
//#define pNvmKeys  ((void *)0x0c060000)

const struct idxBioPolicies_s idxHalSystemPolicies __attribute__((at(0x0c060400))) =
{
#include "idxBioPolicies.i" // content of this file, something like: 0,2,6,1,260
};

const struct idxHalSystemTemplateArea_s idxHalSystemTemplateArea =
{
	pTemplateFlash, kSE_Integrator_maxFlashTemplateSize
};

const idx_MatcherWorkArea_t idxHalSystemWorkAreas[2] =
{
	{RAM0, kSE_Integrator_RAM0_Size},
	{RAM1, kSE_Integrator_RAM1_Size}
};

// Possbile clock speeds
#define IDEX_fsys_72MHz 	0x00		//default
#define IDEX_fsys_36MHz 	0x10
#define IDEX_fsys_24MHz 	0x20
#define IDEX_fsys_18MHz 	0x30
#define IDEX_fsys_14_4MHz 	0x40
#define IDEX_fsys_12MHz 	0x50
#define IDEX_fsys_10_3MHz 	0x60
#define IDEX_fsys_9MHz 		0x70
#define IDEX_fsys_8MHz 		0x80
#define IDEX_fsys_7_2MHz 	0x90
#define IDEX_fsys_6_5MHz 	0xA0
#define IDEX_fsys_6MHz 		0xB0
#define IDEX_fsys_5_5MHz 	0xC0
#define IDEX_fsys_5_1MHz 	0xD0
#define IDEX_fsys_4_8MHz 	0xE0
#define IDEX_fsys_4_5MHz 	0xF0

#define IDEX_SYSTEM_LOWEST_SPEED	IDEX_fsys_4_5MHz
#define IDEX_SYSTEM_LOW_SPEED		IDEX_fsys_24MHz
#define IDEX_SYSTEM_HIGH_SPEED		IDEX_fsys_72MHz

#define UNUSED(p) if(p){}
  
//////////////////////////////////
// Interface functions
//////////////////////////////////
void idxHalSystemInit(void)
{
	idxHalSpiInit();
	idxHalAlgoInit();
	matcherBuffer=RAM0;
}

void idxHalSystemSleep(void){
  uint32_t sysClk = CLKSYSSEL;

  CLKSYSSEL = IDEX_fsys_4_5MHz;

  IdleMode();

  CLKSYSSEL = sysClk; // Restore clock frequency
}

void idxHalSystemWaitForInt(uint8_t mode, const volatile bool *flag)
{
  UNUSED(mode)
  
  __disable_irq();
  if (! (*flag)) {
    idxHalSystemSleep();
  }
  __enable_irq();
  // Notes in ARM application note 321 in section:
  // "Enabling interrupts using CPS instructions and MSR instructions"
  // were reviewed, and concluded that no __ISB() is required here.
}

void idxHalSystemSetPowerState(idxSePowerState powerState)
{
  uint32_t frequency = IDEX_fsys_4_5MHz;

  switch (powerState) {
    case idxPowerStateSE:     frequency = IDEX_fsys_72MHz;  break;
    case idxPowerStateBio:    frequency = IDEX_fsys_24MHz;  break;
    case idxPowerStateComms:  frequency = IDEX_fsys_24MHz;  break;
    case idxPowerStateLow:    frequency = IDEX_fsys_24MHz;  break;
    case idxPowerStateLowest: frequency = IDEX_fsys_4_5MHz; break;
    default:                  frequency = IDEX_fsys_4_5MHz; break;
  }

  CLKSYSSEL = frequency; // Set clock frequency
}

//////////////////////////////////
