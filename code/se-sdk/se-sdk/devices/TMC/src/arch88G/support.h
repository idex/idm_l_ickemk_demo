/*
 * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: support.h
 * SCFID: 
 * Feature: header file for support.c
 * Version: V0.1
 *
 * History: 
 *   2014-11-19 by wangf
 *     1. Original version 0.1
 */
#ifndef		__SUPORT_H__
#define		__SUPORT_H__

void TransBigLittleBW(u8 *pbIn, u32 *pwOut);
void TransBigLittleBB(u8 *pbIn, u8 *pbOut);
void TransBigLittleWB(u32 ulIn, u8 *pbOut);

#endif
