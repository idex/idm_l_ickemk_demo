/******************************************************************************
 Copyright 2013-2020 IDEX Biometrics ASA. All Rights Reserved. 
 www.idexbiometrics.com

 IDEX Biometrics ASA is the owner of this software and all intellectual 
 property rights in and to the software. The software may only be used together 
 with IDEX fingerprint sensors, unless otherwise permitted by IDEX Biometrics 
 ASA in writing.

 This copyright notice must not be altered or removed from the software.

 DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 Biometrics ASA has no obligation to support this software, and the software is 
 provided "AS IS", with no express or implied warranties of any kind, and IDEX 
 Biometrics ASA is not to be liable for any damages, any relief, or for any 
 claim by any third party, arising from use of this software.

 Image capture and processing logic is defined and controlled by IDEX 
 Biometrics ASA in order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __IDX_SERIAL_HOST_INTERFACE_H__
#define __IDX_SERIAL_HOST_INTERFACE_H__

#include "idxBaseDefines.h"

//////////////////////////////////////////////////////
//
// maximum size of a template
//
#define MAXLEN_TEMPLATE		350
#define MAXLEN_TEMPLATE_M	4720

//////////////////////////////////////////////////////
//
// maximum number of merged templates
#define MAX_MERGE			4

//////////////////////////////////////////////////////
//
// commands available
//
#define IDX_HOSTIF_CMD_BIOMETRIC	0x00
#define IDX_HOSTIF_CMD_DATABASE		0x10
#define IDX_HOSTIF_CMD_ENCRYPTION	0x20
#define IDX_HOSTIF_CMD_UPDATE		0x30
#define IDX_HOSTIF_CMD_EAGLE		0x40
#define IDX_HOSTIF_CMD_MANAGEMENT	0x50
#define IDX_HOSTIF_CMD_DEBUG		0x60

//////////////////////////////////////////////////////
//
// sensor commands
//
enum
{
	IDX_HOSTIF_CMD_INITIALIZE = IDX_HOSTIF_CMD_BIOMETRIC,
	IDX_HOSTIF_CMD_UNINITIALIZE,
	IDX_HOSTIF_CMD_PREPAREIMAGE,
	IDX_HOSTIF_CMD_ACQUIRE = IDX_HOSTIF_CMD_PREPAREIMAGE,
	IDX_HOSTIF_CMD_ABORT,
	IDX_HOSTIF_CMD_GETIMAGE,
	IDX_HOSTIF_CMD_GETTEMPLATE,
	IDX_HOSTIF_CMD_MERGETEMPLATES,
	IDX_HOSTIF_CMD_MATCHTEMPLATES,
	IDX_HOSTIF_CMD_GETPARAMETER,
	IDX_HOSTIF_CMD_SETPARAMETER,
	IDX_HOSTIF_CMD_GETRAWDATA,						// GETBASELINE_CMD
	IDX_HOSTIF_CMD_RESTOREPARAM,
	IDX_HOSTIF_CMD_STOREPARAM,
	IDX_HOSTIF_CMD_TEST,
	IDX_HOSTIF_CMD_GETSENSORINFO,
	IDX_HOSTIF_CMD_BIOMETRICSCRIPT
} ;

//////////////////////////////////////////////////////
//
// storage commands
//
enum
{
	IDX_HOSTIF_CMD_STORERECORD = IDX_HOSTIF_CMD_DATABASE,
	IDX_HOSTIF_CMD_DELETERECORD,
	IDX_HOSTIF_CMD_LISTRECORD,
	IDX_HOSTIF_CMD_GETRECORDDETAILS,
	IDX_HOSTIF_CMD_SAVEDB,
	IDX_HOSTIF_CMD_RESTOREDB,
	IDX_HOSTIF_CMD_GETRECORDTEMPLATE,
	IDX_HOSTIF_CMD_ENROLL,
	IDX_HOSTIF_CMD_STORE_BLOB
} ;

//////////////////////////////////////////////////////
//
// encryption commands
//
enum
{
	IDX_HOSTIF_CMD_CREATEKEYS = IDX_HOSTIF_CMD_ENCRYPTION,
	IDX_HOSTIF_CMD_RESET,
	IDX_HOSTIF_CMD_OPENCHANNEL,
	IDX_HOSTIF_CMD_CLOSECHANNEL,
	IDX_HOSTIF_CMD_GETUID,
	IDX_HOSTIF_CMD_INITIALIZE_UPDATE,
	IDX_HOSTIF_CMD_EXTERNAL_AUTHENTICATE,
	IDX_HOSTIF_CMD_PUTKEY,
	IDX_HOSTIF_CMD_FLASHUPDATE,
	IDX_HOSTIF_CMD_GET_RANDOM,
	IDX_HOSTIF_CMD_LOCK,
	IDX_HOSTIF_CMD_AESENC,
	IDX_HOSTIF_CMD_AESDEC
} ;

//////////////////////////////////////////////////////
//
// update commands
//
enum
{
	IDX_HOSTIF_CMD_GETVERSION = IDX_HOSTIF_CMD_UPDATE,
	IDX_HOSTIF_CMD_UPDATESTART,
	IDX_HOSTIF_CMD_UPDATEDETAILS,
	IDX_HOSTIF_CMD_UPDATEDATA,
	IDX_HOSTIF_CMD_UPDATEHASH,
	IDX_HOSTIF_CMD_UPDATEEND,
	IDX_HOSTIF_CMD_GETFWVERSION,
	IDX_HOSTIF_CMD_UPDATEBIST,
	IDX_HOSTIF_CMD_GETCOMPONENTVERSION
} ;

enum {
	UPDATE_FLAGS_ENCRYPTED = ( 1 << 0 ),
	UPDATE_FLAGS_MCU_FIRMWARE = ( 0 << 1 ),
	UPDATE_FLAGS_SENSOR_RAM_FIRMWARE = ( 1 << 1 ),
	UPDATE_FLAGS_SENSOR_OTP_FIRMWARE = ( 1 << 2 ),
	UPDATE_FLAGS_SENSOR_PRIMARY = ( 0 << 3 ),
	UPDATE_FLAGS_SENSOR_SSBL = ( 1 << 3 ),
};

//////////////////////////////////////////////////////
//
// low-level communication commands
//
enum
{
	IDX_HOSTIF_CMD_EAGLE_WRITE = IDX_HOSTIF_CMD_EAGLE,
	IDX_HOSTIF_CMD_EAGLE_READ,
	IDX_HOSTIF_CMD_EAGLE_RESET,
	IDX_HOSTIF_CMD_EAGLE_GETREADYPIN,
	IDX_HOSTIF_CMD_EAGLE_WAITFORREADYSTATE,
	IDX_HOSTIF_CMD_EAGLE_CALIBRATE,
	IDX_HOSTIF_CMD_EAGLE_TRANSFER,
	IDX_HOSTIF_CMD_EAGLE_PRODTEST = IDX_HOSTIF_CMD_EAGLE_TRANSFER,
	IDX_HOSTIF_CMD_SENSOR_COMMAND
} ;

enum
{
	IDX_HOSTIF_CMD_READ_DATA = IDX_HOSTIF_CMD_MANAGEMENT,
	IDX_HOSTIF_CMD_READ_FLASH,
	IDX_HOSTIF_CMD_LOAD_IMAGE,
	IDX_HOSTIF_CMD_LOAD_CPU,
	IDX_HOSTIF_CMD_RESET_CPU,
	IDX_HOSTIF_CMD_SENSOR_STACK_TEST = IDX_HOSTIF_CMD_LOAD_CPU,
	IDX_HOSTIF_CMD_EMK_TEST = IDX_HOSTIF_CMD_RESET_CPU,
	IDX_HOSTIF_CMD_BIODATAFLUSH, // from idx_biodataflush(const idx_CommandOptions managementCommands[], after idx_resetcpu) of "kestrel-sw\biomcu\firmware\idx-serial\idx-commands-eclipse.c"
	IDX_HOSTIF_CMD_IPL_TEST = IDX_HOSTIF_CMD_BIODATAFLUSH,
	IDX_HOSTIF_CMD_ICK_TEST,
	IDX_HOSTIF_CMD_COMMON_TEST,
} ;

enum
{
	IDX_HOSTIF_CMD_BRIDGE_SET_WTX = IDX_HOSTIF_CMD_DEBUG,
	IDX_HOSTIF_CMD_RMA_DEBUG_COMMAND,
	IDX_HOSTIF_CMD_BRIDGE_BMCU_COMM_CONFIG, 
};

#define IDX_HOSTIF_CMD_INITIALIZE_DATA_SIZE      (1)
#define IDX_HOSTIF_CMD_UNINITIALIZE_DATA_SIZE    (0)
#define IDX_HOSTIF_CMD_ACQUIRE_DATA_SIZE         (1)
#define IDX_HOSTIF_CMD_GETPARAM_DATA_SIZE        (1)
#define IDX_HOSTIF_CMD_SETPARAM_DATA_SIZE        (3)
#define IDX_HOSTIF_CMD_STOREPARAM_DATA_SIZE      (0)
#define IDX_HOSTIF_CMD_DELETERECORD_DATA_SIZE    (2)
#define IDX_HOSTIF_CMD_LISTRECORD_DATA_SIZE      (3)
#define IDX_HOSTIF_CMD_RESET_DATA_SIZE           (0)
#define IDX_HOSTIF_CMD_PUTKEY_DATA_SIZE          (82)
#define IDX_HOSTIF_CMD_FLASHUPDATE_DATA_SIZE     (1)
#define IDX_HOSTIF_CMD_UPDATESTART_DATA_SIZE     (0)
#define IDX_HOSTIF_CMD_UPDATEDETAILS_DATA_SIZE   (41)
#define IDX_HOSTIF_CMD_UPDATEEND_DATA_SIZE       (0)
#define IDX_HOSTIF_CMD_CALIBRATE_DATA_SIZE       (1)
#define IDX_HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE    (2)

//////////////////////////////////////////////////////
//
// device option’s bits
//
#define LOCK_BIT				(1<<0)
#define SIGN_BIT				(1<<1)
#define ENCRYPT_BIT				(1<<2)
#define MATCH_DB_BIT			(1<<3)
#define EAGLE_DIRECT_BIT		(1<<4)
#define IMAGE_BIT				(1<<5)
#define TEMPLATE_BIT			(1<<6)
#define ABORT_BIT				(1<<7)
#define HW_AES_BIT				(1<<9)
#define HW_CMAC_BIT				(1<<10)
#define HW_RMAC_BIT				(1<<11)

//////////////////////////////////////////////////////
//
// Set of image retrieval flags
//

#define GETIMAGE_UNPROCESSED			((unsigned char) 0x00)
#define GETIMAGE_BINARIZED				((unsigned char) 0x01)
#define GETIMAGE_NORMALIZED				((unsigned char) 0x02)
#define GETIMAGE_FORDISPLAY             ((unsigned char) 0x03)
#define GETIMAGE_COMPRESSED     		((unsigned char) 0x04)

// parameter definition:

//////////////////////////////////////////////////////
//
// available components ID for IDX_HOSTIF_CMD_GETCOMPONENTVERSION command
//
#define VERSION_COMPONENT_ID_MAIN_APP               (0)
#define VERSION_COMPONENT_ID_KESTREL_FW             (1)
#define VERSION_COMPONENT_ID_MCU_MATCHER            (2)
#define VERSION_COMPONENT_ID_SE_SDK                 (3)
#define VERSION_COMPONENT_ID_SE_MATCHER             (4)
#define VERSION_COMPONENT_ID_GREYLOCK_SCANTABLE     (5)
#define VERSION_COMPONENT_ID_GREYLOCK_UID           (6)

//////////////////////////////////////////////////////
//
// matching commands
//
enum
{
	GET_NEW_IMAGE_FOR_MATCHING = 0,
	USE_PREVIOUS_IMAGE_FOR_MATCHING,
	WAKEUP_IMAGE_FETCH_FOR_MATCHING,
};

//////////////////////////////////////////////////////
//
// available sensor parameters
//
enum
{
	BIO_PARAM_ID_IDEX_SENSOR_TYPE,					// 0
	BIO_PARAM_ID_SENSOR_GAIN,
	BIO_PARAM_ID_IMAGE_MIN_ROW,
	BIO_PARAM_ID_IMAGE_HEIGHT,
	BIO_PARAM_ID_JOINT_THRESHOLD,
	BIO_PARAM_ID_OFFROW_THRESHOLD,					// 5
	BIO_PARAM_ID_NOISE_THRESHOLD,
	BIO_PARAM_ID_SENSOR_WAIT_TIMEOUT,
	BIO_PARAM_ID_EXTERNAL_SIZE,
	BIO_PARAM_ID_EXT_HEAP_SIZE,
	BIO_PARAM_ID_IMAGE_X,							// 10
	BIO_PARAM_ID_IMAGE_Y,
	BIO_PARAM_ID_BIT_DEPTH,
	BIO_PARAM_ID_RECONS_Y,
	BIO_PARAM_ID_SENSOR_DPI,
	BIO_PARAM_ID_LAST_SCANNED_IMAGE_COVERAGE,		// 15
	BIO_PARAM_ID_GETIMAGEPROCESSING_FLAGS,
	BIO_PARAM_ID_MIN_ENROLL_PERCENT_COVERED,
	BIO_PARAM_ID_SENSOR_SETTLING_TIME, // Delay before scanning after finger is detected
	BIO_PARAM_ID_STARTUP_MODE,
	BIO_PARAM_ID_CURRENT_TEMPLATE_SIZE,				// 20
	BIO_PARAM_ID_DEBUG_MODE,
	BIO_PARAM_ID_DEVICE_INTERFACE,
	BIO_PARAM_ID_SELF_TEST_RESULT,
	BIO_PARAM_ID_BIOMETRIC_ALG,
	BIO_PARAM_ID_TEMP_CORRECTION,					// 25
	BIO_PARAM_ID_LAST_SCANNED_IMAGE_FP_STATE,
	BIO_PARAM_ID_LAST_SCANNED_IMAGE_FP_COVERAGE,
	BIO_PARAM_ID_QUADRANT_HISTO_PERCENT,
	BIO_PARAM_ID_BASELINE_GAIN,
	BIO_PARAM_ENROLL_NUM_IMAGES,					// 30
	BIO_PARAM_VSENSE_PROB_THRESHOLD,
	BIO_PARAM_VSENSE_COMP_THRESHOLD,
	BIO_PARAM_MAX_TEMPLATE_UNUSED,
	BIO_PARAM_FIRST_PROBE_MASK,
	BIO_PARAM_SECOND_PROBE_MASK,					// 35
	BIO_PARAM_VSENSE_SLOPE,
	BIO_PARAM_VSENSE_SETTLING_TIME,
	BIO_PARAM_VSENSE_BETWEEN_READ_TIME,
	BIO_PARAM_DEFAULT_MAX_SPEED,
	BIO_PARAM_FASTFINGER_DETECT_TIMEOUT,			// 40
	BIO_PARAM_CLOCK_RAMP_MASK,
	BIO_PARAM_CLOCK_RAMP_DELAY,
	BIO_PARAM_VSENSE_MIN_LEVEL,
	BIO_PARAM_WFF_POLLING_PERIOD,
	BIO_PARAM_VSENSE_SLOPE_PROBE2,					// 45
};

enum
{
	BIO_PARAM_HOST_IF_BURST_LENGTH = 128,
	BIO_PARAM_IN_IMAGE_AGC,
	BIO_PARAM_DEVICE_OPTIONS,						// 130
	BIO_PARAM_MATCH_THRESHOLD,
	BIO_PARAM_LAST_MATCH_SCORE,
	BIO_PARAM_LAST_COMMAND_TIMING,
	BIO_PARAM_MAX_FLASH_COUNT,
	BIO_PARAM_LED_CONTROL_FLAGS,
	BIO_PARAM_COMMUNICATION_FAILURES,
	BIO_PARAM_FLASH_MODE_ON_TIME_0,
	BIO_PARAM_FLASH_MODE_ON_TIME_1,
	BIO_PARAM_FLASH_MODE_ON_TIME_2,
	BIO_PARAM_FLASH_MODE_ON_TIME_3,					// 140
	BIO_PARAM_FLASH_MODE_ON_TIME_4,
	BIO_PARAM_FLASH_MODE_ON_TIME_5,
	BIO_PARAM_FLASH_MODE_ON_TIME_6,
	BIO_PARAM_FLASH_MODE_ON_TIME_7,
	BIO_PARAM_FLASH_MODE_OFF_TIME_0,
	BIO_PARAM_FLASH_MODE_OFF_TIME_1,
	BIO_PARAM_FLASH_MODE_OFF_TIME_2,
	BIO_PARAM_FLASH_MODE_OFF_TIME_3,
	BIO_PARAM_FLASH_MODE_OFF_TIME_4,
	BIO_PARAM_FLASH_MODE_OFF_TIME_5,				// 150
	BIO_PARAM_FLASH_MODE_OFF_TIME_6,
	BIO_PARAM_FLASH_MODE_OFF_TIME_7,
	BIO_PARAM_ENROL_INDICATE_FINGER_1_LED,
	BIO_PARAM_ENROL_INDICATE_FINGER_1_LED_DURATION,
	BIO_PARAM_ENROL_INDICATE_FINGER_2_LED,
	BIO_PARAM_ENROL_INDICATE_FINGER_2_LED_DURATION,
	BIO_PARAM_ENROL_WAIT_FOR_FINGER_1_LED,
	BIO_PARAM_ENROL_WAIT_FOR_FINGER_2_LED,
	BIO_PARAM_ENROL_IMAGE_ACQ_LED,
	BIO_PARAM_ENROL_MATCH_PROC_LED,					// 160
	BIO_PARAM_ENROL_MATCH_SUCCESS_ODD_LED,
	BIO_PARAM_ENROL_MATCH_SUCCESS_ODD_LED_DURATION,
	BIO_PARAM_ENROL_MATCH_SUCCESS_EVEN_LED,
	BIO_PARAM_ENROL_MATCH_SUCCESS_EVEN_LED_DURATION,
	BIO_PARAM_ENROL_MATCH_FAIL_LED,
	BIO_PARAM_ENROL_MATCH_FAIL_LED_DURATION,
	BIO_PARAM_ENROL_WAIT_FOR_FINGER_OFF_LED,
	BIO_PARAM_ENROL_SUCCESS_FINGER_1_PHASE_1_LED,
	BIO_PARAM_ENROL_SUCCESS_FINGER_1_PHASE_1_LED_DURATION,
	BIO_PARAM_ENROL_SUCCESS_FINGER_1_PHASE_2_LED,	// 170
	BIO_PARAM_ENROL_SUCCESS_FINGER_1_PHASE_2_LED_DURATION,
	BIO_PARAM_ENROL_SUCCESS_FINGER_1_PHASE_3_LED,
	BIO_PARAM_ENROL_SUCCESS_FINGER_1_PHASE_3_LED_DURATION,
	BIO_PARAM_ENROL_SUCCESS_FINGER_2_PHASE_1_LED,
	BIO_PARAM_ENROL_SUCCESS_FINGER_2_PHASE_1_LED_DURATION,
	BIO_PARAM_ENROL_SUCCESS_FINGER_2_PHASE_2_LED,
	BIO_PARAM_ENROL_SUCCESS_FINGER_2_PHASE_2_LED_DURATION,
	BIO_PARAM_ENROL_SUCCESS_FINGER_2_PHASE_3_LED,
	BIO_PARAM_ENROL_SUCCESS_FINGER_2_PHASE_3_LED_DURATION,
	BIO_PARAM_ENROL_CATASTROPHIC_FAIL_LED,			// 180
	BIO_PARAM_ENROL_CATASTROPHIC_FAIL_LED_DURATION,
	BIO_PARAM_WAIT_FOR_FINGER_OFF_CONTROL_NOT_USED,
	BIO_PARAM_PREPIMG_WAIT_FOR_FINGER_ON_LED,
	BIO_PARAM_PREPIMG_WAIT_FOR_FINGER_OFF_LED,
	BIO_PARAM_PREPIMG_IMAGE_ACQ_LED,
	BIO_PARAM_GETTEMP_MATCH_PROC_LED,
	BIO_PARAM_GETTEMP_MATCH_SUCCESS_LED,
	BIO_PARAM_GETTEMP_MATCH_SUCCESS_LED_DURATION,
	BIO_PARAM_GETTEMP_MATCH_FAIL_LED,
	BIO_PARAM_GETTEMP_MATCH_FAIL_LED_DURATION,
	BIO_PARAM_MERGETEMP_MATCH_PROC_LED,
	BIO_PARAM_MERGETEMP_MATCH_SUCCESS_LED,
	BIO_PARAM_MERGETEMP_MATCH_SUCCESS_LED_DURATION,
	BIO_PARAM_MERGETEMP_MATCH_FAIL_LED,
	BIO_PARAM_MERGETEMP_MATCH_FAIL_LED_DURATION,
	BIO_PARAM_MATCHTEMP_MATCH_PROC_LED,
	BIO_PARAM_MATCHTEMP_MATCH_SUCCESS_LED,
	BIO_PARAM_MATCHTEMP_MATCH_SUCCESS_LED_DURATION,
	BIO_PARAM_MATCHTEMP_MATCH_FAIL_LED,
	BIO_PARAM_MATCHTEMP_MATCH_FAIL_LED_DURATION,
} ;

//////////////////////////////////////////////////////
//
// state of data returned
//
#define DATA_CRC			(1<<0)
#define DATA_SIGNED			(1<<1)
#define DATA_CRYPTED		(1<<2)
#define NO_TIMING			(1<<3)
#define RETURN_SIZE			(1<<4)
#define SCP03				(1<<5)
#define MESSAGE_CHUNK		(1<<6)
#define MESSAGE_LAST		(1<<7)

//////////////////////////////////////////////////////
//
// startup modes
//
#define STARTUP_MODE_IDLE		0x00
#define STARTUP_MODE_ACQUIRE	0x01
#define STARTUP_MODE_TEMPLATE	0x02
#define STARTUP_MODE_TEST		0x04
#define STARTUP_MODE_CALIB		0x08
#define STARTUP_MODE_ENROLL		0x10

//////////////////////////////////////////////////////
//
// enrollment modes
//
#define ENROLL_STORE_TEMPLATE	0x01
#define ENROLL_CONTROL_LEDS		0x02
#define ENROLL_LED2_OK			0x04
#define ENROLL_DEFAULT_NAMES	0x08

#define ENROLL_ALL_FLAGS_ON		( ENROLL_STORE_TEMPLATE | ENROLL_CONTROL_LEDS | ENROLL_LED2_OK | ENROLL_DEFAULT_NAMES )
#define ACQUIRE_ALL_FLAGS_ON	( 0x01 | 0x02 | 0x04 | 0x08 | 0x20 | 0x40 )


//////////////////////////////////////////////////////
//
// debug modes
//
enum
{
	NO_DEBUG,
	ONLY_ONE,
	CYCLE_MODE
} ;


//////////////////////////////////////////////////////
//
// sensor interface modes
//
enum
{
	INTERFACE_PIN_SELECTION,
	INTERFACE_USB,
	INTERFACE_I2C,
	INTERFACE_UART,
	INTERFACE_SPI,
	INTERFACE_I2C_MASTER,
	INTERFACE_SPI_MASTER
} ;

enum
{
	INTERFACE_SPI_5WIRE = 1,
	INTERFACE_SPI_4WIRE,
	INTERFACE_I2C_3WIRE
} ;


//////////////////////////////////////////////////////
//
// encryption key modes
//
enum
{
	ROOT_KEY,
	SIGN_KEY,
	BIND_KEY
} ;

//////////////////////////////////////////////////////
//
// size of binding key
//
#define BIND_SIZE	128

//////////////////////////////////////////////////////
//
// SCAN_* - set of scan flags, AKA prepareimage flags
//
enum
{
	BIO_PREPIMG_LIVE,				// 0 = scan and returns whatever the image
	BIO_PREPIMG_FINGERDETECT,		// 1 = wait for a finger then return the image
	BIO_PREPIMG_NEWFINGERDETECT,	// 2 = wait for finger off, then finger on and then return image
	BIO_PREPIMG_FINGEROFFDETECT		// 3 = wait for finger off and then return image
};

#define PREPARE_IMAGE_LIVE						0x00
#define PREPARE_IMAGE_SENSOR_FINGERDETECT		0x01
#define PREPARE_IMAGE_FINGER_DETECT				0x02
#define PREPARE_IMAGE_SCAN_AFTER_TIMEOUT		0x04
#define PREPARE_IMAGE_RESERVED3					0x08
#define PREPARE_IMAGE_COMPUTE_COVERAGE			0x10
#define PREPARE_IMAGE_WAIT_FINGER_OFF_AFTER		0x20
#define PREPARE_IMAGE_WAIT_FINGER_OFF_FIRST		0x40
#define PREPARE_IMAGE_DONT_SCAN_AFTER_DETECT	0x80

#define BIO_PREPIMG_FLAG_MASK		0x0000FFFF
#define BIO_PREPIMG_NOWAIT			0x00010000	// this is to avoid prepare image waiting for the image, in case it is handled by the app outside of the functions


//////////////////////////////////////////////////////
//
// Finger presence states
//

#define PERFORM_CALIBRATION         0x01
#define RETURN_CALIBRATION_DATA     0x02
#define SIMPLE_CALIBRATION          0x04
#define STORE_CALIBRATION_PARAM     0x08
#define RETURN_FINGERDETECT_DATA    0x10
#define PERFORM_INDUCTOR_TEST       0x20
#define DO_NOT_FORCE_CALIBRATION    0x40


//////////////////////////////////////////////////////
//
// - gettemplates flags:
//
#define GET_TEMPLATE_MODE				0x02 // 1: first enrolment template
#define GET_TEMPLATE_ENROLL_PHASE		0x80
											 // 0: normal template generation

//////////////////////////////////////////////////////
//
// - mergetemplates flags:
//
#define MERGE_TEMPLATE_SOURCE			0x01 // 1: record handle is part of the message 
											 // 0: template data is part of the message
#define MERGE_TEMPLATE_STORAGE_TYPE		0x02 // 1: store merged record (only valid if the device storage is enabled)
											 // 0: return / do not store merged record
#define MERGE_IS_FINAL					0x04 // 1: indicates this is the final merge to a template
											 // 0: intermediate merge step
#define MERGE_UPDATE_EXISTING_RECORD	0x08 // TODO: DO WE NEED THIS FLAG OR MERGE_TEMPLATE_SOURCE (1) REPLACES IT ?

// - match flags:
#define MATCH_MODE						(0x01) // 1: match to all records and return best match
												// 0: match until first match
#define MATCH_INTERNAL_STORAGE			(0x02) // 1: template data is in internal storage records and command data includes a list of records to match against if flags[2] == 0
												// 0: template data is part of the command data(ONLY SUPPORTED for secure matcher)
#define MATCH_MATCH_SCOPE				(0x04) // 1: match against all records in internal storage
												// 0: do not match against all records in internal storage (ONLY SUPPORTED for secure matcher)
#define MATCH_KEEP_BEST_MATCH			(0x08) // 1: keep the existing best match score 
												// 0: reset the existing best match score
#define MATCH_PHASE						(0x10) // 1: matcher phase 2
												// 0: matcher phase 1
#define MATCH_PASS_MASK					(0x20) //Do we still need this?

#define BIST_OPT_MASK_USE_DEFAULT_LIMITS	0x00
#define BIST_OPT_MASK_USE_PROVIDED_LIMITS	0x01
#define BIST_OPT_MASK_REPORT_EXTENDED		0x02
#define BIST_OPT_MASK_REPORT_LIMITS			0x04

#ifdef __ARMCC_VERSION
#pragma anon_unions
#endif

#ifdef WIN32
#pragma pack(push)
#pragma pack(1)
#endif

//////////////////////////////////////////////////////
//
// idx_CommandHeader - header for a command
//
typedef struct PACKED_ATTRIBUTE __idx_CommandHeader
{
	unsigned short	wSize ;			// size of the command, including the whole header
	unsigned char	bOrdinal ;		// command ordinal
	unsigned char	bOptions ;		// command options
} idx_CommandHeader ;


//////////////////////////////////////////////////////
//
// idx_ReplyHeader - header for a reply to a command
//
typedef struct PACKED_ATTRIBUTE __idx_ReplyHeader
{
	unsigned short	wSize ;			// size of the reply, including the whole header
	unsigned short	wCode ;			// return code of the command
} idx_ReplyHeader ;

#ifdef WIN32
#pragma warning (push, 2)
#endif


//////////////////////////////////////////////////////
//
// idx_PrepareImageData - information about the image
//
typedef struct PACKED_ATTRIBUTE __idx_PrepareImageData
{
	union
	{
		struct 
		{
			short imageQuality;
			short minSkew;
			short maxSkew;
			short minSpeed;
			short maxSpeed;
			short avgSpeed;
		};
		struct
		{
			short imagePercentCovered;
			short fingerPresencePercentCovered;
			short fingerPresenceStatus;
			short unused;
			short imageSignalLevel;
			short imageSignalAveLevel;
		};
	};
} idx_PrepareImageData ;

#ifdef WIN32
#pragma warning (pop)
#endif


//////////////////////////////////////////////////////
//
// idx_GetTemplateData - information about a template
//
typedef struct PACKED_ATTRIBUTE __idx_GetTemplateData
{
	short imageQuality ;
	short templateQuality ;
	short numFeatures ;
	short templateSize ;
} idx_GetTemplateData ;

//////////////////////////////////////////////////////
//
// idx_BioTemplateData - information about a template to match/merge
//
#ifndef __C251__
// Don't compile on C251 ('data' is a keyword)
typedef struct PACKED_ATTRIBUTE __idx_BioTemplateData
{
	unsigned char *data;
	unsigned short size;
}
idx_BioTemplateData;
#endif

//////////////////////////////////////////////////////
//
// idx_MergeTemplateData - information about a merged
//                         template
//
typedef struct PACKED_ATTRIBUTE __idx_MergeTemplateData
{	
	unsigned char numMerged ;
	unsigned char isTemplateFull ;	
	short templateQuality ;
	short templateSize ;
	
} idx_MergeTemplateData ;

//////////////////////////////////////////////////////
//
// idx_SetParameterData - sensor parameter info
//
typedef struct PACKED_ATTRIBUTE __idx_SetParameterData
{
	unsigned char id ;
	unsigned short value ;
} idx_SetParameterData ;


//////////////////////////////////////////////////////
//
// idx_ListRecordData - data about a record in the DB
//
typedef struct PACKED_ATTRIBUTE __idx_ListRecordData
{
	unsigned char options ;
	unsigned short start_handle ;
} idx_ListRecordData ;


//////////////////////////////////////////////////////
//
// idx_GetRecordDetailsData - details of a DB record
//
typedef struct PACKED_ATTRIBUTE __idx_GetRecordDetailsData
{
	unsigned short id ;
	unsigned char type ;
} idx_GetRecordDetailsData ;


//////////////////////////////////////////////////////
//
// idx_GetVersionData - version info for units
//
typedef struct PACKED_ATTRIBUTE __idx_GetVersionData
{
	idx_UnitShortVersion_t core ;
	idx_UnitShortVersion_t connector ;
} idx_GetVersionData ;

//////////////////////////////////////////////////////
//
// idx_UpdateDetails - update descriptor
//
typedef struct PACKED_ATTRIBUTE __idx_UpdateDetails
{
	unsigned int update_address ;
	unsigned int update_size ;
	unsigned char update_flags ;
	unsigned char IV[32] ;
} idx_UpdateDetails ;


//////////////////////////////////////////////////////
//
// idx_EncryptData - encrypted data descriptor
//
typedef struct PACKED_ATTRIBUTE __idx_EncryptData
{
	unsigned short key_size ;
	unsigned char key[32] ;
	unsigned char IV[32] ;
	unsigned char SignSecret[32] ;
	unsigned char Signature[20] ;
} idx_EncryptData ;


//////////////////////////////////////////////////////
//
// idx_SignData - signed data descriptor
//
typedef struct PACKED_ATTRIBUTE __idx_SignData
{
	unsigned char nonce[32] ;
	unsigned char digest[32] ;
} idx_SignData ;

typedef struct PACKED_ATTRIBUTE __idx_ReadData
{
	unsigned short offset ;
	unsigned short size ;
} idx_ReadData ;

typedef struct PACKED_ATTRIBUTE __idx_ReadFlashData
{
	unsigned int address ;
	unsigned short size ;
} idx_ReadFlashData ;

typedef struct PACKED_ATTRIBUTE __idx_StoreBlobData
{
	unsigned char type;
	unsigned int size;

} idx_StoreBlobData;

typedef struct PACKED_ATTRIBUTE __idx_SensorCommandData
{
	unsigned char flag;
	unsigned char command_id;
	unsigned int reply_size;

} idx_SensorCommandData;

// eagle direct access specific command

//////////////////////////////////////////////////////
//
// idx_WaitForReadyState - eagle direct access
//                         specific command
//
typedef struct PACKED_ATTRIBUTE __idx_WaitForReadyState
{
	unsigned int state ;
	unsigned int timeout ;

} idx_WaitForReadyState ;

typedef PrePACKED struct S_PACKED __idx_PutKeyData
{
	unsigned char enc[16] ;
	unsigned char mac[16] ;
	unsigned char dec[16] ;
	unsigned char rand[32] ;
	unsigned short crc ;
} idx_PutKeyData ;

typedef PrePACKED struct S_PACKED __idx_ProdTest
{
	unsigned char flags ;
	unsigned char startx ;
	unsigned char starty ;
	unsigned char sizex ;
	unsigned char sizey ;
} idx_ProdTest ;

typedef PrePACKED struct S_PACKED __idx_ProdTestStats
{
	short mean ;
	short stddev ;
	short min ;
	short max ;
} idx_ProdTestStats ;

#define PRODTEST_USE_RAW_DATA		0x01
#define PRODTEST_SUB_IMAGE			0x02
#define PRODTEST_STATISTICS			0x04
#define PRODTEST_RAW_MINUS_BASELINE	0x08

enum
{
	HEAP_ENTRY_TEMPLATE,
	HEAP_ENTRY_MCU_UPDATE_APP,
	HEAP_ENTRY_SENSOR_SSBL,
	HEAP_ENTRY_SENSOR_UTILITY,
	HEAP_ENTRY_SENSOR_PRIMARY,
	HEAP_ENTRY_CUSTOM_BLOB,
	HEAP_ENTRY_SE_TEMPLATE
};

// enum for biometric script
enum
{
	RUN_INITIALIZE,
	RUN_PREPAREIMAGE,
	RUN_UNINITIALIZE
} ;	

typedef struct PACKED_ATTRIBUTE __idx_BistTestCommonResults
{
	unsigned char test_result;
	unsigned char test_status;
} idx_BistTestCommonResults;

typedef struct PACKED_ATTRIBUTE __idx_BistImageLimitsParams_t
{
	unsigned char max_x_offset;
	unsigned char max_y_offset;
	unsigned char max_theta_offset;
	unsigned char warning_x_offset;
	unsigned char warning_y_offset;
	unsigned char warning_theta_offset;
	unsigned char error_x_offset;
	unsigned char error_y_offset;
	unsigned char error_theta_offset;
	unsigned char max_theta_delta;
	unsigned char max_xcorr_peak_delta;
	unsigned char min_signal;
	unsigned char max_signal;
	unsigned char min_snr;
	unsigned char min_lightmean;
	unsigned char max_darkmean;
} idx_BistImageLimitsParams_t;

#define MAX_REGION	12

typedef struct PACKED_ATTRIBUTE __idx_BistImageTestResults_t
{
	char x_offset;
	char y_offset;
	char theta_offset;
	char theta_delta;
	unsigned char xcorr_peak[4];
	unsigned char regions_means[MAX_REGION];
	short regions_mean_noise[MAX_REGION];
} idx_BistImageTestResults_t;

typedef struct PACKED_ATTRIBUTE   __idx_BistOpenShortResults_t
{
	int errorCount;
} idx_BistOpenShortResults;

typedef struct PACKED_ATTRIBUTE __idx_BistRequestHeader
{
	unsigned short size;
	unsigned char testID;
	unsigned char opt;
} idx_BistRequestHeader;

typedef struct PACKED_ATTRIBUTE __idx_BistResponseHeader
{
	unsigned short size;
	idx_BistTestCommonResults result;
} idx_BistResponseHeader;

//data structure for IDX_HOSTIF_CMD_BRIDGE_BMCU_COMM_CONFIG bridge command
typedef struct PACKED_ATTRIBUTE __idx_BridgeBiomcuCommConfig
{
	unsigned char config_param_id; // 0 - CRC , 1,2,3,.. some others
	unsigned char state; //0 - off, 1 - on
} idx_BridgeBiomcuCommConfig;

enum
{
	BIST_CMD_ID_NONE,
	BIST_CMD_ID_IMAGE_TEST,
	BIST_CMD_ID_OPENSHORT_TEST,
};

#ifdef WIN32
#pragma pack(pop)
#endif

#endif // __IDX_SERIAL_HOST_INTERFACE_H__
