/** ***************************************************************************
 * \file   dispatcher.c
 * \brief  IDEX Crypto infrastructure implementation.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2013-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "idxSerialHostInterface.h"

#include "bio-ctrl.h"
#include "idxHalFlash.h"
#include "idxHalAlgo.h"

#include "crypto.h"
//#include "aes.h"
#include "cipher.h"
#include "cmac.h"
#include "kdf.h"

#if defined(TH89)
#include "type.h"
#include "nvm.h"
#endif

#include "idxSerialInterface.h"                    // Debug

#define BOPTION_ENCRYPTED (DATA_CRYPTED)
#define BOPTION_SIGNED    (DATA_SIGNED)

#define UNUSED(p) if(p){}

session_t session;

void idxCryptoInit()
{
	session.state = SEC_CH_CLOSED;
}

/********************************************************************************
 * Enternal Definitions
 */
#ifdef TH89

/********************************************************************************
 * Global Definitions
 * Note: ukey is the "universal key" used for tests
 */
const uint8_t ukey[16]={0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4a,0x4b,0x4c,0x4d,0x4e,0x4f};
uint8_t *pDbg;

/********************************************************************************
 * Local Definitions
* For key sector : Keys[80], CRC[2], None[6], state[8], cnt[8]*16   
 */
#define pNvmKeys  ((uint8_t *)0x0c060000)
#define KEY_STATE_OFFSET ( 88 )
#define KEY_CNT_OFFSET   ( 96 )
#define KEY_MAX_CNT      ( 16 )

static uint8_t (*pNvmKey)[512] = (uint8_t (*)[512])0x0c060000;
static uint8_t cntValue[8] = {0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88};
static uint8_t validate[8] = {0x55,0x55,0x55,0x55,0x55,0x55,0x55,0x55};

static void genNewKeys(uint8_t *key);
static void genKrnd(uint8_t *Krnd);
static void genHostChallenge(uint8_t *hChallenge);
#endif

#ifdef TH89

/********************************************************************************
 * Generate new basic keys, Krnd and host challenge 
 */
#define RANDOM_KEYS
static void genNewKeys(uint8_t *key){
#ifdef RANDOM_KEYS  
  uint16_t idxErr = (uint16_t)idxHalAlgoGenRandNamber(48,key);
  UNUSED(idxErr)
#else  
  memcpy(key,    ukey, 16);
  memcpy(key+16, ukey, 16);
  memcpy(key+32, ukey, 16);
#endif
}

static void genHostChallenge(uint8_t *hChallenge){
  uint16_t i;
  for(i = 0 ; i < 8 ; i++) hChallenge[i] = 0x11 + ( ( i<<4 ) + i);  
  if(0){
    uint16_t idxErr = (uint16_t)idxHalAlgoGenRandNamber(8,hChallenge);
  }  
}

static void genKrnd(uint8_t *Krnd){
  memcpy(Krnd,    ukey, 16);
  memcpy(Krnd+16, ukey, 16); 
  if(0){
    uint16_t idxErr = (uint16_t)idxHalAlgoGenRandNamber(32,Krnd);
  }  
}

/********************************************************************************
 *  SetFactoryKeys() = Erase + prgm(keys) + prgm(validate) + putKey()
 *
 */
uint16_t setFactoryKeys(const idxBuffer_t *pCommsBuffer){
  uint16_t ret;
  uint16_t crc = 0;
  uint8_t *pdata;
  uint8_t *sector = (uint8_t *)pNvmKey[0]; // sector #1
  uint16_t data_size = 0;
  pdata = &pCommsBuffer->pBuffer[HDR_SIZE];

  genNewKeys(pdata);                 // Kenc, Kmac, Kdek (random or default)
  genKrnd(pdata+48);                 // Krnd (random or default)  
  idxHalAlgoCRC16(pdata, 80, &crc);
  WR_U16(pdata+80, crc);

  if(NvmFastEraseSector(sector)){      // erase sector #1
    return 0x6510;
  }
  if(NvmFastEraseSector(sector+512)){  // erase sector #2 ( just for being on safe side)
        return 0x6511;
  }
  
	
  // Program keys to sector #1
  if(  NvmProgramWords(sector, pdata, 88) ){
     return 0x6512;
  }

  // Here the counter is implicitly set to 0 by NOT writting counter areas
	
  // Set keys #1 to "validated" 
  if(NvmProgramWords(&sector[KEY_STATE_OFFSET], validate, sizeof(validate))){
     return 0x6513;
  }
  
  data_size = 82;
  ret = idxMcuComms( IDX_HOSTIF_CMD_PUTKEY, 0, &data_size, pCommsBuffer );
  
  return ret;
}
#ifdef DBG_CRYPTO
static uint8_t tearingPoint;
void setTearingPoint(uint8_t tPoint){tearingPoint = tPoint;}
#endif
/********************************************************************************
 *  GetNextKeys() = Erase + prgm(keys) + prgm(validate) + putKey()
 *
 */

uint16_t getNextKeys(const idxBuffer_t *pCommsBuffer){
  uint8_t doubleWordEraseCheck( uint8_t *pbSrc );
  uint16_t ret;
  idxBuffer_t rBuffer;
  uint8_t *pData;
  uint16_t data_size;
  uint8_t *ctx = session.context;
  uint8_t cryptogram[16];
  uint8_t uid[16];
  uint8_t cardInfo[8+8+3]; // MCU-challenge[8], MCU-cryptogram[8], counter[3]={cnt,~cnt,0xab}
  uint8_t cdata[9]={0};  // keyID[1] + hChallenge[8] 
  pData = &pCommsBuffer->pBuffer[HDR_SIZE];
  
  pData[0] = 0; // MCU ID
  data_size = 1;
  rBuffer.bufferSize = 12;
  rBuffer.pBuffer = uid;
  ret = idxMcuCommsFull( IDX_HOSTIF_CMD_GETUID, 0, &data_size, pCommsBuffer, NULL, &rBuffer ); // MCU ID
  if( ret != IDX_ISO_SUCCESS ) { return ret; }
  
  pData[0] = 1; // sensor ID
  data_size = 1;
  rBuffer.bufferSize = 4;
  rBuffer.pBuffer = &uid[12];
  ret = idxMcuCommsFull( IDX_HOSTIF_CMD_GETUID, 0, &data_size, pCommsBuffer, NULL, &rBuffer ); // sensor ID
  if( ret != IDX_ISO_SUCCESS ) { return ret; }
  
  genHostChallenge( &cdata[1] ); // hChallenge[8]
  memcpy(&pData[0], &cdata[0], 9);
  data_size = 9;
  rBuffer.bufferSize = 19;
  rBuffer.pBuffer = cardInfo;
  ret = idxMcuCommsFull( IDX_HOSTIF_CMD_INITIALIZE_UPDATE, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
  if( ret != IDX_ISO_SUCCESS ) { return ret; }

#ifdef DBG_CRYPTO  
  if( tearingPoint == 0x01){ // Test tearing immediately after initialiseUpdate
    return 0x6511;
  }
#endif
  
  {
    uint8_t k1state,k2state;
    uint8_t k1cnt,k2cnt;
    uint8_t *nvmSector1 = (uint8_t*)pNvmKey[0];
    uint8_t *nvmSector2 = (uint8_t*)pNvmKey[1];

    // Evaluate the state (validated/erased) and key counters
    k1state = doubleWordEraseCheck(nvmSector1 + KEY_STATE_OFFSET);
    k2state = doubleWordEraseCheck(nvmSector2 + KEY_STATE_OFFSET);  
    if( (k1state == 0) && (k2state == 0) ){
      return 0x6504;  // return if no key available
    }
    for(k1cnt = 0 ; k1cnt < KEY_MAX_CNT ; k1cnt++){
      if( 0 == doubleWordEraseCheck( nvmSector1 + KEY_CNT_OFFSET + k1cnt * 8) ){ 
          break;
      }
    }
    if(k1cnt >= KEY_MAX_CNT){
      return 0x6505;
    }    
    for(k2cnt = 0 ; k2cnt < KEY_MAX_CNT ; k2cnt++){
        if( 0 == doubleWordEraseCheck( nvmSector2 + KEY_CNT_OFFSET + k2cnt * 8) ){ 
            break;
        }
    }
    if(k2cnt >= KEY_MAX_CNT){
      return 0x6506;
    }    

    // Main selection block based on combination SE vs MCU
    // Rule : if cnt from MCU is 1, then validated,cnt=0 should be used, 
    //        if cnt from MCU is not 1, then validated, cnt=1 (or 2, 3, 4,...) should be used
    if( cardInfo[16] == 1 ){    // cardInfo[16] = mcuCnt
        if( k1state && (k1cnt == 0)){ 
            session.page = 0;
        }
        else if( k2state && (k2cnt == 0)){ 
            session.page = 1;
        }
        else {
          // We shouldn't be here. Try to recover with the assumption: kstate > 0 ==> keys have been validated
          //return 0x6507;
          if(k1state > 0) session.page = 0;
          else            session.page = 1;
        }     
    }
    else {  // mcuCnt > 1
        if( k1state && (k1cnt > 0)){ 
            session.page = 0;
        }
        else if( k2state && (k2cnt > 0)){ 
            session.page = 1;   
        }
        else {
          // We shouldn't be here. Try to recover with the assumption: kstate > 0 ==> keys have been validated  
          //return 0x6508;
          if(k1state > 0) session.page = 0;
          else            session.page = 1;
        }     
    }
    session.kstate[0] = k1state;
    session.kstate[1] = k2state;
    session.kcnt[0]   = k1cnt;
    session.kcnt[1]   = k2cnt;

  }
  
  // Test MCU cryptogram
  {
    uint16_t i;
    uint8_t  firstPage;
    for( i = 0 ; i < 8 ; i++) ctx[i]   = cdata[i+1]; 
    for( i = 0 ; i < 8 ; i++) ctx[i+8] = cardInfo[i];
    for( i = 0 ; i < 16; i++) ctx[i]   ^= uid[i];
    
    for( firstPage = 1 ; ; )
    {
      uint16_t j;
      uint8_t *curNvmSector = (uint8_t*)pNvmKey[session.page];
      genkeySENC(curNvmSector, &session);   // Kenc
      genkeySMAC(curNvmSector+16, &session);   // Kmac
      genkeySRMAC(curNvmSector+16, &session);  // Kmac
      genCardCryptogram(cryptogram, &session); // should be tested against mcu_crypto

      for( j = 0 ; j < 8 ; j++){
        if(cardInfo[8+j] != cryptogram[j]) break;
      }
      if( j == 8 ) break;    // Success on either Page #1 or Page #2. Exit the loop 
      else if( firstPage ){  // If failure on first attempt test 2nd page
        session.page ^= 1;   
        if( 0 == session.kstate[session.page]) return 0x6519; // 2nd page is NOT valid
        firstPage = 0;       // Jump to check the cryptogram for 2nd page
      }
      else return 0x6509;   // Both pages have ( validated ) keys but cryptograms failed 
    }

    session.state = SEC_CH_CLOSED;  // init sec channel status    
  }  
  return ret;
}

uint16_t setNextKeys(uint8_t *buf, uint16_t buff_size, uint8_t mode){
  uint16_t ret;
  uint16_t data_size;
  idxBuffer_t cmdIdxBuffer; // THD89 doesn't allow encryption into COMBUF!!!
  uint16_t crc = 0;
  uint8_t Kdec[16];
  uint8_t *curNvmSector = (uint8_t*)pNvmKey[session.page];
  uint8_t *newNvmSector = (uint8_t*)pNvmKey[session.page ^ 1];
  uint8_t *dataBuff = &buf[HDR_SIZE];
  
  cmdIdxBuffer.bufferSize = buff_size;
  cmdIdxBuffer.pBuffer = buf;
  
  memcpy(Kdec, curNvmSector + 32 , 16);
  
#ifdef DBG_CRYPTO
  if(tearingPoint == 0x01){ // before erasing
    return 0x6521;
  }
#endif
  
  // Erase NVM for next session 
  if( NvmFastEraseSector(newNvmSector)){
    return 0x6509;
  }

#ifdef DBG_CRYPTO
  if(tearingPoint == 0x02){ // after erasing
    return 0x6522;
  }
#endif
  
  // Increment the counter for current session
  {
    uint8_t addOff = session.kcnt[session.page] * 8;  // First write is for 0 to 1 increment  
    if( NvmProgramWords(curNvmSector + KEY_CNT_OFFSET + addOff , cntValue, sizeof(cntValue))){
      return 0x650a;
    }
#ifdef DBG_CRYPTO
    if(tearingPoint == 0x03){ // after increment
      return 0x6523;
    }    
#endif
  }


  // Generate keys for next session
  genNewKeys(dataBuff);                // Kenc, Kmac, Kdek (random or default)
  genKrnd(dataBuff+48);                // Krnd[32] (random or default)
  idxHalAlgoCRC16(dataBuff, 80, &crc);
  WR_U16(dataBuff+80, crc);

  // Program keys for next seession (SE flash)
  if( NvmProgramWords(newNvmSector, dataBuff, 88)){  // length should be multiple of 8
    return 0x650b;
  }
  
#ifdef DBG_CRYPTO
  if(tearingPoint == 0x04){ // after flashing the new keys
    return 0x6524;
  }
#endif

  // The counter is implicitly set to 0 by NOT writting counter area
  
  // Set "validated" 
  if( NvmProgramWords(newNvmSector + KEY_STATE_OFFSET, validate, sizeof(validate))){
    return 0x650c;
  }

#ifdef DBG_CRYPTO
  if(tearingPoint == 0x05){ // after validate new keys
    return 0x6525;
  }
#endif
  
  // encode the key fields  
  encryptCBC(Kdec, dataBuff,    NULL, 16);      
  encryptCBC(Kdec, dataBuff+16, NULL, 16);       
  encryptCBC(Kdec, dataBuff+32, NULL, 16);       
  encryptCBC(Kdec, dataBuff+48, NULL, 32);       

  memcpy(dataBuff+80,(uint8_t*)&crc,2);
  data_size = 82;
  ret = idxMcuComms( IDX_HOSTIF_CMD_PUTKEY, ENCRYPT_BIT|SIGN_BIT, &data_size, &cmdIdxBuffer );
  if(ret != IDX_ISO_SUCCESS){return ret;}  

#ifdef DBG_CRYPTO
  if(tearingPoint == 0x06){ // after put keys
    return 0x6526;
  }
#endif
  
  dataBuff[0] = mode;  // set flash update mode
  data_size = 1;
  ret = idxMcuComms( IDX_HOSTIF_CMD_FLASHUPDATE, ENCRYPT_BIT|SIGN_BIT, &data_size, &cmdIdxBuffer );
  return ret;
}

#endif // TH89

#if 0
/********************************************************************************
 *  SetSeKeys() Write keys to SE nvm (standard or random generated)
 *
 */
void setSeKeys(void){
  uint8_t seKeys[48];  
  genNewKeys(seKeys);  
	
	idxHalFlashWrite(pNvmKeys, seKeys, sizeof(seKeys));
}

/********************************************************************************
 *  SetMcuKeys() Compute CRC for the keys block and send PUT-KEY to mcu
 *  Note: This is the very first set of keys for MCU so no need secure channel
 */
uint16_t setMcuKeys(const idxBuffer_t *pCommsBuffer){
  uint16_t ret;
  uint16_t crc = 0;
  uint8_t *pdata;
  uint16_t data_size;
  pdata = &pCommsBuffer->pBuffer[HDR_SIZE];;

  memcpy(pdata,pNvmKeys,48);
  genKrnd(pdata+48);
  
  idxHalAlgoCRC16(pdata, 80, &crc);
  WR_U16(pdata+80, crc);
  
  pDbg = &pCommsBuffer->pBuffer[HDR_SIZE];;
  
  data_size = 82;
  ret = idxMcuComms( IDX_HOSTIF_CMD_PUTKEY, 0, &data_size, pCommsBuffer );
  return ret;
}

/********************************************************************************
 *  GetNewKeys()
 * Generate new set of SE keys in flash
 * Calculate CRC for 80 bytes of data
 * Encrypt Kdec (3 x SeKeys[16] + Krnd[32])
 */
void getNewKeys(uint8_t *buf, const idxBuffer_t *pCommsBuffer){
  uint16_t crc = 0;  // it should be 0 as it is used as IV
  uint8_t Kdec[16];
  memcpy(Kdec,(uint8_t*)pNvmKeys +32,16);
  setSeKeys(); // new set of SE keys  
  
  // prepare message for MCU 
  memcpy(buf,pNvmKeys,48);  // copy Kenc, Kmac, Kdec
  genKrnd(buf+48);          // generate Krnd[32] in the same buffer
  
  idxHalAlgoCRC16(buf, 80, &crc);

  // encode the key fields  
  encryptCBC(Kdec, buf,    NULL, 16);      
  encryptCBC(Kdec, buf+16, NULL, 16);       
  encryptCBC(Kdec, buf+32, NULL, 16);       
  encryptCBC(Kdec, buf+48, NULL, 32);       

  WR_U16(buf+80, crc);
}

void setNewKeys(uint8_t mode, const idxBuffer_t *pCommsBuffer){
  uint16_t comErr;
  uint16_t data_size;
  uint8_t *buf = &pCommsBuffer->pBuffer[HDR_SIZE];;
  getNewKeys(buf);
  data_size = 82;
  comErr = idxMcuComms( IDX_HOSTIF_CMD_PUTKEY, ENCRYPT_BIT|SIGN_BIT, &data_size, pCommsBuffer );

  buf[0] = mode;
  data_size = 1;
  comErr = idxMcuComms( IDX_HOSTIF_CMD_FLASHUPDATE, ENCRYPT_BIT|SIGN_BIT, &data_size, pCommsBuffer );
  UNUSED(comErr);
}
#endif

#ifdef TH89
/********************************************************************************
 * InitializeUpdate() 
 * The items involved:
 * context = ( mcuChallenge || hChallenge) OR ( mcuID || sensorID )
 * hChallenge ( random number generated by SE)
 * mcuChallenge (random number generated by MCU received on initUpdate command) 
 */
uint16_t initUpdate(const idxBuffer_t *pCommsBuffer){
  uint16_t ret;
  uint8_t i = 3; // used by init cmd
  uint8_t *pData;
  uint8_t *ctx = session.context;
  uint8_t cryptogram[16];
//  uint8_t hChallenge[8+1] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88};
  uint8_t cdata[9]={0};  // keyID[1] + hChallenge[8] 
  uint8_t uid[16];
  uint8_t cardInfo[8+8+3]; // MCU-challenge[8], MCU-cryptogram[8], counter[3] 
  idxBuffer_t rBuffer;
  uint16_t data_size;
  scp03_keys_t *keys = (scp03_keys_t *)pNvmKeys;
  pData = &pCommsBuffer->pBuffer[HDR_SIZE];
  
  pData[0] = 0; // MCU ID
  data_size = 1;
  rBuffer.bufferSize = 12;
  rBuffer.pBuffer = uid;
  ret = idxMcuCommsFull( IDX_HOSTIF_CMD_GETUID, 0, &data_size, pCommsBuffer, NULL, &rBuffer ); // MCU ID
  if( ret != IDX_ISO_SUCCESS ) { return ret; }
  
  pData[0] = 1; // sensor ID
  data_size = 1;
  rBuffer.bufferSize = 4;
  rBuffer.pBuffer = &uid[12];
  ret = idxMcuCommsFull( IDX_HOSTIF_CMD_GETUID, 0, &data_size, pCommsBuffer, NULL, &rBuffer ); // sensor ID
  if( ret != IDX_ISO_SUCCESS ) { return ret; }
  
  genHostChallenge( &cdata[1] ); // hChallenge[8]
  memcpy(&pData[0], &cdata[0], 9);
  data_size = 9;
  rBuffer.bufferSize = 19;
  rBuffer.pBuffer = cardInfo;
  ret = idxMcuCommsFull( IDX_HOSTIF_CMD_INITIALIZE_UPDATE, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
  if( ret != IDX_ISO_SUCCESS ) { return ret; }
  
  for( i = 0 ; i < 8 ; i++) ctx[i]   = cdata[i+1]; 
  for( i = 0 ; i < 8 ; i++) ctx[i+8] = cardInfo[i];
  for( i = 0 ; i < 16; i++) ctx[i]   ^= uid[i]; 
  genkeySENC(keys->Kenc, &session);
  genkeySMAC(keys->Kmac, &session);
  genkeySRMAC(keys->Kmac, &session);
  genCardCryptogram(cryptogram, &session); // should be tested against mcu_crypto
  memcpy(session.macChain,cryptogram,8);   // for  debugging
  // if success on comparing
  session.state = SEC_CH_CLOSED;  // init sec channel status
  return ret;
}

/********************************************************************************
 * GetExtAuthData() 
 * Note: For a reason GM doesn't understand THD89 can't encode in COMBUF !  
 * Data has to be build into another buffer 
 * Note: GM (encrypt part) of this command expects session.msgCounter to be 1
 * The next command within this session also expects session.msgCounter to be 1
 * so the counter is reset after this command.
 */
uint16_t getExtAuthData(void){
  uint16_t ret;
  uint8_t cryptogram[16+8+16+2+6]; // encoded[16]+ signed[8] + header[6]
  uint16_t data_size;
  idxBuffer_t cmdIdxBuffer; // THD89 doesn't allow encryption into COMBUF!!!
  session.msgCounter = 0;
  memset(session.macChain, 0, 16);
  genHostCryptogram(&cryptogram[HDR_SIZE], &session);
  session.msgCounter = 1;  // recall that ExtAuth asks for this ( at this time state = SEC_CH_CLOSE
  
  cmdIdxBuffer.bufferSize = sizeof(cryptogram);
  cmdIdxBuffer.pBuffer = cryptogram;
  data_size = 8;
  ret = idxMcuComms( IDX_HOSTIF_CMD_EXTERNAL_AUTHENTICATE, SIGN_BIT|ENCRYPT_BIT, &data_size, &cmdIdxBuffer );

  if(ret == IDX_ISO_SUCCESS)
  {
    session.state      = SEC_CH_OPEN;
    session.msgCounter = 0;
  }
  return ret;
}

/********************************************************************************
 *  Unpad Buffer - Remove ( possible message padding)
 *  Note: this code replicates MCU code
 */
int unpad_message(uint8_t *message, int size)
{
  int i;
  uint8_t *msg = message + size - 1;

  for (i = 0; i < size; i++)
  {
    if (*msg == 0x80)       break;
    else if (*msg != 0x00)  return -1;  // Found non-zero byte before 0x80
    msg--;
  }

  if (i >= 16)   return -1;  // Too much padding

  return size - i - 1;  // Returns length of unpadded message
}
#endif // TH89
/********************************************************************************
 * Data Encryption: plain-text, padding , encrypted (S-ENC , ICV) 
 *   |__Command data -plain text(Lc)__|
 *
 *   |__Command Data -plain text(Lc)__|__'80'__|__(000..)00__|  multiple of 16
 *
 *   \_________________________  ____________________________/
 *                             \/
 *               ICV --> |__AES-CBC__| <-- S-ENC 
 *
 * Note: ICV is message-counter encrypted with S-ENC   
 */
uint16_t dataEncrypt( const idxBuffer_t *pDataToEncrypt, uint16_t Lc, uint8_t bOptions, const idxBuffer_t **pEncryptedData )
{
#ifdef TH89
  if( bOptions & BOPTION_ENCRYPTED )
  { // Encrypt Data
    // THD89 doesn't allow encryption into COMBUF
	// Do in-place encryption in application buffer
    uint8_t  icv[16];
    uint8_t  padding;
    uint16_t newLc;
    uint8_t  *p;

    if(Lc != 0)
    {		
      p    = &pDataToEncrypt->pBuffer[HDR_SIZE] + Lc;
      *p++ = 0x80;
      Lc++;

      padding = ( Lc & 0x0f ) ? 1 : 0;
      newLc   = padding ? ( Lc & ~0x0f) + 16 : Lc;
      if( padding )
	  {
        while( p < &pDataToEncrypt->pBuffer[HDR_SIZE] + newLc) *p++ = 0x00; 
      }
      // compute ICV
      {  
        memset( icv, 0, 16);
        // msg counter big endian format
        icv[14] = ((unsigned char *)&session.msgCounter)[1];
        icv[15] = ((unsigned char *)&session.msgCounter)[0];
        encryptCBC( session.senc, icv, NULL, 16 ); // iv = zeros
      }
      // compute edata
      encryptCBC( session.senc, &pDataToEncrypt->pBuffer[HDR_SIZE], icv, newLc );
	
      Lc = newLc;
	}
  }
#else
  UNUSED(bOptions);
#endif

  // Use main buffer for SPI communication
  *pEncryptedData = pDataToEncrypt;
	
  return Lc;
}

uint16_t dataDecrypt( const idxBuffer_t *pDataToDecrypt,
                      uint16_t Lcc,
                      uint8_t bOptions,
                      const idxBuffer_t *pCommsBuffer,
                      const idxBuffer_t *pRespDataBuf )
{
  uint16_t i;
  uint8_t* pDecryptedBuffer;
  uint16_t decryptedBufferSize;
  if ( ( NULL != pRespDataBuf ) && ( NULL != pRespDataBuf->pBuffer ) )
  {
    // pRespDataBuf doesn't contain header
    pDecryptedBuffer = &pRespDataBuf->pBuffer[0];
    decryptedBufferSize = pRespDataBuf->bufferSize;
  } 
  else if ( ( NULL != pCommsBuffer ) && ( NULL != pCommsBuffer->pBuffer ) )
  {
    // pCommsBuffer contains header, skip it
    pDecryptedBuffer = &pCommsBuffer->pBuffer[HDR_SIZE];
    decryptedBufferSize = pCommsBuffer->bufferSize;
  } else {
    return 0; // Shouldn't reach there.
  }

  if( (bOptions & BOPTION_ENCRYPTED) && ( ( Lcc & 0x0f ) == 0 ) )
  {
#ifdef TH89
    uint8_t icv[16];

    if(Lcc == 0) return 0; 
    // compute ICV
    {
      memset(icv, 0, 16);
      // msg counter big endian format
      icv[0]  = 0x80;
      icv[14] = ((unsigned char *)&session.msgCounter)[1];
      icv[15] = ((unsigned char *)&session.msgCounter)[0];
      encryptCBC(session.senc, icv, NULL, 16); // iv = zeros
    }
    // Check pRespDataBuf size before decryption
    decryptCBC(pDecryptedBuffer, session.senc, &pDataToDecrypt->pBuffer[HDR_SIZE], icv, Lcc);
    Lcc = unpad_message(pDecryptedBuffer, Lcc);
#else
    UNUSED(bOptions);
#endif
  } else {
    // If the main buffer is not same SPI buffer
    if (pDecryptedBuffer != pDataToDecrypt->pBuffer)
    {
      // Make sure that there is enough space
      if (Lcc > decryptedBufferSize)
        return IDX_ISO_ERR_COMM;

      for (i = 0; i < Lcc; i++)
        pDecryptedBuffer[i] = pDataToDecrypt->pBuffer[HDR_SIZE + i];
    }
  }

  return Lcc;
}

/********************************************************************************
 * Message Signing:
 *                 |_'84'_|_INS P1 P2_|_Lc__|__Command data - plain text(Lc)__|
 *
 *   |_MAC chaining|_'84'_|_INS P1 P2_|_Lcc_|__Command data - plain text(Lc)__|
 *
 *   \____________________________  _________________________________________/
 *                                \/
 *                      |_C-MAC calculation_| <-- S-MAC 
 *
 *   |_'84'_|_INS P1 P2_|_Lcc__|__Command data - plain text(Lcc-8)__|_msb(C-MAC)_|
 */
void sign(uint8_t bOrdinal,uint8_t bOptions, uint8_t *cdata, uint16_t Lc)
{
#ifdef TH89
  uint8_t tmp[16];
  uint8_t first16bitBlk[16];
  uint16_t wSize = Lc + 4 + 8;  // Header[4] + C-MAC[8]
  uint16_t i;
  encryptCMAC(tmp,session.smac, session.macChain, NULL, 16 ); // iv = zeros
  first16bitBlk[0] = 0x84;
  first16bitBlk[1] = 0x00;
  first16bitBlk[2] = bOrdinal;
  first16bitBlk[3] = bOptions;
  first16bitBlk[4] = wSize & 0xff;
  first16bitBlk[5] = wSize >> 8;
  if ( Lc == 0 ){ // no command data
    cmac_compute( session.macChain, session.smac, tmp, first16bitBlk, 6);
  }
  else if ( Lc < 10 ){   // here 0 < Lc < 10 
    for(i = 6 ; i < 6 + Lc ; i++) first16bitBlk[i] = cdata[i-6];
    cmac_compute( session.macChain, session.smac, tmp, first16bitBlk, 6 + Lc);
  }
  else{  //  Lc > 10 
    for(i = 6 ; i < 16 ; i++) first16bitBlk[i] = cdata[i-6];  // fill up the first block
    encryptCBC(session.smac, first16bitBlk, tmp, 16 );
    cmac_compute(session.macChain, session.smac, first16bitBlk, &cdata[10], Lc - 10);
    
  }
#else // SLE/SLC/HUADA
  // Do nothing 
  UNUSED(bOrdinal);
  UNUSED(bOptions);
  UNUSED(cdata);
  UNUSED(Lc);
#endif
}
//---------------------------------  End of file -----------------------------------------
