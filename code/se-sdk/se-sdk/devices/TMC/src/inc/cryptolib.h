/*
* Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
* All rights reserved.
*
* FileName: cryptolib.h
* SCFID: 
* Feature: algorithm
* Version: V0.1
*/
#ifndef __CRYPTO_LIB_H__
#define __CRYPTO_LIB_H__
#define SYM_ENC 				0x0A
#define SYM_DEC					0x05
#define SYM_DES_KEY_MSK0 		0x000
#define SYM_DES_KEY_MSK2 		0x200	
#define SYM_AES_MSK0 			0x000
#define SYM_AES_MSK2 			0x200	
#define SYM_DES_IN_MSK0 		0x0000
#define SYM_DES_IN_MSK2 		0x2000		
#define SYM_OUT_MSK0 			0x50000
#define SYM_OUT_MSK2 			0xA0000
#define SYM_NORMAL_SECURITY		0x500000
#define	SYM_NOT_INVERSE			0x5000000
#define SYM_SINGLEDES  			0xA0
#define SYM_TDES_2KEY			0x50
#define SYM_TDES_3KEY			0x30
#define SYM_AES_128KEY  		0xA0
#define SYM_AES_192KEY			0x50
#define SYM_AES_256KEY			0x30

typedef struct{
	u8* cipherKey;        //[IN]��Կ�����ģ�key^MK��
	u8* MK;    			  //[IN]��Կ���Ĵ洢��Կ
	u8* TK;				  //[IN]��Կ������Կ
	u8* cipherInput;	  //[IN]���������������(Input^MKIn)
	u8* MKIn;			  //[IN]����������Ĵ洢��Կ
	u8* TKIn;			  //[IN]���������ݵĴ�����Կ
}DES_SECURE_PARAM;

/* SM3 */
typedef struct
{
	uint32_t blocks;
	uint8_t result[32];	
	uint32_t buffer[16];
	uint32_t length;	
}SM3_Context_t;
/* SM3 end */

/* SHA1 */
#define  SHA1_Context_t  SM3_Context_t
/* SHA1 end */

/* SHA256 */
#define  SHA256_Context_t  SM3_Context_t
/* SHA256 end */

/* RSA */
typedef struct
{
    BigNum_t N;
    BigNum_t E;
}RSAPubKey_t;

typedef struct
{
    BigNum_t N;
    BigNum_t D;
}RSAPrivKey_t;

typedef struct
{
    BigNum_t P;
    BigNum_t Q;
    BigNum_t DP;
    BigNum_t DQ;
    BigNum_t QInv;
}RSAPrivKeyCrt_t;

typedef struct {
    RSAPrivKey_t PriKey;
    RSAPrivKeyCrt_t PriCrtKey;
}RSAKeyGenOut_t;

/* PKE defined */
typedef struct{
	unsigned char* pucN;
	unsigned char* pucD;
	unsigned char* pucP;
	unsigned char* pucQ;
	unsigned char* pucDp;
	unsigned char* pucDq;
	unsigned char* pucIq;
}RSAKGOut;
/* RSA end */

/* SM2 */
typedef struct
{
	uint8_t isSender;
	uint8_t *pbThisPriKey;
	uint8_t *pbThisTempPriKey;
	uint8_t *pbThisTempPubKey;
	uint8_t *pbThisZ;
	uint8_t *pbOtherPubKey;
	uint8_t *pbOtherTempPubKey;
	uint8_t *pbOtherZ;
}SM2KeyExchangeParam_t;
typedef struct
{
	uint8_t *pbSymKey;
	uint8_t *pbThisVerifyData;
	uint8_t *pbOtherVerifyData;
}SM2KeyExchangeOut_t;
/* SM2 end */

/* ECC */
typedef struct{
	BigNum_t* pbnP;
	BigNum_t* pbnN;
	BigNum_t* pbnA;
	BigNum_t* pbnB;
	BigNum_t* pbnGx;
	BigNum_t* pbnGy;
}ECC_Domain_t;
/* ECC end */

/* PKE Function pointer ---------------------------------------------------------*/
/* RSA */
typedef uint16_t (* ptmcRSAEncrypt)(BigNum_t *pResult, BigNum_t *pMessage,RSAPubKey_t *pPubKey);
typedef uint16_t (* ptmcRSADecrypt)(BigNum_t *pResult, BigNum_t *pMessage, RSAPrivKey_t *pPrivKey);
typedef uint16_t (* ptmcRSAKeyPairGen)(RSAKeyGenOut_t *pKey,BigNum_t *pE, uint16_t usBitLenKey, uint8_t mode);
typedef uint16_t (* ptmcRSADecryptCrt_SU)(BigNum_t *pResult,BigNum_t *pMessage, RSAPrivKeyCrt_t *pPrivKeyCrt,BigNum_t *MK, BigNum_t *pE);
typedef uint16_t (* ptmcRSACrtGetE)(BigNum_t *pE,  BigNum_t *pP,BigNum_t *pQ, BigNum_t *pDP, BigNum_t *pDQ, BigNum_t *MK);
/* Pke interface */
typedef uint16_t (* pPkeRSASec_Keygen)(RSAKGOut* prkOut, BigNum_t* pbnE, uint16_t usCLenP, uint16_t usCLenQ, uint8_t ucMode);

/* SM2 */
typedef uint16_t (* ptmcSM2GetHashData)(uint8_t *pbHashData, uint8_t *pbPubKey, BigNum_t *pID, BigNum_t *pMessage);
typedef uint16_t (* ptmcSM2Sign)(uint8_t *pbSignature, uint8_t *pbPriKey, uint8_t *pbInput);
typedef uint16_t (* ptmcSM2Verify)(uint8_t *pbPubKey, uint8_t *pbInput, uint8_t *pbSignature);
typedef uint16_t (* ptmcSM2Encrypt)(BigNum_t *pCrypto, BigNum_t *pMessage, uint8_t *pbPublicKey);
typedef uint16_t (* ptmcSM2Decrypt)(BigNum_t *pMessage, BigNum_t *pCrypto, uint8_t *pbPrivateKey);
typedef uint16_t (* ptmcSM2KeyPairGen)(uint8_t *pbPubkey, uint8_t *pbPrikey, uint8_t mode);
typedef uint16_t (* ptmcSM2ZGen)(uint8_t *pbHashData, uint8_t *pbPubKey, BigNum_t *pID);
typedef uint16_t (* ptmcSM2KeyExchange)(SM2KeyExchangeOut_t *pOut, SM2KeyExchangeParam_t *pParam,uint8_t symKeyLen);

/* Pke interface */
typedef uint16_t  (* pPkeSM2Sec_PtChk)(uint8_t * pucPt);
/* ECC */	
typedef uint16_t (* ptmcECCParamCheck)(ECC_Domain_t *domain);
typedef uint16_t (* ptmcECCPointAdd)(BigNum_t *output, BigNum_t *point1, BigNum_t *point2, ECC_Domain_t *domain);
typedef uint16_t (* ptmcECCPointMul)(BigNum_t *output, BigNum_t *point, BigNum_t *K, ECC_Domain_t *domain);
typedef uint16_t (* ptmcECCKeyGen)(BigNum_t *publicKey, BigNum_t *privateKey, ECC_Domain_t *domain, uint8_t mode);
typedef uint16_t (* ptmcECCSign)(BigNum_t *output, BigNum_t* E, BigNum_t *privateKey, ECC_Domain_t *domain);
typedef uint16_t (* ptmcECCVerify)(BigNum_t *E, BigNum_t *publicKey, BigNum_t *signature, ECC_Domain_t *domain);

/* HASH */
typedef void 		(* ptmcSM3Init)(SM3_Context_t *c);
typedef void 		(* ptmcSM3Update)(SM3_Context_t *c,const void *data,uint32_t length);
typedef uint16_t 	(* ptmcSM3Final)(uint8_t *output,SM3_Context_t *c);
typedef void 		(* ptmcSHA1Init)(SHA1_Context_t *c);
typedef void 		(* ptmcSHA1Update)(SHA1_Context_t *c,const void *data,uint32_t length);
typedef uint16_t 	(* ptmcSHA1Final)(uint8_t *output,SHA1_Context_t *c);
typedef void 		(* ptmcSHA256Init)(SHA256_Context_t *c);
typedef void 		(* ptmcSHA256Update)(SHA256_Context_t *c,const void *data,uint32_t length);
typedef uint16_t 	(* ptmcSHA256Final)(uint8_t *output,SHA256_Context_t *c);

/* Get version */
typedef void (* ptmcPkeGetVer)(uint8_t* pucVer);

/* PKE basic api */
typedef uint8_t (* pPkeGetRandomNum)(uint8_t mode,uint8_t *buf,uint16_t len);
typedef void (*	pPkeCfg)(uint8_t eccA,uint8_t eccZ1,uint8_t mulCfg);

#define ROMTABLEADDR			0x04000000

#define RSAFUNCADDR				(ROMTABLEADDR)
#define SM2FUNCADDR				(ROMTABLEADDR + 0x18)
#define	ECCFUNCADDR				(ROMTABLEADDR + 0x38)
#define	HASHFUNCADDR			(ROMTABLEADDR + 0x50)
#define PKERSAFUNCADDR			(ROMTABLEADDR + 0x74)
#define PKESM2FUNCADDR		    (ROMTABLEADDR + 0x90)
#define PKEBASEAPIADDR			(ROMTABLEADDR + 0x100)

/* RSA */
#define			tmcRSAEncrypt 				((ptmcRSAEncrypt)(*(uint32_t *)(RSAFUNCADDR + 0)))
#define			tmcRSADecrypt 				((ptmcRSADecrypt)(*(uint32_t *)(RSAFUNCADDR + 4)))
#define			tmcRSAKeyPairGen 			((ptmcRSAKeyPairGen)(*(uint32_t *)(RSAFUNCADDR + 0xC)))
#define			tmcRSADecryptCrt_SU 		((ptmcRSADecryptCrt_SU)(*(uint32_t *)(RSAFUNCADDR + 0x10)))
#define			tmcRSACrtGetE 				((ptmcRSACrtGetE)(*(uint32_t *)(RSAFUNCADDR + 0x14)))
/* Pke defined */
#define 		PkeRSASec_Keygen			((pPkeRSASec_Keygen)(*(uint32_t *)(PKERSAFUNCADDR + 0x0C)))
/* ECC */
#define			tmcECCParamCheck 			((ptmcECCParamCheck)(*(uint32_t *)(ECCFUNCADDR + 0)))
#define			tmcECCPointAdd 				((ptmcECCPointAdd)(*(uint32_t *)(ECCFUNCADDR + 4)))
#define			tmcECCPointMul 				((ptmcECCPointMul)(*(uint32_t *)(ECCFUNCADDR + 8)))
#define			tmcECCKeyGen 				((ptmcECCKeyGen)(*(uint32_t *)(ECCFUNCADDR + 0x0C)))
#define			tmcECCSign 					((ptmcECCSign)(*(uint32_t *)(ECCFUNCADDR + 0x10)))
#define			tmcECCVerify 				((ptmcECCVerify)(*(uint32_t *)(ECCFUNCADDR + 0x14)))


/* Hash */
#define			tmcSHA1Init 				((ptmcSHA1Init)(*(uint32_t *)(HASHFUNCADDR + 0xC)))
#define			tmcSHA1Update 				((ptmcSHA1Update)(*(uint32_t *)(HASHFUNCADDR + 0x10)))
#define			tmcSHA1Final 				((ptmcSHA1Final)(*(uint32_t *)(HASHFUNCADDR + 0x14))) 
#define			tmcSHA256Init 				((ptmcSHA256Init)(*(uint32_t *)(HASHFUNCADDR + 0x18)))
#define			tmcSHA256Update 			((ptmcSHA256Update)(*(uint32_t *)(HASHFUNCADDR + 0x1C)))
#define			tmcSHA256Final 				((ptmcSHA256Final)(*(uint32_t *)(HASHFUNCADDR + 0x20)))


/*
* Function:unsigned char tmcDES_ECB_Mask (DES_SECURE_PARAM *pbInput, unsigned char *pbDataOutput, unsigned long mode)
* Description: DES/TDES ECB calculation.
* Input: pbInput - The DES key and data.
*        bmode - bit0~bit3: A-Encrypt, 5-Decrypt.
*                bit4~bit7: A-Sigle DES, 5-Triple DES(2key), 3-Triple DES(3key).
*                bit8~bit19:0
*				 bit20~bit23:(security level) 5-normal security level, others-high security level.
*				 bit24~bit27:(caculate times) 5-no inverse caculate, others-inverse caculate.
* Output: pbDataOutput - Output result. 8 bytes length.
* Return: - 0xAA - success, other - fail.
* Other:
*/

unsigned char tmcDES_ECB_Mask (DES_SECURE_PARAM *pbInput, unsigned char *pbDataOutput, unsigned long mode);

/*
* Function: tmcAES_ECB_Mask (unsigned char *pbKey, unsigned char *pbDataInput, unsigned char *MK, unsigned char *TK, unsigned char *pbDataOutput, unsigned long bmode)
* Description: AES ECB calculation.
* Input: pbKey - The AES key.
*        pbDataInput - The input data.
*        MK - bus mask.
*		 TK - bus mask.
*        bmode - bit0~bit3: A-Encrypt, 5-Decrypt.
*                bit4~bit7: A-128bit key, 5-192bit key, 3-256bit key.
*                bit8~bit19:0
*				 bit20~bit23:(security level) 5-normal security level, others-high security level.
*				 bit24~bit27:(caculate times) 5-no inverse caculate, others-inverse caculate.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -	0xAA - success, other - fail.
* Other:
*/
unsigned char tmcAES_ECB_Mask (unsigned char *pbKey, unsigned char *pbDataInput, unsigned char *MK, unsigned char *TK, unsigned char *pbDataOutput, unsigned long bmode);
/*
* Function: void CRC(u8 * pDataOut, u8 * pDataIn, u32 len, u32 initValue, u8 mode)
* Description: CRC Calculation.
* Input:  pDataIn - Source data.
*         len - Length of source data.
*         initValue - Init value.
*         mode - config of CRCCON1
*    						bit5-4 CRC type
*    							1x:	CRC32
*    							00:	CRC16-CCITT
*    						bit3 result out order
*    							1: high byte out first
*    							0: low byte out first
*    						bit2	result byte endian
*    							1:	change
*    							0: 	not
*    						bit1	input byte endian
*    							1:	change
*    							0: 	not
* Output: dataout - Output random data.
* Return: -
* Other:
*/
void CRC(u8 * pDataOut, u8 * pDataIn, u32 len, u32 initValue, u8 mode);

/*
* Function: void CRC16(u8 *iv, u8 *buf, u32 len)
* Description: CRC Calculation.
* Input:  buf - Source data.
*         len - Source data length.
*         iv - Initialization vector. 16 bits length.
* Output: iv - Output CRC. 16 bits length.
* Return: -
* Other:
*/
void CRC16(u8 *iv, u8 *buf, u32 len);

/*
* Function: void CRC32(u8 *iv, u8 *buf, u32 len)
* Description: CRC Calculation.
* Input:  buf - Source data.
*         len - Source data length.
*         iv - Initialization vector. 32 bits length.
* Output: iv - Output CRC. 32 bits length.
* Return: -
* Other:
*/
void CRC32(u8 *iv, u8 *buf, u32 len);

/* SM2 */
#define			tmcSM2GetHashData 			((ptmcSM2GetHashData)(*(uint32_t *)(SM2FUNCADDR + 0)))
#define 		tmcSM2Sign                  ((ptmcSM2Sign)(*(uint32_t *)(SM2FUNCADDR + 4)))
#define			tmcSM2Verify 				((ptmcSM2Verify)(*(uint32_t *)(SM2FUNCADDR + 8)))
#define			tmcSM2Encrypt 				((ptmcSM2Encrypt)(*(uint32_t *)(SM2FUNCADDR + 0x0C)))
#define			tmcSM2Decrypt 				((ptmcSM2Decrypt)(*(uint32_t *)(SM2FUNCADDR + 0x10)))
#define			tmcSM2KeyPairGen 			((ptmcSM2KeyPairGen)(*(uint32_t *)(SM2FUNCADDR + 0x14)))
#define			tmcSM2ZGen 					((ptmcSM2ZGen)(*(uint32_t *)(SM2FUNCADDR + 0x18)))
#define			tmcSM2KeyExchange 			((ptmcSM2KeyExchange)(*(uint32_t *)(SM2FUNCADDR + 0x1C)))
/* Pke defined */
#define			PkeSM2Sec_PtChk				((pPkeSM2Sec_PtChk)(*((uint32_t *)(PKESM2FUNCADDR + 0))))
/* Hash */
#define			tmcSM3Init 					((ptmcSM3Init)(*(uint32_t *)(HASHFUNCADDR + 0)))
#define			tmcSM3Update 				((ptmcSM3Update)(*(uint32_t *)(HASHFUNCADDR + 4)))
#define			tmcSM3Final 				((ptmcSM3Final)(*(uint32_t *)(HASHFUNCADDR + 8)))

/* PKE get random */
#define			pkeConfig					((pPkeCfg)(*(uint32_t *)(PKEBASEAPIADDR + 0)))
#define 		pkeGetRandomNum				((pPkeGetRandomNum)(*(uint32_t *)(PKEBASEAPIADDR + 0x04)))
#define			PkePrmJdg					((pPkePrmJdg)(*(uint32_t *)(PKEBASEAPIADDR + 0x08)))
#define			tmcPKEGetVer				((ptmcPkeGetVer)(*(uint32_t *)(PKEBASEAPIADDR  + 0x18)))

/*
* Function: void cryptoSM1ECB(u8 * pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode)
* Description: SM1 calculation.
* Input:  pbKey - EK. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - 0 Encrypt, 1 Decrypt.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSM1ECB(u8 * pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode);

/*
* Function: void CryptoSM1Version(unsigned char *version)
* Description: Get CryptoSM1 Version.
* Input: -
* Output: version
* Return: none
* Other:
*/
void CryptoSM1Version(unsigned char *version);

/*
* Function: u8 cryptoSM4ECB(u8 *pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode)
* Description: SM4 ECB calculation.
* Input:  pbKey - SM4 key. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
*                bit1: 0 Use new key, 1 Use last key.
*                bit2: 0 no mask, 1 with mask
*                bit3: 0 no reverse, 1 reverse
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: 0xaa success
*         0x55 fail
* Other:
*/
u8 cryptoSM4ECB(u8 *pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode);

/*
* Function: u8 cryptoSM4CBC(u8 *pbKey, u8 *pbIV, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode)
* Description: SM4 CBC calculation.
* Input:  pbKey - SM4 key. 16 bytes length.
*         pbIV - Initialization vector. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
*                bit1: 0 Use new key, 1 Use last key.
*                bit2: 0 no mask, 1 with mask
*                bit3: 0 no reverse, 1 reverse
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: 0xaa success
*         0x55 fail
* Other:
*/
u8 cryptoSM4CBC(u8 *pbKey, u8 *pbIV, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode);

/*
* Function: void CrytoSM4LibVersion(unsigned char *version)
* Description: Get CryptoSM4 Version.
* Input: -
* Output: version
* Return: none
* Other:
*/
void CrytoSM4LibVersion(unsigned char *version);

/*
* Function: void cryptoSSF33ECB(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode)
* Description: SSF33 calculation.
* Input:  pbKey - MK. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSSF33ECB(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode);

/*
* Function: void CryptoSSF33Version(unsigned char *version)
* Description: Get CryptoSSF33 Version.
* Input: -
* Output: version
* Return: none
* Other:
*/
void CryptoSSF33Version(unsigned char *version);

#endif

