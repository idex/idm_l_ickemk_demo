/*
 *  Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: democmd.c
 *  SCFID: 
 *  Feature: api of communication
 *  Version: V0.1
 * 
 *  History: 
 *    2014-11-18
 *      1. Original version 0.1
 * */
#include <string.h>
#include "halglobal.h"
#include "democmd_EMV.h"
#include "pkecmd.h"
#include "idxHalGpio.h"
#include "idxHalSpi.h"
#include "idxHalSystem.h"

#include "idxSerialInterface.h"
#include "dispatcher.h"
#include "idxSeBioAPI.h"

extern u8 cfgCrypto1Init(u8 mode,u32 ulNVMAddr);
extern u8 readWriteCrypto1(u8 mode,u8* data,u16 offset,u16 block);
extern u8 readWriteCrypto1_purePwd(u8 mode,u8* data,u16 offset,u16 block);
extern u8 restoreDefaults(u16 secterNumber);
extern unsigned char const CRYPTO1DATA[];
extern void GetVersion(void);
//extern idxHostifState SerialState ;

typedef void (*COS_COMMAND) ();
typedef struct {
	u8 bCLA;
	u8 bINS;
	u8 bCase;
	COS_COMMAND pFunc;
} COMMAND_ITEM;

volatile unsigned char SM2BUF[0x880];// __attribute__((at(0x20001000)));

void setCLK(void)
{
	clkConfig(PA1);
	IoSetStatus(0x9000);
}

/*
* Function: u8 NvmUpdateSector(u8 *pbDest, u8 *pbSrc, u16 usLen)
* Description: erase and program nvm 
* Input: dest - destination address for erase and program
*        src - source address
*		 len - erase and program lengh
* Output: -
* Return: 
*		0:successful execution
* 		1:error execution
*/
u8 NvmUpdateSector(u8 *pbDest, u8 *pbSrc, u16 usLen)
{
	if ((((u32)pbDest == 0x0c000000)||((u32)pbDest == 0x0c000080) || ((u32)pbDest == 0x0c000100)) && (0x80 == usLen))
	{
       _HalRamCopy((u8 *)SM2BUF+ ((u32)pbDest - 0x0c000000), pbSrc, usLen);
	}
	else if (((u32)pbDest == 0x0c000180) && (0x80 == usLen))
	{
//		_HalRamCopy(pbDest, pbSrc, usLen);
		if (NvmFastEraseSector((u8 *)0x0c000000))
		{
			return 1;
		}
		if (NvmProgramWords((u8 *)(0x0c000000), (u8 *)SM2BUF, 0x100))
		{
			return 1;
		}
		if (NvmProgramWords((u8 *)(0x0c000100), (u8 *)(SM2BUF+0x100), 0x80))
		{
			return 1;
		}
		if (NvmProgramWords((u8 *)(0x0c000180), (u8 *)pbSrc, 0x80))
		{
			return 1;
		}
//		return 0;

	}
	if ((((u32)pbDest == 0x14000200)||((u32)pbDest == 0x14000280) || ((u32)pbDest == 0x14000300)) && (0x80 == usLen))
	{
       _HalRamCopy((u8 *)SM2BUF+ ((u32)pbDest - 0x14000200), pbSrc, usLen);
	}
	else if (((u32)pbDest == 0x14000380) && (0x80 == usLen))
	{
//		_HalRamCopy(pbDest, pbSrc, usLen);
		if (NvmFastEraseSector((u8 *)0x14000200))
		{
			return 1;
		}
		if (NvmProgramWords((u8 *)(0x14000200), (u8 *)SM2BUF, 0x100))
		{
			return 1;
		}
		if (NvmProgramWords((u8 *)(0x14000300), (u8 *)(SM2BUF+0x100), 0x80))
		{
			return 1;
		}
		if (NvmProgramWords((u8 *)(0x14000380), (u8 *)pbSrc, 0x80))
		{
			return 1;
		}
//		return 0;

	}
	return 0;

}		

void wirteNvm(void)
{
	u32 ulAddr;
	u8 bLen,bSectorNum, i;
	u32	nFlag = 0;
	ulAddr = B2LT(CMM_DAT);
	if (PA1)
	{
		switch(PA2)
		{
			case 0:	 // erase words
				break;
			case 1:
				bLen = LC-4;
				nFlag = NvmProgramWords((u8 *)ulAddr, (u8 *)(CMM_DAT+4), bLen);	 //program nvm
				break;
			case 2:	
				break;
			case 3:
				bSectorNum = *(CMM_DAT + 4);
				for (i=0; i<bSectorNum; i++)
				{
					nFlag = NvmFastEraseSector((u8 *)(ulAddr + 0x200*i));						//sector erase					
				}
				break;
			case 4:
				bLen = *(CMM_DAT + 4);
				nFlag = NvmEraseCheck( (u8 *)ulAddr, bLen );
			default: break;
		}
	}
	else
	{
		if(!PA2)
		{
			bLen = LC-4;
			nFlag = NvmUpdate((u8 *)ulAddr, (u8 *)(CMM_DAT+4), bLen);
		}
		else
		{
			bLen = LC-4;
			nFlag = NvmUpdateSector((u8 *)ulAddr, (u8 *)(CMM_DAT+4), bLen);
		}
	}


	if ( 0 != nFlag )
	{
		ioSetStatus(0x6581);
	}
	else
	{
		ioSetStatus(0x9000);
	}
}
u16 normalWrite(u32 ulAddr, u8 *data, u8 bLen)
{
	u16 rtn = 0x9000;
	u8 i;

	if (((ulAddr&0xff000000) == 0x20000000)||((ulAddr&0xf0000000) == 0x40000000))
	{
		u32 ulWbuf[64];
		_HalRamCopy((u8 *)ulWbuf, data, bLen);
		switch (PA2)
		{
			case 0:
				for ( i = 0; i < bLen; i++ )
				{
					*(u8 *)(ulAddr + i) = *(u8 *)((u32)ulWbuf + i);
				}
				break;
			case 1:
				for ( i = 0; i < bLen/2; i++ )
				{
					*(u16 *)(ulAddr + i*2) = __REV16(*(u16 *)((u32)ulWbuf + i*2));
				}
				break;
			case 2:
				for ( i = 0; i < bLen/4; i++ )
				{
					*(u32 *)(ulAddr + i*4) = __REV(*(u32 *)((u32)ulWbuf + i*4));
				}
				break;
			default:
				break;
		}
	}
	else if (((ulAddr&0xff000000) == 0x0c000000) || ((ulAddr&0xff000000) == 0x14000000))
	{
		u8 flag = 0;
		if (PA2== 0x02)
		{
			flag = NvmProgramWords((u8 *)ulAddr, (u8 *)data, bLen);	 //program nvm
			if (flag == 0x01)
			{
				rtn = 0x6581;
			}
		}
	}

	return rtn;
}

void eraseMemory(void)
{
	u32 ulAddr;
	u8 bSectorNum, i;
	u8 flag = 0;
//	u32	nFlag = 0;
	u16 rtn = 0x9000;
	ulAddr = B2LT(CMM_DAT);
	if (PA2 == 0x04) //sector erase
	{
		bSectorNum = *(CMM_DAT + 4);
		for (i=0; i<bSectorNum; i++)
		{
			if (PA1 == 0x00) // normal erase
			{
				flag =  NvmFastEraseSector((u8 *)(ulAddr + 0x200*i));
			}
			else if (PA1 == 0x02)
			{
				flag = NvmFastEraseSector((u8 *)(ulAddr + 0x200*i));	
			}		
		}
	}
	if (flag == 1)
	{
		rtn = 0x6581;
	}
	ioSetStatus(rtn);	
}

void writeMemory(void)
{
	u32 ulAddr;
	u8 bLen;
	u16 rtn = 0x9000;
	ulAddr = B2LT(CMM_DAT);
	bLen = LC-4;
	if (PA1 == 0x00) //normal write
	{
		rtn = normalWrite(ulAddr, (CMM_DAT + 4), bLen);	
	}

	ioSetStatus(rtn);
}

u8 readRam(u8 mode, u8 *tarAddr, u32 ulAddr, u8 bLen)
{
	u8 rtn = 0;
	u8 i;
	switch (mode)
	{
		case 0:
			for( i = 0; i < bLen; i++ )
			{
				*(u8 *)(tarAddr + i) = *(u8 *)(ulAddr + i);
			}
			break;
		case 1:
			for( i = 0; i < bLen/2; i++ )
			{
				*(u16 *)(tarAddr + i*2) = __REV16(*(u16 *)(ulAddr + i*2));
			}
			break;
		case 2:
			for( i = 0; i < bLen/4; i++ )
			{
				*(u32 *)(tarAddr + i*4) = __REV(*(u32 *)(ulAddr + i*4));
			}
			break;
		default:
			rtn = 1;
			break;
	}
	return rtn;
}
// read memory
void readMemory(void)
{
	u32 ulAddr;
	u8 bLen = 0;
	u8 i;
	u8 rBuf[256] ;
	u16 rtn = 0x9000;
	u8 nFlag = 0;
	if (PA1 == 0x00)
	{
		ulAddr = B2LT(CMM_DAT);
		bLen = *(CMM_DAT+4);
		nFlag = readRam(PA2, rBuf, ulAddr, bLen);
		if(!nFlag)
		{
			_HalRamCopy(g_stCMD.pbBuf, rBuf, bLen);
		}
	}
	else if (PA1 == 0x01) // read strict
	{
		u8 mode = *CMM_DAT;
		ulAddr = B2LT(CMM_DAT+1);
		bLen = bLen = *(CMM_DAT+5);
		if (bLen&0x03)
		{
			rtn = 0x6581;
		}
		else
		{
			for (i = 0; i< bLen/4; i++)
			{
				*(u32 *)(rBuf + i*4) = NvmHalReadStrict(ulAddr + i*4, mode);		
			}
			_HalRamCopy(g_stCMD.pbBuf, rBuf, bLen);
		}	
	}
	else if (PA1 == 0x02) // read redu
	{
		ulAddr = B2LT(CMM_DAT);
		bLen = *(CMM_DAT+4);
		if (bLen&0x03)
		{
			rtn = 0x6581;
		}
		else
		{
			if(NvmReadTolerant(rBuf, (u32 *)ulAddr, bLen))
			{
				rtn = 0x6581;
			}
			_HalRamCopy(g_stCMD.pbBuf, rBuf, bLen);
		}			
	}
	else if (PA1 == 0x03)
	{
		ulAddr = B2LT(CMM_DAT);
		bLen = *(CMM_DAT + 4);
		if (NvmEraseCheck( (u8 *)ulAddr, bLen ))		//erase check
		{
			rtn = 0x6581;
		}
		bLen = 0;
	}
	ioSendData(g_stCMD.pbBuf, bLen);
	ioSetStatus(rtn);
}
void updateMemory(void)
{
	u8 bLen = LC-4;
	u32 ulAddr = B2LT(CMM_DAT);
	u8 nFlag = 0;
	bLen = LC-4;
	if (PA1 == 0)  //normal erase_prog
	{
		nFlag = NvmUpdateWords((u8 *)ulAddr, (u8 *)(CMM_DAT+4), bLen);
	}
	if(nFlag)
	{
		ioSetStatus(0x6581);	
	}
	else
	{
		ioSetStatus(0x9000);
	}		
}
void alterCfgData( void )
{
	u32 i;
	u8 status = 0;

	if (LC > 2)
	{
		for (i=0; i<512; i++)
		{
			SM2BUF[i] = 0;
		}
		_HalRamCopy((u8 *)(SM2BUF+0x100), (u8 *)g_pbComCfg, COM_CFG_SIZE);
		_HalRamCopy((u8 *)(SM2BUF + 0x100+PA2), CMM_DAT+2, LC-2);  
		if(NvmFastEraseSector((u8 *)0x0c050e00))
		{
			status = 1;
		}
		if (NvmProgramWords((u8 *)0x0c050e00, (u8 *)SM2BUF, 0x100))
		{
			status = 1;
		}
		if (NvmProgramWords((u8 *)(g_pbComCfg), (u8 *)(SM2BUF+0x100), 0x100))
		{
			status = 1;
		}
	}
	else
	{
		ioSendData((u8 *)(g_pbComCfg + PA2), COM_CFG_SIZE - PA2);	
	}
	if (status)
	{
		ioSetStatus(0x6581);
	}
	else
	{
		ioSetStatus(0x9000);
	}
}

void SM1Cacul(u8 type, u8 *data, u8* output)
{
	u8 mode;
	u32 wDatAddr;
	u32 EK;

	if (type == 0x01)
	{
		mode = 1;	//dec	
	}
	else
	{
		mode = 0; //enc
	}
	EK = (u32)(data);
	wDatAddr = (u32)(data +16);
	cryptoSM1ECB((u8 *)EK, (u8 *)wDatAddr, output, mode);
}

void SM4Cacul(u8 type, u8 *data, u8 *output)
{
	u32 wKeyAddr, wDatAddr;
	
	if (type&0x10)
	{
		wKeyAddr = (u32)data;
		wDatAddr = (u32)(data + 32);
		cryptoSM4CBC((u8 *)wKeyAddr, (u8 *)(wKeyAddr+16), (u8 *)wDatAddr, output, type);			
	}
	else
	{
		wKeyAddr = (u32)data;
		wDatAddr = (u32)(data + 16);
		cryptoSM4ECB((u8 *)wKeyAddr, (u8 *)wDatAddr, output, type);	
	}

}

void SSF33Cacul(u8 type, u8* data, u8* output)
{
	u8 mode;
	u32 wKeyAddr, wDatAddr;

	if (PA2 == 0x00)
	{
		mode = 0;		
	}
	else
	{
		mode = 1;
	}

	wKeyAddr = (u32)data;
	wDatAddr = (u32)(data + 16);

	cryptoSSF33ECB((u8 *)wKeyAddr, (u8 *)wDatAddr, output, mode);	
}

u16 DESLib(u8 type, u8 * data, u8* output, u32 mode)
{
	DES_SECURE_PARAM DES_PARAM;
	u8 TK[8];
   	u8 MK[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	u16 rtn;
	u32 keyAddr,datAddr;

	mode = B2LT(data);			
	tmcGenRandomNum_SU(8, TK);
	    
	if((mode & 0xF0) == 0x50)		//	Triple DES with 2 key
	{
		keyAddr = (u32)(data+4);			
		datAddr = (u32)(data+20);
	}
	else if((mode & 0xF0) == 0x30)	//	Triple DES with 3 key
	{
		keyAddr = (u32)(data+4);			
		datAddr = (u32)(data+28);
	}
	else							//	Single DES
	{
		keyAddr = (u32)((data+4));				
		datAddr = (u32)(data+12);
	}
				
	DES_PARAM.cipherKey = (u8 *)keyAddr;
	DES_PARAM.MK = MK;
	DES_PARAM.TK = TK;
	DES_PARAM.cipherInput = (u8 *)datAddr;
	DES_PARAM.MKIn = MK;
	DES_PARAM.TKIn = TK;
	mode = (mode & 0xFFF000FF);
	mode = (mode | 0x00052200);

	if((tmcDES_ECB_Mask(&DES_PARAM, output, mode)) != 0xAA)
	{
		rtn = 0x6581;	
	}
	else
	{
		rtn = 0x9000;
	}

	return rtn;
}

u16 DESCacul(u8 type, u8 * data, u8* output)
{
	u32 mode;
	u16 rtn;
	
	mode = B2LT(data);
	
	if(type&0x01)
	{
		mode = (mode&0xFFFFFFF0)|0x05;	//	Decrypt
	}
	else
	{
		mode = (mode&0xFFFFFFF0)|0x0A;	//	Encrypt
	}

	rtn = DESLib(type, data, output, mode);	
	
	return rtn;
}

void RunCRC()
{
	u8 dataLen;
	u16 initData;
	if (PA1)
	{
		dataLen = LC-2;
		initData = *(CMM_DAT)*0x100 + *(CMM_DAT+1);
		CRC( g_stCMD.pbBuf, (u8 *)(CMM_DAT+2), dataLen, initData, PA2 );
	}
	else
	{
		dataLen = LC;
		initData = 0;
		CRC( g_stCMD.pbBuf, CMM_DAT, dataLen, initData, PA2 );
	}
	
	ioSendData(g_stCMD.pbBuf, 2);
	ioSetStatus(0x9000);	
}

void ReadRNG(void)
{
	u16 saveLen;

	if (CLA)
	{ 
		ioSetStatus(0x6D00);
		return;
	}

	saveLen = LC?LC:256;
	if (0xaa != tmcGenRandomNum_SU( saveLen, g_stCMD.pbBuf ))
	{
		ioSetStatus(0x6581);
		return;
	}

	ioSendData(g_stCMD.pbBuf, saveLen);
	ioSetStatus(0x9000);
}


//Mifare API
void Crypto1_Api(void)
{
	u8 mode;
	u16 offset;
	u16 block;
	u16 secterNumber;
	u8 rBuf[64];
	u8 i,rStatus;
	u32 addr;
		
	switch (PA2)
	{
		case 0:
			{
			 	mode = *CMM_DAT;
				addr = B2LT(CMM_DAT+1);
			 	rStatus = cfgCrypto1Init(mode,addr);
				if (!rStatus)
				{
					ioSetStatus(0x9000);	
				}
				else
					ioSetStatus(0x6581);
			}
			break;
		case 1:
			{
				mode = *CMM_DAT;
				offset = *(CMM_DAT+1);
				block = *(CMM_DAT+2);
				for (i=0; i<LC-3; i++)
					rBuf[i] = *(CMM_DAT+3+i);				
			    rStatus = readWriteCrypto1(mode,rBuf,offset,block);
				if (!rStatus)
				{
					if (1 == mode)
					{
						ioSendData(rBuf, 16+offset);
					}
					ioSetStatus(0x9000);
				}
				else
			   	ioSetStatus(0x6581);
			}
			break;
		case 2:
			{
				mode = *CMM_DAT;
				offset = *(CMM_DAT+1);
				block = *(CMM_DAT+2);
				for (i=0; i<LC-3; i++)
					rBuf[i] = *(CMM_DAT+3+i);
				rStatus	= readWriteCrypto1_purePwd(mode,rBuf,offset,block);
				if (!rStatus)
				{
					if (1 == mode)
					{
						ioSendData(rBuf, 16+offset);
					}
					ioSetStatus(0x9000);
				}
				else
				ioSetStatus(0x6581);
			}
			break;
		case 3:
			{
				secterNumber = *CMM_DAT;
				rStatus = restoreDefaults(secterNumber);
				if (!rStatus)
				{
					ioSetStatus(0x9000);
				}
				else
					ioSetStatus(0x6581);	
			}
			break;
		default:
			IoSetStatus(0x6D00);
			break;										
	}
}

void CmdReturn(void)
{
	g_stCMD.wOutLen = 5+g_stCMD.header[4];
	IoSetStatus(0x9000);	
}

u16 AESLib(u8 type, u8 *data, u8 *output, u32 mode)
{	
	u8 TK[8];
	u8 MK[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	u16 rtn;
	u32 keyAddr;
	u32 datAddr;	

	tmcGenRandomNum_SU(8, TK );

	if((mode & 0xf0) == 0x50)
	{
		keyAddr = (u32)(data+4);			
		datAddr = (u32)(data+28);
	}
	else if((mode & 0xf0) == 0x30)
	{
		keyAddr = (u32)(data+4);			
		datAddr = (u32)(data+36);	
	}
	else
	{
		keyAddr = (u32)((data+4));				
		datAddr = (u32)(data+20);
	}
	mode = (mode & 0xFFF000FF);
	mode = (mode | 0x00052200);
	
	if(tmcAES_ECB_Mask((u8*)keyAddr, (u8*)datAddr, (u8*)MK, (u8*)TK, output, mode) != 0xAA)
	{
		rtn = 0x6581;
	}
	else
	{
		rtn = 0x9000;
	}
	
	return rtn;
}

u16 AESCacul(u8 type, u8 *data, u8 *output)
{
	u16 rtn;
	u32 mode;

	mode = B2LT(data);
	
	if((type&0x0F) == 0x00)
	{
		mode = (mode&0xFFFFFFF0)|0x0A;	//	Encrypt	
	}
	else if((type&0x0f)== 0x01 )
	{
		mode = (mode&0xFFFFFFF0)|0x05;	//	Decrypt
	}
	
	if((type == 0x00)||(type == 0x01))
	{
		rtn = AESLib(type, data, output, mode);
	}
	else
	{
		rtn = 0x6581;
	}
	
	return rtn;
}


void asymInput(void)
{
	u16 rtn ;
	uint8_t len = LC -1;
	switch(PA1)
	{
		case 0x0b: rtn = SM2Input( CMM_DAT, len); break;
		case 0x0c: rtn = RSAInput( CMM_DAT, len); break;
		case 0x0d: rtn = ECCInput( CMM_DAT, len); break;
		default:   rtn = 0x6a00;
	}
	IoSetStatus(rtn);
}

void asymCaculAndResult(void)
{
	u16 rtn ;
	u8 len = (u8 )(*(CMM_DAT+1));
	
	switch(PA1)
	{
		case 0x0b: rtn = SM2CaculAndResult( PA2,CMM_DAT, g_stCMD.pbBuf); break;
		case 0x0c: rtn = RSACaculAndResu1t(  PA2,CMM_DAT, g_stCMD.pbBuf); break;
		case 0x0d: rtn = ECCCaculAndResu1t(  PA2,CMM_DAT, g_stCMD.pbBuf);	 break;
		default:   rtn = 0x6a00;
	}

	if (rtn == 0x9000)
	{
		ioSendData(g_stCMD.pbBuf, len);
	}
	IoSetStatus(rtn);
}

u16 CrcCacul(u8 type , u8 *data, u8 *output, u8 len)
{
	u16 rtn = 0x9000;		
	if (type == 0x00) //CRC16 
	{
		CRC16(data+1, data+3, len-3);
		_HalRamCopy(output, data+1, 2);	
	}
	else if (type == 0x01) //CRC32
	{
		CRC32(data+1, data+5, len-5);
		_HalRamCopy(output, data+1, 4);	
	}
	else
	{
		rtn = 0x6d00;
	}
	return rtn;
}
		
void digestInit(void)
{
	u16 rtn;
	switch (PA1)
	{
		case 0x06: rtn = SM3InitFunc(); break;
		case 0x08: rtn = SHA1InitFunc();  break;
		case 0x0a: rtn = SHA256InitFunc(); break;
		default:   rtn = 0x6a00;
	}
	IoSetStatus(rtn);
}		
void digestUpdate(void)
{
	u16 rtn;
	switch (PA1)
	{
		case 0x06: rtn = SM3Input(CMM_DAT, LC);  break;
		case 0x08: rtn = SHA1Input(CMM_DAT, LC); break;
		case 0x0a: rtn = SHA256Input(CMM_DAT,LC); break;
		default:   rtn = 0x6a00;
	}
	IoSetStatus(rtn);	
}
void digestFinal(void)
{
	u16 rtn;
	u8 inputlen = LC - 1;
	u8 len = (u8 )(* (CMM_DAT+inputlen));
	switch (PA1)
	{
		case 0x06: rtn = SM3FinalFunc(CMM_DAT, g_stCMD.pbBuf,inputlen); break;
		case 0x07: 
					rtn = CrcCacul(PA2, CMM_DAT, g_stCMD.pbBuf, LC);
					if(PA2 == 0x00)
					{
						len = 2;
					}
					else if (PA2 == 0x01)
					{
						len = 4;
					}
					break;
		case 0x08: rtn = SHA1FinalFunc(CMM_DAT, g_stCMD.pbBuf,inputlen); break;
		case 0x0a: rtn = SHA256FinalFunc(CMM_DAT, g_stCMD.pbBuf,inputlen);	break;
		default:   rtn = 0x6a00;
	}
	ioSendData(g_stCMD.pbBuf, len);
	IoSetStatus(rtn);
}


void selfTestcmd(void)
{
	u8 i;
	u8 out[4];
	u32 testmsk; 
	u32 NvmAddr;  
	u8 sectorNum;  
	u8 testresult[4];
	u32 length;
//	u8 crcresult[2];
	if(PA1)
	{
		testmsk = B2LT(CMM_DAT);
		NvmAddr = B2LT(CMM_DAT +4);
		sectorNum = *(CMM_DAT+8); 
		selfcheck((u32)testmsk, (u32 )NvmAddr, (u16)sectorNum, (u32 *)&testresult);
		for(i=0;i<4;i++)
		{
			out[3-i] = testresult[i];
		}
		
		ioSendData((u8 *)out, 4);
	}
	else
	{
		u8 result;
		u16 blockNum = B2ST(CMM_DAT + 6);
		NvmAddr = B2LT(CMM_DAT);
		length = B2ST(CMM_DAT + 4);

		result = SelfCrcCheck(NvmAddr, length ,blockNum, (u8 *)(CMM_DAT + 8)); 

		if (result)	
		{
			IoSetStatus(0x6581);
			return;	
		}
	}

	IoSetStatus(0x9000);
}

void symAlg(void)
{
	u8 len = 0;
	u16 rtn = 0x9000;
	switch (PA1)
	{
		case 0x00:		rtn = DESCacul(PA2, CMM_DAT, g_stCMD.pbBuf); 	
						len = 0x08;
						break;
		case 0x01:		SM1Cacul(PA2, CMM_DAT, g_stCMD.pbBuf);
						len = 16;
					 	break;
	    case 0x03:		SSF33Cacul(PA2, CMM_DAT, g_stCMD.pbBuf); 
						len = 16;
						break;
		case 0x04:		SM4Cacul(PA2, CMM_DAT, g_stCMD.pbBuf);
						len = 16;
					 	break;
		case 0x05:		rtn = AESCacul(PA2, CMM_DAT, g_stCMD.pbBuf);
						len = 16;
					 	break;
		default:		break; 			
	}
	if (rtn == 0x9000)
	{
		ioSendData(g_stCMD.pbBuf, len);
	}
	IoSetStatus(rtn);
}
u8 mask[8];
void maskGen(void)
{
	u8 rngData[8];
	u8 i;
	
	if (0xaa != tmcGenRandomNum_SU( 8, rngData ))
	{
		ioSetStatus(0x6581);
		return;
	}
	for (i = 0; i < 8; i++)
	{
		mask[i] = rngData[i];
	}
	ioSetStatus(0x9000);		
}
void GetVersion(void)
{
	u8 bLen = 0;
	u16 rtn = 0x9000;
	switch (PA2)
	{
		//0x00	M1
		case 0x00:
			Crypto1GetVersion(g_stCMD.pbBuf);
			bLen = 2;
			break;
					
		//0x01	SM1
		case 0x01:
			CryptoSM1Version(g_stCMD.pbBuf);
			bLen = 2;
			break;	
		//0x02	SU	
		case 0x02:
			CrytoSULibVersion(g_stCMD.pbBuf);
			bLen = 2;
			break;
		//0x03	 SSF33
		case 0x03:
			CryptoSSF33Version(g_stCMD.pbBuf);
			bLen = 2;	
			break;
		//0x04	 System Clock
		case 0x04:
			CrytoSM4LibVersion(g_stCMD.pbBuf);
			bLen = 2;
			break;
		//0x05	SM4
		case 0x05:
			GetSystemClockLibVersion(g_stCMD.pbBuf);
			bLen = 2;
			break;
			
		//0x06	pke
		case 0x06:
			tmcPKEGetVer(g_stCMD.pbBuf);
			bLen = 3;
			break;
		default: rtn = 0x6d00;	
			break;
		//0x07	demo
		//case 0x07:
		//	rtn = 0x6d00;
	}
	if (rtn == 0x9000)
	{
		ioSendData(g_stCMD.pbBuf, bLen);
	}
	IoSetStatus(rtn);  
}

/* 
****************************************************************************
* 	************DO NOT MOVE THIS FUCNTION FROM HERE*******************
*		The fucntion IDEX_SPIBioHandler must be present inside of this file
*  	otherwise the security error/access violation error occurs. Precise 
*		reason behind this issue is not yet known. This is being discussed 
*		with TMC.
****************************************************************************/
/*************************************************
  Function: cmd_IDEX_SPI_BioHandler
  Description: Bio-handler command interface layer
  Input: 	none
  Output: response data
  Return:
*************************************************/
void IDEX_SPIBioHandler(void) {
	u16 SW1SW2;
	u16 size;

	idxBuffer_t buffer;
	buffer.pBuffer = g_stCMD.pbBuf;
	buffer.bufferSize = APDU_BUFFER_SIZE;

	(void) apdu_handler(&buffer);

	size   = RD_U16(buffer.pBuffer);
	SW1SW2 = RD_U16(&buffer.pBuffer[2]);

	g_stCMD.wOutLen = size;

	IoSetStatus(SW1SW2);
}

COMMAND_ITEM const g_cmdTable[] = {
 {0x00, 0xe1, 0x01, setCLK},
 {0x00, 0xEB, 0x03, alterCfgData},
 {0x00, 0xe0, 0x03, eraseMemory},
 {0x00, 0xe2, 0x03, writeMemory},
 {0x00, 0xE4, 0x04, readMemory},
 {0x00, 0xe6, 0x03, updateMemory},
 {0x00, 0xEC, 0x03, wirteNvm},
 {0x14, 0x14, 0x04, symAlg},
 {0x00, 0x84, 0x02, ReadRNG},
 {0x00, 0xf5, 0x03, Crypto1_Api},
 {0x00, 0xf6, 0x04, CmdReturn},
 {0x14, 0xf8, 0x04, selfTestcmd},
 {0x13, 0x12, 0x03, asymInput},
 {0x14, 0x12, 0x04, asymCaculAndResult},
 {0x11, 0x10, 0x01, digestInit},
 {0x13, 0x10, 0x03, digestUpdate},
 {0x14, 0x10, 0x04, digestFinal},
 {0x00, 0xfc, 0x01, maskGen},
 //The following content is for EMV transaction simulation that can realise communication protocol testing
 {0x00, 0xA4, 0x04, APDU_SelectADF},
 {0x80, 0xA8, 0x04, APDU_GPO},
 {0x00, 0xB2, 0x02, APDU_ReadRecord},
 {0x80, 0xCA, 0x02, APDU_GetData},
 {0x00, 0x20, 0x03, APDU_VerifyPin},
 {0x80, 0xAE, 0x04, APDU_GAC},
 {0xEF, 0xDC, 0x02, GetVersion},
 {0x00, 0x54, 0x04, IDEX_SPIBioHandler}
};
const u8 g_cmdItemSize = sizeof(g_cmdTable)/sizeof(COMMAND_ITEM);

u8 getCmdIndex(void)
{
	u8 i;
	for (i = 0; i < g_cmdItemSize; i++)
	{
		if ((CLA == g_cmdTable[i].bCLA) && (INS == g_cmdTable[i].bINS))
		{
			return i;
		}
	}
	return 0xff;
}



void demoMain(void)
{
	u8 bIndex;
	bIndex = getCmdIndex();
	if (bIndex == 0xff)
	{
		ioSetStatus(0x6d00);
	}
	else
	{
		if (g_cmdTable[bIndex].bCase > 2)
		{
			ioRxCmdBody(g_stCMD.pbBuf);
		}
		g_cmdTable[bIndex].pFunc();
	}
}

