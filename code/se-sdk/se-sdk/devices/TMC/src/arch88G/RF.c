/*
 *  Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: rf.c
 *  SCFID: 
 *  Feature: rf process of PICC
 *  Version: V0.1
 * 
 *  History: 
 *    2012-01-04 by Luqian
 *      1. Original version 0.1
 * */
#include "halglobal.h"
#include "typeA.h"
#include "RF.h"
#include "CL4.h"

#include "idxSerialInterface.h" // Integration of IDEX biometrics

extern void RFtxOnly(void);
extern void RFrxOnly(void);
extern void RFtxrx(void);

extern u8 rfActiveA(void);
extern u8 rfProtocolA(void);
u8 rfActive(void);

//the relationship between frame length and parameter
u16 const framesize[16] = { 16, 24, 32, 40, 48, 64, 96, 128, 256, 256, 256, 256, 256, 256, 256, 256 };

//PPSA/ATTRIB use to judge whether support boad or not
u8 const DI[8] = {0x00, 0x01, 0x02, 0x04, 0x00, 0x10, 0x20, 0x40};
u16 const g_pbTimerInit[7] = { 0xFCB0, 0xF830, 0xF060, 0xE0C0, 0xC180, 0x8300, 0x600 };	
u8 const g_pbDiv[8] = { 0, 2, 6, 14, 30, 62, 126, 253 };
extern volatile u8 g_inverseEGT;

//Anti collision function list
Anti_COMMAND  const JumpFunc[5] = {
	ActiveA,
	ActiveA,
	ProtocolA,
	ActiveA,
	rfActive
};

//RF struct golbal variable include state mathine and other information RF needed
ST_RFCurState g_stCurRFEnv;

u8 rfActive(void)
{
	if ( ((RF_BUF[0] & 0xe2) == 0x02)  \
		|| ((RF_BUF[0] & 0xe6) == 0xa2) \
		|| ((RF_BUF[0] & 0xf7) == 0xf2) \
		|| ((RF_BUF[0] & 0xf7) == 0xc2) )
	{
		return RCVDATOK;
	}
	return RFERROR;
}

void RFtxrx(void)
{	
	if((RFRSCL == 0) && (RFRSCH == 0))
	{
		RFTRMSK |= 0x80;	
		RFTRMODCLR = 0x04;
		RFTRMODSET = 0x08;
		RFTRCONSET = 0x82;
	}
	else
	{
		RFTRMSK |= 0x80;
		RFTRMODSET = 0x0C;
		RFTRCONSET = 0x82;
	}
	
	IDLECON0 |= (1<<5);		//set rf sleep
}

/*
* Function    : RFrxOnly 
* Description : open receiving,then after finishing sending data,no interrupt will be triggering
* Input  : -
* Output : -
* Return : -
*/
void RFrxOnly(void)
{ 
	RFRSCL = 0;
	RFRSCH = 0;
	RFTRMSK |= 0x80;	
	RFTRMODCLR = 0x04;
	RFTRMODSET = 0x08;
	RFTRCONSET = 0x82;
	IDLECON0 |= (1<<5);		//set rf sleep
 }

/*
* Function    : RFtxOnly 
* Description : open receiving,then after finishing sending data,interrupt will be triggering
* Input  : -
* Output : -
* Return : -
*/
void RFtxOnly(void)
{	
	RFTRMSK &= 0x7F;	
	RFTRMODCLR = 0x08;
	RFTRMODSET = 0x04;
	RFTRCONSET = 0x82;
	IDLECON0 |= (1<<5);		//set rf sleep
}

/*
* Function    : RFinit 
* Description : init RF,initialise AB(A or B or ABAuto),according to config information
* Input  : -
* Output : -
* Return : -
*/

void RFinit(void)
{
	CLKIOCON |= (1<<5);				//RF CLK enable
//	CLKIOCON &= ~0x01;				//7816 clock disable

	RFADEMCON = 0x11;
	RFPOWSTA = 0x0;	
	RFTRMODCLR = 0xFF;	
	RFTRMSK  = 0xFA;		//enable only RX, disable all error(not including timer out interrupt) and TX
	g_stCurRFEnv.err = 0;

	TypeAInit();
	
	RFTRMODSET	= 0x08;
    RFTRCONSET  = 0x83;	//set anticollision,rxtx start
	RFCKCON = 0x3F; // Modify for ASH alarm. nvm erese or write operation may cause ASH alarm when system clock is RF.
	IDLECON0 |= (1<<5);		//let rf interface sleep
}

/*
* Function    : RFProc 
* Description : data judgement of RF receiving  
* Input  : -
* Output : -
* Return :
*	0- the data has been dealt,there will be no need to treatment afterwords
* 	1- having received APDU, need to treatment afterwords
*/
u8 RFProc(void)
{
	u8 retVal;
    //State machine processing
	retVal = JumpFunc[g_stCurRFEnv.curRFState]() ;	//jump to corresponding function
    
    //return value 0,means 14443-3 command,ready to response and start send/receive
	if (retVal == RFOK)
	{
        RFtxrx();
		retVal = 0;

	}
    //return value 2,means 14443-4 command,need CL4 judge 
	else if (retVal== RCVDATOK)
	{
		g_stCurRFEnv.RF_DataLen = (RFRSCH << 8) + RFRSCL;	
		RFRSCH = 0;
		RFRSCL = 0;
		retVal = CL4_Receive();
	}
#ifdef TYPEA_SOFTANTI
	else if (retVal== REQAOK)
	{
		retVal = 0;
	}
#endif
	else //no response,receive next frame
	{
		RFRSCH = 0;
		RFRSCL = 0;
		retVal = 0;
        RFrxOnly();
	}
		
	return retVal;		
}

/*
* Function    : RFRxCmdHeader 
* Description : RF event judgement, use _RFProc to judge the receiving data
* Input  : -
* Output : -
* Return :
*	0- the data has been dealt,there will be no need to treatment afterwords
* 	1- having received APDU, need to treatment afterwords
*/
u8 RFRxCmdHeader(void)
{
	u8 rtn = 0;
	//RF power on
	if( g_stCurRFEnv.event & IS_RF_POW_POS )
	{
		g_stCurRFEnv.event = 0;	//power on and clear all event flags
		
		RSTIOCON &= ~(1<<5);	
		while(!(RSTIOCON & (1<<5)));	//RF interface soft reset

		RFinit();
	}
	//have receive RF data
	if( g_stCurRFEnv.event & IS_RF_RX_FIN )
	{
		g_stCurRFEnv.event &= CLR_RF_RX_FIN;

		RFTRCONCLR	= 0x01;	//clear Start_anti_collision
		if (g_stCurRFEnv.err)
		{
			g_stCurRFEnv.err = 0;

			if (g_stCurRFEnv.curRFState == TYPEA_ACTIVE_ATS)
			{
				TypeAInit();
			}
			else if(0xD0 == RF_BUF[0])//PPS
			{
				g_stCurRFEnv.curRFState = RF_ACTIVE;
			}
			RFrxOnly();	
		}
		else if (RFProc() == 1)
		{
			rtn = 1;
		}
	}

	return rtn;
}

void RFTxResponse(ST_COMCMD *stCMD)
{
	u16 wLen;
	u16 wStatusWord;
    u16 wExpectLen;
    wLen = stCMD->wOutLen;
    wStatusWord = stCMD->wStatusWord;

    if(stCMD->header[4])
    {
        wExpectLen = stCMD->header[4];
    }
    else
    {
        wExpectLen = 256;
    }

    if(stCMD->wOutLen && (!stCMD->bFlagIn)) //case2
    {
        if((stCMD->wOutLen != wExpectLen) && (stCMD->header[4]))
        {
            //wStatusWord = 0x6C00 | (stCMD->wOutLen&0xFF);
            //wLen = 0;
        }
    }
	if (wLen != 0)
	{
		if ((stCMD->pbBuf != gCL.pbCL4_FrameBuffer) && (stCMD->pbBuf != NULL))
			DMAMemcopy(gCL.pbCL4_FrameBuffer, stCMD->pbBuf, wLen);	
		
		gCL.pbCL4_FrameBuffer[wLen++] = *(((u8 *)&wStatusWord)+1);
		gCL.pbCL4_FrameBuffer[wLen++] = *(u8 *)&wStatusWord;
		
		CL4_SendData(wLen, 0 );
	}
	else
	{
		gCL.pbCL4_FrameBuffer[1] = *(u8 *)&wStatusWord;
		gCL.pbCL4_FrameBuffer[0] = *(((u8 *)&wStatusWord)+1);
		CL4_SendData(2, 0);
	}
}

/*
* Function    : RF_SendData 
* Description : RF send, CL4 layer can use it
* Input       :
* 	len   :	length of the sending data
* Output : -
* Return : -
*/
void RF_SendData(u8 len)
{	
	RFRSCH = 0;
	RFRSCL = len;
	if(len)
	{
		RFtxrx();
    }
	else
	{
		RFrxOnly();
	}
}

/*
* Function     : SetNeedCID 
* Description  : RF send, CL4 layer can use it
* Input : 
* 	bNeedCID: whether need CID or not, 
*		0- no need
*		1- need 
* Output : -
* Return : -
*/
void SetNeedCID( u8 *pbNeedCID )
{
    g_stCurRFEnv.pbNeedCID = pbNeedCID;
}

/*
* Function     : RF_Deselect 
* Description  : RF  Deselect command dealt,used by CL4
* Input  : -
* Output : -
* Return : -
*/
void RF_Deselect( void )
{
	if (*g_stCurRFEnv.pbNeedCID != 0)
	{										  
		RFRSCH = 0;
		RFRSCL = 2;
		RF_BUF[0] = 0XCA;	//Deselect PCB
		RF_BUF[1] = g_stCurRFEnv.CID_Value;
	}
	else
	{
		RFRSCH = 0;
		RFRSCL = 1;
		RF_BUF[0] = 0XC2;	//Deselect PCB
	}
	RFBAUDSEL &= 0x80;	//set baud 106, change baud after tx
	RFtxOnly();
	IdleMode();			//send

    TypeAInit();
    RFTRCONSET = 0x11;	//TYPEA hard Anti collision enter HALT��so it can enter into the next Anti collision
    g_stCurRFEnv.curRFState = TYPEA_HALT;
}

/*
* Function     : RF_Deselect 
* Description  : RF  WTX command dealt,used by CL4.transfer WTX,
*		   because in cos program NVM is time consuming,so count while NVM programing,
*		   when the value of counter is bigger than maximum,this function can be used
* Input  :  
*	WTX- WTXM
* Input  : -
* Output : -
* Return : -	
*/
void RFSendWTX(u8 bWTX)
{
	u8 rtn;
	rtn = 0;
	if (RF_ACTIVE == g_stCurRFEnv.curRFState)
	{
		if (bWTX > 59)
			bWTX = 59;
		while(1)
		{
			if(*g_stCurRFEnv.pbNeedCID == 0x08)	//need CID
			{
				RF_BUF[0] = 0xfa;	
				RF_BUF[1] = g_stCurRFEnv.CID_Value;
				RF_BUF[2] = bWTX;	
				RFRSCH = 0;
				RFRSCL = 3;
			}
			else
			{
				RF_BUF[0] = 0xf2;	
				RF_BUF[1] = bWTX;
				RFRSCH = 0;
				RFRSCL = 2;
			}
			RFtxrx();
			IdleMode();
			while(1)
			{
				g_stCurRFEnv.event &= CLR_RF_RX_FIN;
				if(g_stCurRFEnv.err )	//crc error
				{
					g_stCurRFEnv.err = 0;
					RFrxOnly();
					IdleMode();	
				}
				else if( 0xF2 == (RF_BUF[0] & 0xF7))	//S(WTX) response from reader
				{
					if((0x03 == RFRSCL) && (0 == RFRSCH) && (*g_stCurRFEnv.pbNeedCID == 0x08)&&(RF_BUF[1]==g_stCurRFEnv.CID_Value) )
					{
						if(bWTX == (RF_BUF[2]))
						{
							*g_stCurRFEnv.pbNeedCID = 0x08;
							return;
						}																	   
					}
					else if((2 == RFRSCL) && (0 == RFRSCH) && ((8 != *g_stCurRFEnv.pbNeedCID) || (0 == g_stCurRFEnv.CID_Value)))
					{
						if(bWTX == (RF_BUF[1]))
						{
							*g_stCurRFEnv.pbNeedCID = 0;
							return;
						}
					}
					g_stCurRFEnv.err = 1; 
				}
				else	//R(NAK), or unexpected I-block
				{
					rtn = 1;

                    if((PCB & 0xE6) == 0xA2) //R-Block
                    {
					    if(8 == *g_stCurRFEnv.pbNeedCID)
					    {
					    	if((RF_BUF[0]&0x08) && (RF_BUF[1] == g_stCurRFEnv.CID_Value))
					    	{
					    		rtn = 0;
					    	}
					    	if((!(RF_BUF[0]&0x08)&& (0 == g_stCurRFEnv.CID_Value)))
					    	{
					    		rtn = 0;
					    	}
					    }
					    else
					    {
					    	if(!(RF_BUF[0]&0x08))
					    	{
					    		rtn = 0;
					    	}
					    }
                        if(1 == rtn) break; //CID error
                        //blockNum
                        if ((gCL.CurBlockNumber & 0x01) ==  ((*gCL.pbRFBlockBuffer) & 0x01)) //blockNum
                        {
                            rtn = 0;
                            break;
                        }
                        else
                        {
                            CL4_SendACK();
                            rtn = 0;
                        }
                    }
                    else
                    {
					    break;	//from this while
                    }
				}
			}
			if(rtn)
			{
				RFrxOnly();
				IdleMode();
			}
		}
	}
}

/*
* Function     : TIMER0_IRQHandler 
* Description  : Timer0 interrupt service routine ,require WTX and WTXM is related by FWI
*	one time overflow ,the time is	67.7ms,make sure FWTtemp>=68ms,is suitable to FWT>=2
* Input  : -
* Output : -
* Return : -
*/
static bool pkeSuspended = false;

void TIMER0_IRQHandler(void)
{
	TIMER0STSCLR = 0x01;
	PKESTS = 0x04; // Suspend the PKE block
	pkeSuspended = true;
	
	idxProcessWTX();
	
	// Continuing PKE and starting the timer is done in idxSendWTX(), below
}

void idxSendWTX(void){
	// Stop the timer so that a WTX timer interrupt cannot occur while the
	// WTX request/response communication is occurring (for example from a
	// call to sendWTX() not initiated by an interrupt, but aimed at
	// ensuring that an IDEX biometric command to the MCU is not interrupted
	// by a WTX request/response).
	// Note that sendWTX() may be called recursively if the timer interrupt
	// occurs just after sendWTX() is called which will lead to two WTX
	// request/response communications in quick succession, but the actual
	// RF communication cannot be interrupted by the WTX timer interrupt.
	TIMER0STSCLR = 0x01;

	// Actually send the WTX request and get its response
	if(g_stCurRFEnv.bFWI > 13)
	{
		RFSendWTX(2);
	}
	else
	{
		RFSendWTX(1);
	}
	
	if (pkeSuspended) {
		PKECON2 = 0x02; // Continue the PKE block
		pkeSuspended = false;
	}
	// Start the timer for the next waiting period
	TIMER0CONSET = 0x0d;
}


/*
* Function     : Start WTXTimer 
* Description  : start WTX, TIMER0,clock source select RF clock
* Input  : -
* Output : -
* Return : -
**/
void StartWTXTimer(void)
{
    u16 usTimerInit = 0;
    u8 div = 0;
	u32 bkPriority = NVIC_GetPriority(RF_IRQn);
	u32 bkTimerPriority = NVIC_GetPriority(TIMER0_IRQn);
	NVIC_SetPriority(TIMER0_IRQn, bkPriority+1);//set Timer priority level lower than RFpriority level
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
	NVIC_EnableIRQ(TIMER0_IRQn);
    if(g_stCurRFEnv.bFWI < 7)
    {
        usTimerInit = g_pbTimerInit[g_stCurRFEnv.bFWI];
    }
	else if(7 == g_stCurRFEnv.bFWI)
	{
		usTimerInit = 0x395e;		//50850	* 2 / 3.39M = 30ms	 50850 == 0xc6a2
		div = 1;
	}
    else
    {
        div = g_pbDiv[g_stCurRFEnv.bFWI-7];
    } 
// 	div = 1;
//	usTimerInit = 0xfff2;		//10us
// 	usTimerInit = 0xff56;		//100us
//	usTimerInit = 0xfcb0;		//500us
// 	usTimerInit = 0xbdca;		//5ms
	
	CLKTRCON |= 0x01;		//enabel timer0 clk
	CLKTR0SEL = 0x07;	//select RF clock source is 3.39M
	TIMER0CONCLR = 0xff; 	//clear sfr
	TIMER0RLD = usTimerInit;
	TIMER0DIV = div;
	TIMER0CONSET = 0x0d; 	//set the condition when CPU is sleeping TIMER0 remains to count and start start to counting
	INTTRCON |= 0x01;        //enable timer0 interrupt
	ABORTTRCON |= 1;	
}

/*
* Function     : StopTimer0 
* Description  : stop TIMER0 count
*/
void StopTimer0(void)
{
	TIMER0CONCLR = 0x01;	//stop TIMER0
	RSTTRCON = 0xfffffffe; //when stop timer0��softreset timer0 to make sure it is stoped indeed
    INTTRCON &= ~(0x01);     //disable timer0 interrupt
	CLKTRCON &= ~0x01;		//disabel timer0 clk
	ABORTTRCON &= 0xfe;

}

/*
* Function     : RF_IRQHandler 
* Description  : interrupt service routine
* Input  : -
* Output : -
* Return : -
*/
void RF_IRQHandler(void)
{
	if(RF_RX_FIN)
	{
		RFTRSTACLR = 0x81;		//Clear RX_FIN
        RFTRCONCLR = 0x01; //close auto anticollision

		//after deselect, wake-up must be active state.
		if (g_stCurRFEnv.curRFState == TYPEA_HALT)
		{
			g_stCurRFEnv.curRFState = TYPEA_ACTIVE_ATS;
			RFTRCONCLR = 0x01;
		}

		g_stCurRFEnv.event |= IS_RF_RX_FIN;
		g_M1State |= IS_RF_RX_FIN;
	}
	
    if (RF_POW_POS)	
	{
	 	RFPOWSTA = 0;
		if(!(RFPOWSTA&0x02))
		{
			g_stCurRFEnv.event |= IS_RF_POW_POS;				
		}
		else
		{
			NVIC_EnableIRQ(CT_IRQn);
		}

		rtnBackup();
	}

	if(RF_TX_FIN)	//it should be placed before processing RF_RX_FIN.
	{
		RFTRSTACLR = 0x80;		//Clear TX_FIN
	}	
	
	if(RF_TMR_OVR)
	{
		RFTRSTACLR = 0x04;
		g_stCurRFEnv.event |= IS_RF_TMR_OVR;
		g_M1State |= IS_RF_TMR_OVR;			//added for M1
	}

	if( RFTRSTA )//error while transmission
	{
		RFTRSTACLR = 0xff;
		g_stCurRFEnv.err = 1;
		g_M1Err = 1;						//added for M1
	}
}

