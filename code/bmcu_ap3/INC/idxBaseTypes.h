/*****************************************************************************
* \file  idxBaseTypes.h
* \brief header for object for communication with Idex serial interface
* \author IDEX ASA
* \version 0.0.0
******************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __BASE_TYPES_H__
#define __BASE_TYPES_H__

#ifdef  __cplusplus
	#ifdef WIN32
		#if defined( IDX_SERIAL_DLL )   // building a DLL            
			#define CLASS_DECLSPEC __declspec(dllexport)
			#define CFUNC_DECLSPEC extern "C" __declspec(dllexport)
			#define FUNC_DECLSPEC __declspec(dllexport)
			#define CDECL_DECLSPEC
		#elif defined( IDX_SERIAL_LIB ) // building a LIB or EXE
			#define CLASS_DECLSPEC
			#define FUNC_DECLSPEC
			#define CFUNC_DECLSPEC extern "C"
			#if defined( IDX_WBF_LIB )
				#define CDECL_DECLSPEC __cdecl
			#else 
				#define CDECL_DECLSPEC
			#endif
		#else                           // using functions from a DLL
			#define CLASS_DECLSPEC __declspec(dllimport)
			#define FUNC_DECLSPEC __declspec(dllimport)
			#define CFUNC_DECLSPEC extern "C" __declspec(dllimport)
			#define CDECL_DECLSPEC
		#endif
	#else
		#define CLASS_DECLSPEC
		#define FUNC_DECLSPEC
		#define CFUNC_DECLSPEC extern "C"
		#define CDECL_DECLSPEC
	#endif
#else
	#ifdef WIN32
		#if defined( IDX_SERIAL_DLL )   // building a DLL            
			#define FUNC_DECLSPEC
			#define CFUNC_DECLSPEC __declspec(dllexport)
			#define CDECL_DECLSPEC
		#elif defined( IDX_SERIAL_LIB ) // building a LIB or EXE
			#define FUNC_DECLSPEC
			#define CFUNC_DECLSPEC
			#if defined( IDX_WBF_LIB )
				#define CDECL_DECLSPEC __cdecl
			#else 
				#define CDECL_DECLSPEC
			#endif
		#else                           // using functions from a DLL
			#define FUNC_DECLSPEC
			#define CFUNC_DECLSPEC __declspec(dllimport)
			#define CDECL_DECLSPEC
		#endif
	#else
		#define FUNC_DECLSPEC
		#define CFUNC_DECLSPEC 
		#define CDECL_DECLSPEC
	#endif
#endif

#if defined (__ICCARM__)
	#define S_PACKED
	#define PrePACKED     __packed
	#define POST_PACKED
#else
	#define PrePACKED
	#define POST_PACKED
	#ifdef __GNUC__
		#ifndef S_PACKED
			#define S_PACKED    __attri##bute__ ((__packed__))
		#endif
	#else
		#define S_PACKED
	#endif
#endif

#ifdef __GNUC__
#define PACKED_ATTRIBUTE __attribute__ ((__packed__))
#define USED_ATTRIBUTE __attribute__((used))
#else
#define PACKED_ATTRIBUTE
#define USED_ATTRIBUTE
#endif

#endif // __BASE_TYPES_H__
