/******************************************************************************
* \file  idxEmkExAPI.h
* \brief header for Enroll/Match Kit Extended API
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright 2014-2019 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR / FRR performance.
******************************************************************************/

#ifndef __IDX_EMK_EX_API_H__
#define __IDX_EMK_EX_API_H__

#include "idxEmkExDefines.h"
#include "idxBaseTypes.h"
#include "idxHeapAPI.h"

/**
* @brief Function to initialize the EMK
*
*
* @param[in] pConfig is a matcher configuration provided by user
* @param[in] heapCallbacks specify heap allocation functions
* @param[out] ppHandle is internal context created in Emk
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExInitialize(const idx_MatcherCfg_t* pConfig,
                   const SensorHeapCallbacks* heapCallbacks,
                   void** ppHandle);

/**
* @brief Function to reset the EMK partial enroll to the initial state.
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExResetEnroll(void* pHandle);

/**
* @brief Function to flush the residual data generated by the matcher.
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExFlush(void* pHandle);


/**
* @brief Function to uninitialize the EMK. Deletes the internal context
* and sets it to NULL
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExUninitialize(void* pHandle);

/**
* @brief Function to get a copy of EMK's default configuration
*
*
* @param[out] pConfig is copy of EMK's default config object
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExGetDefaultConfig(idx_MatcherCfg_t* pConfig);

/**
* @brief Function to set of EMK's configuration
*
*
* @param[in] pHandle is the pointer of the Emk's internal context.
* @param[in] pConfig is a matcher configuration provided by user.
*
* @return IDX_SUCCESS if successful, or an error code.
*/

CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExSetMatcherConfig(void* pHandle, idx_MatcherCfg_t* pConfig);

/**
* @brief Function to get current handle's matcher configuration
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
* @param[out] pConfig is the configuration of the handle
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExGetMatcherConfig(void* pHandle,
                         idx_MatcherCfg_t* pConfig);

/**
* @brief Function to retrieve type of matcher in the EMK
*
*
* @param[out] pMatcherInfo will contain the matcher info
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExGetInfo(idx_MatcherInfo_t* pMatcherInfo);

/**
* @brief Function to retrieve the Emk version
*
*
* @param[out] pEmkVersion will contain the Emk info
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExGetUnitVersion(idx_UnitVersion_t* pEmkVersion);

/**
* @brief Function used to extract template from the image
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
* @param[in] pImageData stores image data, width and height
* @param[out] pExtractInfo is the quality information about
*  received template
* @param[out] ppTemplateData stores received template data and size
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExExtract(void* pHandle,
                const idx_ImageInfo_t* pImageData,
                idx_ExtractInfo_t* pExtractInfo,
                idx_TemplateData_t* pTemplateData);

/**
* @brief Function used to merge several templates into one multitemplate
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
* @param[in] isFinalMerge indicates this is the final merge to this template 
   (allows matchers that need an extra step to finalize template 
   to know when to perform this step without an additional API call // (NEEDS EQUIVALENT FLAG ADDING TO MergeTemplates!!!
* @param[in] num_templates is the number of templates to merge.
* @param[in] pInputTemplates stores templates data and size to be merged
* @param[in] pStoredTemplate pointer to template to merge pInputTemplates with
* @param[out] pMergeInfo is the quality information about merged
*  template
* @param[out] ppTemplateData stores merged templates data and size
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExMerge(void* pHandle,
              const uint8_t isFinalMerge,
			  const uint16_t numTemplates,
			  const idx_TemplateData_t* pInputTemplates,
			  const idx_TemplateData_t* pStoredTemplate,
			  idx_MergeInfo_t* pMergeInfo,
			  idx_TemplateData_t* pTemplateData);

/**
* @brief Function used to retrieve extra template info
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
* @param[out] pTemplateData stores merged templates data and size
*
* @return IDX_SUCCESS if successful, or an error code.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExGetTemplateToStore(void* pHandle,
						   idx_TemplateData_t* pTemplateData);

/**
* @brief Function used to match template with several templates
*
*
* @param[in] pHandle is the pointer of the Emk's internal context
* @param[in] matchPhase indicates which phase the match process is in for
   matchers that require multiple phases
* @param[in] pVerificationTemplate is the template to match
* @param[in] numEnrolledTemplates is the number of enrolled templates
* @param[in] pEnrolledTemplates pointer to a list of stored enrolled templates
* @param[out] matchTemplateIndex stores the index of match template
* @param[out] pUpdatedTemplate stores updated template data and size
*             - can be used for dynamic enroll
*             - used in split matcher to return the match data
* @param[out] pScore return the pointed to score if score != 0
* May be set to 0 if not needed.
*
* @return IDX_SUCCESS if successful,
*         IDX_EMK_NO_MATCH if not match.
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC
idxEmkExMatch(void* pHandle,
              const uint8_t matchPhase,
              const idx_TemplateData_t* pVerificationTemplate,
              const uint16_t numEnrolledTemplates,
              const idx_TemplateData_t* pEnrolledTemplates,
              uint16_t* pMatchTemplateIndex,
              idx_TemplateData_t* pUpdatedTemplate,
              uint32_t* pScore);
              
#endif //__IDX_EMK_EX_API_H__
