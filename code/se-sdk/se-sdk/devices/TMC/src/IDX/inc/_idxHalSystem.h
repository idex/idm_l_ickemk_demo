/** ***************************************************************************
 * \file   _idxHalSystem.h
 * \brief  Implementation-specific symbols associated with the IDEX system HAL module.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef __IDX_HAL_SYSTEM_H_
#define __IDX_HAL_SYSTEM_H_

/**
 * @defgroup hal_system Hal system module
 * hal_system module provides system initialization/sleep/control frequency APIs for implementation
 * @{
 */
#define SE_MODULE_NAME THD_

/** @}*/
#endif /* __IDX_HAL_SYSTEM_H_ */
