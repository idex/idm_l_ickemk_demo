/*
 * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: typeA.c
 * SCFID: 
 * Feature: 14443-3 typeA state machine of PICC
 * Version: V0.1
 *
 * History: 
 *   2011-12-31 by Luqian
 *     1. Original version 0.1
 */
#include "halglobal.h"
#include "typeA.h"
#include "RF.h"
#include "CL4.h"
#include "M1Lib.h"

u8 const FDTValue[16] = {0x80, 0x90, 0xa0, 0xb0, 0x60, 0x70, 0xC0, 0xD0, 0xE0, 0xF0, 0x50, 0x40, 0x30, 0x20, 0x10, 0x00};

/*
* Function: u8 TypeAInit(void)
* Description: typeA initialize, set ATQA��UID and so on.
* Input: -
* Output: -
* Return: - 0:success, 1:fail.	
* Other: -
*/
u8 TypeAInit(void)
{
    u8 volatile *RFUIDADDR;

    RFUIDADDR = (u8 volatile *)g_dwSNAddr;

	RFATQA0 = g_pbComCfg[CL3A_ATQA0];
    RFATQA1 = g_pbComCfg[CL3A_ATQA1];  
    RFSAK0 = g_pbComCfg[CL3A_SAK0];
    RFSAK1 = g_pbComCfg[CL3A_SAK1];
    RFSAK2 = g_pbComCfg[CL3A_SAK2];
    RFCASA = g_pbComCfg[CL3A_CASA];
    RFUID0 = *RFUIDADDR;
    RFUID1 = *(RFUIDADDR + 1);
    RFUID2 = *(RFUIDADDR + 2);
    RFUID3 = *(RFUIDADDR + 3);
    if(g_pbComCfg[CL3A_CASA])
    {
        RFUID4 = *(RFUIDADDR + 4);
        RFUID5 = *(RFUIDADDR + 5);
        RFUID6 = *(RFUIDADDR + 6);
        RFUID7 = *(RFUIDADDR + 7);
        RFUID8 = *(RFUIDADDR + 8);
        RFUID9 = *(RFUIDADDR + 9);
    }
    RFCKCON |= 0x04;    //enable anticollision control 1 clk and anticollision control 2 clk
    RFCRCCON = 0xC0;    //enable send data auto generate CRC and receive data judge CRC
    RFTRMSK |= 0xFA;	//enable only RX, disable all error(including timer out interrupt) and TX
    RFTRCONCLR = 0x01;	//clear anticollision
    RFTRCONSET = 0x01;	//set anticollision
    RFBAUDSEL &= 0x80;	//set baudrate 106 kbps, change baud after tx
    RFAFDT = FDTValue[g_pbComCfg[RF_TYPA_FDT]];
    RFATXSCON = 0x10; // silent time before transmission; send time delay
	// RF-level
	g_stCurRFEnv.curRFState = TYPEA_ACTIVE_ATS;

	cfgCrypto1Init(g_pbComCfg[M1_MODE],B2LT((u8 *)(g_pbComCfg+M1_DATA_ADDR))); //Configure M1 api init
    return 0;
}

/*
* Function: u8 HLTA(void)
* Description: handle HLTA command, no response 5000.
* Input: - 
* Output: -
* Return: RFERROR - 1	
* Other: - 
*/
u8 HLTA(void)
{
    u8 tmp = CID;

    if ((0x00 == tmp) && (0 == RFRSCH) && (2 == RFRSCL))	//RFRSCH & RFRSCL: tx & rx not CRC
    {
        //enter HALT: PICC rules B
        TypeAInit();
        RFTRCONSET = 0x11;	//set HALT. BTW: clear HALT is NOT needed.
        // RF-level
        g_stCurRFEnv.curRFState = TYPEA_HALT;
    }

    if (g_stCurRFEnv.curRFState == TYPEA_ACTIVE_PPS)
    {
        RFRSCH = 0;
        RFRSCL = 0;
    }
	return RFERROR;
}

/*
* Function: u8 RATS(void)
* Description: handle RATS command. right command will ATS into RF_RAM and set ATS length, return OK; Otherwise,return FALSE.
* Input: - 
* Output: -
* Return: RFOK - 0(ATS response)
*		  RFERROR - 1(No response)	
* Other: -
*/
u8 RATS(void)
{
	u8 rtn = RFERROR;
    u8 i,j;
    u8 T0;
    u16 index;

    T0 = g_pbComCfg[CL3A_ATS_PROTOCOLINFO+1];
    // Allow the state can handle RATS
    if ((g_stCurRFEnv.curRFState == TYPEA_ACTIVE_ATS) && ((0 == RFRSCH) && (2 == RFRSCL)))
    {
        g_stCurRFEnv.CID_Value = RF_BUF[1]&0x0F;

        if (15 != g_stCurRFEnv.CID_Value)
        {																							   
            index = (RF_BUF[1]>>4)&0xFF;

            g_stCurRFEnv.curRFState = TYPEA_ACTIVE_PPS;//enter receive PPS state��PPS not necessary��no PPS��the default is normal data frame.
            RF_BUF[0] = g_pbComCfg[CL3A_ATS_PROTOCOLINFO] + g_pbComCfg[CL3A_ATS_HISTORY] + 1 + g_pbComCfg[HISTORY_SN_RFA_LEN];
            for (i = 0; i < g_pbComCfg[CL3A_ATS_PROTOCOLINFO]; i++)
            {
                RF_BUF[i+1] = g_pbComCfg[CL3A_ATS_PROTOCOLINFO+i+1];
            }
            for (j = 0; j < g_pbComCfg[CL3A_ATS_HISTORY]; j++)
            {
                RF_BUF[i+j+1] = g_pbComCfg[CL3A_ATS_HISTORY+j+1];
            }
            i = i+j+1;
            for (j = 0; j < g_pbComCfg[HISTORY_SN_RFA_LEN]; j++)
            {
                RF_BUF[i+j] = *(u8 *)(g_dwSNAddr + j);
            }
            if (T0&0x10)	
            {
                g_stCurRFEnv.DSDR = g_pbComCfg[CL3A_ATS_PROTOCOLINFO+2];
                if (T0&0x20)
                {
                    g_stCurRFEnv.bFWI = g_pbComCfg[CL3A_ATS_PROTOCOLINFO+3]>>4;
                }
                else
                {
                    g_stCurRFEnv.bFWI = 4;
                }
            }
            else
            {
                if (T0&0x20)
                {
                    g_stCurRFEnv.bFWI = g_pbComCfg[CL3A_ATS_PROTOCOLINFO+2]>>4;
                }
                else
                {
                    g_stCurRFEnv.bFWI = 4;
                }
                g_stCurRFEnv.DSDR = 0;
            }
            //CL4
            CL4_Reset();
            //CL4
            SetCL4CID(g_stCurRFEnv.CID_Value);
            //CL4
            SetCL4MaxFrameSize(framesize[index]);
			rtn = RFOK;
			RFATXSCON = 0xb0;
            RFRSCH = 0;
            RFRSCL = RF_BUF[0];
        }
    }
    return rtn;
}

#define PPSS (RF_BUF[0])
#define PPS0 (RF_BUF[1])
#define PPS1 (RF_BUF[2])
/*
* Function: u8 PPSA(void)
* Description: handle typeA PPS command.
* Input: -
* Output: -
* Return: RFOK - 0(response)
*		  RFERROR - 1(No response)	
* Other: -
*/
u8 PPSA(void)
{
    u8 rtn;
    u8 DSI;
    u8 DRI;
	rtn = RFERROR;
    if (PPS0&0x10)//present PPS1
    {
        if ((0 == RFRSCH) 
        && (3 == RFRSCL)
        && (0x00 == (PPS1&0xF0))
        && (g_stCurRFEnv.CID_Value == (PPSS&0x0F)))//valid pps
        {
            DSI = (PPS1&0x0C)>>2;
            DRI = PPS1&0x03;
             if ((0 == DSI) ||(g_stCurRFEnv.DSDR&DI[DSI+4]))//106Kbps support
             {
                 if ((0 == DRI) || (g_stCurRFEnv.DSDR&DI[DRI]))//106Kbps support
                 {
 				    if(DRI ) //212
 					{
 						RFADEMCON = 0x12;	
 					}
 
 					rtn = RFOK;
                 }
             }
			if (RFOK == rtn)//support
            {
                RFBAUDSEL &= 0x80;
                RFBAUDSEL |= PPS1;	//valid after sending the PPS response

                RFRSCL = 1;	//rebounce PPSS
				rtn = RFOK;
            }
        }
    }
    else  //absent PPS1
    {
        if ((0 == RFRSCH) && (2 == RFRSCL))
        {
            RFRSCL = 1;
			rtn = RFOK;		
        }
    }
    return rtn;
}
#undef PPSS
#undef PPS0
#undef PPS1

/*
* Function: u8 ActiveA()
* Description: PICC in ACTIVE state, handle RATS/HLTA/MAUTH/MREAD command.
* Input: -
* Output: -
* Return: RFOK - 0(response)
*		  RFERROR - 1(No response)	
* Other: -
*/
u8 ActiveA()
{
	u8 rtn = RFOK;

    switch(PCB)
    {
        case 0x50:
            rtn = HLTA();	
            break;
        case 0xE0:
            rtn = RATS();	
            break;
        case 0x60:
        case 0x61:
        case 0x30:
			if (g_pbComCfg[PROTOCOL_SUPPORT]&M1SUPPORT)	//Configure M1 effect
			{
				rtn = mainCrypto1(g_pbComCfg[M1_MODE], B2LT((u8 *)(g_pbComCfg+M1_DATA_ADDR))); //mainCrypt1 function calls the M1 library into the M1 process.																	
			}
			else
			{
				rtn = RFERROR;
			}
            break;
        default:
			rtn = RFERROR;	
            break;
    }

    //fail��Need to return IDLE state��in the IDLE state hardware automatic processing of anticollision process.
	//Therefore, in the software involved has to receive the ATS state.
	if (RFERROR == rtn)
    {
        TypeAInit();
    }
    return rtn;
}

/*
* Function:u8 ProtocolA()
* Description: handle typeA PROTOCOL state, handle PPS or enter ISO/IEC 14443-4.
* Input: -
* Output: -
* Return: RCVDATOK - 2(response)
*		  RFERROR - 1(No response)	
* Other: -
*/
u8 ProtocolA()
{
    u8 rtn;
	rtn = RFERROR;

    if ((PCB&0xF0) == 0xD0)
    {
        rtn = PPSA();
    } 
    else
    {
        //CL4-level
        // only accord I\S\R frame data is received, otherwise not accepted.
        if (((RF_BUF[0]&0xC2) == 0x02)
        || ((RF_BUF[0]&0xe6) == 0xa2) 
        || ((RF_BUF[0]&0xf7) == 0xf2) 
        || ((RF_BUF[0]&0xf7) == 0xc2))
        {
			rtn = RCVDATOK;
        }
    }
    g_stCurRFEnv.curRFState = RF_ACTIVE;
    return rtn;
}


