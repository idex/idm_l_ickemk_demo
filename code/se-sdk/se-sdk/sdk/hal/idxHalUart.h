/** ***************************************************************************
 * \file   idxHalUart.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         UART data transfers. If a new platform is to be supported then it is
 *         expected that the HAL APIs are implemented for that platform in order
 *         to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_UART_H_
#define _IDX_HAL_UART_H_

#include <stdint.h>

#include "idxSeSdkTypes.h"


/** \brief Initialize the UART HAL module at startup.
 *
 * \par Required by:
 * \b All IDEX CSs.
 */
void idxHalUartInit(void);

/** \brief Function for refresh ISO7816
 *
 * This function is used to work-around a problem with a particular
 * example card O/S that has a defect in its handling of waiting time
 * in contact-based communication with the card-reader. This function
 * is called to send a T=0 NULL procedure byte (0x60) during the enroll
 * process in order to keep the contact card-reader satisfied, so that
 * it does not remove power. This function should not be required on a
 * properly functioning card O/S which will have a timer that sends the
 * NULL procedure byte when the waiting time period (less some margin)
 * expires. It is expected that all implementations of this function for
 * integration in a customer's card O/S will simply do nothing.
 *
 * \par Required by:
 * See description - should not be required.
 */ 
void idxHalUartISO7816Refresh(void);

/** \brief Send the ATR to the reader.
 *
 * This IDEX code inserts a delay after sending the
 * first byte of ATR to allow the MCU to boot.
 *
 * \par Required by:
 * \b CS4
 */
void idxHalUartSendATR(const uint8_t *pAtr, uint8_t atrLen);

/** \brief Transmit data via UART
 *
 * @param[in]  *pBuf    Data to transmit.
 * @param[in]  len      Length of data to transmit.
 */
void idxHalUartTx(const uint8_t *pBuf, uint16_t len);

/** \brief Receive data over SPI
 *
 * @param[out] *pBuf    Received data.
 * @param[in]  len      Length of data to receive.
 */
void idxHalUartRx(uint8_t *pBuf, uint16_t len);

#endif /* _IDX_HAL_UART_H_ */
