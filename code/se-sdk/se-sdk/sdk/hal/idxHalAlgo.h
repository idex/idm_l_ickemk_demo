/** ***************************************************************************
 * \file   idxHalAlgo.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         algorithm engines. If a new platform is to be supported
 *         then it is expected that the HAL APIs are implemented for that
 *         platform in order to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#ifndef _IDX_HAL_ALGO_H_
#define _IDX_HAL_ALGO_H_

#include <stdint.h>

/**
* @brief Function to initialize.
*/   
void idxHalAlgoInit(void);

/**
* @brief Function to generate CRC16-CCITT for len bytes of data.
*
* @param[in] pData is a pointer to the data
* @param[in] dataLen data length
* @param[out] pCrcData receive 16 bit crc data
*
* @return IDX_ISO_SUCCESS or another error code
*/   

uint16_t idxHalAlgoCRC16(const uint8_t *pData, uint32_t dataLen, uint16_t* pCrcData);

/**
* @brief Function to Generate DataLen bytes of random data.
*
* @param[in] dataLen data length
* @param[out] pOutData is a ptr to generated random data
*
* @return IDX_ISO_SUCCESS or another error code
*/    

uint16_t idxHalAlgoGenRandNamber(uint16_t dataLen, uint8_t *pOutData);

/**
* @brief Function to AES128 encrypt/decrypt - generate (de)encrypted data (AES-128 algorithm).
*
* @param[in] pData is a pointer to the data
* @param[in] pKey is a pointer to 128 bit key
* @param[in] pIV is a pointer to IV
* @param[out] pOutDat encrypted/decrypted data
*
* @return IDX_ISO_SUCCESS or another error code
*/   

uint16_t idxHalAlgoAES128encrypt(const uint8_t *pData, uint8_t *pKey, uint8_t *pIV, uint8_t *pOutDat);

uint16_t idxHalAlgoAES128decrypt(const uint8_t *pData, uint8_t *pKey, uint8_t *pIV, uint8_t *pOutDat);

#endif //_IDX_HAL_ALGO_H_
