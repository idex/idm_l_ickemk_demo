
#include "main.h"
uint32_t g_ui32Count = 0;
uint8_t stimer_flag=0;
extern uint8_t led_ms_status;

extern void uSleep( uint32_t us );

extern void se_test_read(void);
extern void led_light_process(void);

void am_stimer_cmpr0_isr(void)//   1s counter
{

	NVIC_DisableIRQ(STIMER_CMPR0_IRQn);
  	CTIMER->STMINTCLR|=0xffffffff;//clear interrupt
	CTIMER->STCFG|=0xc0000000;//clear counter
	CTIMER->STCFG&=~0xc0000000;

	stimer_flag=1;

NVIC_EnableIRQ(STIMER_CMPR0_IRQn);


}




void stimer_init(void)
{
	//1s interrupt timer
	NVIC_DisableIRQ(STIMER_CMPR0_IRQn);
	CTIMER->STMINTEN=0;
	CTIMER->STMINTCLR|=0xffffffff;//clear interrupt
	CTIMER->STCFG=0xc0000000;//clear counter
	CTIMER->STCFG |=0x00000003; //select 32khz
	CTIMER->SCMPR0 = 0x00008000;// 1hz 32768
	CTIMER->STCFG |=0x00000100;// enable compare A
	CTIMER->STMINTEN |= 0x00000001;//enable interrupt
	NVIC_EnableIRQ(STIMER_CMPR0_IRQn);
	CTIMER->STCFG&=~0xc0000000;
}




void am_ctimer_isr(void)
{
	NVIC_DisableIRQ(CTIMER_IRQn);
	CTIMER->INTCLR=0xffffffff;//clear all the ctimer interrupt
	if(1==led_ms_status)
	{
		led_light_process();
	}

	
	NVIC_EnableIRQ(CTIMER_IRQn);

}


void crd_ctimer_int(void)
{
	//250ms interrupt timer
	NVIC_DisableIRQ(CTIMER_IRQn);
	CTIMER->INTEN=0;
	CTIMER->INTCLR=0xffffffff;//clear all the ctimer interrupt

	CTIMER->CTRL0=0x000800;//disable compra0, and clear ctimera0;
	CTIMER->CMPRAUXA0=0;
	CTIMER->CMPRA0=250;//set interval
	CTIMER->CTRL0 |=0x0000001A;//LFRC selected
	CTIMER->CTRL0 |=0x00000200;//interrupt enalbe;
	CTIMER->CTRL0 |=0x00000040;//auto restart
	CTIMER->INTEN =0x00000001;//enable interrupt

	NVIC_EnableIRQ(CTIMER_IRQn);
	CTIMER->CTRL0|=0x00000001;//enable timerA0
	CTIMER->CTRL0 &=~0x000800;//start counter
	//

}




