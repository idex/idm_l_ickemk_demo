/*
* Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
* All rights reserved.
*
* FileName: THD89_hal.c
* SCFID:
* Feature: 
* Version: V0.1
*
* History: 
*   2014-11-18 
*     1. Original version 0.1
*/
#include "halglobal.h"
#include "support.h"

void CPUIdleWFI(IRQn_Type IRQ_num)
{
	NVIC_ClearPendingIRQ(IRQ_num);
	NVIC_EnableIRQ(IRQ_num);
	__WFI();
	NVIC_DisableIRQ(IRQ_num);
}

/*
* Function: void DMAMemcopyTrans( u8 *pbDest, u8 *pbSrc, u16 wLen)
* Description: data copy and transfer and teansfer big to little.
*			   This is used for algorithm or CCP module
* Input:  pbDest - destination address
*		  pbSrc  - source address
*         wLen - length of data to copy 
* Output: - 
* Return: - 
*/
void DMAMemcopyTrans(u8 *pbDest, u8 *pbSrc, u16 wLen)
{
	u16 i;
	for (i=0; i<wLen; i+=4)
	{
		TransBigLittleBB(pbSrc+i, pbDest+i);
	}
}

/*
* Function: void DMAMemcopyTransWW( u32 *pDest, u32 *pSrc, u16 len)
* Description: data copy and transfer and teansfer big to little.
*			   The data can only be the integer times of WORD 
* Input:  pDest - destination address
*		  pSrc  - source address
*         len - length of data to copy 
* Output: - 
* Return: - 
*/
void DMAMemcopyTransWW(u32 *pDest, u32 *pSrc, u16 len)
{
	u16 i,j;
	union
	{
		u32 wtempdata;
		u8 btempdata[4];
	}tempdata1, tempdata2;

	for (i = 0; i < len; i++)
	{
		tempdata1.wtempdata = pSrc[i];
		for (j = 0; j < 4; j++)
		{
			tempdata2.btempdata[j] = tempdata1.btempdata[3-j];
		}
		pDest[i] = tempdata2.wtempdata;
	}
}

/*
* Function: u8 DMAMemcmp(unsigned char *pbDest, unsigned char *pbSrc, unsigned short wLen)
* Description: Data compare.
* Input:  pbDest - destination address to compare
*		  pbSrc  - source address
*         mode - data length
* Output: - 
* Return: 0: Data is the same
*		  1: Data is difference 
*/
u8 DMAMemcmp(unsigned char *pbDest, unsigned char *pbSrc, unsigned short wLen)
{
	u16 i;
	for (i=0; i<wLen; i++)
	{
		if (pbDest[i] != pbSrc[i])
		{
			return 1;
		}	
	}
	return 0;
}

/*
* Function: void MemSetL(u32 *pDst, u32 dat, unsigned char wLen)
* Description: DMA by bytes.
* Input:  pDst - destination address 
*		  dat  - source address
*         wLen - data length
* Output: - 
* Return: - 
*/
void MemSetL(u32 *pDst, u32 dat, unsigned char wLen)
{
	u8 i;
	for ( i = 0; i < wLen; i++)
	{
		*(pDst++) = dat;
	}
}

/*
* Function: unsigned char MemCmpL(u32 *pDst, u32 *pbSrc, unsigned char wLen)
* Description: Data compare.
* Input:  pDst - destination address to compare
*		  pbSrc  - source address
*         wLen - data length
* Output: - 
* Return: 0: Data is the same
*		  1: Data is difference 
*/
unsigned char MemCmpL(u32 *pDst, u32 *pbSrc, unsigned char wLen)
{
	unsigned char i;
	unsigned char flag = 0;

	for (i = 0; i < wLen; i++)
	{
		if (*pDst < *pbSrc)
		{
			if (flag == 0)
			{
				flag = 1;
			}
		}
		else if (*pDst > *pbSrc)
		{
			if (flag == 0)
			{
				flag = 2;
			}
		}
		pDst++;
		pbSrc++;
	}

	return flag;
}

/*
* Function: void _halChipStop(void)
* Description: stop chip.
* Input: - 
* Output: - 
* Return: - 
*/
void _halChipStop(void)
{
    __disable_irq();
    while(1);
}

/*
* Function: void _HalRamCopy(u8 *pbDest, u8 *pbSrc, u16 wLen)
* Description: data copy.
* Input:  pbDest - destination address
*		  pbSrc  - source address
*         wLen - length of data to copy 
* Output: - 
* Return: - 
*/
void _HalRamCopy(u8 *pbDest, u8 *pbSrc, u16 wLen)
{
	u16 i;
	if ((pbSrc + wLen) > pbDest && (pbDest > pbSrc))
	{
		if (((u32)pbDest & 0x03) || ((u32)pbSrc & 0x03) || (wLen &0x03))
		{
			for (i = wLen; i > 0; i--)
			{
				*(pbDest+i-1) = *(pbSrc+i-1);
			}
		}
		else
		{
			for (i = wLen/4; i > 0; i--)
			{
				*((u32 *)(pbDest) + i-1) = *((u32 *)(pbSrc) + i - 1);
			}
		}
	}
	else 
	{
		if (((u32)pbDest & 0x03) || ((u32)pbSrc & 0x03) || (wLen &0x03))
		{
			for (i = 0; i < wLen; i++)
			{
				*(pbDest+i) = *(pbSrc+i);
			}
		}
		else
		{
			for (i = 0; i < wLen/4; i++)
			{
				*((u32 *)(pbDest)+i) = *((u32 *)(pbSrc)+i);
			}
		}
	}
}

/*
* Function: void _HalRamSet(u8 *dest, u8 value, u16 len)
* Description: Write RAM.
* Input:  dest - destination address
*		  value  - data that to be wirte
*         wLen - length of data to write 
* Output: - 
* Return: - 
*/
void _HalRamSet(u8 *dest, u8 value, u16 len)
{
 	u16 i;

	for (i = 0; i < len; i++)
	{
		*(dest++) = value;
	}
}

/*
* Function: void initRam1(void)
* Description: inital RAM.
* Input:  -
* Output: - 
* Return: - 
*/
void initRam1(void)
{
	CLKMEMCON = 0x08;
	RAM1WDTCON |= 0x10000;
	while (!(RAM1WDTSTS&0x04));
}

__asm unsigned long disableIRQ(void)
{
    MRS     R0, PRIMASK
    CPSID   I
    BX      LR
}
__asm void restoreIRQ(unsigned long backup)
{
    MSR     PRIMASK, R0
    BX      LR
}
