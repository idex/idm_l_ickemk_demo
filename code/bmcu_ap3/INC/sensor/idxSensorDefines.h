/*******************************************************************************
* File Name: idxSensorDefines.h
* Version 1.0
*
*  Description:
*   Provides platform definitions.
*******************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR / FRR performance.
**************************************************************************************/

#pragma once

#ifndef _IDX_SENSOR_DEFINES_H_
#define _IDX_SENSOR_DEFINES_H_

#include "idxBaseTypes.h"
#include "idxBaseDefines.h"

#include <stdbool.h>
#include <inttypes.h>
#include <string.h>

#ifdef WIN32
#pragma pack(push)
#pragma pack(1)
#endif

typedef enum
{
	FO_TEST					= 0,    // Special test mode.
	
	//finger detect only modes:
	FO_FINGER_ON			= 1,    // Wait for finger to be on the sensor. No scan is performed.
	FO_FINGER_OFF           = 2,	// Wait for finger to be off the sensor. No scan is performed.
	
	//navigation modes:
	FO_NAVIGATION			= 9,    // Navigation mode. Detect Navigation events.

	//regular scan modes:
	FO_UNPACK				= 100,
	FO_TOUCH_AND_SCAN		= 101,  // Image is scanned if the finger covers the sensor. Including the case when it was put on the sensor prior to the call and is still on it during the call. 
	FO_NEW_TOUCH_AND_SCAN	= 102,  // New touch & scan mode. As apposed to the above case the image is only scanned into the buffer if a new touch is detected.
	FO_LIVE_SCAN			= 103,  // Scan mode. Image is scanned into the buffer unconditionally.
	FO_BACKGROUND_SCAN		= 104,  // Image is scanned into the buffer only if no finger is placed on sensor.
	
	//modes for scan and providing packed image : (used in embedded case with embedded version of unpacking and postprocessing)
	FO_PACK					   = 200,
	FO_TOUCH_AND_SCAN_PACK	   = 201,  // Image is scanned if the finger covers the sensor. Including the case when it was put on the sensor prior to the call and is still on it during the call. 
	FO_NEW_TOUCH_AND_SCAN_PACK = 202,  // New touch & scan mode. As apposed to the above case the image is only scanned into the buffer if a new touch is detected.
	FO_LIVE_SCAN_PACK		   = 203,  // Scan mode. Image is scanned into the buffer unconditionally.
	FO_BACKGROUND_SCAN_PACK    = 204,  // Image is scanned into the buffer only if no finger is placed on sensor.
} sensor_FingerOperation_t;

/* sensor_NavEvent_t - navigation motion event type */
typedef enum
{
	NAV_NO_EVENT = 0,               // no event
	NAV_EVENT_LONG_PRESS = 28,      // long press event
	NAV_EVENT_ONE_CLICK = 174,      // single click event
	NAV_EVENT_DOUBLE_CLICK = 111,   // double-click event
	NAV_EVENT_UP = 103,             // up event: finger moved up while on sensor
	NAV_EVENT_DOWN = 108,           // down event: finger moved down while on sensor
	NAV_EVENT_LEFT = 105,           // left event: finger moved left while on sensor
	NAV_EVENT_RIGHT = 106,          // right event: finger moved right while on sensor
} sensor_NavEvent_t;

typedef  struct S_PACKED
{
	uint8_t     fw_ver_major;
	uint8_t     fw_ver_minor;
	uint32_t    fw_build;

// Hardware fw family id
#define HW_FAMILYID_EAGLE   1
#define HW_FAMILYID_KESTREL 2
	uint8_t     hw_family;
	uint8_t     hw_revision;

// Idex Sensor fw family id
#define FW_FAMILYID_STANDARD		1 // 4.5 - 6.3
#define FW_FAMILYID_SCANEX 			2 // 7.x 2.x scanex
#define FW_FAMILYID_G3				3 // gen3 FW
#define FW_FAMILYID_KESTREL_SSBL	0x81
#define FW_FAMILYID_KESTREL_FSBL	0xa0
	uint8_t     fw_familyID;
	uint8_t     fw_revisionID;

// Idex Sensor Config BitMask
#define CONFIG_MSK_SECURE_MODE			0x0001
#define CONFIG_FLG_SECURE_MODE_STANDARD	0x0000
#define CONFIG_FLG_SECURE_MODE_SECURE	0x0001

#define CONFIG_FLG_PSK_EXIST			0x0002
#define CONFIG_FLG_MANUF_TEST_MODE		0x2000
#define CONFIG_FLG_TEST_MODE			0x4000
#define CONFIG_FLG_BOOTLOADER_MODE		0x8000	
	uint16_t    configFlgs;

} POST_PACKED sensor_PrmVersion_t;

typedef struct __eagle_Geometry
{
	uint16_t   ver;
	uint16_t   lineLen1; /// length of first line segment in bytes(2 bytes)
	uint16_t   addSz1; /// Size of additional Info for the rev.1, 2
	/// for the rev. 3, 4 - first vertical receiver index in the middle
	uint16_t   lineLen2; /// length of first line segment in bytes(2 bytes)
	uint16_t   addSz2; /// Size of additional Info for the rev.1, 2
	/// for the rev 3, 4 - 0 - ignored 
	uint16_t   numGroups; /// Number of groups, each contains 2 line desriptors above
	uint16_t   packedImageSize; /// size of packed image
} eagle_Geometry_t;

typedef struct __scanex_Geometry
{
	uint16_t ver;
	uint8_t  splitRowN;
	uint8_t  halfRcvColW;
	uint8_t  halfRcvColE;
	uint8_t  blindPixelCnt;
	uint8_t  reserved[8];
} scanex_Geometry_t;

typedef struct __g3fw_Geometry
{
#define G3FW_GEOMETRY_16BIT_CDM_ENCODED			201 //version for 16bit CDM encoded raw data
#define G3FW_GEOMETRY_CDM_DECODED				200 //version for CDM decoded raw data
	uint16_t ver;
	uint8_t  RcvSpcWest;
	uint8_t  RcvSpcEast;
	uint8_t  DrvNumNorth;
	uint8_t  DrvNumSouth;
	uint8_t  RcvNum;
	uint8_t  isCdm;
	uint8_t  reserved[6];
} g3fw_Geometry_t;

typedef struct __sensor_SensorInfo
{
	uint16_t width;
	uint16_t height;
	uint16_t DPI;
	uint16_t depth;

//sensors silicon/chip IDs
#define ASIC_ID_KESTREL_A0 (0x10)
#define ASIC_ID_KESTREL_A1 (0x11)
#define ASIC_ID_GREYLOCK_A0 (0x20)
#define ASIC_ID_GREYLOCK_A1 (0x21)
	uint8_t	 siliconID;
	uint8_t  pad0;   //formerly ManufacturerID (unused)

//Eagle sensors substrate IDs 
#define SUB_ID_ROUND           (0xE8)
#define SUB_ID_BLUE_BIRD_L8    (0x9B)
#define SUB_ID_BLUE_BIRD_L5    (0x9A)
#define SUB_ID_SQUARE_9x9      (0x99)
#define SUB_ID_SQUARE_7x7      (0x77)
#define SUB_ID_RECT_9x5        (0x95)
//Kestrel sensors substrate IDs (scanex)
#define SUB_ID_KESTREL_9x9_BGA_SCANEX (0x9C)
#define SUB_ID_AURORA_9x9_SCANEX      (0x9D)
//Kestrel sensors substrate IDs (G3FW)
#define SUB_ID_KESTREL_9x9_BGA    (0x10)
#define SUB_ID_AURORA_9x9_L1_AMK  (0x11) //amkor
#define SUB_ID_AURORA_9x9_L2_SPIL (0x12) //spil
#define SUB_ID_AURORA_TANKLESS    (0x13)
#define SUB_ID_POLARIS            (0x20)
#define SUB_ID_POLARIS_TANKLESS   (0x21)
#define SUB_ID_URSA_MAJOR         (0x30)
#define SUB_ID_URSA_MINOR         (0x31)
#define SUB_ID_ECLIPSE_yxy_manuf  (0xCC) //symbolic name to change based on form factor
#define IS_CDM_SUB_ID(x) ((SUB_ID_AURORA_TANKLESS==x) || (SUB_ID_POLARIS_TANKLESS==x) || (SUB_ID_URSA_MAJOR==x) || (SUB_ID_URSA_MINOR==x))

	uint8_t  substrateID;

#define WHITE_RIDGE (255)
#define BLACK_RIDGE (0)
	uint8_t  ridgeColor;
	PrePACKED union S_PACKED
	{
		eagle_Geometry_t eagle_sz_info;
		scanex_Geometry_t scanex_sz_info;
		g3fw_Geometry_t g3fw_sz_info;
		uint16_t   	common_sz_info[7];
	} geometry_data;
	uint8_t calibration_info;

#define IMG_ORIENT_WEST_MIRROR  (0) //host will flip by diagonal line
#define IMG_ORIENT_NORTH_MIRROR (1) //host will mirror horizontally  
#define IMG_ORIENT_EAST_MIRROR  (2) //reserved for future use
#define IMG_ORIENT_SOUTH_MIRROR (3) //reserved for future use
#define IMG_ORIENT_WEST         (4) //reserved for future use
#define IMG_ORIENT_NORTH        (5) //host will not rotate or flip
#define IMG_ORIENT_EAST         (6) //reserved for future use
#define IMG_ORIENT_SOUTH        (7) //reserved for future use

	uint8_t orientation_info;
} sensor_SensorInfo_t;

typedef void( *SensorMessageCallback )( void* ctx, int function, int index, const char *message );

#define INIT_DATA_CURRENT_VERSION (0)
#define INIT_DATA_HEAP_CALLBACK_ID "HeapCallbacks"
#define INIT_DATA_DRV_CALLBACK_ID "DriverCallbacks"
#define INIT_DATA_KEY_CALLBACK_ID "SensorSecurityCallbacks"
#define INIT_DATA_APP_ENV_ID "Environment"
#define INIT_DATA_SEC_CTX_ID "SecurityContext"
#define INIT_DATA_STACK_CONFIG_ID "ConfigParams"

// Configure compress mode
// params[0] low byte
// 0 - 8-bit (ordered)
// 1 - 4-bit normalizing
// 3 - 8-bit (non-ordered)
// 255 - extended mode (compression flags(see SCAN Control Flags) in high word (16-bit) of params[0])
//GET_IMAGE_RAW_16BIT            (0x0000)
//GET_IMAGE_SUBTRACT_BACKROUND   (0x0001)
//GET_IMAGE_CONVERT_TO_8BIT      (0x0002)
//GET_IMAGE_CONVERT_TO_8BIT_UNSIGNED (0x0040)
//GET_IMAGE_DO_AUTOSCALE         (0x0004)
//GET_IMAGE_REMOVE_VERT_LINE_ART (0x0010)
//GET_IMAGET_REMOVE_CENTER_DOTS  (0x0020)

// Configure sensorStack modes
// params[1] low 16-bit word - magic "V1" (0x3156)
// params[1] high 16-bit word - extended sensor stack modes
#define SS_RAW_CMD_MODE            (0x0001)

typedef struct ConfigParamsContainer_t
{
	uint32_t params[4];
} ConfigParamsContainer;

#define USE_DEFAULT_SENSOR_TIMEOUT (-1)

typedef struct __scan_config
{
	uint16_t settling_time_finger_off;
	uint8_t settling_time_finger_on_contact;
	uint8_t settling_time_finger_on_contactless;
	uint32_t img_valid_timeout;
	uint8_t background_avg;
	uint8_t reserv;
#define GENERIC_FLAG_SKIP_IMAGE_CRC  (0x80) //skip calculating CRC for image data
	uint8_t generic_flags;
	uint8_t wtx_mode;
	uint16_t scale_factor;
	int16_t scale_offset;
} ScanConfig;

typedef struct __detect_config
{
	uint32_t wff_timeout;
	uint32_t wff_period;
} DetectConfig;

typedef struct __nav_config
{
	uint32_t longPress_timeout;
	uint32_t doubleClick_timeout;
} NavConfig;

typedef struct {
	uint32_t  time_low;
	uint16_t  time_mid;
	uint16_t  time_hi_and_version;
	uint8_t   clock_seq_hi_and_reserved;
	uint8_t   clock_seq_low;
	uint8_t   node[6];
} huuid_t;

#ifdef WIN32
#pragma pack(pop)
#endif

/****************************************************************************
*	Configuration options (set of defines)
*****************************************************************************/
#define USE_SECURITY
#define BPS_ALLOW_ALL_COMMANDS

#if !defined( IDEX_TZ ) && !defined( EMBEDDED_IDXSERIAL )
#define USE_CALLBACK_FOR_FEEDBACK
#endif

#if !defined( IDEX_TZ ) && !defined( EMBEDDED_IDXSERIAL )
#define MULTI_THREAD_ENVIROMENT //use lock for support abort and simultaneous commands processing
#endif

#if defined( EMBEDDED_IDXSERIAL )
#define FW_UPGRADE_DISABLED
#endif

#if !defined( EMBEDDED_IDXSERIAL )
#define CHECK_ESD_ON_IMAGE_ACQUIRE
#define SUPPORT_DEBUG_COUNTERS
#if !defined( IDEX_SPI_DRV )
#define DEBUG_COUNTERS_IS_NOT_PROVIDED_BY_DRIVER
#endif
#endif


#if !defined( EMBEDDED_IDXSERIAL )
#define NAVIGATION_ENABLED
#endif

#define SUPPORT_FRAME_BASED_NAV
//#define SUPPORT_FW_BASED_NAV

#define SUPPORT_HUGE_REPLY

#ifdef _DEBUG
#define DUMP_EAGLE_PACKET
#endif

//#define USE_DELAY_INSTEAD_OF_WAIT

#define WTX_SUPPORT
#if !defined( EMBEDDED_IDXSERIAL )
#define USE_SEMA
#endif

#endif //_IDX_SENSOR_DEFINES_H_




