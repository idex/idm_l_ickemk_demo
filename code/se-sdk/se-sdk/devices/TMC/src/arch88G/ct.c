/*
 *  Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: ct.c
 *  SCFID: 
 *  Feature: 7816 t=0 state machine of PICC
 *  Version: V0.1
 * 
 *  History: 
 *    2012-01-03 by Luqian
 *      1. Original version 0.1
 *    2014-01-15 by zhoucc
 *      2. modified version 0.2    
 * */
#include "halglobal.h"
#include "ct.h"
#include "CTCloseClassC.h"

//the definition of global array,struct and variable
ST_CT g_stCT;
volatile u8 g_inverseEGT ;
u8 COMBUF[512] __attribute__((at(0x20002200)));	//the default address of communication buffer
const u8 ValidFIDI[] ={11, 0x11, 0x12, 0x13, 0x18, 0x91, 0x92, 0x93, 0x95, 0x96, 0x97}; 
void StartEGTTimer(void);
void CheckEGTTimer(void);
extern void StopTimer0(void);

void ISO7816ConfigFIDI(u8 bFIDI)
{
    u8 counter = 0;

    while(counter < ValidFIDI[0])
    {
        if(bFIDI == ValidFIDI[counter+1])
        {
            CTBRC = bFIDI;
            break;
        }
        else
		{
            counter++;
		}
    }    
}

void CTtxFinish(void)
{
    //wait for tx finish
    while(!(CTSTS & 0x01));        //wait until tbe = 1
    CTCON2 &= 0x7f;                //clr sbit
    CTCON &= 0xdf;                 //ready for receive
	CTMSK = 0xff;					//disable 7816 interrupt
    
    CTDMARXBFAD = (u32)(COMBUF);
    CTDMARXBFLEN = COMMAND_HEAD_LEN;
    CTDMACON |= 0x06;				//enable 7816dma rx start and start 60 timer after rx over
    CTDMAMSK &= 0xfd;				//enable 7816 dma rxov interrupt
	while(!(CTDMASTS & 0x01));         //wait until dma tx finish
	IDLECON0 |= 0x01;			//set 7816 sleep
}
/*
* Function     : CTinit 
* Description  : initialise of hardware of 7816
* Input  : -
* Output : -
* Return : -
*/
u8 CTinit(void)
{
	g_inverseEGT =0;
	g_stCT.flow = 0;
	g_stCT.wRemainLen =	0;
	g_stCT.wStatusBk = 0x9000;
	CLKIOCON |= 0x01;			 //enable 7816 clk
	CTATRWAIT = 2;
    CTATRCON = 0x01;             //enable auto '3b'
    CTATRMSK = 0x01;             //enable complete sending '3b' interrupt
    INTIOCON |= 0x01;            //enable 7816 interrupt 
    CTCON1 = (g_pbComCfg[CT_EGT]<<3)|(g_pbComCfg[CT_TRY]);
	CTCON2 |= 0x01;              //error signal 2 etu
	IOPULLSEL0 |= 0x7f;		     //set 7816clk and gpio0,1 pullup	set PU enable 
	IOPULLSEL2 = 0;	     //set PU_1 disable
	return 0;
}

/*
* Function     : CTrxByte 
* Description  : receive one byte
* Input  : -
* Output : -
* Return : -
*/
u8 CTrxByte(void)
{
	while(!(CTSTS & 0x02));             //wait until rbf=1
	return CTBUF;
}

/*
* Function     : CTrxByte 
* Description  : send a byte
* Input  : -
* Output : -
* Return : -
*/
void CTtxByte(u8 bInData)
{
    CTCON |= 0x20;
	CTBUF = bInData;
	while(!(CTSTS & 0x01)); 			//wait  until  tbe=1	
    CTCON &= 0xdf;
}

/*
* Function     : CTrx 
* Description  : receive several bytes
* Input  : -
* Output : -
* Return : -
*/
void CTrx( u8 *pReadbuf, u8 bLen )
{
    CTDMARXBFAD = (u32)COMBUF;
    CTDMARXBFLEN = bLen;
	
    CTDMACON |= 0x06;			//enable 7816 dma rx and strat 60 timer after rx over
    
	while(!(CTDMASTS & 0x02));
    CTDMACON &= 0xfd;			//disable 7816 dma rx
    CTDMASTS |= 0x02;
    _HalRamCopy(pReadbuf, COMBUF, bLen);
}

/*
* Function     : CTrx 
* Description  : send more than one byte
* Input  : -
* Output : -
* Return : -
*/
void CTtx(u8 *pbSendBuf, u16 wLen)
{
	CheckEGTTimer();
    _HalRamCopy(COMBUF, pbSendBuf, wLen);
    CTCON |= 0x20;                     //IO Tx
    CTDMATXBFAD = (u32)COMBUF;       
    CTDMATXBFLEN = wLen;
    CTDMACON |= 0x01;                  //DMA tx enable
    while(!(CTDMASTS & 0x01));         //wait until dma tx finish
    CTDMASTS |= 0x01;                  //clr DMA tx finish
    CTCON &= 0xdf;                     //IO Rx
}

void CTtxrx(u16 wLen, u16 wRxLen)
{	
	CheckEGTTimer();
	CTDMACON |= 0x08; 		//set TXRX
	CTDMATXBFAD = (u32)COMBUF;
	CTDMATXBFLEN = wLen;
	CTDMARXBFAD = (u32)COMBUF;
	CTDMARXBFLEN = wRxLen;
	CTDMACON |= 0x04; 		//enable 7816 start 60 timer after rxov
	CTDMAMSK &= 0xfd; 		//enable 7816 dma rxov interrupt
	CTDMACON |= 0x01; 		//set DMA txs
	while(!(CTDMASTS & 0x01));         //wait until dma tx finish
	IDLECON0 |= 0x01; 	//set 7816 sleep;
}

void CT_IRQHandler(void)
{

    if(CTATRSTS & 2)//rst
    {
      CTATRSTS = 0x02;       //clear rst and send 3b flag
      g_stCT.event |= IS_CT_RST_POS;   
      CTClassC_Closed();      
    }

    if( CTSTS & IS_CT_RX )    //RBF:receive buffer full
    {
		CTSTS = 0x02;
        CTMSK = 0x0f;
        g_stCT.event |= IS_CT_RX;
    }

	if( CTDMASTS & 4)  //RXBYTE | RXOV
    {
		CTDMASTS = 0x07;
		CTDMAMSK = 0x0f;
        CTMSK = 0x0f;
        g_stCT.event |= IS_CT_RX;
    }

	if (g_stCT.event & IS_CT_RST_POS)
	{
		rtnBackup();
	}
}

#define _PPSS (g_stCT.g_commBuf[0])
#define _PPS0 (g_stCT.g_commBuf[1])
#define _PPS1 (g_stCT.g_commBuf[2])
u8 PPS(void)
{
    u8 bFI, bDI;
    u8 bFI_TA1, bDI_TA1;
    u8 bSupport;
	u8 i =0;
	u8 expectedCharNum = 0;
	u8 xor = 0;
    bSupport = 0;

    if(g_pbComCfg[CT_ATR_PROTOCOL+1]&0x10)// ATR protocol bytes include TA1
    {
        bFI_TA1 = g_pbComCfg[CT_ATR_PROTOCOL+2]&0xF0;
        bDI_TA1 = g_pbComCfg[CT_ATR_PROTOCOL+2]&0x0F;
    }
    else		//without TA1, support 96
    {
        bFI_TA1 = 0x90;
        bDI_TA1 = 0x06;
    }

    _PPS0 = CTrxByte();
    expectedCharNum = 1;
    if(_PPS0&0x10)	expectedCharNum++;
    if(_PPS0&0x20)	expectedCharNum++;
    if(_PPS0&0x40)	expectedCharNum++;
    
	//_PPS1,2,3, and PCK
    for(i = 2; i < expectedCharNum + 2; i++)
    {
     	g_stCT.g_commBuf[i] = CTrxByte();
    }
	StartEGTTimer();
	//legality judgement
    for(xor = _PPSS, i = 1; i < expectedCharNum + 2; i++)
    {
     	xor ^= g_stCT.g_commBuf[i];
    }
    if(	(_PPS0&0x0F)	|| xor )
	{
		while(1); //keep silent, waiting reset
	}

    //supported baud judgement
    if(_PPS0&0x10)
    {
        bFI = _PPS1&0xF0;
        bDI = _PPS1&0x0F;
        if( 0x10 == bFI )
        {
			if(bFI_TA1 == 0x90)
			{
				if(( 0x04 > bDI ) || (bDI == 0x08))
				{
					bSupport = 1;
				}	
			}
			else
			{
				if(bDI <= bDI_TA1)
				{
					bSupport = 1;
				}
			}
        }
        else if( 0x90 == bFI )
        {
            if(( 0x90 == bFI_TA1)&&(bDI <= bDI_TA1))
            {
                bSupport = 1;
            }
        }
        if(bSupport)
        {
            _HalRamCopy(COMBUF, g_stCT.g_commBuf, expectedCharNum+2);
   		    CTtxrx(expectedCharNum + 2, COMMAND_HEAD_LEN);
    	    CTBRC = _PPS1;
        }
        else
    	{
    		//NAK
    		_PPS0 = 0;
    		g_stCT.g_commBuf[2] = 0xFF;
            COMBUF[0] = g_stCT.g_commBuf[0];
            COMBUF[1] = g_stCT.g_commBuf[1];
            COMBUF[2] = g_stCT.g_commBuf[2];
		    CTtxrx(3, COMMAND_HEAD_LEN);
    	}
    } 
    else 
    {
        {
            _HalRamCopy(COMBUF, g_stCT.g_commBuf, expectedCharNum+2);
   		    CTtxrx(expectedCharNum + 2, COMMAND_HEAD_LEN);
        }
    }
	g_stCT.flow = RS_PPS;
    
	return 0;
}

u8 SendATR(void)
{
	u8 pbATR[30];
	u8 bLen, i, j;
	u8 bHisLen;
	while(!((CTATRSTS & 0x01) && (CTSTS & 0x01)));
	CTATRSTS =0x01;	
	pbATR[0] = 0x3B;
	bHisLen = g_pbComCfg[CT_ATR_HISTORY] + g_pbComCfg[HISTORY_SN_LEN];
	bLen = g_pbComCfg[CT_ATR_PROTOCOL]+bHisLen;
	for(i = 1; i <= g_pbComCfg[CT_ATR_PROTOCOL]; i++)
	{
		pbATR[i] = g_pbComCfg[CT_ATR_PROTOCOL+i];
	}
	i--;
	for( j = 1; j <= g_pbComCfg[CT_ATR_HISTORY]; j++)
	{
		pbATR[i+j] = g_pbComCfg[CT_ATR_HISTORY+j];
	}
	i = i + j;
	for( j = 0; j < g_pbComCfg[HISTORY_SN_LEN]; j++ )
	{
		pbATR[i+j] = *(u8 *)(g_dwSNAddr+j);
	}

	pbATR[1] |= bHisLen;
	bLen++; //0x3B

    //set 60 TIMER
    CTTCON = 0x00;
    CTTRLD = 8000;
    CTTDAT = 8000;

    CTtx(pbATR+1, bLen-1);
	g_stCT.flow |= RS_ATR;
	
	return 0;
}


/*
* Function     : Start EGTTimer 
* Description  : start EGT, TIMER0,clock source select 7816 clock
* Input  : -
* Output : -
* Return : -
**/
void StartEGTTimer(void)
{
    u16 usTimerInit;
	u32 brc;

	
    g_inverseEGT = 1;
	brc = CTBRC;
	switch(brc)
	{
		case 0x02:
		case 0x12:
			usTimerInit = 186;
			break;
		case 0x03:
		case 0x13:
			usTimerInit = 93;
			break;	
		case 0x91:
			usTimerInit =512;
			break;	
		case 0x92:
			usTimerInit =256;
			break;
		case 0x93:
			usTimerInit =128;
			break;
		case 0x94:
			usTimerInit =64;
			break;	
		case 0x95:
			usTimerInit =32;
			break;
		case 0x96:
			usTimerInit =32;
			break;
		case 0x97:
			usTimerInit =32;
			break;	
		default:
			usTimerInit = 372;
			break;			
	}
	CLKTRCON |= 0x01;		//enabel timer0 clk
	CLKTR0SEL = 0x01;		//select 7816 clock source
	TIMER0CONCLR = 0xff; 	//clear sfr
	RSTTRCON = 0xfffffffe; //when stop timer0, softreset timer0 to make sure it is stoped indeed
	while(!(RSTTRCON&0x01));
	TIMER0RLD = 0xffff-usTimerInit*g_pbComCfg[CT_CWT];
	TIMER0DIV = 0;
	TIMER0CONSET = 0x05; 	//Set TIMER0 do not count when CPU is sleep and start run
}
void CheckEGTTimer(void)
{
	if(g_inverseEGT)
	{
		while(!TIMER0STSRD);
		TIMER0STSCLR = 0x01;
		TIMER0CONCLR = 0x01;	//stop TIMER0
		CLKTRCON &= ~0x01;		//disabel timer0 clk
		g_inverseEGT = 0;
	}
}
/*
* Function     : GetResponse 
* Description  : judge command whether get response or not
* Input  : -
* Output : -
* Return : -
*/
u8 GetResponse (void)
{
	u16 len;
	u16 wSendLen;
	wSendLen = 0;

    //_GetResponse require correct
    if ((*g_stCT.g_commBuf == 0x00) && (*(g_stCT.g_commBuf+1) == 0xc0))
    {
    	if ((*(g_stCT.g_commBuf+2) | *(g_stCT.g_commBuf+3)) != 0x00) 	//p1: 0,p2: 0
    	{
            COMBUF[0] = 0x6A; 
            COMBUF[1] = 0x86;
    		CTtxrx(2, COMMAND_HEAD_LEN);
    		return 0;
    	}
    	if(g_stCT.wRemainLen == 0)	  	//last time send no 61xx,so donot deal with _GetResponse
    	{
    		return 1;				//card receive _GetResponse at a wrong time,so give it to upper lavel to do
    	}		
    	
    	//response length
    	len = *(g_stCT.g_commBuf+4);		
    	
    	//_GetResponse which lenth is 0, means 256 bytes
    	if (len == 0)
    	{
    		len = 0x100;
    	}
    			
    	if (len > g_stCT.wRemainLen)	//_GetResponse which length is cross the border
    	{
    		len = g_stCT.wRemainLen;
    		
    		//attention��some applictions only to return all data��donot send 6cxx��so only to remove this section of code
            COMBUF[0] = 0x6C;
            COMBUF[1] = g_stCT.wRemainLen&0xFF;
			CTtxrx(2, COMMAND_HEAD_LEN);
    		return 0;			
    	}

        COMBUF[0] = g_stCT.g_commBuf[1]; //INS
        wSendLen++;
    	g_stCT.wRemainLen -= len;
        _HalRamCopy(COMBUF+1, g_stCT.pbBuf, len);
        wSendLen += len;

    	if (g_stCT.wRemainLen != 0)
            DMAMemcopy(g_stCT.pbBuf, (u8 *)(g_stCT.pbBuf+len), g_stCT.wRemainLen);

    	if (g_stCT.wRemainLen != 0)
    	{
            COMBUF[wSendLen++] = 0x61;
            COMBUF[wSendLen++] = g_stCT.wRemainLen&0xFF;
    	}
    	else
    	{
            COMBUF[wSendLen++] = g_stCT.wStatusBk>>8;
            COMBUF[wSendLen++] = g_stCT.wStatusBk&0xFF;
    	}
        CTtxrx(wSendLen, COMMAND_HEAD_LEN);

    	return 0;
    }

	return 1; 
}

/*
* Function     : Get6CXX 
* Description  : whether the command is the resending command after responsing 6CXX
* Input  : -
* Output : -
* Return : 
*	1: no, 0:yes
*/
u8 Get6CXX(void)
{
	u16 wTmp;
    u16 wSendLen;
    wSendLen = 0;

	//last frame ACK state word is 6Cxx��and this frame of data is consistent with last frame
	if((g_stCT.wStatusBk == 0x6C00) && 
		(g_stCT.g_commBuf[0] == g_stCMD.header[0]) &&
		(g_stCT.g_commBuf[1] == g_stCMD.header[1]) &&
		(g_stCT.g_commBuf[2] == g_stCMD.header[2]) &&
		(g_stCT.g_commBuf[3] == g_stCMD.header[3]) 
		)
	{
        COMBUF[0] = g_stCT.g_commBuf[1]; //INS
        wSendLen++;
	    if(g_stCT.g_commBuf[4] != g_stCT.wRemainLen)  //LC != outLen, 6CXX
	    {
			g_stCT.wStatusBk = 0x9000;
			return 1; 
	    }
	    else
	    {
	    	wTmp = g_stCMD.wStatusWord;	//last statusword
            _HalRamCopy(COMBUF+1, g_stCT.pbBuf, g_stCT.wRemainLen);
            wSendLen += g_stCT.wRemainLen;
            g_stCT.wStatusBk = 0x9000; //restore 9000
	    }
        COMBUF[wSendLen++] = wTmp>>8;
        COMBUF[wSendLen++] = wTmp&0xFF;
        CTtxrx(wSendLen, COMMAND_HEAD_LEN);
     
	    return 0;
    }
    else
	{
        return 1;
	}
}

/*
* Function     : CTRxCmdHeader 
* Description  : 7816 read command header
* Input  : -
* Output : -
* Return : 
*	1: valid command, 0:other
*/
u8 CTRxCmdHeader(void)
{
	u8 rtn = 0; 
	
	if(g_stCT.event & IS_CT_RST_POS) 
	{	
		g_stCT.event &= CLR_CT_RST_POS;
		CTinit();
		SendATR();
        CTMSK &= 0xfd;         //enable RX interrupt
        IDLECON0 |= 0x01;	//set iso7816 sleep
		
		return 0;
	} 
	if(g_stCT.event & IS_CT_RX)
	{	
		g_stCT.event &= CLR_CT_RX;
	    if( RS_ATR == g_stCT.flow )	//after atr and no pps
        {
			_PPSS = CTrxByte();
			if(0xff == _PPSS)
			{	
           		rtn = PPS();
			}
			else 
			{
				g_stCT.g_commBuf[0] = _PPSS;
				CTrx( g_stCT.g_commBuf + 1, 4 );
				rtn = 1;
			 	g_stCT.flow = RS_PPS;
			}
			StartEGTTimer();
       }
       else
	   {	 
	       rtn = 1;				
		   StartEGTTimer();
			_HalRamCopy(g_stCT.g_commBuf, COMBUF, 5);
			//CTrx( g_stCT.g_commBuf, 5 );
            rtn = GetResponse();
            if(rtn)
            {
                rtn = Get6CXX(); //1:if not 6Cxx then resend 
            }
		}
		//didnot receive the command, wait to receive the next frame
        if (rtn)
			g_stCT.wRemainLen =	0;
	} 
	return rtn;
}

#undef _PPSS
#undef _PPS0
#undef _PPS1

void CTRxCmdBody(void)
{
    if(LC)
    {
	    CTtx(&INS,1);	//in case3 ,receive command head ,then send command number,before receive command body
        CTrx(g_stCT.g_commBuf+5, LC);	//receive command body
		StartEGTTimer();
    }
}
u8 CTTxResponse(ST_COMCMD *stCMD)
{
    u16 wTmp;
	u16 wSendLen;
	
	g_stCT.wStatusBk = stCMD->wStatusWord; //backup status word
	wTmp = stCMD->wStatusWord; 
	
	if(LC)	   //expect len: LC
	{
		wSendLen = LC;
	}
	else	//expect len: 256
	{
		wSendLen = 256;
	}

    if(stCMD->wOutLen)	 //response: data field
    {
    	if(stCMD->bFlagIn)//apdu body. case4
    	{
    		wTmp = 0x6100|stCMD->wOutLen; //9000->61xx
    	    g_stCT.wRemainLen = stCMD->wOutLen;	
    	    //case 4 result backup
            DMAMemcopy( g_stCT.pbBuf, (u8 *)stCMD->pbBuf, g_stCT.wRemainLen );	 //backup data filed
            stCMD->wOutLen = 0;
    	}
    	else if(wSendLen != stCMD->wOutLen)	 //no apdu body: case2, LC != outLen, 6CXX
    	{
            wTmp = 0x6c00|(stCMD->wOutLen&0xFF);
    	    g_stCT.wRemainLen = stCMD->wOutLen;	
    		g_stCT.wStatusBk = 0x6C00;
    	    //case 2 result backup
            DMAMemcopy( g_stCT.pbBuf, (u8 *)stCMD->pbBuf, g_stCT.wRemainLen );
            stCMD->wOutLen = 0;
    	}
        else  //no apdu body: case2 LC=outLen
        {
            COMBUF[0] = INS;
            _HalRamCopy(COMBUF+1, stCMD->pbBuf, stCMD->wOutLen);
            stCMD->wOutLen++;
        }
    }

    COMBUF[stCMD->wOutLen++] = wTmp>>8; 
    COMBUF[stCMD->wOutLen++] = wTmp; 
    CTtxrx(stCMD->wOutLen, COMMAND_HEAD_LEN);	//send data/sw and receive next apdu header 
	
	return 0;
}
