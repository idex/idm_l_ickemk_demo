#ifndef __CTCLOSSCLASSC__
#define __CTCLOSSCLASSC__

/*
* Function: void CTClassC_Closed(void);
* Description: Detect ClassC and Stop chip
* Input: none
* Output: none
* Return: none
* Other:
*/
void CTClassC_Closed(void);

/*
 * version[0]: main version
 * version[1]: sub version
 * Such as V1.20, version[2] = {0x01, 0x20}
 */
void GetCTCloseClassCLibVersion(unsigned char *version);

#endif
