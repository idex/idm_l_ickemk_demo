/** ***************************************************************************
 * \file   idxSeBioAPI.c
 * \brief  Implement the IDEX SE-biometric API
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2018-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *
 *****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "idxSerialHostInterface.h"

#include "idxSeEmkAPI.h"
#include "dispatcher.h"
#include "idxSeBioAPI.h"
#include "bio-ctrl.h"
#include "idxSerialInterface.h"
#include "idxVersion.h"
#include "idxBaseDefines.h"
#include "idxHalSystem.h"

#ifdef TH89
#include "crypto.h"
#endif

#define UNUSED(p) if(p){}


const static struct idxBioSystemConfig_s *gpSystemConfig = NULL;

uint16_t idxBioInitialize( enum idxBioInitializeOptions_e Options,
          const struct idxBioSystemConfig_s *pSystemConfig,
          const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_INITIALIZE_DATA_SIZE;
  uint8_t bOptions = get_p2();
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
  
  switch( Options )
  {
    case IDX_BIO_INITIALIZE_CONTACT:
    {
      pDataBuf[0] = pSystemConfig->Policies->sensorinit ? 0x02 : 0x00;
      break;
    }
    case IDX_BIO_INITIALIZE_CONTACTLESS:
    {
      pDataBuf[0] = pSystemConfig->Policies->sensorinit ? 0x03 : 0x01;
      break;
    }
    case IDX_BIO_INITIALIZE_BATTERY:
    {
      pDataBuf[0] = 0x03;
      break;
    }
    default:
    {
      ret = IDX_ISO_ERR_NOT_SUPPTD;
    }
  }

  if ( ret == IDX_ISO_SUCCESS )
    ret = idxMcuComms( IDX_HOSTIF_CMD_INITIALIZE, bOptions, &data_size, pBuffer );

  if ( ret == IDX_ISO_SUCCESS )
    gpSystemConfig = pSystemConfig;
  
  return ret;
}

uint16_t idxBioUninitialize( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_UNINITIALIZE_DATA_SIZE;
  uint8_t bOptions = get_p2();

  ret = idxMcuComms( IDX_HOSTIF_CMD_UNINITIALIZE, bOptions, &data_size, pBuffer );
  
  gpSystemConfig = NULL;
  return ret;
}

uint16_t idxBioGetImage( const idxBuffer_t *pBuffer, uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();

  ret = idxMcuComms( IDX_HOSTIF_CMD_GETIMAGE, bOptions, &data_size, pBuffer );
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = data_size;

  return ret;	
}

uint16_t idxBioAcquire( const idxBuffer_t *pBuffer,
                        uint8_t flags,
                        uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_ACQUIRE_DATA_SIZE;
  uint8_t bOptions = get_p2();
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
  
  pDataBuf[0] = flags;

  ret = idxMcuComms( IDX_HOSTIF_CMD_ACQUIRE, bOptions, &data_size, pBuffer );
  
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = data_size;

  return ret; 
}

uint16_t idxBioLed( uint16_t ledControlFlags, const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )
  UNUSED( ledControlFlags )

  return ret;
}

uint16_t idxBioSelfTest( const idxBuffer_t *pBuffer, uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )
  UNUSED( pOutputDataSize )

  return ret;
}

uint16_t idxBioCalibrate( uint8_t Options,
                          const idxBuffer_t *pBuffer,
                          uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_CALIBRATE_DATA_SIZE;
  uint8_t bOptions = get_p2();
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
  
  pDataBuf[0] = Options;

  if ( ret == IDX_ISO_SUCCESS )
    ret = idxMcuComms( IDX_HOSTIF_CMD_EAGLE_CALIBRATE, bOptions, &data_size, pBuffer );

  if ( pOutputDataSize != NULL )
    *pOutputDataSize = data_size;

  return ret;
}

uint16_t idxBioDeleteRecord( uint16_t index, const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_DELETERECORD_DATA_SIZE;
  uint8_t bOptions = get_p2();
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];

  pDataBuf[0] = index & 0xFF;
  pDataBuf[1] = ( index >> 8 ) & 0xFF;

  ret = idxMcuComms( IDX_HOSTIF_CMD_DELETERECORD, bOptions, &data_size, pBuffer );

  return ret;
}

uint16_t idxBioUpdateStart( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_UPDATESTART_DATA_SIZE;
  uint8_t bOptions = get_p2();

  ret = idxMcuComms( IDX_HOSTIF_CMD_UPDATESTART, bOptions, &data_size, pBuffer );

  return ret;
}

uint16_t idxBioUpdateDetails( enum idxBioUpdateOptions_e Options,
             uint32_t Address, uint32_t Size,
             const uint8_t iv[32],
             const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
#if 0
  uint8_t buffer[41];
  uint8_t * pBuf = buffer;
  uint8_t flags = 0;
  uint8_t options;
  int i;
  
  switch ( Options )
  {
    case IDX_BIO_UPDATE_SUBSYSTEM:
    {
      flags = 0x00;
      break;
    }
    case IDX_BIO_UPDATE_SENSOR_RAM:
    {
      flags = 0x01;
      break;
    }
    case IDX_BIO_UPDATE_SENSOR_OTP:
    {
      flags = 0x02;
      break;
    }
    default:
    {
      ret = IDX_BIO_NOT_SUPPTD;
    }
  }
  
  if ( ret == IDX_ISO_SUCCESS )
  {
    options = get_p2();

    memcpy( pBuf, &Address, sizeof( uint32_t ) );
    pBuf += 4;
    memcpy( pBuf, &Size, sizeof( uint32_t ) );
    pBuf += 4;

    *pBuf++ = flags;

    for ( i = 0; i < 32; ++i )
      *pBuf++ = iv[i];

    ret = idxMcuComms( IDX_HOSTIF_CMD_UPDATEDETAILS, options, buffer, IDX_HOSTIF_CMD_UPDATEDETAILS_DATA_SIZE, NULL, NULL );
  }
#endif
  
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();

  UNUSED( Options )
  UNUSED( Address )
  UNUSED( Size )
  UNUSED( iv )
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_UPDATEDETAILS, bOptions, &data_size, pBuffer );
  return ret;
}

uint16_t idxBioUpdateChunk( const idxBuffer_t *pBuffer, uint16_t InputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();

  UNUSED( InputDataSize )
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_UPDATEDATA, bOptions, &data_size, pBuffer );
  return ret;
}

uint16_t idxBioUpdateHash( const uint8_t Hash[8], const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();
  UNUSED( Hash )
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_UPDATEHASH, bOptions, &data_size, pBuffer );
  return ret;
}

uint16_t idxBioUpdateEnd( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_UPDATEEND_DATA_SIZE;
  uint8_t bOptions = get_p2();

  ret = idxMcuComms( IDX_HOSTIF_CMD_UPDATEEND, bOptions, &data_size, pBuffer );
  return ret;
}

uint16_t idxBioStoreBlob( const idxBuffer_t *pBuffer,
                                  uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();

  ret = idxMcuComms( IDX_HOSTIF_CMD_STORE_BLOB, bOptions, &data_size, pBuffer );
  
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = data_size;

  return ret;  
}

uint16_t idxBioPairSensor( uint8_t Random[IDX_BIO_PAIR_RANDOM_SIZE],
                           const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )
  UNUSED( Random )

  return ret;
}

uint16_t idxBioLock( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t options = get_p2();
  uint16_t data_size = (uint16_t)get_lc();

  ret = idxMcuComms( IDX_HOSTIF_CMD_LOCK, options, &data_size, pBuffer );
  return ret;
}

uint16_t idxBioOnCardEnroll( const idx_MatcherWorkArea_t *pWorkAreas,
            const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;

  UNUSED(pBuffer)

#ifdef IMM_ONLY	
  UNUSED(gpSystemConfig)	
  UNUSED(pWorkAreas)	
  ret =  idxSeEmkMcuEnroll();
						
#else  // here IDM EMK API calls  
  {
    idx_SeEmkConfig_t idxConfig;

    if (!gpSystemConfig)
      return IDX_ISO_ERR_CONDITIONS;

    idxConfig.pIdxBuffer           = pBuffer;
    idxConfig.pWorkAreas          = pWorkAreas;
    idxConfig.securityLevel       = SE_EMK_SECURITY_LVL_5;
    // get max touches/max fingers
    idxConfig.maxTouchesPerFinger = gpSystemConfig->Policies->max_touches;
    idxConfig.maxEnrolledFingers  = gpSystemConfig->Policies->max_fingers;
    idxConfig.pTemplateFlashPtr    = gpSystemConfig->TemplateArea->pTemplateBase;
    idxConfig.templateFlashSize   = gpSystemConfig->TemplateArea->TemplateSize;    

    ret = idxSeEmkEnroll( &idxConfig );
              
    // If communications has failed then there's really no way to send
    // the BIODATAFLUSH command (and it makes it very confusing to debug
    // communications problems if another attempt is made after a failure).
    if (IDX_ISO_ERR_COMM != ret) {
      uint16_t ret2;
	  uint16_t data_size = IDX_HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE;
      uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
      
      pDataBuf[0] = 0x00; pDataBuf[1] = 0x00;
      ret2 = idxMcuComms( IDX_HOSTIF_CMD_BIODATAFLUSH, 0, &data_size, pBuffer );
      
      if (IDX_ISO_SUCCESS != ret2) {
        // If the BIODATAFLUSH command returned an error then return it, else
        // the result is that from the enroll
        ret = ret2;
      }
    }
  }
#endif

  return ret;
}

uint16_t idxBioSleeveEnroll( const idx_MatcherWorkArea_t *pWorkAreas,
                             const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED(pBuffer)

#ifdef IMM_ONLY	

  ret =  idxSeEmkImmSleeveEnroll();
						
#else // here IDM EMK API calls
  {
    idx_SeEmkConfig_t idxConfig;  

    if (!gpSystemConfig)
      return IDX_ISO_ERR_CONDITIONS;

    idxConfig.pIdxBuffer           = pBuffer;
    idxConfig.pWorkAreas          = pWorkAreas;
    idxConfig.securityLevel       = SE_EMK_SECURITY_LVL_5;
    idxConfig.maxTouchesPerFinger = gpSystemConfig->Policies->max_touches;
    idxConfig.maxEnrolledFingers  = gpSystemConfig->Policies->max_fingers;
    idxConfig.pTemplateFlashPtr    = gpSystemConfig->TemplateArea->pTemplateBase;
    idxConfig.templateFlashSize   = gpSystemConfig->TemplateArea->TemplateSize;     

    ret = idxSeEmkSleeveEnroll( &idxConfig );        // idx_SeEmkSecurityLevel_t securityLevel,
              
    // If communications has failed then there's really no way to send
    // the BIODATAFLUSH command (and it makes it very confusing to debug
    // communications problems if another attempt is made after a failure).
    if (IDX_ISO_ERR_COMM != ret) {
      uint16_t ret2;
      uint16_t data_size = IDX_HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE;
      uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
      
      pDataBuf[0] = 0x00; pDataBuf[1] = 0x00;
      ret2 = idxMcuComms( IDX_HOSTIF_CMD_BIODATAFLUSH, 0, &data_size, pBuffer );
      
      if (IDX_ISO_SUCCESS != ret2) {
        // If the BIODATAFLUSH command returned an error then return it, else
        // the result is that from the enroll
        ret = ret2;
      }
    }
  }
#endif

  return ret;
}

uint16_t idxBioSingleEnroll( const idx_MatcherWorkArea_t *pWorkAreas,
                             const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED(pBuffer)

#ifdef IMM_ONLY	

  //ret =  idxSeEmkImmEnroll();

#else  // here IDM EMK API calls  
  {	
    uint8_t p2 = get_p2();
    uint8_t touch =  p2 & 0x0F;  // These will come later from SystemConfig
    uint8_t finger = ( p2 >> 4 ) & 0x0F;

    idx_SeEmkConfig_t idxConfig; 

    if (!gpSystemConfig)
      return IDX_ISO_ERR_CONDITIONS;

    idxConfig.pIdxBuffer           = pBuffer;
    idxConfig.pWorkAreas          = pWorkAreas;
    idxConfig.securityLevel       = SE_EMK_SECURITY_LVL_5;
    idxConfig.maxTouchesPerFinger = gpSystemConfig->Policies->max_touches;
    idxConfig.maxEnrolledFingers  = gpSystemConfig->Policies->max_fingers;
    idxConfig.pTemplateFlashPtr    = gpSystemConfig->TemplateArea->pTemplateBase;
    idxConfig.templateFlashSize   = gpSystemConfig->TemplateArea->TemplateSize;     

    ret = idxSeEmkSingleEnroll( &idxConfig, touch, finger );
              
    // If communications has failed then there's really no way to send
    // the BIODATAFLUSH command (and it makes it very confusing to debug
    // communications problems if another attempt is made after a failure).
    if (IDX_ISO_ERR_COMM != ret) {
      uint16_t ret2;
      uint16_t data_size = IDX_HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE;
      uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
      
      pDataBuf[0] = 0x00; pDataBuf[1] = 0x00;
      ret2 = idxMcuComms( IDX_HOSTIF_CMD_BIODATAFLUSH, 0, &data_size, pBuffer );
      
      if (IDX_ISO_SUCCESS != ret2) {
        // If the BIODATAFLUSH command returned an error then return it, else
        // the result is that from the enroll
        ret = ret2;
      }
    }
  }
#endif

	return ret;
}





extern uint8_t capture_buffer[0x120];
extern void idxHalTimerDelayMsec(uint16_t msec);//done
extern uint16_t idxHalFlashErase(uint8_t *pAddr, uint32_t bytesToErase);
extern uint16_t idxHalFlashWrite(uint8_t *pAddr, 
                          const uint8_t *pData, 
                          uint16_t bytesToWrite);
#define Pos_verify_improve 1
#define aurora_image_size 17292
#define polaris_image_size 16641//
#define pos_image_size aurora_image_size
#define debug_flash_start 0x0c061000




#define BIOVERIFY_STORE_DATA_SIZE 32


uint16_t POS_image_capture_to_NVM(void)
{
	//viarable
	uint16_t ret = IDX_ISO_SUCCESS;
	uint8_t bOptions;
	uint16_t cdata_size;
	uint16_t recvDataSize;
	uint16_t image_size;
	uint8_t i;

	uint16_t image_data_offset;

	uint16_t image_data_rest_length;
	uint32_t debug_nvm_addr=0;


	idxBuffer_t temp_buffer;
	temp_buffer.pBuffer=capture_buffer;
	temp_buffer.bufferSize = sizeof(capture_buffer);



	
	//memset(capture_buffer,0xff,sizeof(capture_buffer));

	//clear debug area of nvm
	debug_nvm_addr=debug_flash_start;

	idxHalFlashErase((uint8_t *)debug_nvm_addr,0x5000);//0x0c060000 is used by cripto, 1k for key, offset 4k, 0x0c061000 for debug
	//read image from bmcu and write to nvm
	//send get image
	capture_buffer[HDR_SIZE]=0x00;

	bOptions=0x10;


	cdata_size=IDX_HOSTIF_CMD_ACQUIRE_DATA_SIZE;
	ret = idxMcuComms( IDX_HOSTIF_CMD_GETIMAGE, bOptions, &cdata_size, &temp_buffer );

	if(0==cdata_size) return IDX_ISO_ERROR_CMND_NOT_ALLOWED;

	image_size=((uint16_t)capture_buffer[HDR_SIZE] &0x00ff)+(((uint16_t)capture_buffer[HDR_SIZE+1]<<8)&0xff00);
	if((aurora_image_size!=image_size)&&(polaris_image_size!=image_size)) return IDX_ISO_ERR_DATA_SIZE;
	//read data   //matcherBuffer


	temp_buffer.pBuffer=capture_buffer;
	temp_buffer.bufferSize = sizeof(capture_buffer);


	bOptions=0x00;
	image_data_offset=0;
	image_data_rest_length=image_size;
	for(i=0;i<255;i++)
		{
		//memset(capture_buffer+(sizeof(capture_buffer)>>1),0xff,0x100);
		debug_nvm_addr=(uint32_t)(debug_flash_start+image_data_offset);


		capture_buffer[6]=(uint8_t)(image_data_offset&0x00ff);
		capture_buffer[7]=(uint8_t)((image_data_offset&0xff00)>>8);
		recvDataSize=0x100;
		if(0x100>image_data_rest_length) recvDataSize=image_data_rest_length;
		capture_buffer[8]=(uint8_t)(recvDataSize&0x00ff);
		capture_buffer[9]=(uint8_t)((recvDataSize&0xff00)>>8);
		image_data_offset +=recvDataSize;
		image_data_rest_length -=recvDataSize;
		recvDataSize=0x04;

		ret = idxMcuComms( IDX_HOSTIF_CMD_READ_DATA, bOptions, &recvDataSize, &temp_buffer );




		if(IDX_ISO_SUCCESS!=ret) return ret;
		//if(2>=recvDataSize) return IDX_ISO_ERROR_CMND_NOT_ALLOWED;
		//memcpy(dataBUF+128,&dataBUF[HDR_SIZE],recvDataSize);
		
		ret=idxHalFlashWrite((uint8_t *)debug_nvm_addr,&capture_buffer[HDR_SIZE] , recvDataSize);
		if(IDX_ISO_SUCCESS!=ret) return ret;
		if(0==image_data_rest_length)
			{
			//cdata[0]=(uint8_t)(image_size&0x00ff);
			//cdata[1]=(uint8_t)((image_size&0xff00)>>8);
			//ret=idxHalFlashWrite((uint8_t *)0x0c066000,cdata , 2);
			//while(1);
			break;
			}
		}


return IDX_ISO_SUCCESS;
}








uint16_t idxBioPosVerify( const idx_MatcherWorkArea_t *pWorkAreas, const idxBuffer_t *pBuffer, uint16_t *pScore, uint8_t imgsrc, uint16_t mwfTimeBudget )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_INITIALIZE_DATA_SIZE;
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
 // UNUSED(mwfTimeBudget);
  #ifdef Pos_verify_improve
	uint16_t Pos_verify_retry_count=0;
  uint16_t Pos_verify_step=0;
  #endif
#ifdef Pos_verify_improve
Pos_verify_retry_count=50;
Pos_verify_step=0;
while(0<Pos_verify_retry_count)
{pDataBuf[0] = 0x03;
ret = idxMcuComms( IDX_HOSTIF_CMD_INITIALIZE, 0, &data_size, pBuffer );
if (ret == IDX_ISO_SUCCESS)
{  extern const struct idxBioSystemConfig_s SystemConfig;
  gpSystemConfig = &SystemConfig;
  Pos_verify_step=1;}
else
{	switch (ret)
		{
		case IDX_ISO_ERR_SNS_POWER: 
		case IDX_ISO_ERR_BIO_POWER:
		case IDX_ISO_ERR_COMM:
		case IDX_ISO_ERR_TIMEOUT:
			{			Pos_verify_step=0;
			break;			}
		case IDX_ISO_ERR_CONDITIONS:
			{			Pos_verify_step=1;
			break;			}
		default:
			{			return ret;			}
		}}
if(1==Pos_verify_step)
	{	ret=idxBioVerify( pWorkAreas, pBuffer, pScore, imgsrc, mwfTimeBudget );
	if((IDX_ISO_SUCCESS==ret)||(IDX_ISO_ERR_NO_MATCH==ret))return ret;	}
Pos_verify_retry_count--;
idxHalTimerDelayMsec(100);
}
return ret;
#else
pDataBuf[0] = 0x03;
ret = idxMcuComms( IDX_HOSTIF_CMD_INITIALIZE, 0, &data_size, pBuffer );
if (ret != IDX_ISO_SUCCESS) return ret;
{  extern const struct idxBioSystemConfig_s SystemConfig;
  gpSystemConfig = &SystemConfig;}
return idxBioVerify( pWorkAreas, pBuffer, pScore, imgsrc, mwfTimeBudget );
#endif
}
uint16_t idxBioVerify( const idx_MatcherWorkArea_t *pWorkAreas,
                       const idxBuffer_t *pBuffer,
             uint16_t *pScore, uint8_t imgsrc, uint16_t mwfTimeBudget )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED(pScore)

#ifdef IMM_ONLY	
  UNUSED(pBuffer)  
  ret =  idxSeEmkImmMatch();

#else  // here IDM EMK API calls  
  {
    idx_SeEmkConfig_t idxConfig;

    if (!gpSystemConfig){
      pBuffer->pBuffer[0] = 0x06; // set response size to minimum ( =6 )   
      pBuffer->pBuffer[1] = 0x00;
      return IDX_ISO_ERR_CONDITIONS;
    }
    
    idxConfig.pIdxBuffer           = pBuffer;
    idxConfig.pWorkAreas          = pWorkAreas;
    idxConfig.securityLevel       = SE_EMK_SECURITY_LVL_5;
    idxConfig.maxTouchesPerFinger = gpSystemConfig->Policies->max_touches;
    idxConfig.maxEnrolledFingers  = gpSystemConfig->Policies->max_fingers;
    idxConfig.pTemplateFlashPtr    = gpSystemConfig->TemplateArea->pTemplateBase;
    idxConfig.templateFlashSize   = gpSystemConfig->TemplateArea->TemplateSize;     

    ret = idxSeEmkMatch( &idxConfig,               // uint8_t numEnrolledFingers,
                         imgsrc,
                         mwfTimeBudget);           // matcher work function
            
    // If communications has failed then there's really no way to send
    // the BIODATAFLUSH command (and it makes it very confusing to debug
    // communications problems if another attempt is made after a failure).
    if (IDX_ISO_ERR_COMM != ret) {
      uint8_t tmpDataBuff[BIOVERIFY_STORE_DATA_SIZE];
      uint16_t ret2;
      uint16_t data_size = IDX_HOSTIF_CMD_BIODATAFLUSH_DATA_SIZE;
      uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
      
      // Store ReplyHeader and first 10 bytes of ReplyData (signature + (possible) CRC for data)	
      memmove(tmpDataBuff, pBuffer->pBuffer, BIOVERIFY_STORE_DATA_SIZE);

      pDataBuf[0] = 0x00; pDataBuf[1] = 0x00;
      ret2 = idxMcuComms( IDX_HOSTIF_CMD_BIODATAFLUSH, 0, &data_size, pBuffer );
      
      // Restore reply header and first 10 bytes of reply data (signature + (possible) CRC for data)	
      memmove(pBuffer->pBuffer, tmpDataBuff, BIOVERIFY_STORE_DATA_SIZE);

      if (IDX_ISO_SUCCESS != ret2) {
        // If the BIODATAFLUSH command returned an error then return it, else
        // the result is that from the match
        ret = ret2;
      }
    }
    
    if ((IDX_ISO_SUCCESS != ret) && (IDX_ISO_ERR_NO_MATCH != ret))
    {
      pBuffer->pBuffer[0] = IDX_BIO_BUFFER_DATA_OFFSET;
    }

  }
#endif
  return ret;
}

uint16_t idxBioTemplateDetails( enum idxBioTemplateOptions_e Options,
               const struct idxBioEnrolFingerInfo_s *pFingerInfo,
               const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )
  UNUSED( pFingerInfo )

  switch( Options )
  {
    case IDX_BIO_TEMPLATE_DEFAULT:
    {
      break;
    }
    case IDX_BIO_TEMPLATE_BIO:
    {
      break;
    }
    case IDX_BIO_TEMPLATE_SE:
    {
      break;
    }
    default:
    {
      ret = IDX_ISO_ERR_NOT_SUPPTD;
    }
  }
  return ret;
}

uint16_t idxBioTemplateChunk( const idxBuffer_t *pBuffer, uint16_t InputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )
  UNUSED( InputDataSize )

  return ret;
}

uint16_t idxBioUpdateTemplateEnd( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )

  return ret;
}

uint16_t idxBioResetEnrollment( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED( pBuffer )

  return ret;
}

uint16_t idxBioReset( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_RESET_DATA_SIZE;
  uint8_t bOptions = get_p2();
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_RESET, bOptions, &data_size, pBuffer );
  
  return ret;
}

uint16_t idxBioGetParameter( uint8_t id, uint16_t *pValue, const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_GETPARAM_DATA_SIZE;
  uint8_t bOptions = get_p2();
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
  
  pDataBuf[0] = id;
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_GETPARAMETER, bOptions, &data_size, pBuffer );
  
  *pValue = RD_U16(pDataBuf);

  return ret;
}

uint16_t idxBioSetParameter( uint8_t id, uint16_t Value, const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_SETPARAM_DATA_SIZE;
  uint8_t bOptions = get_p2();
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
  
  pDataBuf[0] = id;
  pDataBuf[1] = Value & 0xFF;
  pDataBuf[2] = ( Value >> 8 ) & 0xFF;	
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_SETPARAMETER, bOptions, &data_size, pBuffer );

  return ret;
}

uint16_t idxBioStoreParams( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_STOREPARAM_DATA_SIZE;
  uint8_t bOptions = get_p2();

  ret = idxMcuComms( IDX_HOSTIF_CMD_STOREPARAM, bOptions, &data_size, pBuffer );
  return ret;
}


uint16_t idxBioReadImageFromNvm( uint16_t BufferSize, uint8_t *pBuffer, uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t *cdata = pBuffer;
  uint8_t *rdata = pBuffer + (BufferSize >> 1);
  uint16_t offset=0;
  uint16_t length=0;
  uint8_t *flash_test_pointer=0;
	offset= cdata[0] +cdata[1]*256;
	length +=cdata[2];
	flash_test_pointer=(uint8_t *)debug_flash_start;
	if(17292<offset)return IDX_ISO_ERR_DATA_SIZE;
	if(255<length)return IDX_ISO_ERR_DATA_SIZE;
	memcpy(rdata,flash_test_pointer+offset,length);
    *pOutputDataSize = length;
  return ret;	
}




uint16_t idxBioReadData( const idxBuffer_t *pBuffer, uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();

  ret = idxMcuComms( IDX_HOSTIF_CMD_READ_DATA, bOptions, &data_size, pBuffer );

  if ( pOutputDataSize != NULL )
    *pOutputDataSize = data_size;

  return ret;	
}

uint16_t idxBioLoadImage( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t bOptions = get_p2();
  uint16_t data_size = (uint16_t)get_lc();
  
  ret = idxMcuComms( IDX_HOSTIF_CMD_LOAD_IMAGE, bOptions, &data_size, pBuffer );
  return ret;  
}

#ifndef SE_MODULE_NAME
  #error Module name undefined.
#endif

ADD_FUZEID(SE_MODULE_NAME);

uint16_t idxBioGetComponentVersion( 
          const idxBuffer_t *pBuffer, uint16_t * pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint8_t *pDataBuf  = &pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET];
  uint8_t component = pDataBuf[0];
  
  if( component == IDX_BIO_VERSION_COMPONENT_ID_SE_SDK){ 
    pDataBuf[0]     = IDX_VERSION;
    pDataBuf[1]     = IDX_REVISION;
    pDataBuf[2]     = (uint16_t)IDX_BUILD        & 0xff;  // LSB
    pDataBuf[3]     = ((uint16_t)IDX_BUILD >> 8) & 0xff;  // MSB
    pDataBuf[4]     = 0; // Padding 2 bytes
    pDataBuf[5]     = 0;
    strcpy((char*)&pDataBuf[6],(char *)GET_FUZEID_STR(SE_MODULE_NAME)); // IDEXSE__yymmdd-ttt-sssss
    pDataBuf[31]    = VERSION_SCHEMA_FUZE; 
    if ( pOutputDataSize != NULL ) *pOutputDataSize = 32;
  }
  else if( component == IDX_BIO_VERSION_COMPONENT_ID_SE_MTCH){
    idx_UnitFuzeVersion_t EmkVersion;
    idxSeEmkGetUnitVersion(&EmkVersion);
    pDataBuf[0]     = EmkVersion.version;
    pDataBuf[1]     = EmkVersion.revision;
    pDataBuf[2]     = EmkVersion.build        & 0xff;  // LSB
    pDataBuf[3]     = (EmkVersion.build >> 8) & 0xff;  // MSB
    pDataBuf[4]     = 0; // Padding 2 bytes
    pDataBuf[5]     = 0; 
    memcpy((char*)&pDataBuf[6],(char *)EmkVersion.module_prefix_name,8); // IDEXMTCH    
    strcpy((char*)&pDataBuf[14],(char *)EmkVersion.module_fuze_id);      // yymmdd-ttt-sssss
    pDataBuf[31]    = VERSION_SCHEMA_FUZE;     
    if ( pOutputDataSize != NULL ) *pOutputDataSize = 32;
  }
  else
  {
      uint8_t bOptions = get_p2();
      uint16_t data_size = 1;

      ret = idxMcuComms( IDX_HOSTIF_CMD_GETCOMPONENTVERSION, bOptions, &data_size, pBuffer );
      
      if ( pOutputDataSize != NULL )
           *pOutputDataSize = data_size;
  }
  
  return ret;
}

uint16_t idxBioListTemplates( const idxBuffer_t *pBuffer, uint16_t *pOutputDataSize )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  uint16_t data_size = IDX_HOSTIF_CMD_LISTRECORD_DATA_SIZE;
  uint8_t bOptions = 0;

  ret = idxMcuComms( IDX_HOSTIF_CMD_LISTRECORD, bOptions, &data_size, pBuffer );
  
  if ( pOutputDataSize != NULL )
    *pOutputDataSize = data_size;

  return ret;  
}

#ifndef IMM_ONLY
uint16_t idxBioDeleteFinger( const idxBuffer_t *pBuffer, uint8_t options )
{
  uint16_t ret = IDX_ISO_SUCCESS;

  idx_SeEmkConfig_t idxConfig;
  uint8_t fingerID = options;

  idxConfig.pIdxBuffer           = pBuffer;
  idxConfig.maxTouchesPerFinger = gpSystemConfig->Policies->max_touches;
  idxConfig.maxEnrolledFingers  = gpSystemConfig->Policies->max_fingers;
  idxConfig.pTemplateFlashPtr    = gpSystemConfig->TemplateArea->pTemplateBase;
  idxConfig.templateFlashSize   = gpSystemConfig->TemplateArea->TemplateSize;   

  ret = idxSeEmkDeleteFinger( &idxConfig,
                              fingerID,
                              SE_EMK_DELETE_ENROLLMENT_TEMPLATE);
  
  return ret;
}
#endif

#if 0
/********************************************************************************
 *  Secure Channel Commands
 */
uint16_t idxBioInitializeUpdate( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
#ifdef TH89  
  ret = initUpdate(pBuffer);
#endif
  return ret;
}

uint16_t idxBioExternalAuthenticate( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED(pBuffer)
#ifdef TH89  
  ret = getExtAuthData();
#endif
  return ret;
}

uint16_t idxBioPutKeyHeader( enum idxBioSecureOptions_e Options,
                             const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
  UNUSED(pBuffer)
  UNUSED(Options)
#ifdef TH89	
  setSeKeys();        // generate random keys in SE flash
  ret = setMcuKeys(pBuffer); // set the very first set of keys for MCU
#endif
	
  return ret;
}

uint16_t idxBioFinalizeKeys( const idxBuffer_t *pBuffer )
{
  uint16_t ret = IDX_ISO_SUCCESS;
	
#ifdef TH89
  uint16_t data_size;
  getNewKeys(&pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET], pBuffer);
  
  data_size = IDX_HOSTIF_CMD_PUTKEY_DATA_SIZE;
  ret = idxMcuComms(IDX_HOSTIF_CMD_PUTKEY, ENCRYPT_BIT|SIGN_BIT, &data_size, pBuffer);
  
  pBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET] = 1;
  data_size = IDX_HOSTIF_CMD_FLASHUPDATE_DATA_SIZE;
  ret = idxMcuComms(IDX_HOSTIF_CMD_FLASHUPDATE, ENCRYPT_BIT|SIGN_BIT, &data_size, pBuffer);
#endif
  UNUSED(pBuffer)
	
  return ret;
}
#endif
/*  Secure Channel Commands
 ********************************************************************************/
