/*
 * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: THD89.h
 * SCFID: 
 * Feature: base addr define
 * Version: V0.1
 *
 * History: 
 *   2014-11-19 
 *     1. Original version 0.1
 */
#ifndef __THD89_H__
#define __THD89_H__

//base addr
#define IDLE_BASE_ADDR				0x40000000L
#define RST_BASE_ADDR				0x40004000L
#define CLK_BASE_ADDR				0x40008000L
#define INT_BASE_ADDR				0x40010000L
#define IO_BASE_ADDR				0x40014000L
#define MCU_BASE_ADDR				0x40700000L
#define ROM_BASE_ADDR				0x41500000L
#define NVM_BASE_ADDR				0x41540000L
#define RAM0_BASE_ADDR				0x41580000L
#define RAM1_BASE_ADDR				0x41584000L
#define CT_BASE_ADDR				0x41c00000L
#define SPI_BASE_ADDR				0x41c08000L
#define GPIO_BASE_ADDR				0x41c0c000L
#define RF_BASE_ADDR				0x41c14000L
#define I2C_BASE_ADDR				0x41c18000L
#define CRC_BASE_ADDR				0x42300000L
#define DES_BASE_ADDR				0x42304000L
#define	AES_BASE_ADDR				0x42308000L
#define SM4_BASE_ADDR				0x42310000L
#define TRNG_BASE_ADDR				0x4231c000L
#define DRNG_BASE_ADDR				0x42320000L
#define TIMER_BASE_ADDR				0x42500000L
#define EVD_BASE_ADDR				0x42600000L
#define IVD_BASE_ADDR				0x42604000L
#define EGS_BASE_ADDR				0x42608000L
#define IGS_BASE_ADDR				0x4260c000L
#define ASH_BASE_ADDR				0x42610000L
#define FD_BASE_ADDR				0x42614000L
#define TD_BASE_ADDR				0x42618000L
#define LD_BASE_ADDR				0x4261c000L
#define WDT_BASE_ADDR				0x42620000L
#define LDO_BASE_ADDR				0x42804000L
#define OSC_BASE_ADDR				0x42808000L
#define HFF_BASE_ADDR				0x4280c000L
#define ACB_BASE_ADDR				0x42900000L
#define MMU_BASE_ADDR 				0x42A04000L
#define PKE_BASE_ADDR 				0x43100000L
#define SYSTICK_BASE_ADDR			0xe000e000L

//IDLE sfr
#define IDLECON0				*((volatile unsigned long *)(IDLE_BASE_ADDR +0x000))
#define IDLECON1				*((volatile unsigned long *)(IDLE_BASE_ADDR +0x004))


//RST sfr
#define RSTIOCON				*((volatile unsigned long *)(RST_BASE_ADDR +0x000))
#define RSTTRCON				*((volatile unsigned long *)(RST_BASE_ADDR +0x004))
#define RSTALGCON				*((volatile unsigned long *)(RST_BASE_ADDR +0x008))

//CLK sfr
#define CLKSYSSEL       *((volatile unsigned long *)( 0x40008000L))
#define CLKTR0SEL				*((volatile unsigned long *)(CLK_BASE_ADDR +0x090))
#define CLKTR1SEL				*((volatile unsigned long *)(CLK_BASE_ADDR +0x094))
#define CLKTR2SEL				*((volatile unsigned long *)(CLK_BASE_ADDR +0x098))
#define CLKPKESEL				*((volatile unsigned long *)(CLK_BASE_ADDR +0x1d8))
#define CLKWDTSEL				*((volatile unsigned long *)(CLK_BASE_ADDR +0x2c0))
#define CLKIOCON				*((volatile unsigned long *)(CLK_BASE_ADDR +0x700))
#define CLKTRCON				*((volatile unsigned long *)(CLK_BASE_ADDR +0x704))
#define CLKALGCON				*((volatile unsigned long *)(CLK_BASE_ADDR +0x708))
#define CLKMEMCON				*((volatile unsigned long *)(CLK_BASE_ADDR +0x710))
#define CLKSECCON				*((volatile unsigned long *)(CLK_BASE_ADDR +0x714))			
//INT sfr
#define INTIOCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x000))
#define INTTRCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x004))
#define INTALGCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x008))
#define INTMEMCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x010))
#define INTSWICON				*((volatile unsigned long *)(INT_BASE_ADDR +0x018))
#define INTALGSTS				*((volatile unsigned long *)(INT_BASE_ADDR +0x048))
#define INTSSTS					*((volatile unsigned long *)(INT_BASE_ADDR +0x058))
#define EVTALGCON 				*((volatile unsigned long *)(INT_BASE_ADDR +0x088))
#define EVTMEMCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x090))
#define ABORTTRCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x0c4))
#define ABORTCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x0f0))
#define IRQMSK 					*((volatile unsigned long *)(INT_BASE_ADDR +0x100))
#define INTSCCON				*((volatile unsigned long *)(INT_BASE_ADDR +0x014))
#define INTSCSTS				*((volatile unsigned long *)(INT_BASE_ADDR +0x054))

//IO sfr
#define IOSEL0					*((volatile unsigned long *)(IO_BASE_ADDR +0x000))
#define IOPULLSEL0				*((volatile unsigned long *)(IO_BASE_ADDR +0x020))
#define IOPULLSEL2				*((volatile unsigned long *)(IO_BASE_ADDR +0x028))
#define IOSTS0					*((volatile unsigned long *)(IO_BASE_ADDR +0x040))

//NVM sfr
#define NVMSTS					*((volatile unsigned long *)(NVM_BASE_ADDR+0x000))
#define NVMSTS1					*((volatile unsigned long *)(NVM_BASE_ADDR+0x004))
#define NVMMSK1					*((volatile unsigned long *)(NVM_BASE_ADDR+0x010))
#define NVMSDP1					*((volatile unsigned long *)(NVM_BASE_ADDR+0x020))
#define NVMSDP2					*((volatile unsigned long *)(NVM_BASE_ADDR+0x024))
#define NVMCON					*((volatile unsigned long *)(NVM_BASE_ADDR+0x030))
#define NVMSRSADR				*((volatile unsigned long *)(NVM_BASE_ADDR+0x060))
#define NVMSRLEN				*((volatile unsigned long *)(NVM_BASE_ADDR+0x064))
#define NVMSRCON				*((volatile unsigned long *)(NVM_BASE_ADDR+0x068))
#define NVMNRSADR				*((volatile unsigned long *)(NVM_BASE_ADDR+0x080))
#define NVMNRLEN				*((volatile unsigned long *)(NVM_BASE_ADDR+0x084))

//RAM1 sfr
#define RAM1WDTCON				*((volatile unsigned long *)(RAM1_BASE_ADDR +0x000))
#define RAM1WDTSADR				*((volatile unsigned long *)(RAM1_BASE_ADDR +0x004))
#define RAM1WDTEADR				*((volatile unsigned long *)(RAM1_BASE_ADDR +0x008))
#define RAM1WDTSTS				*((volatile unsigned long *)(RAM1_BASE_ADDR +0x00c))
#define RAM1STS					*((volatile unsigned long *)(RAM1_BASE_ADDR +0x040))

//CT sfr
#define CTCON					*((volatile unsigned long *)(CT_BASE_ADDR +0x000))
#define CTCON1					*((volatile unsigned long *)(CT_BASE_ADDR +0x004))
#define CTCON2					*((volatile unsigned long *)(CT_BASE_ADDR +0x008))
#define CTSTS					*((volatile unsigned long *)(CT_BASE_ADDR +0x00c))
#define CTBRC					*((volatile unsigned long *)(CT_BASE_ADDR +0x010))
#define CTBUF					*((volatile unsigned long *)(CT_BASE_ADDR +0x014))
#define CTDIO					*((volatile unsigned long *)(CT_BASE_ADDR +0x018))
#define CTMSK					*((volatile unsigned long *)(CT_BASE_ADDR +0x01c))
#define CTATRCON				*((volatile unsigned long *)(CT_BASE_ADDR +0x020))
#define CTATRSTS				*((volatile unsigned long *)(CT_BASE_ADDR +0x024))
#define CTATRMSK				*((volatile unsigned long *)(CT_BASE_ADDR +0x028))
#define CTATRWAIT				*((volatile unsigned long *)(CT_BASE_ADDR +0x02c))
#define CTDMACON				*((volatile unsigned long *)(CT_BASE_ADDR +0x030))
#define CTDMASTS				*((volatile unsigned long *)(CT_BASE_ADDR +0x034))
#define CTDMATXBFAD				*((volatile unsigned long *)(CT_BASE_ADDR +0x038))
#define CTDMATXBFLEN			*((volatile unsigned long *)(CT_BASE_ADDR +0x03c))
#define CTDMABFPT				*((volatile unsigned long *)(CT_BASE_ADDR +0x040))
#define CTDMAMSK				*((volatile unsigned long *)(CT_BASE_ADDR +0x044))
#define CTDMARXBFAD				*((volatile unsigned long *)(CT_BASE_ADDR +0x048))
#define CTDMARXBFLEN			*((volatile unsigned long *)(CT_BASE_ADDR +0x04c))
#define CTTCON					*((volatile unsigned long *)(CT_BASE_ADDR +0x050))
#define CTTDAT					*((volatile unsigned long *)(CT_BASE_ADDR +0x054))
#define CTTRLD					*((volatile unsigned long *)(CT_BASE_ADDR +0x058))
#define CTTMSK					*((volatile unsigned long *)(CT_BASE_ADDR +0x05c))
#define CTNULL					*((volatile unsigned long *)(CT_BASE_ADDR +0x060))

#define SPICON2					*((volatile unsigned long *)(SPI_BASE_ADDR +0x008))
#define SPIDAT					*((volatile unsigned long *)(SPI_BASE_ADDR +0x00c))
#define SPISTS					*((volatile unsigned long *)(SPI_BASE_ADDR +0x010))
#define SPIMSK					*((volatile unsigned long *)(SPI_BASE_ADDR +0x014))
#define SPIRESUMEMSK			*((volatile unsigned long *)(SPI_BASE_ADDR +0x018))
#define SPIDMALEN				*((volatile unsigned long *)(SPI_BASE_ADDR +0x020))
#define SPIDMACON				*((volatile unsigned long *)(SPI_BASE_ADDR +0x028))
#define SPIDMASTS				*((volatile unsigned long *)(SPI_BASE_ADDR +0x02c))
#define SPIDMAMSK				*((volatile unsigned long *)(SPI_BASE_ADDR +0x030))
#define SPITADR					*((volatile unsigned long *)(SPI_BASE_ADDR +0x040))
#define SPIRADR					*((volatile unsigned long *)(SPI_BASE_ADDR +0x044))
#define SPIDMARPT				*((volatile unsigned long *)(SPI_BASE_ADDR +0x048))

#define RF_BUF					((volatile unsigned char *)(0x20200000L))
#define RFTRMODRD				*((volatile unsigned long *)(RF_BASE_ADDR+0x000))
#define RFTRMODSET				*((volatile unsigned long *)(RF_BASE_ADDR+0x004))
#define RFTRMODCLR				*((volatile unsigned long *)(RF_BASE_ADDR+0x008))
#define RFTRCONRD				*((volatile unsigned long *)(RF_BASE_ADDR+0x00c))
#define RFTRCONSET				*((volatile unsigned long *)(RF_BASE_ADDR+0x010))
#define RFTRCONCLR				*((volatile unsigned long *)(RF_BASE_ADDR+0x014))
#define RFTRSTA					*((volatile unsigned long *)(RF_BASE_ADDR+0x018))
#define RFTRSTACLR				*((volatile unsigned long *)(RF_BASE_ADDR+0x01c))
#define RFRSCH					*((volatile unsigned long *)(RF_BASE_ADDR+0x020))
#define RFRSCL					*((volatile unsigned long *)(RF_BASE_ADDR+0x024))
#define RFCRCH					*((volatile unsigned long *)(RF_BASE_ADDR+0x030))
#define RFCRCL					*((volatile unsigned long *)(RF_BASE_ADDR+0x034))
#define RFMODSEL				*((volatile unsigned long *)(RF_BASE_ADDR+0x038))
#define RFPOWSTA				*((volatile unsigned long *)(RF_BASE_ADDR+0x044))
#define RFPOWMSK				*((volatile unsigned long *)(RF_BASE_ADDR+0x048))
#define RFMODCON				*((volatile unsigned long *)(RF_BASE_ADDR+0x04c))
#define RFTRMSK					*((volatile unsigned long *)(RF_BASE_ADDR+0x054))
#define RFBAUDSEL				*((volatile unsigned long *)(RF_BASE_ADDR+0x058))
#define RFCKCON					*((volatile unsigned long *)(RF_BASE_ADDR+0x05c))
#define RFCRCCON				*((volatile unsigned long *)(RF_BASE_ADDR+0x070))
#define RFPROCA					*((volatile unsigned long *)(RF_BASE_ADDR+0x0c0))
#define RFBITPOS				*((volatile unsigned long *)(RF_BASE_ADDR+0x0c4))
#define RFAFDT					*((volatile unsigned long *)(RF_BASE_ADDR+0x0c8))
#define RFCASA					*((volatile unsigned long *)(RF_BASE_ADDR+0x0d0))
#define RFUID0					*((volatile unsigned long *)(RF_BASE_ADDR+0x0d4))
#define RFUID1					*((volatile unsigned long *)(RF_BASE_ADDR+0x0d8))
#define RFUID2					*((volatile unsigned long *)(RF_BASE_ADDR+0x0dc))
#define RFUID3					*((volatile unsigned long *)(RF_BASE_ADDR+0x0e0))
#define RFUID4					*((volatile unsigned long *)(RF_BASE_ADDR+0x0e4))
#define RFUID5					*((volatile unsigned long *)(RF_BASE_ADDR+0x0e8))
#define RFUID6					*((volatile unsigned long *)(RF_BASE_ADDR+0x0ec))
#define RFUID7					*((volatile unsigned long *)(RF_BASE_ADDR+0x0f0))
#define RFUID8					*((volatile unsigned long *)(RF_BASE_ADDR+0x0f4))
#define RFUID9					*((volatile unsigned long *)(RF_BASE_ADDR+0x0f8))
#define RFATQA0					*((volatile unsigned long *)(RF_BASE_ADDR+0x0fc))
#define RFATQA1					*((volatile unsigned long *)(RF_BASE_ADDR+0x100))
#define RFSAK0					*((volatile unsigned long *)(RF_BASE_ADDR+0x104))
#define RFSAK1					*((volatile unsigned long *)(RF_BASE_ADDR+0x108))
#define RFSAK2					*((volatile unsigned long *)(RF_BASE_ADDR+0x10c))
#define RFATXSCON				*((volatile unsigned long *)(RF_BASE_ADDR+0x110))
#define RFADEMCON				*((volatile unsigned long *)(RF_BASE_ADDR+0x130))

//TR_STA
#define RF_TX_FIN	( RFTRSTA & 0X80 )
#define RF_RX_FIN	( RFTRSTA & 0X01 )
#define RF_CRCERR	( RFTRSTA & 0x02 )
#define RF_TMR_OVR	( RFTRSTA & 0x04 )
#define RF_DATOVR	( RFTRSTA & 0x08 )
#define RF_FERR		( RFTRSTA & 0x10 )
#define RF_PERR		( RFTRSTA & 0x20 )
#define RF_FDT_OVR	( RFTRSTA & 0x40 )
#define RF_POW_POS	( RFPOWSTA & 0x01 )

//CRC sfr
#define CRCDAT					*((volatile unsigned long *)(CRC_BASE_ADDR +0x000))
#define CRCCON					*((volatile unsigned long *)(CRC_BASE_ADDR +0x004))
#define CRCCON1					*((volatile unsigned long *)(CRC_BASE_ADDR +0x008))

//RNG sfr
#define RNGCON					*((volatile unsigned long *)(TRNG_BASE_ADDR+0x00))
#define RNGSTS					*((volatile unsigned long *)(TRNG_BASE_ADDR+0x10))
#define RNGCLR					*((volatile unsigned long *)(TRNG_BASE_ADDR+0x18))
#define RNGDAT					*((volatile unsigned long *)(TRNG_BASE_ADDR+0x20))
#define RNTCON					*((volatile unsigned long *)(TRNG_BASE_ADDR+0x1C80))
#define RNTSTS					*((volatile unsigned long *)(TRNG_BASE_ADDR+0x0048))

//TIMER sfr
#define TIMER0CONSET			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x000))
#define TIMER0CONCLR			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x004))
#define TIMER0CONRD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x008))
#define TIMER0STSCLR			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x010))
#define TIMER0STSRD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x014))
#define TIMER0DIV				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x01c))
#define TIMER0RLD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x020))
#define TIMER0VAL				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x024))
#define TIMER1CONSET			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x040))
#define TIMER1CONCLR			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x044))
#define TIMER1CONRD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x048))
#define TIMER1STSCLR			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x050))
#define TIMER1STSRD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x054))
#define TIMER1DIV				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x05c))
#define TIMER1RLD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x060))
#define TIMER1VAL				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x064))
#define TIMER2CONSET			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x080))
#define TIMER2CONCLR			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x084))
#define TIMER2CONRD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x088))
#define TIMER2STSCLR			*((volatile unsigned long *)(TIMER_BASE_ADDR +0x090))
#define TIMER2STSRD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x094))
#define TIMER2DIV				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x09c))
#define TIMER2RLD				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x0a0))
#define TIMER2VAL				*((volatile unsigned long *)(TIMER_BASE_ADDR +0x0a4))


//WDT sfr
#define WDTCONSET				*((volatile unsigned long *)(WDT_BASE_ADDR +0x000))
#define WDTCONCLR				*((volatile unsigned long *)(WDT_BASE_ADDR +0x004))
#define WDTCONRD				*((volatile unsigned long *)(WDT_BASE_ADDR +0x008))
#define WDTSTSCLR				*((volatile unsigned long *)(WDT_BASE_ADDR +0x010))
#define WDTSTSRD				*((volatile unsigned long *)(WDT_BASE_ADDR +0x014))
#define WDTCFG 					*((volatile unsigned long *)(WDT_BASE_ADDR +0x018))
#define WDTRLD 					*((volatile unsigned long *)(WDT_BASE_ADDR +0x01c))
#define WDTVAL 					*((volatile unsigned long *)(WDT_BASE_ADDR +0x020))
#define WDTSRVINI				*((volatile unsigned long *)(WDT_BASE_ADDR +0x024))
#define WDTSRV 					*((volatile unsigned long *)(WDT_BASE_ADDR +0x028))

//PKE sfr
#define PKESTS					*((volatile unsigned long *)(PKE_BASE_ADDR+0x10))
#define PKECON2					*((volatile unsigned long *)(PKE_BASE_ADDR+0x04))
#define	PKECFG					*((volatile unsigned long *)(PKE_BASE_ADDR+0x18))
//SYSTICK sfr
#define SYSTCSR					*((volatile unsigned long *)(SYSTICK_BASE_ADDR +0x010))
#define SYSTRVR					*((volatile unsigned long *)(SYSTICK_BASE_ADDR +0x014))
#define SYSTCVR					*((volatile unsigned long *)(SYSTICK_BASE_ADDR +0x018))
#define SYSTCALIB				*((volatile unsigned long *)(SYSTICK_BASE_ADDR +0x01c))

//GPIO sfr
#define GPIODAT					*((volatile unsigned long *)(GPIO_BASE_ADDR +0x000))
#define GPIODAT_B0			*((volatile unsigned char *)(GPIO_BASE_ADDR +0x100))
#define GPIODAT_B1			*((volatile unsigned char *)(GPIO_BASE_ADDR +0x101))
#define GPIODAT_B2			*((volatile unsigned char *)(GPIO_BASE_ADDR +0x102))
#define GPIODAT_B3			*((volatile unsigned char *)(GPIO_BASE_ADDR +0x103))
#define GPIODIR					*((volatile unsigned long *)(GPIO_BASE_ADDR +0x004))
#define GPIOINTEN				*((volatile unsigned long *)(GPIO_BASE_ADDR +0x020))
#define GPIOINTSTS			*((volatile unsigned long *)(GPIO_BASE_ADDR +0x028))
#define GPIORESUMEEN		*((volatile unsigned long *)(GPIO_BASE_ADDR +0x040))
#define GPIORESUMESTS		*((volatile unsigned long *)(GPIO_BASE_ADDR +0x048))
#define GPIOCFG0				*((volatile unsigned long *)(GPIO_BASE_ADDR +0x060))
#define GPIOCFG1				*((volatile unsigned long *)(GPIO_BASE_ADDR +0x064))

#endif
