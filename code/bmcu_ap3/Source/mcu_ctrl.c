


#include "main.h"




unsigned char cmd_string_size,cmd_ready;

uint8_t se_hso_int_flag=0;
uint8_t wtx_sensor_init_status=0;
uint8_t wtx_sensor_interrupt_flag=0;

uint8_t wtx_sensor_process_flag=0;

uint8_t wtx_durition_counter;
 



uint8_t  string_temp[100];

uint8_t se_spi_common_buffer[2560];
uint8_t se_spi_TX_buffer[256];
uint8_t se_spi_RX_buffer[256];

unsigned char* dataImage;


//uint32_t gAmHalResetStatus = 0;

uint32_t SharedHeap[MAX_SHARED_MEM_ALLOCATION / 4];

uint32_t DedicatedHeap[MAX_DEDICATED_MEM_ALLOCATION / 4];

uint32_t dedicated_heap_size = sizeof(DedicatedHeap);
uint8_t* dedicated_heap_start_address = (uint8_t *)DedicatedHeap;
uint8_t* dedicated_heap_pointer = (uint8_t *)DedicatedHeap;

uint32_t shared_heap_size = sizeof(SharedHeap);
uint8_t* shared_heap_start_address = (uint8_t *)SharedHeap;
uint8_t* shared_heap_ick_pointer = (uint8_t *)SharedHeap;
uint8_t* shared_heap_emk_pointer = (uint8_t *)SharedHeap;

idx_State state;

am_devices_led_t am_bsp_psLEDs[AM_BSP_NUM_LEDS] =
{
    {AM_BSP_GPIO_LED0, AM_DEVICES_LED_ON_HIGH | AM_DEVICES_LED_POL_DIRECT_DRIVE_M},
    {AM_BSP_GPIO_LED1, AM_DEVICES_LED_ON_HIGH | AM_DEVICES_LED_POL_DIRECT_DRIVE_M},
    {AM_BSP_GPIO_LED2, AM_DEVICES_LED_ON_HIGH | AM_DEVICES_LED_POL_DIRECT_DRIVE_M},
    {AM_BSP_GPIO_LED3, AM_DEVICES_LED_ON_HIGH | AM_DEVICES_LED_POL_DIRECT_DRIVE_M},
    {AM_BSP_GPIO_LED4, AM_DEVICES_LED_ON_HIGH | AM_DEVICES_LED_POL_DIRECT_DRIVE_M}
};



extern unsigned char* dataImage;
extern const am_hal_gpio_pincfg_t g_AM_HAL_GPIO_OUTPUT;
extern uint8_t ios_long_data_transfer_flag;


extern void EPD_PORT_INITIAL(void);
extern void SE_SPI_PORT_INIT(void);
extern void se_read_data_init(void);

extern void stimer_init(void);
extern void crd_ctimer_int(void);
extern void wtx_stage_two_callback(void);
extern void LED_port_initial(void);
extern uint32_t am_hal_pwrctrl_periph_enable(am_hal_pwrctrl_periph_e ePeripheral);

const volatile char* compatibility_lib_version = { "ver_modulename:x.x.x" };








const am_hal_gpio_pincfg_t       g_AM_BSP_GPIO_COM_UART_TX=
{
	0,//.uFuncSel		uart tx
	0,//	ePowerSw
	0,//	ePullup
	AM_HAL_GPIO_PIN_DRIVESTRENGTH_2MA,//.eDriveStrength = 
	AM_HAL_GPIO_PIN_OUTCFG_PUSHPULL,//.eGPOutcfg	 =
	0,// eGPInput
	0,// eIntDir
	0,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22

};


const am_hal_gpio_pincfg_t       g_AM_BSP_GPIO_COM_UART_RX=
{
	0,//.uFuncSel		uart tx
	0,//	ePowerSw
	0,//	ePullup
	0,//.eDriveStrength = 
	0,//.eGPOutcfg	 =
	0,// eGPInput
	0,// eIntDir
	0,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22

};


const am_hal_gpio_pincfg_t g_AM_PIN_ADC =
{
    .uFuncSel       = AM_HAL_PIN_16_ADCSE0,
};


void *system_heap_malloc( size_t size )
{
        void *ret = NULL;
        ret = malloc(size);
        return ret ;
}
void system_heap_free( void *ptr )
{
        free(ptr);
}



void idx_reset_heap(void)
{
	dedicated_heap_pointer=dedicated_heap_start_address;
	shared_heap_ick_pointer=shared_heap_start_address;
	shared_heap_emk_pointer=shared_heap_start_address;

}

void *demo_dedicated_malloc(size_t size)
{
	//printf("\nallocating %u bytes in dedicated memory, address: 0x%x \n", size, dedicated_heap_pointer);
	if (size % MEM_ALIGNMENT_UNIT)
	{
		//printf("***Alligning memory to %u byte boundary...\n", MEM_ALIGNMENT_UNIT);
		size = (size & MEM_ALIGNMENT_MASK) + MEM_ALIGNMENT_UNIT;
		//printf("***Alligned allocation size: %u.\n", size);
	}
	void *ret = NULL;
	uint32_t used_dedicated_heap = (uint32_t)(dedicated_heap_pointer - dedicated_heap_start_address);
	uint32_t available_dedicated_heap = (dedicated_heap_size - used_dedicated_heap);
	//printf("***used_dedicated_heap = %u \n", used_dedicated_heap);
	//printf("***available_dedicated_heap = %u \n", available_dedicated_heap);
	//printf("***requested_heap = %u \n", size);
	if (size <= available_dedicated_heap)
	{
		ret = dedicated_heap_pointer;
		dedicated_heap_pointer += size;
	}
	else
	{
		printf("Unable to allocate requested memory (%u) in dedicated heap! \n", (unsigned int)size);
        printf( "***requested_heap(%u bytes) > available_dedicated_heap(%u bytes) \n", (unsigned int)size, (unsigned int)available_dedicated_heap );
	}
	//printf("new_address : 0x%x , returned_address : 0x%x\n", dedicated_heap_pointer, ret);
	return ret;
}

void demo_dedicated_free(void *ptr)
{
	if (ptr)
	{
		//uint32_t deallocated_heap = (dedicated_heap_pointer - ((unsigned char*)ptr));
		//printf("\ndeallocating %u bytes from dedicated memory, previous_address: 0x%x, new_address: 0x%x \n", deallocated_heap, dedicated_heap_pointer, ptr);
		dedicated_heap_pointer = (unsigned char*) ptr;
		//uint32_t used_dedicated_heap = (dedicated_heap_pointer - dedicated_heap_start_address);
		//uint32_t available_dedicated_heap = (dedicated_heap_size - used_dedicated_heap);
		//printf("***used_dedicated_heap (after deallocation) = %u \n", used_dedicated_heap);
		//printf("***available_dedicated_heap (after deallocation) = %u \n", available_dedicated_heap);
		//printf("***deallocated_heap = %u \n", deallocated_heap);
	}
}

void *demo_shared_ick_malloc(size_t size)
{
	//printf("\nallocating %u bytes in shared memory (ICK), address: 0x%x \n", size, shared_heap_ick_pointer);
	if (size % MEM_ALIGNMENT_UNIT)
	{
		//printf("***Alligning memory to %u byte boundary...\n", MEM_ALIGNMENT_UNIT);
		size = (size & MEM_ALIGNMENT_MASK) + MEM_ALIGNMENT_UNIT;
		//printf("***Alligned allocation size: %u.\n", size);
	}
	void *ret = NULL;
	uint32_t used_ick_shared_heap = (uint32_t)(shared_heap_ick_pointer - shared_heap_start_address);
	uint32_t available_ick_shared_heap = (shared_heap_size - used_ick_shared_heap);
	//printf("***used_ick_shared_heap = %u \n", used_ick_shared_heap);
	//printf("***available_ick_shared_heap = %u \n", available_ick_shared_heap);
	//printf("***requested_heap = %u \n", size);
	if (size <= available_ick_shared_heap)
	{
		ret = shared_heap_ick_pointer;
		shared_heap_ick_pointer += size;
	}
	else
	{
        printf( "Unable to allocate requested memory (%u) for ick in shared heap! \n", (unsigned int)size );
        printf( "***requested_heap(%u bytes) > available_ick_shared_heap(%u bytes) \n", (unsigned int)size, (unsigned int)available_ick_shared_heap );
	}
	//printf("new_address : 0x%x , returned_address : 0x%x\n", shared_heap_ick_pointer, ret);
	return ret;
}

void demo_shared_ick_free(void *ptr)
{
	if (ptr)
	{
		//uint32_t deallocated_heap = (shared_heap_ick_pointer - ((unsigned char*)ptr));
		//printf("\ndeallocating %u bytes from shared memory (ICK), previous_address: 0x%x, new_address: 0x%x \n", deallocated_heap, shared_heap_ick_pointer, ptr);
		shared_heap_ick_pointer = (unsigned char*) ptr;
		//uint32_t used_ick_shared_heap = (shared_heap_ick_pointer - shared_heap_start_address);
		//uint32_t available_ick_shared_heap = (shared_heap_size - used_ick_shared_heap);
		//printf("***used_ick_shared_heap (after deallocation) = %u \n", used_ick_shared_heap);
		//printf("***available_ick_shared_heap (after deallocation) = %u \n", available_ick_shared_heap);
		//printf("***deallocated_heap = %u \n", deallocated_heap);
	}
}

void *demo_shared_emk_malloc(size_t size)
{
	//printf("\nallocating %u bytes in shared memory (EMK), address: 0x%x \n", size, shared_heap_emk_pointer);
	if (size % MEM_ALIGNMENT_UNIT)
	{
		//printf("***Alligning memory to %u byte boundary...\n", MEM_ALIGNMENT_UNIT);
		size = (size & MEM_ALIGNMENT_MASK) + MEM_ALIGNMENT_UNIT;
		//printf("***Alligned allocation size: %u.\n", size);
	}
	void *ret = NULL;
	uint32_t used_emk_shared_heap = (uint32_t)(shared_heap_emk_pointer - shared_heap_start_address);
	uint32_t available_emk_shared_heap = (shared_heap_size - used_emk_shared_heap);
	//printf("***used_emk_shared_heap = %u \n", used_emk_shared_heap);
	//printf("***available_emk_shared_heap = %u \n", available_emk_shared_heap);
	//printf("***requested_heap = %u \n", size);
	if (size <= available_emk_shared_heap)
	{
		ret = shared_heap_emk_pointer;
		shared_heap_emk_pointer += size;
	}
	else
	{
        printf( "Unable to allocate requested memory (%u) for emk in shared heap! \n", (unsigned int)size );
        printf( "***requested_heap(%u bytes) > available_emk_shared_heap(%u bytes) \n", (unsigned int)size, (unsigned int)available_emk_shared_heap );
	}
	//printf("new_address : 0x%x , returned_address : 0x%x\n", shared_heap_emk_pointer, ret);
	return ret;
}

void demo_shared_emk_free(void *ptr)
{
	if (ptr)
	{
		//uint32_t deallocated_heap = (shared_heap_emk_pointer - ((unsigned char*)ptr));
		//printf("\ndeallocating %u bytes from shared memory (EMK), previous_address: 0x%x, new_address: 0x%x \n", deallocated_heap, shared_heap_emk_pointer, ptr);
		shared_heap_emk_pointer = (unsigned char*) ptr;
		//uint32_t used_emk_shared_heap = (shared_heap_emk_pointer - shared_heap_start_address);
		//uint32_t available_emk_shared_heap = (shared_heap_size - used_emk_shared_heap);
		//printf("***used_emk_shared_heap (after deallocation) = %u \n", used_emk_shared_heap);
		//printf("***available_emk_shared_heap (after deallocation) = %u \n", available_emk_shared_heap);
		//printf("***deallocated_heap = %u \n", deallocated_heap);
	}
}




const SensorHeapCallbacks ick_heap_callbacks = { demo_dedicated_malloc, demo_shared_ick_malloc, demo_dedicated_free, demo_shared_ick_free };
const SensorHeapCallbacks emk_heap_callbacks = { demo_dedicated_malloc, demo_shared_emk_malloc, demo_dedicated_free, demo_shared_emk_free };




extern int idx_bmf_enroll(void);
extern int matching_process(void);
extern int idx_record_list(void);
extern int idx_data_delete(void);
extern void mem_test(void);
extern void sensor_initial_process(void);
extern void appolo3_spi_init( void );



void clear_idx_heap(void)
{
	shared_heap_ick_pointer = (uint8_t *)SharedHeap;
	shared_heap_emk_pointer = (uint8_t *)SharedHeap;
	dedicated_heap_pointer = (uint8_t *)DedicatedHeap;
}


void HardFault_Handler(void)
{
	printf( "int hf in.\r\n" );
	while(1);

}
void MemManage_Handler(void)
	{
	printf( "int mm in.\r\n" );
	while(1);

}
void	BusFault_Handler(void)
	{
	printf( "int bh in.\r\n" );
	while(1);

}
void	UsageFault_Handler(void)
	{
	printf( "int uf in.\r\n" );
	while(1);

}
void	SecureFault_Handler(void)
	{
	printf( "int sf in.\r\n" );
	while(1);

}
void	SysTick_Handler(void)
	{
	printf( "int st in.\r\n" );
	while(1);

}
void	am_watchdog_isr(void)
	{
		printf( "int wdt in.\r\n" );
		while(1);
	
	}

void am_gpio_isr()
{
	GPIO->INT0CLR|= 0xffffffff;
	GPIO->INT1CLR|= 0xffffffff;

	printf("int gpio\r\n");


}


void am_hal_flash_delay(uint32_t ui32Iterations)
{
    g_am_hal_flash.bootrom_delay_cycles(ui32Iterations);
} // am_hal_flash_delay()





void uart_ouput_data(unsigned char *str,unsigned int data_lenth)
{
	unsigned int n=0;
	uint8_t data;
	while(data_lenth>n)
		{
		data=str[n];
		while(( UART0->FR & (UART0_FR_BUSY_Msk)));
		UART0->DR=data;
		n++;
		}
}


void
am_hal_flash_store_ui32(uint32_t *pui32Address, uint32_t ui32Value)
{
#if (defined (__ARMCC_VERSION) || defined(__IAR_SYSTEMS_ICC__))
    uint32_t SRAM_write_ui32[12 / 4] =
    {
        //
        // A very simple, word-aligned function residing in SRAM (stack).  This
        // function writes a given memory location while executing outside of
        // flash. It then does a read back to ensure that the write completed.
        // Prototype:   uint32_t SRAM_write_ui32(ui32Addr, ui32Value);
        //
        0xBF006001,         // 6001   str    r1,[r0,#0]
                            // BF00   nop
        0xBF006800,         // 6800   ldr    r0,[r0,#0]
                            // BF00   nop
        0xBF004770          // 4770   bx lr
                            // BF00   nop
    };
#elif defined(__GNUC_STDC_INLINE__)
#else
#error Compiler is unknown, please contact Ambiq support team
#endif

    //
    // Call the simple routine that has been coded in SRAM.
    // First set up a function pointer to the array, being sure to set the
    //  .T bit (Thumb bit, bit0) in the branch address, then use that
    //  function ptr to call the SRAM function.
    //
    uint32_t SRAMCode = (uint32_t)SRAM_write_ui32 | 0x1;
    uint32_t (*pFunc)(uint32_t*, uint32_t) = (uint32_t (*)(uint32_t*, uint32_t))SRAMCode;
    (*pFunc)(pui32Address, ui32Value);

} // am_hal_flash_store_ui32()

uint32_t am_hal_flash_load_ui32(uint32_t *pui32Address)
{
    return g_am_hal_flash.flash_util_read_word(pui32Address);
} // am_hal_flash_load_ui32()


int stdout_putchar (int ch)
{
	uint8_t data=(uint8_t)(ch&0x00ff);
	while(( UART0->FR & (UART0_FR_BUSY_Msk)));
	UART0->DR=data;
	return ch;
}


int debug_putchar (int ch)
{
	uint8_t data=(uint8_t)(ch&0x00ff);
	while(( UART0->FR & (UART0_FR_BUSY_Msk)));
	UART0->DR=data;
	return ch;
}
int stderr_putchar( int ch)
{
	return ch;

}
int ttywrch (int ch)
{
	return ch;


}
int stdin_getchar (void)
{
int ch;
return ch;
}
void Sleep( uint32_t ms )
{
	uint32_t i=0;
	uint32_t j=0;
	uint32_t temp_data=0;
	temp_data=40*ms;
	for (j = 0; j < temp_data; j++)
	{
		for (i = 0; i < 200; i++)
			{
                        __asm__ ("nop");
						__asm__ ("nop");
			}
	}

}



void uSleep( uint32_t us )
{
	uint32_t j,temp_data;
	temp_data=(50*us*13)/64;
	for (j = 0; j < temp_data; j++)
	{
        __asm__ ("nop");
	}

}

//wtx_stage_two_callback
void Sleep_with_wtx( uint32_t ms)
{
	uint32_t i=0;
	uint32_t j=0;
	for (i = 0; i < ms; i++)
		{
		for (j = 0; j < 10; j++)
			{
			uSleep(100);
			wtx_stage_two_callback();
			}
		}
}


void uart0_uninitial(void)
{
	PWRCTRL->DEVPWREN&=~PWRCTRL_DEVPWREN_PWRUART0_Msk;
	while(PWRCTRL->DEVPWREN&PWRCTRL_DEVPWREN_PWRUART0_Msk);


	am_hal_gpio_pinconfig(AM_BSP_GPIO_COM_UART_TX, g_AM_HAL_GPIO_DISABLE);	
	am_hal_gpio_pinconfig(AM_BSP_GPIO_COM_UART_RX, g_AM_HAL_GPIO_DISABLE);

}




void uart0_initial(void)
{
	PWRCTRL->DEVPWREN|=PWRCTRL_DEVPWREN_PWRUART0_Msk;
	while(!(PWRCTRL->DEVPWREN&PWRCTRL_DEVPWREN_PWRUART0_Msk));
	UART0->IBRD=0x0000000d;
	UART0->FBRD=0x00000001;
	UART0->LCRH=0x00000070;
	UART0->IER=0x00000070;

	UART0->CR=0x00000319;
	am_hal_gpio_pinconfig(AM_BSP_GPIO_COM_UART_TX, g_AM_BSP_GPIO_COM_UART_TX);	
	am_hal_gpio_pinconfig(AM_BSP_GPIO_COM_UART_RX, g_AM_BSP_GPIO_COM_UART_RX);

}

void SystemInit(void)
{
	CLKGEN->CLKKEY         = CLKGEN_CLKKEY_CLKKEY_Key;
	CLKGEN->CCTRL          = CLKGEN_CCTRL_CORESEL_HFRC;
	CLKGEN->CLKKEY         = 0;

}



void apollo3_system_init(void)
{

	am_hal_cachectrl_config(&am_hal_cachectrl_defaults);
	am_hal_cachectrl_enable();

	CLKGEN->OCTRL |= CLKGEN_OCTRL_OSEL_Msk;//am_hal_rtc_osc_select(AM_HAL_RTC_OSC_LFRC);// Run the RTC off the LFRC.
	CLKGEN->OCTRL |=CLKGEN_OCTRL_STOPXT_Msk;//am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_XTAL_STOP, 0);// Stop the XTAL.
	RTC->RTCCTL|=RTC_RTCCTL_RSTOP_Msk;//	am_hal_rtc_osc_disable();// Disable the RTC.
	am_hal_gpio_pinconfig(RESET_pin,g_AM_HAL_GPIO_OUTPUT);
}

void enable_96mhz (void)
{

	if(MCUCTRL->SKU & MCUCTRL_SKU_ALLOWBURST_Msk )
		{
		MCUCTRL->FEATUREENABLE |=MCUCTRL_FEATUREENABLE_BURSTREQ_Msk;
		while(!(MCUCTRL->FEATUREENABLE &MCUCTRL_FEATUREENABLE_BURSTREQ_Msk));
		CLKGEN->CLKKEY =CLKGEN_CLKKEY_CLKKEY_Key;
		CLKGEN->FREQCTRL |=CLKGEN_FREQCTRL_BURSTREQ_Msk;
		while(!(CLKGEN->FREQCTRL &CLKGEN_FREQCTRL_BURSTSTATUS_Msk));
		}
	else
		{
		
		printf("Error to enable 96mhzMCUCTRL->SKU=%x\r\n",MCUCTRL->SKU);
		}
	
}
void disable_96mhz(void)
{
	CLKGEN->CLKKEY =CLKGEN_CLKKEY_CLKKEY_Key;
	CLKGEN->FREQCTRL &=~CLKGEN_FREQCTRL_BURSTREQ_Msk;
	while(CLKGEN->FREQCTRL &CLKGEN_FREQCTRL_BURSTSTATUS_Msk);
	MCUCTRL->FEATUREENABLE &=~MCUCTRL_FEATUREENABLE_BURSTREQ_Msk;

}




char input_string(void)
{

 while (UART0->FR & UART0_FR_BUSY_Msk);

 return  (char)UART0->DR;

}

unsigned char wait_cmd(uint8_t *s_buffer, unsigned char *s_count)
{
	char temp;

	if(0==( UART0->FR & (UART0_FR_RXFE_Msk)))
		{
		temp=input_string();
		if(13==temp)
			{
			//printf("\r\n");
			return 1;
			}
		else
			{
			if((32<temp)&&(127>temp))
				{
				s_buffer[*s_count]=temp;
				s_count[0]++;
				//print_data(&temp,1);
				}
			return 0;
			}
		}
	return 0;
}



void Vol_detect_ADC_init(void)
{
	//g_AM_PIN_ADC
	am_hal_gpio_pinconfig(Vol_detect_Pin,g_AM_PIN_ADC);
	PWRCTRL->DEVPWREN |=PWRCTRL_DEVPWREN_PWRADC_Msk;
	uSleep(2);
	ADC->CFG=0x01071010;
	ADC->SL0CFG=0x02070001;
	ADC->INTEN=0x00000001;
	ADC->INTCLR|=0x000000ff;
	ADC->CFG|=0x00000001;
	printf("adc init done\r\n");
}

int Vol_detect_process(void)
{
	uint32_t adc_data;
	int i;
	am_hal_gpio_output_clear(CS_pin);
	for(i=0;i<4;i++)
		{
		ADC->INTCLR|=0x000000ff;
		ADC->SWT=0x37;
		while(0x02!=(ADC->INTSTAT & 0x02));
		}
	adc_data=ADC->FIFO;
	//printf("adc convert done,data=%x\r\n",adc_data);
	adc_data=ADC->FIFOPR;
	am_hal_gpio_output_set(CS_pin);
	printf("prdata=%x\r\n",adc_data);



	return 0;
}


int mcu_system_init(void)
{

	NVIC_DisableIRQ(STIMER_CMPR0_IRQn);
	NVIC_DisableIRQ(IOSLAVE_IRQn);


	am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_SYSCLK_MAX, 0);
	am_hal_clkgen_control(AM_HAL_CLKGEN_CONTROL_LFRC_START, 0);
	am_hal_cachectrl_config(&am_hal_cachectrl_defaults);   
	am_hal_cachectrl_enable();
	uart0_initial();
	appolo3_spi_init();
	EPD_PORT_INITIAL();
	stimer_init();
	crd_ctimer_int();
	LED_port_initial();
	se_read_data_init();
	SE_SPI_PORT_INIT();
	Vol_detect_ADC_init();

	dataImage  = NULL;
	wtx_sensor_init_status=0;
	wtx_durition_counter=0;
	wtx_sensor_process_flag=0;
	ios_long_data_transfer_flag=0;
	se_hso_int_flag=0;
	wtx_sensor_interrupt_flag=0;
	clear_idx_heap();

	state.connection_mode=BIOMETRIC_MODE_CONTACTLESS;
	state.pCommData=&se_spi_common_buffer[6];
	state.pFlashStartAddress=INDEX_TABLE_ADDR;
	state.pTemplateStartAddress=TEMPLATE_START_ADDRESS;
	state.pCommHeader=&se_spi_common_buffer[0];
	state.pLoadImage_ob.flag=0;
	state.mcuState = BIOMETRIC_UNINITIALIZED;


	return 0;
}




/*************************** End of file ****************************/
