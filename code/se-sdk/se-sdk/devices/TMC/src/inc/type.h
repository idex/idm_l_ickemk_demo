/*
 * Copyright (c) 2010, Beijing Tongfang Microelectroics Co., Ltd.
 * All rights reserved.
 *
 * FileName: type.h
 * SCFID: 
 * Feature: type define
 * Version: V0.1
 *
 * History: 
 *   2014-05-07 by Liujy
 *     1. Original version 0.1
 */
#ifndef    __TYPE_H__
#define    __TYPE_H__
typedef struct{
	unsigned char* p;
	unsigned short length;
}BigNum_t;
typedef unsigned char	    u8;
typedef unsigned short      u16;
typedef signed char	        s8;
typedef signed short	    s16;
//typedef unsigned char       boolean;
//typedef	unsigned char       bool;

typedef unsigned long int   u32;
typedef signed   long int   s32;
typedef signed   long int   i32;

#endif
