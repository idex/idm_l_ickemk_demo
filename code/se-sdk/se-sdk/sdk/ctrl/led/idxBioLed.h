/** ***************************************************************************
 * \file   idxBioLed.h
 * \brief  header for LEDs control
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2019, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#ifndef _IDX_BIO_LED_H_
#define _IDX_BIO_LED_H_

#include <stdint.h>
#include "idxSeSdkTypes.h"


typedef enum {
  IDX_CARD_LED_START = 1,
  IDX_SLEEVE_LED_START,
  IDX_CARD_LED_NEXT_FINGER,
  IDX_SLEEVE_LED_NEXT_FINGER,
  IDX_CARD_LED_TOUCH_SUCCESS,
  IDX_SLEEVE_LED_NEXT_TOUCH,
  IDX_CARD_LED_TOUCH_FAIL,  
  IDX_CARD_LED_LAST_TOUCH,
  IDX_SLEEVE_LED_ENROLL_FINISHED,
  IDX_CARD_LED_STOP,
  IDX_SLEEVE_LED_STOP,
  IDX_CARD_LED_BLINK,
  IDX_MATCH_STATE_SUCCESS,
  IDX_MATCH_STATE_FAIL
} idxLedModeStateType;

/**
* @brief Function for controlling LEDs
*
* @param[in] state Enrollment state machine.
*
* @param[in]    pCommsBuffer Pointer to a structure giving the size and location
*                            of the communication buffer. Any command data is
*                            passed in this buffer (starting at
*                            pCommsBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET])
*                            and, if pRespDataBuf is NULL, any response data is
*                            returned in the same location.
*
* @return IDX_ISO_SUCCESS if successful,
*         or another error code (taken from wCode in reply header) if not successful
*/
uint16_t idxBioLedSetMode( idxLedModeStateType state, const idxBuffer_t *pCommsBuffer );

#endif /* _IDX_BIO_LED_H_ */
