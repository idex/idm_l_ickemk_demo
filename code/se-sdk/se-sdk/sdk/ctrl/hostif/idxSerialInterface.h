/** ***************************************************************************
 * \file   idxSerialInterface.h
 * \brief  Interface for communication with IDEX serial interface.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2012-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_SERIAL_INTERFACE_H_
#define _IDX_SERIAL_INTERFACE_H_

#include <stdint.h>
#include "idxIsoErrors.h"

#include "idxSeSdkTypes.h"

/*******************************************************************************
 * Endianess Macros 
 *
 * These macros read or write 16-bit values in little-endian order, they
 * are intended for accessing data that is communicated, over the host
 * interface, with the MCU - such data is generally in little-endian order.
 *
 * An optimization may be possible on a little-endian processor.
 * Rather than complicate the source code here, rely on the compiler
 * code-generator to deal with this.
 *
 * NOTE: RD_U16() evaluates its argument multiple times
 *       it must therefore not have side-effects.
 *       For example, RD_U16(p++) will not work as expected!
 ******************************************************************************/
#define RD_U16(p) ((((uint16_t) (((const uint8_t *)(p))[1])) <<  8) | \
                   ( (uint16_t) (((const uint8_t *)(p))[0])       )  )

#define WR_U16(p, d) do { uint8_t *q = ((uint8_t *)(p)); uint16_t v = (d); \
                          q[1] = ((v >>  8) & 0xff); \
                          q[0] = ( v        & 0xff); \
                        } while (0)

enum  {
  MODE_CONTACT      = 0x00,
  MODE_CONTACTLESS  = 0x01,
  MODE_SLEEVE       = (0x04 | MODE_CONTACT) ,      // 00000101b
  MODE_UNINITIALIZED= 0xff
};

/** \brief Initialize the serial interface module
 */
void idxSerialInterfaceInit(void);

/** \brief Set the connect mode
 *
 * Set the connect mode assumed by the serial interface module
 *
 * \param[in] newMode The mode to record
 */
void idxSetConnectionMode( uint8_t newMode );

/** \brief Get the connect mode
 *
 * \retun The connection mode
 */
uint8_t idxGetConnectionMode(void);

/** \brief Send a command and get its response.
 *
 * This function sends a command and gets its response. It is the interface
 * to the low-level protocol controller that conveys host interface
 * commands over a physical interface such as SPI, I2C, or UART. Any
 * data encryption and signing must have taken place before this function is
 * called, and any signature checking and decryption after its return.
 *
 * It is assumed that command data in the buffer is preceded by a header
 * formatted as follows:
 *
 *  * Unsigned 16-bit (little-endian) \a wSize.
 *  * Byte \a bOrdinal.
 *  * Byte \a bOptions.
 *  * Two unused bytes
 *
 * \a wSize will reflect the size of the data that follows the header plus
 * four - bOrdinal, bOptions, and wSize itself.
 *
 * The data in the buffer is modified to attain the required format for
 * transmission over the particular host interface being used.
 *
 * For example, in the case of transmission over SPI, \a wSize will be read from
 * its position in the buffer, incremented by two, and written into the first
 * two bytes of the buffer. \a bOptions is modified to set the least signficiant
 * bit. The fifth and sixth bytes will be cleared, and a CRC calculated over the
 * whole command. \a wSize will incremented by a further two, a CRC is
 * calculated over the first four bytes of the buffer and written into the fifth
 * and sixth bytes.
 *
 * For physical interfaces where WTX signalling cannot interrupt communication,
 * the driver then determines whether it has sufficient time to send the command
 * over the host interface before the next WTX, waiting if necessary for WTX
 * signaling to take place before it starts to send the data.
 *
 * On return, any data in the buffer will be preceded by a buffer formatted as
 * follows:
 *
 *  * Unsigned 16-bit (little-endian) \a wSize.
 *  * Unsigned 16-bit (little-endian) \a wCode.
 *  * Two unused bytes
 *
 * \a wSize will reflect the size of the data that follows the header plus
 * four - wCode and wSize itself.
 *
 * \param[in] pCommsBuffer Pointer to a structure giving the size and location
 *                         of the communication buffer.
 * \return                 \ref IDEX_SUCCESS normally, any other value
 *                         indicating an error.
 */
uint16_t idxBioDrvComms(const idxBuffer_t *pCommsBuffer);

/** \brief Process a WTX
 *
 * Implements the interactions with the IDEX biometric module
 * that are necessary for WTX integration. This function should
 * be called from the WTX timer interrupt service routine (which
 * should no longer directly send the WTX request). If necessary,
 * the biometric module will be transitioned to a low power
 * state. idxSendWTX() will be called to actually send the WTX
 * request.
 */
void idxProcessWTX(void);

/** \brief Send a WTX now
 *
 * This is not implemented in IDEX code, but is assumed to be provided
 * by the card O/S to allow integration of the biometric module with
 * the WTX timer interrupt service routine.
 */
void idxSendWTX(void);

#endif // _IDX_SERIAL_INTERFACE_H_
