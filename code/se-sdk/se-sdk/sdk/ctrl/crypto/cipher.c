/******************************************************************************
* \file  cipher.c
* \brief code cipher
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/
#include <stdint.h>
#include <stdlib.h>

//#define WINDOWS

#define UNUSED(p) if(p){}
  
#include "idxHalAlgo.h"
#include "cipher.h"

/******************************************************************************
 *
 * size = multiple of 16 
 */
uint8_t *encryptCBC(uint8_t *K, uint8_t *in, uint8_t *iv, uint16_t size){
  uint16_t i,ret;
  UNUSED(ret);
  for(i = 0 ; i < (size >> 4) ; i++){
    ret = idxHalAlgoAES128encrypt(in, K, iv, in);              
    iv = in;
    in  += 16;
  }
  return iv;  
}
void encryptCMAC(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint16_t size){
  uint16_t i,ret;
  UNUSED(ret);
  for(i = 0 ; i < (size >> 4) ; i++){
    ret = idxHalAlgoAES128encrypt(in, K, iv, out);       
    iv  = out;
    in  += 16;
  }
}

void decryptCBC(uint8_t *out, uint8_t *K, uint8_t *in, uint8_t *iv, uint16_t size){
  uint16_t i,ret;
  uint8_t *tmp_in = in + size - 16;
  uint8_t *tmp_out = out + size - 16;
  uint8_t *tmp_iv = tmp_in - 16;
  for(i = 0 ; i < (size >> 4) - 1 ; i++){
    ret = idxHalAlgoAES128decrypt(tmp_in, K, tmp_iv, tmp_out);
    tmp_in = tmp_iv;
    tmp_iv -= 16;
    tmp_out -= 16;
  }

  ret = idxHalAlgoAES128decrypt(tmp_in, K, iv, tmp_out);  

  UNUSED(ret);
}



