/******************************************************************************
* \file  idxIckDefines.h
* \brief Defines for the matcher library
* \author IDEX ASA
* \version 0.0.0
*******************************************************************************
* Copyright 2013-2017 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*  
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __IDX_ICK_DEFINES_H__
#define __IDX_ICK_DEFINES_H__

#include "idxBaseDefines.h"

//////////////////////////////////////////////////////
//
// PREPIMG_* - set of scan flags - for idxIckPrepareImage(...)
//
typedef enum
{
	PREPIMG_ERROR_FLAG = -1,     //-1 = Error Flag
	PREPIMG_LIVE,				// 0 = scan and returns whatever the image 
	PREPIMG_FINGERDETECT,		// 1 = wait for a finger then return the image
	PREPIMG_NEWFINGERDETECT,	// 2 = wait for finger off, then finger on and then return image
	PREPIMG_FINGEROFFDETECT		// 3 = wait for finger off and then return image
}PREP_IMAG_OPTIONS;

enum IDEX_PARAMETERS
{
	IDEX_SENSOR_TYPE,             //The type of sensor
	IMAGE_X,                     //Size of image on X axis
	IMAGE_Y,                     //Size of image on Y axis
	BIT_DEPTH,                  //Number of bits per pixel
	SENSOR_DPI,                 //DPI of the image
	LAST_SCANNED_IMAGE_COVERAGE,// The coverage parameters of the last scanned image
	GETIMAGEPROCESSING_FLAGS,   //Retrieving image processing flags.
	SENSOR_SETTLING_TIME,           // Delay before scanning after finger is detected
	LAST_SCANNED_IMAGE_FP_STATUS,   // Finger presence status
	LAST_SCANNED_IMAGE_FP_COVERAGE,  // coverage of the finger on the sensor
	MIN_ENROLL_PERCENT_COVERED, // minimum coverage threshold for enrollment
	FW_FAMILYID, // FW  Family ID (G1FW - 1 , Scanex - 2, G3FW-primary - 3, G3FW-utility - 203, FSBL - 160)
	SUBSTRATE_ID, // Sensor substrate id.
	SILICON_ID, // Silicon id.
	MANUFACTURER_ID, // Manufacturer id.
	WTX_MODE, // 0 - disable (default), 1 enable 
	SENSOR_SETTLING_TIME_CONTACT, // Delay before scanning after finger is detected (finger exist on first poll, contact mode)
	SENSOR_SETTLING_TIME_CONTACTLESS, // Delay before scanning after finger is detected (finger exist on first poll, contactless mode)
	CHECK_WATERMARK, // 0 - disable, 1 enable (default)
};

//////////////////////////////////////////////////////
//
// FWIMG_* - set of firmare types - for idxIckLoadFirmware(...)
//
#define FW_DEST_OTP ((uint32_t)(-1))

typedef enum
{
	FW_IMG_SSBL_APP    = 0x20,	
	FW_IMG_PRIMARY_APP = 0x30,	
	FW_IMG_UTILITY_APP = 0x31,	
	FW_IMG_AWOF_APP    = 0x33,
} FWIMG_TYPE;

#endif //__IDX_ICK_DEFINES_H__
