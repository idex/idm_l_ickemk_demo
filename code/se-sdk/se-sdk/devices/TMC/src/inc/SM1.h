#ifndef	__SM1_H__
#define	__SM1_H__

//SM1加解密运算函数
/************************************************************/
/** 函数说明                                               **/
/** 名称：SM1_Crypto_sw解密模块调用函数            		   **/
/** 参数说明：											   **/
/** EK：基本密钥首地址                                     **/
/** input:密文数据首地址                                   **/
/** output:明文数据首地址                                  **/
/** encrypt:   1：加密	0：解密							   **/
/** AK：辅助密钥首地址,使用默认值则输入0                   **/
/** SK：系统密钥首地址,使用默认值则输入0                   **/
/** CryptRound:分组算法的轮数,使用默认值（8轮）则输入0     **/
/** 参数返回值：                                           **/
/** 0：函数调用成功                                        **/
/** 2：圈数不符要求                                        **/
/************************************************************/          
typedef unsigned char (*pSM1_Crypto)(unsigned char *EK, unsigned char *input, unsigned char *output, unsigned char encrypt, unsigned char *AK, unsigned char *SK, unsigned char CryptRound);
#define CryptoSM1_ROMADDR			0x0400d800
#define SM1_Crypto_sw			((pSM1_Crypto)(*(unsigned long*)(CryptoSM1_ROMADDR + 0)))
#endif
