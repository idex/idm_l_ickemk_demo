/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: Nvm.h
 * * SCFID: 
 * * Feature: function list for irahandler
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-11-18
 * *     1. Original version 0.1
 * */
#ifndef	__NVM_H__
#define	__NVM_H__

u8 NvmEraseCheck( u8 *pbSrc, u16 usLen );
u8 NvmProgramWords(u8 *pbDest, u8 *pbSrc, u16 usLen);
u8 NvmFastEraseSector(u8 *pbDest);
u8 NvmUpdateWords(u8 *pbDest, u8 *pbSrc, u16 usLen);
u8 NvmUpdateBytes(u8 *pbDest, u8 *pbSrc, u16 usLen);
void NvmHalErasePage(u8 *dest);
void NvmBufferWrite(u32 ulDstAddr, u32 ulSrcAddr, u32 ulLen, u32 ulSetNVMCON);
u32 NvmHalReadStrict(u32 wSrc, u8 bMode);
u8 NvmReadTolerant(u8 *ulDest, u32 *ulSrc, u32 ulByteLen);
u8 NvmUpdateWords(u8 *pbDest, u8 *pbSrc, u16 usLen);
u8 NvmUpdate(u8 *pbDest, u8 *pbSrc, u16 usLen);	
u32 nvmLowPowerConfigBegin(void);
void nvmLowPowerConfigEnd(u32 config);
#endif
