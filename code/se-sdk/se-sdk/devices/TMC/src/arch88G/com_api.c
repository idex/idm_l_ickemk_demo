/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: com_api.c
 * * SCFID: 
 * * Feature: api of communication
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-02-19 by zhoucc
 * *     1. Original version 0.1
 * */
//head file: include the declaration of the function, variables used in this file
#include "halglobal.h"
#include "ct.h"
#include "RF.h"
#include "CL4.h"
#include "CTCloseClassC.h"
#include "idxHalSystem.h"

extern stCL_4 gCL;						  
extern const u32 transactionStart;
ST_COMCMD g_stCMD;
u32 g_dwSNAddr;

// Global Variables to track test progress
u32 g_labelBP; //BP
u32 g_MSP; //MSP backup

//jump from interrupt service program to the back point of the main program
__asm void rtnBackup(void)
{
	IMPORT g_labelBP
	IMPORT g_MSP
	LDR r0, =g_MSP
	LDR r1, [r0]		 ;r1=backup pc addr
	SUBS r1,r1,#0x20 	
	msr	MSP,r1 
	LDR r0, =g_labelBP
	LDR r1, [r0]		 ;r1=backup pc addr
	mrs r2,MSP
	LDR r3,=0x18
	add r3,r2			 ;msp+18, pc
	STR r1,[r3]
	LDR r3,=0x1c
	add r3,r2			 ;msp+1c, xPSR
	LDR r1, =0x01000000	 
	STR r1,[r3]			 ;xPSR=0x01000000, Thumb state bit = 1
	ldr r2,=0xfffffff9	 ;return to thread mode
	bx r2
}

//backup PC value
__asm void BackupPC(void)
{
	IMPORT g_labelBP
	IMPORT g_MSP
	LDR  r0, =g_labelBP
	PUSH {lr}
	LDR  r1, [sp, #0x0]
	STR	 r1, [r0]
	LDR  r0, =g_MSP
	mrs  r1, msp
	adds r1, #0x04
	STR	 r1, [r0]
	pop {pc} 
}

//set work clock of CPU and PKE 

void InitHardware(void)
{
	g_stCT.event = 0;
	g_stCurRFEnv.event = 0;
	IDLECON0 = (1<<6) | (1<<5) | (1<<3) | (0 << 2) | (1<<0);// 0xff;
	/*SCB->SCR = 0:
	*	1. do not sleep when returning from Handler mode to Thread mode
	*	2. the processor uses sleep as its low power mode
	*	3. only enabled interrupts or events can wakeup the processor, disabled interrupts are 
	*excluded
	*/
	SCB->SCR = 0;

#ifdef DEBUG
	CLKIOCON |= 0x20;				//RF CLK enable for interface detect
	INTIOCON |= (1<<5);					//enable rf interrupt
	NVIC_ClearPendingIRQ(RF_IRQn);
	NVIC_EnableIRQ(RF_IRQn);
#endif

	//detect 7816 interface
	if (((IOSTS0 & 0x10)) && (g_pbComCfg[PROTOCOL_SUPPORT]&T0SUPPORT))	//(IOSTS0 & 0x10) == ISO7816POWERDET
	{
		CTinit();
		NVIC_ClearPendingIRQ(CT_IRQn);
		NVIC_EnableIRQ(CT_IRQn);
	}
	else   //7816 no power on, wake up circuit to use RF clock
	{	
		RFinit();	//check RF interface
		INTIOCON |= (1<<5);					//enable rf interrupt
		NVIC_ClearPendingIRQ(RF_IRQn);
		NVIC_EnableIRQ(RF_IRQn);
	}


	CLKMEMCON |= 0x08; //turn on tht clock of RAM1 								  
}

void ioInit(u8 *pbCL4Buf, u8 *pbCTBuf)
{
	u32 wTimer;
	wTimer = RamLoadWord(g_pbComCfg+CT_CWT);
	g_dwSNAddr = B2LT((u8 *)(g_pbComCfg+CL3A_UID));
	
	InitHardware();	//hardware initialise
	CTSoftInit(pbCTBuf);
	CL4_Init(pbCL4Buf, (u8 *)RF_BUF);
	CTINSTimerInit(wTimer);
}

void IdleMode(void)
{
	u32 bkINTIOCon;
	if(!(IOSTS0&0x10))
	{
		RFIdleMode(g_pbComCfg[CPU_RF_CLK]);
	}
	else
	{
//	#ifndef DEBUG
//	    if(0x6d == (IDLECON0 & 0x6d))		//SPI, I2C, gpio, 7816, rf interface all sleep
//	#endif
		{
			bkINTIOCon = INTIOCON;
			INTIOCON = 0;  
			IRQMSK = 1;
			//back up security settings
			INTIOCON = bkINTIOCon;			
			__WFI();
		}
	}	
}

static u8 delay  = 10;
static u8 sleeve = 1;
CMDTYPE g_cmd = NOCMD;

u8 ioRxCmdHead(u8 *pbBuf)
{
	u8 rtn = 0;

#if 0   // GM: commented out for sleeve work  
	if((!g_stCT.event) && (!g_stCurRFEnv.event))	//neither RFevent nor CTevent
	{
		IdleMode();
	}
#endif
	if(g_stCurRFEnv.event)
	{
    delay = 0;
    sleeve = 0;
		rtn = RFRxCmdHeader();
		g_stCMD.bPipeNum = RFCMD;
		
		if(rtn)
		{
			g_stCMD.pbBuf = CL4_GetFrameBuffer();
			//start WTX
			StartWTXTimer();
		}
    g_cmd = RFCMD;
	}
	else if( g_stCT.event )
	{
    sleeve = 0;
		rtn = CTRxCmdHeader();
		g_stCMD.bPipeNum = CTCMD;
		g_stCMD.pbBuf = CT_GetFrameBuffer();
        g_cmd = CTCMD;
	}
	else if(delay){
      volatile unsigned long cnt = 2500;
      while(cnt--);
      delay--;
  }
  else if(sleeve){
    volatile unsigned long cnt2 = 100000; // Compensate MCU start delay
    while(cnt2--);
    if(IOSTS0 & (1 << 4))      // powered by ISO7816 PWR pin
    { 
      //if(!(RFPOWSTA & 0x01)) 
      //if(RFPOWSTA & 0x02)  
      { // ISO14443 power  
        void sleeveIdmEnrol(const idxBuffer_t *pCommsBuffer);
		idxBuffer_t buffer;
		buffer.pBuffer = g_stCMD.pbBuf;
		buffer.bufferSize = APDU_BUFFER_SIZE;
		g_cmd = BATCMD;
        sleeveIdmEnrol(&buffer);

        idxHalSystemSetPowerState(idxPowerStateLowest);
      }
    }
    sleeve = 0;
    IdleMode();
  }
  else{
    IdleMode();
  }
  
	g_stCMD.wOutLen = 0; //ACK length return 0
	if(rtn)
	{
		g_stCMD.bFlagIn = 0;
		g_stCMD.header[0] = g_stCMD.pbBuf[0];
		g_stCMD.header[1] = g_stCMD.pbBuf[1];
		g_stCMD.header[2] = g_stCMD.pbBuf[2];
		g_stCMD.header[3] = g_stCMD.pbBuf[3];
		g_stCMD.header[4] = g_stCMD.pbBuf[4];
		if(pbBuf)
		{
			pbBuf[0] = g_stCMD.pbBuf[0];
			pbBuf[1] = g_stCMD.pbBuf[1];
			pbBuf[2] = g_stCMD.pbBuf[2];
			pbBuf[3] = g_stCMD.pbBuf[3];
			pbBuf[4] = g_stCMD.pbBuf[4];
		}
	}

	return rtn;
}

void ioTxResponse (void)
{
	if( CTCMD == g_stCMD.bPipeNum )	
	{
		CTTxResponse(&g_stCMD);
	}
	else
	{
		StopTimer0();
		RFTxResponse(&g_stCMD);
	}
}

u16 ioRxCmdBody(u8 *pbBuf)
{
	u16 usLen;
	g_stCMD.bFlagIn = 1;
	if (g_stCMD.bPipeNum == RFCMD)
	{
		if((gCL.wCL4_FrameLen>5) && pbBuf)
		{
			_HalRamCopy(pbBuf, g_stCMD.pbBuf, gCL.wCL4_FrameLen);
		}
		usLen = gCL.wCL4_FrameLen;
	}
	else
	{
		CTRxCmdBody();
		usLen = LC+5;
		if(LC && pbBuf)
		{
			_HalRamCopy(pbBuf, g_stCMD.pbBuf, usLen);
		}
	}

	return usLen;
}

void _halInitAllSecurity(void)
{
	INTSCCON = 0xffffffff;
	NVIC_ClearPendingIRQ(SECURITY_IRQn);
	NVIC_EnableIRQ(SECURITY_IRQn);
}

/*
* Function    :  ioSendData
* Description :instruction: put the ACK data into sending buffer
*     (according to the sequence of calling the function, put the data into sending buffer in turn)
* Input : -
*    [IN] pbDat:  ACK data buffer
*    [IN] len  �� ACK data length 
* Output : -
* Return : -
**/
void ioSendData(u8 *pbDat, u16 uslen)
{
	_HalRamCopy(g_stCMD.pbBuf + g_stCMD.wOutLen, pbDat, uslen);
	g_stCMD.wOutLen += uslen;
}
