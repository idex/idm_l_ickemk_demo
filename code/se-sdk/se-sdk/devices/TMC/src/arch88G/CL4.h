/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: com_api.c
 * * SCFID: 
 * * Feature: api of communication
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-11-18
 * *     1. Original version 0.1
 * */
#ifndef CL4_H
#define CL4_H

#define SetCL4CID(n) CL4_SetCID(n)
#define SetCL4MaxFrameSize(n) CL4_SetMaxFrameSize(n)
#define CL4_GetFrameBuffer() (gCL.pbCL4_FrameBuffer)

//the result of analyze the receiving RF data
typedef enum 
{
	RCV_NOPROCESING	= 0x00,
	RCV_SEND_NEXTBLCOK,
	RCV_SEND_ACK,	
	RCV_RCV1FRAME,
	RCV_REPEAT,
	RCV_DESELECT,
	RCV_RESPONSECMD,
	RCV_INBUF_OVERFLOW	= -1
}CL_RCV_STATE;

typedef enum 
{
	CL4_IDLE = 0x00,
	CL4_TX_ING,
	CL4_TX_FIN,
	CL4_RX_ING,
	CL4_RX_FIN
}CL4_STATE;

typedef struct
{
	u8 bNeedCID;				//need CID or not��0:no��1:need��decide by the receiving data PCB
	u8 bCID;
	u16 usMaxFrameSize;		//maximum frame length
	u16 wCL4_FrameLen;		//length of the receiving data, or the total length of the sending data
	u8 *pbCL4_FrameBuffer;		//receiveing/sending data in application layer

	u8 *pbCurPtr;				//currecting point,receive:top of the receiving data,send:current send data
		
	u8 *pbCL4_WinBottom;
	u8 *pbCL4_WinTop;
	
	u8   bRFBlockMaxLen;		//the maximum frame of RF
	u8 * pbRFBlockBuffer;		//the address of buffer which is the RF send and receive

	CL4_STATE bCL4_State;		//the valid infomation,when received,need to managed by upper layer
	
	u8 CurBlockNumber;			//current block num:0,1
	u8 bPCB;					//PCB when send last time,used to resend
	u16 RemainedLen;
}stCL_4;
extern stCL_4 gCL;

void CL4_Init(u8 *pbCL4Buf, u8 *pbRFBuf);
void CL4_Reset(void);

/*
* Function    : CL4_SendData 
* Description : send data this function is to send out the content in the sending buffer  
* input :
* 	wSendLength : the data length this time send out 
* 	wRemainedLen: the data length next time send out
* output : -
* return : -
*/
void CL4_SendData(u16 wSendLength, u16 wRemainedLen);

/*
* Function    : CL4_ContinueSend 
* Description : obatin data of next section to be sent
* input : -
* output : -
* return : -
*/
void CL4_ContinueSend(void);

/*
* Function    : CL4_SendACK 
* Description : send ACK
* inout  : -
* output : -
* return : - 
*/
void CL4_SendACK(void);

/*
* Function    : CL4_SendRepeat 
* Description : resend the last frame
* Input  : -
* Output : -
* Return : -
*/
void CL4_SendRepeat(void);

/*
* Function    : CL4_RcvData 
* Description : receive and analyze data in active state
* Input  : -
* Output : -
* Return : -
*/
CL_RCV_STATE CL4_RcvData(void);

/*
* Function    : CL4_Receive 
* Description : circle detection receive RF data
* Input  : -
* Output : -
* Return : -
*/
u8 CL4_Receive(void);
void CL4_SetCID(u8 bcid);
void CL4_SetMaxFrameSize(u16 usMaxFrameSize);

#endif
