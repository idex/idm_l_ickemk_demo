/*******************************************************************************
* File Name: IDEXWarnings.h
*
*  Description:
*   master include for all warnings for all IDEX software
*
*  Revision History:
*

**************************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __IDX_WARNINGS_H__
#define __IDX_WARNINGS_H__

#ifdef WIN32

// disable warning C4820: 'N' bytes padding added after data member 'name'
#pragma warning( disable : 4820 )

// disable warning C4668 : 'THING' is not defined as a preprocessor macro, replacing with '0' for '#if/#elif'
#pragma warning( disable : 4668 )

// disable warning C4127: conditional expression is constant
#pragma warning( disable : 4127 )

// disable warning C4100: unreferenced formal parameter
#pragma warning( disable : 4100 )

// disable warning C4244: conversion from 'type1' to 'type2', possible loss of data
//#pragma warning( disable : 4244 ) //// needswork - find a way to suppress only W4 4244 and allow W2/W1 4244 - temporary - too much output for now 

#endif // WIN32


#endif // __IDX_WARNINGS_H__
