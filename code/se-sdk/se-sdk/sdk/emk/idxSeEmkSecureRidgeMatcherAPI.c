/******************************************************************************
* \file  idxSeEmkAPI.c
* \brief code for SE Enroll/Match Kit API fro Secure Ridge Matcher
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2018 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <string.h>
#include "stdlib.h"

#include "idxSeEmkDefines.h"
#if __C251__
// Shut up warning in "IDEX_M_Interface.h"
#define _MSC_VER 0
#endif
#include "IDEX_M_Interface.h"

#include "idxSeEmkAPI.h"

#include "bio-ctrl.h" // added temporarily for idxMcuComms (), then it needs to be replaced with another header.
#include "idxBioLed.h"

#include "idxHalFlash.h"
#include "idxHalUart.h"
#include "idxHalTimer.h"
#include "idxAppDefines.h"
#include "idxBaseDefines.h"
#include "idxHalSystem.h"


#define UNUSED(p) if(p){}
  
#define SE_EMK_IDEX_SECURE_MAX_TOUCHES      (8)
#define SE_EMK_IDEX_SECURE_MAX_FINGERS      (2)
#define SE_EMK_IDEX_SECURE_MIN_RAM_KB      (12)
#define SE_EMK_IDEX_SECURE_MIN_FLASH_KB   (128)
#define SE_EMK_IDEX_SECURE_FLASH_OFFSET   (512)

#define BAD_IMAGE_NUM_TRIES ( 3 )
#define COVERAGE_THRESHOLD ( 50 )  


uint8_t errPoint;
//###############################  Local Function Declarations    ###############################

static uint16_t ClearFlash(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                           const idxBuffer_t *pCommsBuffer,
                           uint8_t *pTemplateFlash,
                           uint32_t szTemplateFlash);

static uint16_t Extract(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                        const idxBuffer_t *pCommsBuffer);

static uint16_t Merge(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                      const idxBuffer_t *pCommsBuffer,
                      uint8_t* pHandle);

static uint16_t Match(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                      const idxBuffer_t *pCommsBuffer,
                      uint8_t *pTemplateFlash);

//###############################   API Functions   ###############################

// Command and Response buffers with provision for Encrypt and Signature

uint16_t idxSeEmkInit()
{
  errPoint = 0xff;

  return IDX_ISO_SUCCESS;
}
/*******************************************************************
 * Get EMK version : 
 * Return info about Matcher on SE side
 */
void idxSeEmkGetUnitVersion(idx_UnitFuzeVersion_t* pEmkVersion)
{
    // fill 32 bytes of idx_UnitFuzeVersion_t 
  IDEX_ccMatcherInfoS1 *mtchInfo = IDEX_GetFP_SEccMatcherInfo_S1();
  pEmkVersion->version   = mtchInfo->matcherVersion;
  pEmkVersion->revision  = mtchInfo->matcherRevision;
  pEmkVersion->build     = mtchInfo->matcherBuildNumber;
  memcpy((char *)pEmkVersion->module_prefix_name, (char *)mtchInfo->FuzeID, 8); // IDEXMTCH
  strcpy((char*)pEmkVersion->module_fuze_id, (char *)&mtchInfo->FuzeID[8]);     // yymmdd-ttt-sssss
  pEmkVersion->version_schema = VERSION_SCHEMA_FUZE;     
}

/*******************************************************************
 * Single Touch  Enrol : 
 * This is used by SVT for testing synthetic images and image data bases
 */
uint16_t idxSeEmkSingleEnroll( idx_SeEmkConfig_t *pConfig,
                               uint8_t touch,
                               uint8_t finger)
{
  uint16_t rcode;
  uint8_t handle;
  uint8_t entry;
  IDEX_ccSE_MatcherDataS1 ccSE_Data;
  idex_FlashTemplateEntry_t flashTemplateEntries[SE_EMK_IDEX_SECURE_MAX_TOUCHES*SE_EMK_IDEX_SECURE_MAX_FINGERS];

  //      ----    Setup Paramters for IDEX.M      ----
  ccSE_Data.SE_StoredTemplateFlashBasePtr          = pConfig->pTemplateFlashPtr + SE_EMK_IDEX_SECURE_FLASH_OFFSET; // point to stored flash templates
  ccSE_Data.externalAllocatedMatcherBaseMemPtr     = pConfig->pWorkAreas[0].pointer;      // point to RAM allocated for IDEX Matcher
  ccSE_Data.externalAllocatedMatcherBaseDATAMemPtr = pConfig->pWorkAreas[1].pointer;   // point to data RAM1  
  ccSE_Data.targetSecurityLevelMatch               = pConfig->securityLevel;       // target securityLevel
  ccSE_Data.abortProcess                           = false;          // aways false

  ccSE_Data.enrollMatchFingerInfo.enrollMaxTouchCount  = pConfig->maxTouchesPerFinger; // number of touches per enroll
  ccSE_Data.enrollMatchFingerInfo.enrollMaxFingerCount = pConfig->maxEnrolledFingers;  // number of enrolled fingers
  ccSE_Data.EnrollOrMatch                              = kDataForEnrollS1;    // specify this is an enroll

  ccSE_Data.ProcessStage = kProcessStageEnrollS1;              //  prepare for enroll
  ccSE_Data.enrollMatchFingerInfo.enrollTouchCount  = touch;   //  communicate to Matcher touch count up to kSE_Integrator_maxEnrollTouchCount
  ccSE_Data.enrollMatchFingerInfo.enrollFingerCount = finger;  //  communicate to Matcher finger count up to kSE_Integrator_maxEnrollFingerCount
  ccSE_Data.InternalUse1                            = false;   //  don't execute prepare-image  

  if( ( finger == 1) && (touch == 1)){
    rcode = ClearFlash(&ccSE_Data, pConfig->pIdxBuffer, pConfig->pTemplateFlashPtr, pConfig->templateFlashSize);
    if (IDX_ISO_SUCCESS != rcode){ return(rcode);}
    // ---- Set 0xFF in templates entries buffer ----
    memset(flashTemplateEntries, 0xFF, sizeof(flashTemplateEntries));
  }
  else
  {
    // ---- Read template entries from the FLASH ----
    memcpy(flashTemplateEntries, pConfig->pTemplateFlashPtr, sizeof(flashTemplateEntries));
  }
  
  rcode = Extract(&ccSE_Data, pConfig->pIdxBuffer);
  if (IDX_ISO_SUCCESS != rcode){ return(rcode); }

  rcode = Merge(&ccSE_Data, pConfig->pIdxBuffer, &handle);
  if (IDX_ISO_SUCCESS != rcode){ return(rcode); }

  // ---- Update template entries in RAM ----
  entry = (finger - 1) * SE_EMK_IDEX_SECURE_MAX_TOUCHES + (touch - 1);
  flashTemplateEntries[entry].finger = (finger - 1);
  flashTemplateEntries[entry].touch = (touch - 1);
  flashTemplateEntries[entry].blobIndex = handle;

  // ---- Push template entries to flash ----
  rcode = idxHalFlashWrite(pConfig->pTemplateFlashPtr, (uint8_t*)flashTemplateEntries, sizeof(flashTemplateEntries));
  if (IDX_ISO_SUCCESS != rcode)
  {
    return(IDX_ISO_ERR_FLASH_WRITE);
  }
  
  return(IDX_ISO_SUCCESS);
}
/*******************************************************************
 * Sleeve Enrol : ( 2 Finger ) x ( 6 Touches) - Hard-coded
 * LED Control for Sleeve 
 */
uint16_t idxSeEmkSleeveEnroll( idx_SeEmkConfig_t *pConfig )
{
  uint16_t rcode;
  uint8_t handle;
  uint8_t entry;
  int8_t enrollFingerCount;
  int8_t enrollTouchCount;
  IDEX_ccSE_MatcherDataS1  ccSE_Data;
  idex_FlashTemplateEntry_t flashTemplateEntries[SE_EMK_IDEX_SECURE_MAX_TOUCHES*SE_EMK_IDEX_SECURE_MAX_FINGERS];

  //      ----    Setup Paramters for IDEX.M      ----
  ccSE_Data.SE_StoredTemplateFlashBasePtr              = pConfig->pTemplateFlashPtr + SE_EMK_IDEX_SECURE_FLASH_OFFSET; // point to stored flash templates
  ccSE_Data.externalAllocatedMatcherBaseMemPtr         = pConfig->pWorkAreas[0].pointer;      // point to RAM allocated for IDEX Matcher
  ccSE_Data.externalAllocatedMatcherBaseDATAMemPtr     = pConfig->pWorkAreas[1].pointer;   // point to data RAM1  
  ccSE_Data.targetSecurityLevelMatch                   = pConfig->securityLevel;       // target securityLevel
  ccSE_Data.abortProcess                               = false;               // aways false

  ccSE_Data.enrollMatchFingerInfo.enrollMaxTouchCount  = pConfig->maxTouchesPerFinger;  // number of enrolled fingers
  ccSE_Data.enrollMatchFingerInfo.enrollMaxFingerCount = pConfig->maxEnrolledFingers;   // number of touches per enroll
  ccSE_Data.EnrollOrMatch                              = kDataForEnrollS1;     // specify this is an enroll
  ccSE_Data.InternalUse1                               = true;                 // execute prepare-image  

  rcode = ClearFlash(&ccSE_Data, pConfig->pIdxBuffer, pConfig->pTemplateFlashPtr, pConfig->templateFlashSize);
  if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x60; return(rcode);}

  // ---- Set 0xFF in templates entries buffer ----  
  memset(flashTemplateEntries, 0xff, sizeof(flashTemplateEntries));

  rcode = idxBioLedSetMode( IDX_SLEEVE_LED_START, pConfig->pIdxBuffer );
  if ( rcode != IDX_ISO_SUCCESS ){ errPoint = 0x61; return rcode; }
 
  ccSE_Data.ProcessStage = kProcessStageEnrollS1;   //  prepare for enroll

  for ( enrollFingerCount = 1; enrollFingerCount < pConfig->maxEnrolledFingers + 1; enrollFingerCount++ )
  {
    if ( enrollFingerCount == 2 ){ // Play for the second finger
      rcode = idxBioLedSetMode( IDX_SLEEVE_LED_NEXT_FINGER, pConfig->pIdxBuffer );
      if ( rcode != IDX_ISO_SUCCESS ){ errPoint = 0x62;  return rcode; }
    }
    
    for( enrollTouchCount = 1; enrollTouchCount < pConfig->maxTouchesPerFinger + 1; enrollTouchCount++ )   // loop to count enrollment touch count
    {
      uint8_t maxNumTries = BAD_IMAGE_NUM_TRIES;
      uint16_t wcode;
      ccSE_Data.enrollMatchFingerInfo.enrollTouchCount  = enrollTouchCount;   //  communicate to Matcher touch count up to kSE_Integrator_maxEnrollTouchCount
      ccSE_Data.enrollMatchFingerInfo.enrollFingerCount = enrollFingerCount;  //  communicate to Matcher finger count up to kSE_Integrator_maxEnrollFingerCount
      
      do {  
        wcode = Extract(&ccSE_Data, pConfig->pIdxBuffer);
        if (IDX_ISO_SUCCESS == wcode) break;
        if (IDX_ISO_ERR_TIMEOUT == wcode) break;
      } while(--maxNumTries); 
// return justBlink(IDX_ISO_SUCCESS,2);
      
      if ( IDX_ISO_SUCCESS != wcode )
      {
          idxBioLedSetMode( IDX_SLEEVE_LED_STOP, pConfig->pIdxBuffer );
          return(wcode);
      }
      
      rcode = Merge(&ccSE_Data, pConfig->pIdxBuffer, &handle);
      if (IDX_ISO_SUCCESS != rcode)
      { 
        errPoint = 0x63; 
        idxBioLedSetMode( IDX_SLEEVE_LED_STOP, pConfig->pIdxBuffer );
        return(rcode);
      }
// return justBlink(IDX_ISO_SUCCESS,2);

      // ---- Update template entries in RAM ----
      entry = (enrollFingerCount - 1) * SE_EMK_IDEX_SECURE_MAX_TOUCHES + (enrollTouchCount - 1);
      flashTemplateEntries[entry].finger = (enrollFingerCount - 1);
      flashTemplateEntries[entry].touch = (enrollTouchCount - 1);
      flashTemplateEntries[entry].blobIndex = handle;

      rcode = idxBioLedSetMode( IDX_SLEEVE_LED_NEXT_TOUCH, pConfig->pIdxBuffer );
      if ( rcode != IDX_ISO_SUCCESS ){ errPoint = 0x64; return rcode;}
    }
// return justBlink(IDX_ISO_SUCCESS,2);

    rcode = idxBioLedSetMode( IDX_SLEEVE_LED_ENROLL_FINISHED, pConfig->pIdxBuffer );
    if ( rcode != IDX_ISO_SUCCESS ){ errPoint = 0x65; return rcode;}
  }

  // ---- Push template entries to flash ----
  rcode = idxHalFlashWrite(pConfig->pTemplateFlashPtr, (uint8_t*)flashTemplateEntries, sizeof(flashTemplateEntries));
  if (IDX_ISO_SUCCESS != rcode)
  {
    return(IDX_ISO_ERR_FLASH_WRITE);
  }

  rcode = idxBioLedSetMode( IDX_SLEEVE_LED_STOP, pConfig->pIdxBuffer );
  if ( rcode != IDX_ISO_SUCCESS ){ errPoint = 0x66;  return rcode;}

  return(IDX_ISO_SUCCESS);
}
/*******************************************************************
 * Full Enrol : ( 2 Fingers ) x ( 6 Touches) 
 * LED Control 
 */
uint16_t idxSeEmkEnroll( idx_SeEmkConfig_t *pConfig )
{
  uint16_t rcode;
  uint8_t handle;
  uint8_t entry;
  int8_t enrollFingerCount;
  int8_t enrollTouchCount;
  IDEX_ccSE_MatcherDataS1  ccSE_Data;
  idex_FlashTemplateEntry_t flashTemplateEntries[SE_EMK_IDEX_SECURE_MAX_TOUCHES*SE_EMK_IDEX_SECURE_MAX_FINGERS];

  //      ----    Setup Paramters for IDEX.M      ----
  ccSE_Data.SE_StoredTemplateFlashBasePtr      = pConfig->pTemplateFlashPtr + SE_EMK_IDEX_SECURE_FLASH_OFFSET; // point to stored flash templates
  ccSE_Data.externalAllocatedMatcherBaseMemPtr = pConfig->pWorkAreas[0].pointer;      // point to RAM allocated for IDEX Matcher
  ccSE_Data.externalAllocatedMatcherBaseDATAMemPtr = pConfig->pWorkAreas[1].pointer;   // point to data RAM1   
  ccSE_Data.targetSecurityLevelMatch           = pConfig->securityLevel;       // target securityLevel
  ccSE_Data.abortProcess                       = false;               // aways false

  ccSE_Data.enrollMatchFingerInfo.enrollMaxTouchCount  = pConfig->maxTouchesPerFinger;  // number of enrolled fingers
  ccSE_Data.enrollMatchFingerInfo.enrollMaxFingerCount = pConfig->maxEnrolledFingers;   // number of touches per enroll
  ccSE_Data.EnrollOrMatch                              = kDataForEnrollS1;     // specify this is an enroll
  ccSE_Data.InternalUse1                               = true;                 // execute prepare-image  

  rcode = ClearFlash(&ccSE_Data, pConfig->pIdxBuffer, pConfig->pTemplateFlashPtr, pConfig->templateFlashSize);
  if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x10; return(rcode);}

  // ---- Set 0xFF in templates entries buffer ----  
  memset(flashTemplateEntries, 0xff, sizeof(flashTemplateEntries));
  
  rcode = idxBioLedSetMode( IDX_CARD_LED_START, pConfig->pIdxBuffer );  // LED Start Sequence LED Control
  if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x11; return(rcode);}

  ccSE_Data.ProcessStage = kProcessStageEnrollS1;                             //  prepare for enroll
  for (enrollFingerCount = 1; enrollFingerCount < pConfig->maxEnrolledFingers + 1; enrollFingerCount++)   // loop to count enrollment touch count
  {
    if(1  &&  (enrollFingerCount == 2)){
      rcode = idxBioLedSetMode( IDX_CARD_LED_NEXT_FINGER, pConfig->pIdxBuffer );// Second Finger LED Control
      if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x12; return(rcode);}
    }
    
    for(enrollTouchCount = 1; enrollTouchCount < pConfig->maxTouchesPerFinger + 1; enrollTouchCount++)   // loop to count enrollment touch count
    {
      uint8_t numTries = BAD_IMAGE_NUM_TRIES;
      ccSE_Data.enrollMatchFingerInfo.enrollTouchCount  = enrollTouchCount;   //  communicate to Matcher touch count up to kSE_Integrator_maxEnrollTouchCount
      ccSE_Data.enrollMatchFingerInfo.enrollFingerCount = enrollFingerCount;  //  communicate to Matcher finger count up to kSE_Integrator_maxEnrollFingerCount
      
      while( numTries ){
        uint16_t wcode;
        idxBioLedSetMode( IDX_CARD_LED_BLINK, pConfig->pIdxBuffer );
        wcode = Extract(&ccSE_Data, pConfig->pIdxBuffer);
        if (IDX_ISO_SUCCESS == wcode) break;
        if (IDX_ISO_ERR_COMM != wcode) {
          // If communication has failed there's no way to control the card LEDs
	  rcode = idxBioLedSetMode( IDX_CARD_LED_TOUCH_FAIL, pConfig->pIdxBuffer ); // Touch - LED control here
          if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x13; return(rcode);}
        }
        if(wcode != IDX_ISO_ERR_BAD_QUALITY) return wcode;      // return for all errors except "bad-quality"
        if(!--numTries){ errPoint = 0x17; return(wcode);}   // return if too many "bad-quality" warnings
      } 
      
      idxBioLedSetMode( IDX_CARD_LED_STOP, pConfig->pIdxBuffer );
      
      rcode = Merge(&ccSE_Data, pConfig->pIdxBuffer, &handle);
      if (IDX_ISO_SUCCESS != rcode){ return(rcode);}

    // ---- Update template entries in RAM ----
    entry = (enrollFingerCount - 1) * SE_EMK_IDEX_SECURE_MAX_TOUCHES + (enrollTouchCount - 1);
    flashTemplateEntries[entry].finger = (enrollFingerCount - 1);
    flashTemplateEntries[entry].touch = (enrollTouchCount - 1);
    flashTemplateEntries[entry].blobIndex = handle;
      
      rcode = idxBioLedSetMode( IDX_CARD_LED_TOUCH_SUCCESS, pConfig->pIdxBuffer );// Touch - LED control here
      if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x15; return(rcode);}
 #ifdef SLE78     
      idxHalUartISO7816Refresh();  // send NULL in contact mode
 #endif
    } // touch loop
    if(1 && (enrollFingerCount == 2)){
      rcode = idxBioLedSetMode( IDX_CARD_LED_LAST_TOUCH, pConfig->pIdxBuffer ); // // Last Touch - LED control here
      if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x16; return(rcode);}
    }
  } //finger loop

  // ---- Push template entries to FLASH ----
  rcode = idxHalFlashWrite(pConfig->pTemplateFlashPtr, (uint8_t*)flashTemplateEntries, sizeof(flashTemplateEntries));
  if (IDX_ISO_SUCCESS != rcode)
  {
    return(IDX_ISO_ERR_FLASH_WRITE);
  }

  rcode = idxBioLedSetMode( IDX_CARD_LED_STOP, pConfig->pIdxBuffer ); // End of enrolment - LED control here
  if (IDX_ISO_SUCCESS != rcode) { errPoint = 0x17; return(rcode);}
    
  return(IDX_ISO_SUCCESS);
}

/***************************************************************************************
 * Verify : 
 * LED Control 
 */
static uint16_t getNumFingersAndTouches(void *pTemplateFlash)
{
  idex_FlashTemplateEntry_t *pEntry  = (idex_FlashTemplateEntry_t *)pTemplateFlash;
  // find maxT and maxF
  int8_t maxT = -1; // 
  int8_t maxF = -1;
  uint16_t i;
  for(i = 0;  i < SE_EMK_IDEX_SECURE_MAX_FINGERS * SE_EMK_IDEX_SECURE_MAX_TOUCHES ; i++){ 
    if(pEntry[i].finger == 0xff) continue; // finger check should be enough, avoid using memcmp
    else{
      if((int)pEntry[i].finger > maxF) maxF = pEntry[i].finger;
      if((int)pEntry[i].touch  > maxT) maxT = pEntry[i].touch;        
    }        
  }
  if( ( maxF < 0 ) || ( maxT < 0 )) { errPoint = 0x55; return IDX_ISO_ERR_NOT_FOUND;} 
  return IDX_ISO_SUCCESS;
}

uint16_t idxSeEmkMatch( idx_SeEmkConfig_t *pConfig,
                        uint8_t imgsrc,
                        uint16_t mwfTimeBudget)
{
  uint16_t rcode;
  IDEX_ccSE_MatcherDataS1  ccSE_Data;
  uint16_t maxMwfValue = 32767;	// MWF valid range 0 <> 32767
  //      ----    Setup Paramters for IDEX.M      ----
  ccSE_Data.SE_StoredTemplateFlashBasePtr          = pConfig->pTemplateFlashPtr + SE_EMK_IDEX_SECURE_FLASH_OFFSET; // point to stored flash templates
  ccSE_Data.externalAllocatedMatcherBaseMemPtr     = pConfig->pWorkAreas[0].pointer;      // point to RAM allocated for IDEX Matcher
  ccSE_Data.externalAllocatedMatcherBaseDATAMemPtr = pConfig->pWorkAreas[1].pointer;  // point to data RAM1   
  ccSE_Data.targetSecurityLevelMatch               = pConfig->securityLevel;       // target securityLevel
  ccSE_Data.abortProcess                           = false;                        // aways false

  rcode = idxHalFlashEraseCheck(pConfig->pTemplateFlashPtr, SE_EMK_IDEX_SECURE_FLASH_OFFSET);
  if(IDX_ISO_SUCCESS == rcode) { errPoint = 0x54; return IDX_ISO_ERR_NOT_FOUND; }

  ccSE_Data.enrollMatchFingerInfo.enrollMaxTouchCount  = pConfig->maxTouchesPerFinger;   // number of enrolled fingers
  ccSE_Data.enrollMatchFingerInfo.enrollMaxFingerCount = pConfig->maxEnrolledFingers; // number of touches per enroll  
  rcode = getNumFingersAndTouches((void *)pConfig->pTemplateFlashPtr);
  if(IDX_ISO_SUCCESS != rcode) { return(rcode); }
  
  errPoint = ccSE_Data.enrollMatchFingerInfo.enrollMaxFingerCount + (ccSE_Data.enrollMatchFingerInfo.enrollMaxTouchCount << 4 );
  
  ccSE_Data.enrollMatchFingerInfo.enrollTouchCount  = 0;                     // reset touch count
  ccSE_Data.enrollMatchFingerInfo.enrollFingerCount = 0;                     // reset finger count

  ccSE_Data.EnrollOrMatch = kDataForMatchS1;                                 // specify this is a match
  ccSE_Data.InternalUse1                              = imgsrc ? true : false;   // Execute prepareimage or not based on image source

  ccSE_Data.ProcessStage = kProcessStageMatch_1_S1;   //  prepare for stage1
  rcode = Extract(&ccSE_Data, pConfig->pIdxBuffer);
  if (IDX_ISO_SUCCESS != rcode){ return(rcode);}
  
  ccSE_Data.ProcessStage = kProcessStageMatch_2_S1;   //  prepare for stage2
  if(mwfTimeBudget > maxMwfValue)
    ccSE_Data.MWFScratchBuffer.MWFMaxAllowance = IDEX_MWF_DEFAULT;
  else
  ccSE_Data.MWFScratchBuffer.MWFMaxAllowance = mwfTimeBudget;
    
  rcode = Match(&ccSE_Data, pConfig->pIdxBuffer, pConfig->pTemplateFlashPtr);
  idxHalTimerStopRun();
  
#if defined(ENABLE_CARD_MATCH_LEDS)
  if(IDX_ISO_SUCCESS == rcode)
  {
    uint16_t rcode1 = idxBioLedSetMode( IDX_MATCH_STATE_SUCCESS, pConfig->pIdxBuffer );
    if (IDX_ISO_SUCCESS != rcode1) { errPoint = 0x18; return(rcode1);}
  }
  else
  {
    uint16_t rcode1 = idxBioLedSetMode( IDX_MATCH_STATE_FAIL, pConfig->pIdxBuffer );
    if (IDX_ISO_SUCCESS != rcode1) { errPoint = 0x19; return(rcode1);}
  }
#endif

  return(rcode);
}

//###############################   Helper functions   ###############################


/* The endianess should be consider as following
 * The COMBUF is always little endian 
 */ 

static void wrU16(uint8_t *pU16, uint16_t val)
{
  *pU16++ = val & 0xff;
  *pU16   = val >> 8;
}

static uint16_t rdU16(uint8_t *pU16)
{
  uint16_t val = (uint16_t)*pU16 | ((uint16_t)*(pU16 +1) << 8 ) ; 
  return val;
}

//###############################   Local Functions    ###############################

static uint16_t ClearFlash(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                           const idxBuffer_t *pCommsBuffer,
                           uint8_t *pTemplateFlash,
                           uint32_t szTemplateFlash)
{  
  uint16_t wCode = 0; // TH89 asks for initialisation

  // ---- Begin template flash erase ----
  // ---- erase MCU template flash ----
  {
    uint16_t data_size = DELETERECORD_CMD_DATA_SIZE;
    uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
    pDataBuf[0] = 0xff;
    pDataBuf[1] = 0xff;
    wCode = idxMcuComms( DELETERECORD_CMD, 0, &data_size, pCommsBuffer );
    if(( IDX_ISO_SUCCESS != wCode) && (IDX_ISO_ERR_NOT_FOUND != wCode)){
      errPoint = 2; return wCode;
    }
  }

  // ---- erase SE template entries ----
  wCode = idxHalFlashErase(pTemplateFlash, SE_EMK_IDEX_SECURE_FLASH_OFFSET);
  if (IDX_ISO_SUCCESS != wCode)
  {
    return(IDX_ISO_ERR_FLASH_ERASE);
  }


  // ---- erase SE template flash ----
  wCode = idxHalFlashErase(ccSE_Data->SE_StoredTemplateFlashBasePtr, szTemplateFlash - SE_EMK_IDEX_SECURE_FLASH_OFFSET);
  if(IDX_ISO_SUCCESS != wCode)
  {
    return(IDX_ISO_ERR_FLASH_ERASE);
  }
//  return blink(0xffff);
  
  return(IDX_ISO_SUCCESS);
}

// Retrieves about 1.5KB of Data from BMCU
#define TEMPLATE_INFO_DATA_SIZE  ( 8 )
extern uint16_t POS_image_capture_to_NVM(void);

static uint16_t Extract(IDEX_ccSE_MatcherDataS1 *ccSE_Data, const idxBuffer_t *pCommsBuffer)
{
  uint16_t wCode;
  uint16_t rdOffset;
  uint16_t rdSize;

  uint16_t templateSize;
  int8_t   imageSubmitResult;
  uint8_t *pTemplate;
  uint16_t data_size;
  idxBuffer_t rBuffer;

  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];

  imageSubmitResult = IDEX_ccSE_GetBasePtrForSensorImageData(ccSE_Data);               //  setup
  if(kMatcher_NoErrorNoMatchS1 > imageSubmitResult)
  {
      return(IDX_ISO_ERR_MATCHER_EXTRACT_INIT);
  }

  // ---- Capture Image (coverage for enroll only) ----
  if( ccSE_Data->InternalUse1 == true ){                 // execute prepare-image  
    if( ccSE_Data->EnrollOrMatch == kDataForEnrollS1){
//      uint8_t response[12]; // space for response 
      data_size = PREPAREIMAGE_CMD_DATA_SIZE;
      pDataBuf[0] = 0x03 | 0x50;        // add COVERAGE[b4] |  | WAIT_FINGER_OFF[b6]
      wCode = idxMcuComms( PREPAREIMAGE_CMD, 0, &data_size, pCommsBuffer );
//         return justBlink(0xffff,2);

      if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x30; return wCode; }
      { 
        uint16_t coverage = rdU16(pDataBuf); // first 2 bytes of the response 
        if( coverage < COVERAGE_THRESHOLD ) { 
           errPoint = (uint8_t)coverage; 
           return IDX_ISO_ERR_BAD_QUALITY;
        }
      }      
    } else {
      data_size = PREPAREIMAGE_CMD_DATA_SIZE;
      pDataBuf[0] = 0x03;
      wCode = idxMcuComms( PREPAREIMAGE_CMD, 0, &data_size, pCommsBuffer );
      if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x30; return wCode; }
	//wCode=POS_image_capture_to_NVM();
	//if( IDX_ISO_SUCCESS != wCode)  { return wCode;}
    }
  }

  // ---- Enable the Extract time ( added to the first timestamp )
#if 0
  if( ccSE_Data->EnrollOrMatch == kDataForMatchS1 )
    idxHalTimerFreeRun();
#endif

  // ---- Extract Template ----
  // Need to perform transfer in "chunks" as extracted data size > MAX_SE_TRANSFER_SZ

  // First send command and retrieve size of response data
  pDataBuf[0] = 0x01; // SLE70 and TH89-match
#ifdef TH89
  if( ccSE_Data->EnrollOrMatch == kDataForEnrollS1)   pDataBuf[0] = 0x81; // TH89-enroll ( IDM-L )
#endif
  data_size = GETTEMPLATE_CMD_DATA_SIZE;
  wCode = idxMcuComms( GETTEMPLATE_CMD, RETURN_SIZE, &data_size, pCommsBuffer );
  if( IDX_ISO_SUCCESS != wCode) { errPoint = 0x31; return wCode; }

  rdOffset = 0;
  rdSize   = TEMPLATE_INFO_DATA_SIZE;
  wrU16( &pDataBuf[0], rdOffset);
  wrU16( &pDataBuf[2], rdSize);
  data_size = READ_DATA_CMD_DATA_SIZE;
  wCode = idxMcuComms( READ_DATA_CMD, 0, &data_size, pCommsBuffer );
// return justBlink(0xffff,2);

  if( (IDX_ISO_SUCCESS != wCode) || ( data_size != TEMPLATE_INFO_DATA_SIZE) ) { errPoint = 0x32; return wCode;}

  // Retrieve relevant info from ReplyData buffer    
  templateSize = pDataBuf[TEMPL_SIZE_OFF] + ( pDataBuf[TEMPL_SIZE_OFF+1] << 8) ;
  ccSE_Data->sensorImageDataSize = templateSize;     //  place new sensor data size into the IDEX.M environment
  pTemplate = ccSE_Data->SensorImageDataMemoryInternalPtr;

  rdOffset += TEMPLATE_INFO_DATA_SIZE;


  while( templateSize > MAX_SE_TRANSFER_SZ )
  {
    wrU16( &pDataBuf[2],MAX_SE_TRANSFER_SZ);  // read-data size    
    wrU16( &pDataBuf[0], rdOffset);           // read-data offset

    data_size = READ_DATA_CMD_DATA_SIZE;
    rBuffer.bufferSize = MAX_SE_TRANSFER_SZ + 16; // secure channel needs more 16 bytes for decryption
    rBuffer.pBuffer = pTemplate;
    wCode = idxMcuCommsFull( READ_DATA_CMD, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
    if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x33; return wCode;}
    
    templateSize -= MAX_SE_TRANSFER_SZ;
    pTemplate    += MAX_SE_TRANSFER_SZ;
    rdOffset     += MAX_SE_TRANSFER_SZ;
  }    
  // Now last chunk
  wrU16( &pDataBuf[0],rdOffset );     // read-data offset
  wrU16( &pDataBuf[2],templateSize ); // read-data size
// return justBlink(0xffff,2);

  data_size = READ_DATA_CMD_DATA_SIZE;
  rBuffer.bufferSize = MAX_SE_TRANSFER_SZ + 16; // secure channel needs more 16 bytes for decryption
  rBuffer.pBuffer = pTemplate;
  wCode = idxMcuCommsFull( READ_DATA_CMD, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
  if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x34; return wCode; }
  idxHalSystemSetPowerState(idxPowerStateSE);

  imageSubmitResult = IDEX_ccSE_SubmitSensorDataToMatcherRDS( ccSE_Data );      //  process new sensor data
  if( kMatcher_NoErrorNoMatchS1 > imageSubmitResult )
  {
    errPoint = imageSubmitResult;
    return(IDX_ISO_ERR_MATCHER_EXTRACT_PROCESS);
  }
// return justBlink(0xffff,2);

  return(IDX_ISO_SUCCESS);
}

/********************************************************************************
 * MERGE
 * Read about 3.5 KB of Data to BMCU
 */
#define MERGETEMPLATES_RES_DATA_SIZE (6)
static uint16_t Merge(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                      const idxBuffer_t *pCommsBuffer,
                      uint8_t* pHandle)
{
  uint16_t wCode;
  uint16_t templateSize;
  uint8_t  *pTemplate    = ccSE_Data->Data_2_Ptr;
  uint16_t rdOffset;
  uint16_t rdSize;
  uint16_t data_size;
  idxBuffer_t rBuffer;

  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  
  // ---- Save High Resolution Data on SE ----
  wCode = idxHalFlashWrite( ccSE_Data->SE_FlashLocation_1_Ptr, ccSE_Data->Data_1_Ptr, ccSE_Data->DataSize_1 );
  if( IDX_ISO_SUCCESS != wCode )
  {
      return(IDX_ISO_ERR_FLASH_WRITE);
  }

  // ---- Retrieve Low resolution Ridge Dta from MCU ----

  // Firstly send command and retrieve size of response data
  {
//    uint8_t tsize[6];
    data_size = MERGETEMPLATES_CMD_DATA_SIZE;
    pDataBuf[0] = 0;              // flags
    pDataBuf[1] = NUM_TEMPLATES;  // num templates, size[2] }
    wrU16( &pDataBuf[2], 0 );        // size[2]
    wCode = idxMcuComms( MERGETEMPLATES_CMD, RETURN_SIZE, &data_size, pCommsBuffer );
    
    if( IDX_ISO_SUCCESS != wCode) { errPoint = 0x40;  return wCode; }
  }
  rdOffset = 0;
  wrU16( &pDataBuf[0], rdOffset );
  // First Read Data retrieves meta-data {6 bytes) as defined on HOST I/F { num merged[1], num fetures[1], tquality[2], tsize[2]} 
  {
    data_size = READ_DATA_CMD_DATA_SIZE;
    rdSize  = MERGETEMPLATES_RES_DATA_SIZE;
    wrU16( &pDataBuf[2], rdSize );
    wCode = idxMcuComms( READ_DATA_CMD, 0, &data_size, pCommsBuffer );
    if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x41; return wCode; }
    templateSize = pDataBuf[4] + (pDataBuf[5] << 8);
    rdOffset += MERGETEMPLATES_RES_DATA_SIZE;
  }
  
  // Subsequent reads contain the information to be stored on RAM ( pTemplate )
  while (templateSize > MAX_SE_TRANSFER_SZ)
  {
    wrU16( &pDataBuf[0], rdOffset );   // read-data offset and size   
    wrU16( &pDataBuf[2], (uint16_t)MAX_SE_TRANSFER_SZ );    // Read-data Size
    
    data_size = READ_DATA_CMD_DATA_SIZE;
    rBuffer.bufferSize = MAX_SE_TRANSFER_SZ + 16; // secure channel needs more 16 bytes for decryption
    rBuffer.pBuffer = pTemplate;
    wCode = idxMcuCommsFull( READ_DATA_CMD, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
    if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x42; return wCode; }
    
    templateSize -= MAX_SE_TRANSFER_SZ;
    pTemplate    += MAX_SE_TRANSFER_SZ;
    rdOffset     += MAX_SE_TRANSFER_SZ;
  }        

  // Now last read
  wrU16( &pDataBuf[0],rdOffset );     // read-data offset
  wrU16( &pDataBuf[2],templateSize ); // read-data size
  data_size = READ_DATA_CMD_DATA_SIZE;
  rBuffer.bufferSize = MAX_SE_TRANSFER_SZ + 16; // secure channel needs more 16 bytes for decryption
  rBuffer.pBuffer = pTemplate;
  wCode = idxMcuCommsFull( READ_DATA_CMD, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
  if( IDX_ISO_SUCCESS != wCode )  { errPoint = 0x43; return wCode; }

  // ---- Merge Templates on MCU ( Store blob command ) ----
  {
    pDataBuf[0] = 0x06; 
    memset( &pDataBuf[1], 0x00, 4 ); //{0x06,0x00, 0x00, 0x00, 0x00};  // HOST I/F { flags, size[4] }
    data_size = STOREBLOB_CMD_DATA_SIZE;
    wCode = idxMcuComms( STOREBLOB_CMD, 0, &data_size, pCommsBuffer );
    *pHandle = pDataBuf[0];
    if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x44; return wCode; }
  }

  // ---- Save Low Resolution Data on SE ----
  wCode = idxHalFlashWrite(ccSE_Data->SE_FlashLocation_2_Ptr, ccSE_Data->Data_2_Ptr, ccSE_Data->DataSize_2);
  if(IDX_ISO_SUCCESS != wCode)
  {
      return(IDX_ISO_ERR_FLASH_WRITE);
  }
// return justBlink(0xffff,2);

  return(IDX_ISO_SUCCESS);
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Note: mapping records SE-MCU 
 * SE  stores records based on matrix like index record[finger,touch]
 * MCU stores records in a linear way so based on enroll/match loops the map
 * should be (mT being maxim number of touches). Please note different loop start
 *   SE        MCU            SE       MCU
 *  [1,1]       n+0          [2,1]     n+mT+0
 *  [1,2]       n+1          [2,1]     n+mT+1
 *   ....      .....          ....      .....
 *  [1,mT]      n+mT-1       [2,mT]    n+2*mT-1
 * So within matcher loop:
 *            [finger,touch] <-> handle = handle[0] + finger*mT +touch  
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
static const uint8_t  cmdMatch[] = {0,MASK_LEN,NUM_TEMPLATES,1,0};  // {flags, mask_len, num_templates,templ_size[2],handle}
static uint16_t Match(IDEX_ccSE_MatcherDataS1 *ccSE_Data,
                      const idxBuffer_t *pCommsBuffer,
                      uint8_t *pTemplateFlash)
{
  uint8_t  fingerCnt;
  uint8_t  touchCnt;
  uint16_t timestamps[SE_EMK_IDEX_SECURE_MAX_FINGERS * SE_EMK_IDEX_SECURE_MAX_TOUCHES];
  idex_FlashTemplateEntry_t flashTemplateEntries[SE_EMK_IDEX_SECURE_MAX_TOUCHES*SE_EMK_IDEX_SECURE_MAX_FINGERS];
  uint16_t previousTStamp = 0;
  uint16_t timestamp = 0;
  uint16_t data_size;

  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];

  // ---- Read template entries from the FLASH ----
  memcpy(flashTemplateEntries, pTemplateFlash, sizeof(flashTemplateEntries));

#ifdef SLE70
  IDEX_ccSE_GetProcess2Info(ccSE_Data);
#endif

  { uint16_t i; for(i = 0 ; i < sizeof(timestamps)/sizeof(timestamps[0]) ; i++) timestamps[i] = (uint16_t)-1;}

#ifdef TH89
  {
    // ---- Save High Resolution Data on SE ----
    uint16_t wCode;
    wCode = idxHalFlashWrite(ccSE_Data->SE_FlashLocation_1_Ptr, ccSE_Data->Data_1_Ptr, ccSE_Data->DataSize_1);
    if(IDX_ISO_SUCCESS != wCode)
    {
      return(IDX_ISO_ERR_FLASH_WRITE);
    }
  
  IDEX_ccSE_GetProcess2Info(ccSE_Data);
  }
#endif
  
  // ---- Start timer for recording match attempts duration
  idxHalTimerFreeRun();
  
  // ****    loop for final match processing     ****
  for( touchCnt = 0; touchCnt < ccSE_Data->enrollMatchFingerInfo.enrollMaxTouchCount; touchCnt++ )
  {
    //uint8_t  cmdMatch[] = {0,MASK_LEN,NUM_TEMPLATES,1,0,0};  // {flags, mask_len, num_templates,templ_size[2],handle}
    for (fingerCnt = 0; fingerCnt < ccSE_Data->enrollMatchFingerInfo.enrollMaxFingerCount; fingerCnt++)
    {   // ---- Match Templates Phase 2 on MCU ----
      uint8_t entry;
      uint16_t wCode;      
      uint8_t *pTemplate = ccSE_Data->Data_1_Ptr;
      uint16_t templateSize;    // local endianess
      uint16_t rdOffset = 0;
      idxBuffer_t rBuffer;

      entry = fingerCnt * SE_EMK_IDEX_SECURE_MAX_TOUCHES + touchCnt;  // index in entries list

      if(flashTemplateEntries[entry].finger == 0xff) continue; // finger check should be enough, avoid using memcmp
            
      // First send command and retrieve size of response data
      memcpy( pDataBuf, cmdMatch, MATCHTEMPLATES_CMD_DATA_SIZE-1 );
      pDataBuf[MATCHTEMPLATES_CMD_DATA_SIZE-1] = flashTemplateEntries[entry].blobIndex;
      data_size = MATCHTEMPLATES_CMD_DATA_SIZE;
      wCode = idxMcuComms( MATCHTEMPLATES_CMD, RETURN_SIZE, &data_size, pCommsBuffer ); 
      if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x51; return wCode;}
      templateSize = pDataBuf[0] + (pDataBuf[1] << 8 );  // it needs only 2 bytes ( 16 bit number)

      while (templateSize > MAX_SE_TRANSFER_SZ)
      {
        wrU16(&pDataBuf[0], rdOffset);
        wrU16(&pDataBuf[2], (uint16_t)MAX_SE_TRANSFER_SZ);    // Read-data Size
        
        // Read comparison result data in chunks using ReadData command
        data_size = READ_DATA_CMD_DATA_SIZE;
        rBuffer.bufferSize = MAX_SE_TRANSFER_SZ + 16; // secure channel needs more 16 bytes for decryption
        rBuffer.pBuffer = pTemplate;
        wCode = idxMcuCommsFull( READ_DATA_CMD, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
        if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x52; return wCode;}

        templateSize -= MAX_SE_TRANSFER_SZ;
        pTemplate    += MAX_SE_TRANSFER_SZ;
        rdOffset     += MAX_SE_TRANSFER_SZ;
      }
      // last transfer
      wrU16(&pDataBuf[0], rdOffset);
      wrU16(&pDataBuf[2], (uint16_t)templateSize);    // Read Data Size
      data_size = READ_DATA_CMD_DATA_SIZE;
      rBuffer.bufferSize = MAX_SE_TRANSFER_SZ + 16; // secure channel needs more 16 bytes for decryption
      rBuffer.pBuffer = pTemplate;
      wCode = idxMcuCommsFull( READ_DATA_CMD, 0, &data_size, pCommsBuffer, NULL, &rBuffer );
      
      if( IDX_ISO_SUCCESS != wCode)  { errPoint = 0x53; return wCode;}

  idxHalSystemSetPowerState(idxPowerStateSE);
      ccSE_Data->enrollMatchFingerInfo.enrollTouchCount  = touchCnt;     //  communicate to Matcher touch count up to kSE_Integrator_maxEnrollTouchCount
      ccSE_Data->enrollMatchFingerInfo.enrollFingerCount = fingerCnt;    //  communicate to Matcher finger count up to kSE_Integrator_maxEnrollFingerCount
      {
	  	
        int8_t imageSubmitResult = IDEX_ccSE_SubmitSensorDataToMatcherRDS(ccSE_Data);      //  process data
        // comment for TH89
        // Internal timer is 32 bit SW counter, the timer tick is 28.44 usec so the max time is 2^32 * 28.44 usec = 122167 seconds,
        // but in this case the timer counter is 16 bit which means 2^16*28.44 usec = 1.86 sec
        timestamp = idxHalTimerGetTicks();
        {
          uint16_t tmp = timestamp;        
          timestamp -= previousTStamp;
          previousTStamp = tmp;
          wrU16((uint8_t *)&timestamps[entry], timestamp);
        }

        if(kMatcher_MatchFoundS1 == imageSubmitResult)
        {
//          rcode = pCallbacks->enrollState(MATCH_STATE_SUCCESS);
//          if (IDX_ISO_SUCCESS != rcode) { errPoint = 0xa1; return(rcode);}
          // the information about touch/finger will be embedded in reply data
          setReplyData(&touchCnt, 1, pCommsBuffer);
          addReplyData(&fingerCnt, 1, pCommsBuffer);
          addReplyData((uint8_t*)timestamps, sizeof(timestamps), pCommsBuffer);

          IDEX_ccSE_GetMWFConsumed(ccSE_Data);
          // The ccSE_Data->Match_Entropy structure is not packaged
          addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.ExtractEntropy.NumRidgePixels, sizeof(ccSE_Data->MatchEntropy.ExtractEntropy.NumRidgePixels), pCommsBuffer);
          addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.ExtractEntropy.NumRidges, sizeof(ccSE_Data->MatchEntropy.ExtractEntropy.NumRidges), pCommsBuffer);
          addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedPrimaryRidges, sizeof(ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedPrimaryRidges), pCommsBuffer);
          addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedSecondaryRidges, sizeof(ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedSecondaryRidges), pCommsBuffer);
          addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.CompareEntropy.MWFConsumed, sizeof(ccSE_Data->MatchEntropy.CompareEntropy.MWFConsumed), pCommsBuffer);
          addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.MatchResult, sizeof(ccSE_Data->MatchEntropy.MatchResult), pCommsBuffer);

          return IDX_ISO_SUCCESS;
        }
      }
    }
  }
//  rcode = pCallbacks->enrollState(MATCH_STATE_FAIL);
//  if (IDX_ISO_SUCCESS != rcode) { errPoint = 0xa2; return(rcode);}
  {  
    uint8_t no = 0xff;    // NO match : finger = touch = -1
    setErrorData(&no, 1, pCommsBuffer);
    addReplyData(&no, 1, pCommsBuffer);
    addReplyData((uint8_t*)timestamps, sizeof(timestamps), pCommsBuffer);

    IDEX_ccSE_GetMWFConsumed(ccSE_Data);
    // The ccSE_Data->Match_Entropy structure is not packaged
    addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.ExtractEntropy.NumRidgePixels, sizeof(ccSE_Data->MatchEntropy.ExtractEntropy.NumRidgePixels), pCommsBuffer);
    addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.ExtractEntropy.NumRidges, sizeof(ccSE_Data->MatchEntropy.ExtractEntropy.NumRidges), pCommsBuffer);
    addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedPrimaryRidges, sizeof(ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedPrimaryRidges), pCommsBuffer);
    addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedSecondaryRidges, sizeof(ccSE_Data->MatchEntropy.CompareEntropy.NumMatchedSecondaryRidges), pCommsBuffer);
    addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.CompareEntropy.MWFConsumed, sizeof(ccSE_Data->MatchEntropy.CompareEntropy.MWFConsumed), pCommsBuffer);
    addReplyData((uint8_t*)&ccSE_Data->MatchEntropy.MatchResult, sizeof(ccSE_Data->MatchEntropy.MatchResult), pCommsBuffer);

  }
  return(IDX_ISO_ERR_NO_MATCH);
}

uint16_t idxSeEmkDeleteFinger(idx_SeEmkConfig_t *pConfig,
                              uint8_t finger,
                              int8_t mode)
{
  uint16_t wCode;
  uint16_t data_size;
  OffsetSizePair offsetSizePair;

  uint8_t *pDataBuf = &pConfig->pIdxBuffer->pBuffer[HDR_SIZE];

  if( mode == SE_EMK_DELETE_ENROLLMENT_TEMPLATE )
  {
    uint8_t touchCnt;
    uint8_t recordsCnt = 0;
    idex_FlashTemplateEntry_t flashTemplateEntries[SE_EMK_IDEX_SECURE_MAX_TOUCHES*SE_EMK_IDEX_SECURE_MAX_FINGERS];

    if( (finger < 1) || (finger > pConfig->maxEnrolledFingers) )
      return IDX_ISO_ERR_NOT_FOUND;

      wCode = idxHalFlashEraseCheck(pConfig->pTemplateFlashPtr, SE_EMK_IDEX_SECURE_FLASH_OFFSET);
      if(IDX_ISO_SUCCESS == wCode) { errPoint = 0x54; return IDX_ISO_ERR_NOT_FOUND; }

    // ---- Read template entries from the FLASH ----
    memcpy( flashTemplateEntries, pConfig->pTemplateFlashPtr, sizeof(flashTemplateEntries) );

    for( touchCnt = 0; touchCnt < pConfig->maxTouchesPerFinger; touchCnt++ )
    {
      uint8_t entry;

      entry = ((finger - 1) * SE_EMK_IDEX_SECURE_MAX_TOUCHES) + touchCnt;  // index in entries list
      if( flashTemplateEntries[entry].finger == 0xFF ) continue; // finger check should be enough, avoid using memcmp

      recordsCnt += 1;
      wrU16( &pDataBuf[0], flashTemplateEntries[entry].blobIndex) ;

      data_size = DELETERECORD_CMD_DATA_SIZE;
      wCode = idxMcuComms( DELETERECORD_CMD, 0, &data_size, pConfig->pIdxBuffer );
      if( (IDX_ISO_SUCCESS != wCode) && (IDX_ISO_ERR_NOT_FOUND != wCode) ) {
        errPoint = 2; return wCode;
      }

      // get verification template offset and size
#ifdef TH89
      offsetSizePair = IDEX_ccSE_GetTemplateOffsetAndSize( (finger - 1), touchCnt, pConfig->maxTouchesPerFinger, 1 );
#else
      offsetSizePair = IDEX_ccSE_GetTemplateOffsetAndSize( (uint8_t)(finger - 1), touchCnt, pConfig->maxTouchesPerFinger );
#endif
      // ---- erase finger from SE flash ----
      wCode = idxHalFlashErase( pConfig->pTemplateFlashPtr + SE_EMK_IDEX_SECURE_FLASH_OFFSET + offsetSizePair.Offset, offsetSizePair.Size );
      if( IDX_ISO_SUCCESS != wCode )
      {
        return( IDX_ISO_ERR_FLASH_ERASE );
      }

      // ---- Update template entries in RAM ----
      flashTemplateEntries[entry].finger = 0xFF;
      flashTemplateEntries[entry].touch = 0xFF;
      flashTemplateEntries[entry].blobIndex = 0xFF;
    }
    // Found at least one record
    if( recordsCnt )
    {
      // ---- Push template entries to FLASH ----
      wCode = idxHalFlashWrite( pConfig->pTemplateFlashPtr, (uint8_t*)flashTemplateEntries, sizeof(flashTemplateEntries) );
      if(IDX_ISO_SUCCESS != wCode)
      {
        return( IDX_ISO_ERR_FLASH_WRITE );
      }
    }
    else
    {
      return IDX_ISO_ERR_NOT_FOUND;
    }
  }
  else if( mode == SE_EMK_DELETE_VERIFICATION_TEMPLATE )
  {
    UNUSED(finger);
#ifdef TH89
    // get verification template offset and size
    offsetSizePair = IDEX_ccSE_GetTemplateOffsetAndSize(0, 0, 0, 0);
    // ---- erase SE template flash ----
    wCode = idxHalFlashErase(pConfig->pTemplateFlashPtr + SE_EMK_IDEX_SECURE_FLASH_OFFSET + offsetSizePair.Offset, offsetSizePair.Size);
    if( IDX_ISO_SUCCESS != wCode )
    {
      return( IDX_ISO_ERR_FLASH_ERASE );
    }
#endif // should we return non supported error code in case of SLE, or just SUCCESS?
  }
  else
    return IDX_ISO_ERR_BAD_PARAMS;

  return IDX_ISO_SUCCESS;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
