/******************************************************************************
* \file  idxEmkDefines.h
* \brief Defines for the matcher library
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright 2014-2019 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR / FRR performance.
******************************************************************************/

#ifndef __IDX_EMK_BASE_DEFINES_H__
#define __IDX_EMK_BASE_DEFINES_H__

#include "idxBaseDefines.h"

#include <stdint.h>

#ifndef __GNUC__
#pragma pack(push)
#pragma pack(1)
#endif

typedef struct PACKED_ATTRIBUTE __idx_TemplateData
{
    unsigned char *data;    // reorganise order to make it easier for ridge matcher if it doesn't need the additional fields
    uint32_t size;
} 
idx_TemplateData_t;

typedef enum __idx_EnrollMode
{
    ACCEPT_ALL = 0,
    FORCE_CORRELATION = 1,
    NO_DUPLICATES = 2,
    ALLOW_DUPLICATES = 3,
} 
idx_EnrollMode_t;

typedef enum __idx_MatchMode
{
	MATCH_FIRST = 0,
	MATCH_RANGE,
	MATCH_IDEAL
} idx_MatchMode_t;

typedef enum __idx_PerfCommand
{
    EMK_NO_CMD = 0,
    EMK_INITIALIZE_CMD,
    EMK_RESETENROLL_CMD,
    EMK_EXTRACT_TEMPLATE_CMD,
    EMK_ADD_IMAGE_CMD,
    EMK_GET_TEMPLATE_CMD,
    EMK_MATCH_IMAGE_CMD,
    EMK_MATCH_TEMPLATE_CMD,
    EMK_PUT_TEMPLATE_CMD,
} 
idx_PerfCommand_t;

#ifndef __GNUC__
#pragma pack(pop)
#endif

#endif //__IDX_EMK_BASE_DEFINES_H__
