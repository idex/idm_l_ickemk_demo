/** ***************************************************************************
 * \file   idxHalSPI.c
 * \brief  Implementation of SPI data transfer HAL API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#include "halglobal.h"
 
#include "idxSerialinterface.h"
#include "idxHalSpi.h"

/* defines
****************************************************************/
#define UNUSED(p) ((p)=(p))

// SPI clock divider
#define IDEX_fsys_div_2     0
#define IDEX_fsys_div_4     1
#define IDEX_fsys_div_8     2
#define IDEX_fsys_div_16    3
#define IDEX_fsys_div_32    4
#define IDEX_fsys_div_64    5
#define IDEX_fsys_div_128   6
#define IDEX_fsys_div_256   7

#define IDEX_SPI_MODE_0     0
#define IDEX_SPI_MODE_1     1
#define IDEX_SPI_MODE_2     2
#define IDEX_SPI_MODE_3     3
#define IDEX_SPI_MODE       IDEX_SPI_MODE_0

#define RAM0_DMA_REGION_START ((uint8_t *) 0x20002200)
#define RAM0_DMA_REGION_END   ((uint8_t *) 0x20002700)


#define DEMO_SET_BIT(x,y)       (x |= (0x01UL <<y))
// GPIO pin definitions/
#define IDEX_GPIO0              0x01	
#define IDEX_GPIO1              0x02	
#define IDEX_GPIO2              0x04
#define IDEX_GPIO3              0x08
#define IDEX_GPIO4              0x10

#define IDEX_SPI_CLK            IDEX_GPIO0	
#define IDEX_SE_HSO             IDEX_GPIO0	
#define IDEX_SPI_MOSI           IDEX_GPIO2		
#define IDEX_SE_HSI             IDEX_GPIO1	
#define IDEX_SPI_MISO           IDEX_GPIO3
#define IDEX_ISO_IO             IDEX_GPIO4	


/**********************************************************************************
 *  HAL SPI - Implementation
 *
 ***********************************************************************************/

static idxHalSpiSpeedType spiSpeed;


extern void TMC_GPIO_SetOPDir(unsigned char dir);
extern void TMC_GPIO_Write(unsigned int data);


//////////////////////////////////
// Interface functions
//////////////////////////////////
void idxHalSpiInit(void)
{
	DEMO_SET_BIT(CLKIOCON,3);	   // Enable GPIO clock
	TMC_GPIO_SetOPDir( IDEX_SE_HSI );  // HSO direction = O/P
	TMC_GPIO_Write(IDEX_SE_HSI);		 

	SPI_MasterInit(IDEX_fsys_div_8, (IDEX_SPI_MODE & 0x03));
}

void idxHalSpiConfigure(void)
{
  uint8_t speed = IDEX_fsys_div_32; // With 24MHz sysclk gives 0.75 MHz
  if (spiSpeed == IDX_HAL_SPI_SPEED_HIGH) {
    speed = IDEX_fsys_div_4;        // With 24MHz sysclk gives 6.00 MHz
  }
  SPI_MasterInit(speed, (IDEX_SPI_MODE & 0x03));
}

void idxHalSpiTx(const uint8_t *pBuf, uint16_t len)
{
#ifdef SPIDMAEN
// If len is only 1 byte there's no point setting up a DMA,
// and if the buffer is outside of the legal range for DMA
// access then it's impossible.
	if ((len > 1) &&
	    ( pBuf        >= RAM0_DMA_REGION_START) &&
	    ((pBuf + len) <= RAM0_DMA_REGION_END  ))
	{
		SPI_TxNBytes((uint8_t *)pBuf, len); // Transmit at high speed
	}
	else
#endif
	{
		uint16_t  i;

		for(i=0;i<len;i++)
		{
			SPI_TxByte(pBuf[i]);
		}
	}
}

void idxHalSpiRx(uint8_t *pBuf, uint16_t len)
{
#ifdef SPIDMAEN
// If len is only 1 byte there's no point setting up a DMA,
// and if the buffer is outside of the legal range for DMA
// access then it's impossible.
	if ((len > 1) &&
	    ( pBuf        >= RAM0_DMA_REGION_START) &&
	    ((pBuf + len) <= RAM0_DMA_REGION_END  ))
	{
		SPI_RxNBytes(pBuf, len); // Receive at high speed
	}
	else
#endif
	{
		uint16_t  i;

		for(i=0;i<len;i++)
		{
			pBuf[i] = SPI_RxByte();
		}
	}
}

void idxHalSpiSetSpeed(idxHalSpiSpeedType speed)
{
  spiSpeed = speed;
}
