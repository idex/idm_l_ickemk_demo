/*************************************************************************************
* \file  idxSleep.h
* \brief header for delay routines
* \author IDEX ASA
* \version 0.0.0
**************************************************************************************
* Copyright
* 2013-2016 IDEX ASA. All Rights Reserved.www.idex.no [Date of copyright notice should
* start from the year the code was first created and end with current year.]
*
* IDEX ASA is the owner of this software and all intellectual property rights in
* and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for any
* claim by any third party, arising from use of this software.
*.
**************************************************************************************/

#ifndef __IDX_SLEEP_H__
#define __IDX_SLEEP_H__

#include <stdint.h>
#include "Compatibility.h"

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(EMBEDDED_IDXSERIAL) || defined(STM32F4XX)
	void initSleepCnt( uint32_t cnt );
	void Sleep( uint32_t ms );
	void uSleep( uint32_t us );
#else
#if !defined(WIN32) && !defined(_WIN32_WCE)
	void Sleep( DWORD ms ) ;
#endif
	void uSleep( uint32_t us ) ;
#endif

#ifdef __cplusplus
}
#endif

#endif // __IDX_SLEEP_H__
