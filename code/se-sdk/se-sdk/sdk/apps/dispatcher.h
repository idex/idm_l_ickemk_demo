#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <stdint.h>
#include "idxSeSdkTypes.h"

uint8_t *apdu_handler( const idxBuffer_t *pCommsBuffer );

const struct idxBioSystemConfig_s *get_SystemConfig(void);
uint8_t get_p2( void );
void set_p2(uint8_t n);
uint8_t get_lc( void );

#endif

