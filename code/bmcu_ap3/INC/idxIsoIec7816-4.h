/** ***************************************************************************
 * \file   idxIsoIec7816-4.h
 * \brief  Declaration of error codes defined by ISO/IEC 7816-4
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *
 * This file defines the various error and warning codes that may be returned
 * (in SW1/SW2) by an APDU. All symbols are prefixed with "IDX" to avoid
 * possible (even likely) name clashes with symbols in the card operating
 * system into which the SDK is being integrated.
******************************************************************************/
#ifndef _IDX_ISO_IEC_7816_4_H_
#define _IDX_ISO_IEC_7816_4_H_

/** \name Normal processing
*
* 61XX - SW2 encodes the number of bytes still available
*
*/
/**@{*/
#define IDX_ISO_SUCCESS                   0x9000 //!< No further qualification
/**@}*/

/** \name Warning processing: Non-volatile memory unchanged
*
* 62XX - State of non-volatile memory is unchanged
*
* 6202 to 6280 - Triggering by card
*/
/**@{*/

#define IDX_ISO_WARNING_NVM_UNCHANGED     0x6200 //!< No information given
#define IDX_ISO_PART_CORRUPTED            0x6281 //!< Part of returned data may be corrupted
#define IDX_ISO_UNEXPECTED_EOF            0x6282 //!< End of file or record reached before reading Ne bytes, or unsuccessful search
#define IDX_ISO_FILE_DEACTIVATED          0x6283 //!< Selected file deactivated
#define IDX_ISO_FORMAT_ERROR              0x6284 //!< File or data control information not formatted correctly
#define IDX_ISO_FILE_TERMINATION          0x6285 //!< Selected file in termination state
#define IDX_ISO_SENSOR_UNAVAILABLE        0x6286 //!< No input data available from a sensor on the card
#define IDX_ISO_RECORD_DEACTIVATED        0x6287 //!< At least one of the referenced records is deactivated
/**@}*/

/** \name Warning processing: State of non-volatile memory may have changed
*
* 63XX - State of non-volatile memory may have changed
*/
/**@{*/
#define IDX_ISO_WARNING_NVM_CHANGED       0x6300 //!< No information given
#define IDX_ISO_UNSUCCESSFUL_COMPARE      0x6340 //!< Unsuccessful comparison 
#define IDX_ISO_WRITE_FILLED_FILE         0x6381 //!< File filled by last write
#define IDX_ISO_COUNTER_X                 0x63C0 //!< Counter has value X
#define IDX_ISO_COUNTER_X_MASK            0xFFF0 //!< Mask for \ref IDX_ISO_COUNTER_X
/**@}*/

/** \name Execution error: Non-volatile memory unchanged
*
* 64XX - State of non-volatile memory is unchanged
*
* 6402 to 6480 - Triggering by card
*/
/**@{*/
#define IDX_ISO_ERROR_NVM_UNCHANGED      0x6400 //!< No information given
#define IDX_ISO_IMMEDIATE_RESPONSE       0x6401 //!< Immediate response required by the card
#define IDX_ISO_LCHAN_SHARE_DENIED       0x6481 //!< Logical channel shared access denied
#define IDX_ISO_LCHAN_OPEN_DENIED        0x6482 //!< Logical channel opening denied
/**@}*/

/** \name Execution error: State of non-volatile memory may have changed
*
* 65XX - State of non-volatile memory may have changed
*/
/**@{*/
#define IDX_ISO_ERROR_NVM_CHANGED        0x6500 //!< No information given
#define IDX_ISO_MEMORY_FAILURE           0x6581 //!< Memory failure
/**@}*/

/** \name Execution error: Security-related issues
*
* 66XX - Security-related issues
*/
/**@{*/
#define IDX_ISO_ERROR_SECURITY           0x6600 //!< No information given - other values RFU
/**@}*/

/** \name Checking error: Wrong length
*
* 67XX, except 6700, 6701, and 6702 - Proprietary
*
* Values other than 6700, 6701, and 6702 are proprietary
*/
/**@{*/
#define IDX_ISO_ERROR_LENGTH             0x6700 //!< No information given
#define IDX_ISO_APDU_FORMAT              0x6701 //!< Command APDU format not compliant with standard
#define IDX_ISO_LC_UNEXPECTED            0x6702 //!< The value of Lc is not the one expected
/**@}*/

/** \name Checking error: Functions in CLA not supported
*
* 68XX - Functions in CLA not supported
*/
/**@{*/
#define IDX_ISO_ERROR_CLA_FUNC_NOT_SUPP  0x6800 //!< No information given
#define IDX_ISO_LOG_CHAN_NOT_SUPPORTED   0x6881 //!< Logical channel not supported
#define IDX_ISO_SEC_MSG_NOT_SUPPORTED    0x6882 //!< Secure messaging not supported
#define IDX_ISO_LAST_CMD_CHAIN_EXPECTED  0x6883 //!< Last command of the chain expected
#define IDX_ISO_CMD_CHAIN_NOT_SUPPORTED  0x6884 //!< Command chaining not supported
/**@}*/

/** \name Checking error: Command not allowed
*
* 69XX - Command not allowed
*/
/**@{*/
#define IDX_ISO_ERROR_CMND_NOT_ALLOWED   0x6900 //!< No information given
#define IDX_ISO_INCOMPATIBLE_STRUCT      0x6981 //!< Command incompatible with file structure
#define IDX_ISO_SECURITY_NOT_SATISFIED   0x6982 //!< Security status not satisfied
#define IDX_ISO_AUTHENTICATION_BLOCKED   0x6983 //!< Authentication method blocked
#define IDX_ISO_REFERENCE_DATA_UNUSABLE  0x6984 //!< Reference data not usable
#define IDX_ISO_CONDITIONS_NOT_SATISFIED 0x6985 //!< Conditions of use not satisfied
#define IDX_ISO_CMND_NOT_ALLOWED_NO_EF   0x6986 //!< Command not allowed (no current EF)
#define IDX_ISO_SEC_MSG_EXPCT_DO_MISSING 0x6987 //!< Expected secure messaging DOs missing
#define IDX_ISO_INCORRECT_SEC_MSG_DO     0x6988 //!< Incorrect secure messaging DOs
/**@}*/

/** \name Checking error: Wrong parameters P1-P2
*
* 6AXX - Wrong parameters P1-P2
*/
/**@{*/
#define IDX_ISO_ERROR_WRONG_PARAMS_P1P2  0x6A00 //!< No information given
#define IDX_ISO_BAD_PARAM_CMD_DATA       0x6A80 //!< Incorrect parameters in the command data field
#define IDX_ISO_FUNCTION_NOT_SUPPORTED   0x6A81 //!< Function not supported
#define IDX_ISO_FILE_OR_APP_NOT_FOUND    0x6A82 //!< File or application not found
#define IDX_ISO_RECORD_NOT_FOUND         0x6A83 //!< Record not found
#define IDX_ISO_NOT_ENOUGH_MEM_IN_FILE   0x6A84 //!< Not enough memory space in the file
#define IDX_ISO_NC_INCONSISTENT_TLV      0x6A85 //!< Nc inconsistent with TLV structure
#define IDX_ISO_INCORRECT_PARAMS_P1P2    0x6A86 //!< Incorrect parameters P1-P2
#define IDX_ISO_NC_INCONSISTENT_P1P2     0x6A87 //!< Nc inconsistent with parameters P1-P2
#define IDX_ISO_REF_DATA_OR_NOT_FOUND    0x6A88 //!< Referenced data or reference data not found
#define IDX_ISO_FILE_EXISTS              0x6A89 //!< File already exists
#define IDX_ISO_DF_NAME_EXISTS           0x6A8A //!< DF name already exists
/**@}*/

/** \name Checking error: Wrong parameters P1-P2
*
* 6BXX, except 6B00 - Proprietary
*/
/**@{*/
#define IDX_ISO_WRONG_PARAMETERS_P1P2    0x6B00 //!< Wrong parameters P1-P2
/**@}*/

/** \name Checking error: Wrong Le field
*
* 6CXX - Wrong Le field; SW2 encodes the exact number of available data bytes
*/
/**@{*/
#define IDX_ISO_WRONG_LE_FIELD_XX        0x6C00 //!< Wrong Le field, XX encodes number of bytes
#define IDX_ISO_WRONG_LE_FIELD_XX_MASK   0xFF00 //!< Mask for \ref IDX_ISO_WRONG_LE_FIELD_XX
/**@}*/

/** \name Checking error: Instruction code not supported or invalid
*
* 6DXX, except 6D00 - Proprietary
*/
/**@{*/
#define IDX_ISO_INSTR_CODE_NOT_SUPP      0x6D00 //!< Instruction code not supported 
/**@}*/

/** \name Checking error: Class not supported
*
* 6EXX, except 6E00 - Proprietary
*/
/**@{*/
#define IDX_ISO_CLASS_NOT_SUPPORTED      0x6E00 //!< Class not supported 
/**@}*/

/** \name Checking error: No precise diagnosis
*
* 6FXX, except 6F00 - Proprietary
*/
/**@{*/
#define IDX_ISO_NO_PRECISE_DIAGNOSIS     0x6F00 //!< No precise diagnosis
/**@}*/

#endif /* _IDX_ISO_IEC_7816_4_H_ */
