#ifndef __SYSCLOCKLIB__
#define __SYSCLOCKLIB__

/*
* Function: void clkInit(unsigned char CTclk, unsigned char minFS);
* Description: clock init.
* Input: CTclk - CT clock init
*		 minFS - minimum field strength
* Output: none
* Return: none
* Other:
*/
void clkInit(unsigned char CTclk, unsigned char minFS);

/*
* Function: void RFIdleMode(u8 RFclk);
* Description: clock init.
* Input: RFclk - RF clock 
* Output: none
* Return: none
* Other:
*/
void RFIdleMode(unsigned char RFclk);

/*
* Function: void M1IdleMode(u8 RFclk);
* Description: clock init.
* Input: RFclk - RF clock 
* Output: none
* Return: none
* Other:
*/
void M1IdleMode(unsigned char RFclk);

/*
* Function: void clkConfig(u8 sysclk);
* Description: clock init.
* Input: sysclk - system clock 
* Output: none
* Return: none
* Other:
*/
void clkConfig(unsigned char sysclk);
/*
 * version[0]: main version
 * version[1]: sub version
 * Such as V1.20, version[2] = {0x01, 0x20}
 */
void GetSystemClockLibVersion(unsigned char *version);

#endif
