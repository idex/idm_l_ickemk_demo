/** ***************************************************************************
 * \file   bio-ctrl.c
 * \brief  Code for testing SE EMK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2013-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "idxSerialHostInterface.h"
#include "idxSeEmkAPI.h"

#include "bio-ctrl.h"

#include "idxHalFlash.h"
#include "idxHalUart.h"
#include "idxHalSystem.h"
#include "idxHalSpi.h"

uint8_t ctrlErr;
static uint8_t replyDataOffset;

#ifdef TH89
#include "dispatcher.h"
#include "halglobal.h"      // for __WFI  definition
#endif

#include "crypto.h"

#define HDR_NO_CRC_SIZE 4
#define CMAC_SIZE 8
#define BOPTION_SIGNED    (DATA_SIGNED)



extern uint16_t idxBioDrvComms_apollo3(const idxBuffer_t *pCommsBuffer);

void idxBioCtrlInit()
{
  ctrlErr = 0xff;
  replyDataOffset = 0;
}

uint16_t idxMcuCommsFull(uint8_t            bOrdinal,
                         uint8_t            bOptions,
                         uint16_t          *pDataSize,
                         const idxBuffer_t *pCommsBuffer,
                         uint16_t          *pwCode,
                         const idxBuffer_t *pRespDataBuf)		
{
  uint16_t ret;
  uint16_t rDataSize;
  uint16_t wCode = IDX_ISO_SUCCESS;
  const idxBuffer_t *pEncryptedData = NULL; 

  if(session.state == SEC_CH_OPEN) { // Secure channel is open
    session.msgCounter++;
    bOptions |= (DATA_SIGNED | DATA_CRYPTED);
  }

  // Encrypt data
  *pDataSize = dataEncrypt(pCommsBuffer, *pDataSize, bOptions, &pEncryptedData);

  // Sign Data
  if(bOptions & BOPTION_SIGNED)
  {
    // sign data in communication buffer
    sign(bOrdinal, bOptions, &pEncryptedData->pBuffer[HDR_SIZE], *pDataSize);
    memcpy(&pEncryptedData->pBuffer[HDR_SIZE + *pDataSize], session.macChain, CMAC_SIZE);
    *pDataSize += CMAC_SIZE;
  }

  // Build Command Header
  // Note The Command Data is already in the buffer
  WR_U16(&pEncryptedData->pBuffer[0], HDR_NO_CRC_SIZE + *pDataSize); // wSize
  pEncryptedData->pBuffer[2] = bOrdinal;
  pEncryptedData->pBuffer[3] = bOptions;

  // Send Command = {Hdr + Data}
  ret = idxBioDrvComms_apollo3(pEncryptedData);

  // Return SE error if communication failed
  if (ret != IDX_ISO_SUCCESS) {
    return ret;
  }
  
  // Retrieve relevant info from Reply message = {Hdr + Data}
  rDataSize = RD_U16(&pEncryptedData->pBuffer[0]); //wSize
  if ( NULL != pwCode) {
    *pwCode = RD_U16(&pEncryptedData->pBuffer[2]); //wCode
  } else {
  // If pwCode is NULL "fold" the MCU response wCode into the return value
    wCode = RD_U16(&pEncryptedData->pBuffer[2]); //wCode
  }
  
  // Clear the first six bytes of pEncryptedData before returning
  // The caller should use pwCode and pDataSize
  memset(&pEncryptedData->pBuffer[0], 0, HDR_SIZE);
  
  // Guard against (wSize - HDR_NO_CRC_SIZE) overflowing to a large positive value
  rDataSize = (rDataSize >= HDR_NO_CRC_SIZE) ? (rDataSize - HDR_NO_CRC_SIZE) : 0; // data size

  // Drop cmac
  if(bOptions & BOPTION_SIGNED) {
    uint8_t cmac[CMAC_SIZE];
    memcpy(cmac, &pEncryptedData->pBuffer[HDR_SIZE], CMAC_SIZE); // save for matching
    rDataSize = (rDataSize >= CMAC_SIZE) ? (rDataSize - CMAC_SIZE) : 0; //  drop cmac from received response
  }

  // Decrypt data
  rDataSize = dataDecrypt( pEncryptedData,
                           rDataSize,
                           bOptions,
                           pCommsBuffer,
                           pRespDataBuf );

  // Only data size, doesn't include header size
  *pDataSize = rDataSize;

  return wCode;
}

uint16_t idxMcuComms(uint8_t            bOrdinal,
                     uint8_t            bOptions,
                     uint16_t          *pDataSize,
                     const idxBuffer_t *pCommsBuffer)
{
  return idxMcuCommsFull(bOrdinal, bOptions, pDataSize, pCommsBuffer, NULL, NULL);
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Sleeve-IDM (enroll) cmds test entry poin - TH89
 * This should be moved to IDM SE EMK
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

void setReplyData(uint8_t *buf, uint8_t bsize, const idxBuffer_t *pCommBuff)
{
  uint16_t i;
  uint8_t *pBuffer = pCommBuff->pBuffer;
  uint16_t bufferSize = pCommBuff->bufferSize;
  uint8_t *dbuf = &pBuffer[HDR_SIZE];
  
  if ((HDR_SIZE + bsize) > bufferSize)
    return; // TODO, add error code later
  
  WR_U16(&pBuffer[0], HDR_SIZE + bsize); // wSize
  for (i = 0 ; i < bsize ; i++) dbuf[i] = buf[i];
  replyDataOffset = bsize;
}
void setErrorData(uint8_t *buf, uint8_t bsize, const idxBuffer_t *pCommBuff)
{
  uint16_t i;
  uint8_t *pBuffer = pCommBuff->pBuffer;
  uint16_t bufferSize = pCommBuff->bufferSize;
  uint8_t *dbuf = &pBuffer[HDR_SIZE];
  
  if ((HDR_SIZE + bsize) > bufferSize)
    return; // TODO, add error code later
  
  WR_U16(&pBuffer[0], HDR_SIZE + bsize); // wSize
  WR_U16(&pBuffer[2], 0x6300); // wCode
  for (i = 0 ; i < bsize ; i++) dbuf[i] = buf[i];
  replyDataOffset = bsize;
}
void addReplyData(uint8_t *buf, uint8_t bsize, const idxBuffer_t *pCommBuff)
{
  uint16_t i;
  uint8_t *pBuffer = pCommBuff->pBuffer;
  uint16_t bufferSize = pCommBuff->bufferSize;
  uint8_t *dbuf = &pBuffer[HDR_SIZE + replyDataOffset];
  
  uint16_t old_wSize = RD_U16(&pBuffer[0]);
  
  if ((old_wSize + bsize) > bufferSize)
    return; // TODO, add error code later
  
  WR_U16(&pBuffer[0], old_wSize + bsize); // wSize
  for (i = 0 ; i < bsize ; i++) dbuf[i] = buf[i];
  replyDataOffset += bsize;
}


#ifdef TH89
void posVerify(const idxBuffer_t *pCommsBuffer)
{
	static const uint8_t initBuff[]       = {0x00,0x54,0x00,0x00,0x01,0x03}; // size = header(5 byte) + data(1 byte). data -> mode = CONTACT-LESS 
  static const uint8_t idmverifyBuff[]  = {0x00,0x54,0x88,0x00,0x00};  // size = header(5 byte)

  memcpy(pCommsBuffer->pBuffer, initBuff, sizeof(initBuff));
  apdu_handler(pCommsBuffer);

  memcpy(pCommsBuffer->pBuffer, idmverifyBuff, sizeof(initBuff));
  apdu_handler(pCommsBuffer);  	
}

void sleeveIdmEnrol(const idxBuffer_t *pCommsBuffer)
{
  uint16_t *pErrCode;
  
	static const uint8_t initBuff[]       = {0x00,0x54,0x00,0x00,0x01,0x06}; // size = header(5 byte) + data(1 byte). data -> mode = SLEEVE  
	static const uint8_t uninitBuff[]     = {0x00,0x54,0x01,0x00,0x00};  // size = header(5 byte)
	static const uint8_t dmenrolx1Buff[]  = {0x00,0x54,0x8b,0x26,0x00}; // size = header(5 byte)
  static const uint8_t openscp03Buff[]  = {0x00,0x54,0xdd,0x00,0x00}; // size = header(5 byte)
  
	static const uint8_t setParam07Buff[]  = {0x00,0x54,0x09,0x00,0x03,0x07,0x10,0x27}; // size = header(5 byte) + data(3 byte). data -> Set timeout to 10 second 0x2710
	static const uint8_t setParam20Buff[]  = {0x00,0x54,0x09,0x00,0x03,0x20,0x00,0x00}; // size = header(5 byte) + data(3 byte). data -> Switch off BoD (set to 0x00)
	static const uint8_t setParam23Buff[]  = {0x00,0x54,0x09,0x00,0x03,0x23,0xd2,0x80}; // size = header(5 byte) + data(3 byte). data -> Change Power Probe mask 2 to 0x80D2
	static const uint8_t setParam2bBuff[]  = {0x00,0x54,0x09,0x00,0x03,0x2b,0x00,0x00}; // size = header(5 byte) + data(3 byte). data -> Set Vsense to default value 

 	IOPULLSEL0 |= 0x7f;		     //set 7816clk and gpio0,1 pullup	set PU enable 
	IOPULLSEL2 = 0;	     //set PU_1 disable

  do {

    memcpy(pCommsBuffer->pBuffer, setParam2bBuff, sizeof(setParam2bBuff));
    pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);   // test MCU mode - plain or scp03 
    if(pErrCode[1] != 0x9000)
    { 
      if(pErrCode[1] == 0x6986) // this error means : scp03 in use
      {
        memcpy(pCommsBuffer->pBuffer, openscp03Buff, sizeof(openscp03Buff));
        pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);  
        if(pErrCode[1] != 0x9000) break;
        
        memcpy(pCommsBuffer->pBuffer, setParam2bBuff, sizeof(setParam2bBuff));
        pErrCode = (uint16_t *)apdu_handler(pCommsBuffer); // re-set Vsense-min 
        if(pErrCode[1] != 0x9000) break;
      }
      else break;
    }
    memcpy(pCommsBuffer->pBuffer, initBuff, sizeof(initBuff));
    pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);
    if(pErrCode[1] != 0x9000) break;
    
    
    //pErrCode = (uint16_t *)apdu_handler(&openscp03);  
    //if(pErrCode[1] != 0x9000) return;

    memcpy(pCommsBuffer->pBuffer, setParam20Buff, sizeof(setParam20Buff));
    pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);
    if(pErrCode[1] != 0x9000) break;
    memcpy(pCommsBuffer->pBuffer, setParam23Buff, sizeof(setParam23Buff));
    pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);
    if(pErrCode[1] != 0x9000) break;
    memcpy(pCommsBuffer->pBuffer, setParam07Buff, sizeof(setParam07Buff));
    pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);
    if(pErrCode[1] != 0x9000) break;

  //apdu_handler(&setParam87);

    memcpy(pCommsBuffer->pBuffer, dmenrolx1Buff, sizeof(dmenrolx1Buff));
    pErrCode = (uint16_t *)apdu_handler(pCommsBuffer);
    if(pErrCode[1] != 0x9000) break;
       
  }while(0);
    
  memcpy(pCommsBuffer->pBuffer, uninitBuff, sizeof(uninitBuff));
  apdu_handler(pCommsBuffer);
     
}
#endif
//-----------------------------  end of file ------------------------------------
