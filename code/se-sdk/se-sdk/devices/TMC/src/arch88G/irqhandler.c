/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: irqhandler.c
 * * SCFID: 
 * * Feature: funcitons of interrupt
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-11-18
 * *     1. Original version 0.1
 * */
#include "halglobal.h"
/*
* Function: void NVM0_IRQHandler(void)
* Description: nvm systick interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void NVM0_IRQHandler(void)
{	
	NVMSTS1 = 0xFF;

	if(NVMSTS & 0xfe)
	{
		NVMSTS = 0xff;
		NVMSTS1 = 0xff;
		__disable_irq();
		while(1);
	}
	else
	{
		NVMSTS = 0x01;
	}

}

/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: Algorithm systick interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void ALGORITHM_IRQHandler(void)
{

}

/*
* Function: void SECURITY_IRQHandler(void)
* Description: Security interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void SECURITY_IRQHandler(void)
{
	//SUStopChip();
  *(unsigned long volatile *)0x40010054L = 0xFFFFFFFFL;	
}

/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: Timer1 systick interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void TIMER1_IRQHandler(void)
{
  void t1IrqHandler(void);
  t1IrqHandler();
}

/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: Timer2 systick interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void TIMER2_IRQHandler(void)
{
  void t2IrqHandler(void);
  t2IrqHandler();
}

/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: GPIO interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
typedef void TMC_Gpio_Callback_Type(void *param);
static TMC_Gpio_Callback_Type *gpio_callback = (TMC_Gpio_Callback_Type *) NULL;

void GPIO_IRQHandler(void)
{
	if(gpio_callback)
		gpio_callback(NULL);
}

/**
* GPIO listener for callback
*
**/
void TMC_Gpio_Register_Callback(TMC_Gpio_Callback_Type *cb) {
	gpio_callback = cb;
}

/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: hardfault interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void HardFault_IRQHandler(void)
{
	while (1);
}

/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: NMI interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/
void NMI_IRQHandler(void)
{
//	SUStopChip();	
}


/*
* Function: void ALGORITHM_IRQHandler(void)
* Description: systick interrupt service routine.
* Input: - 
* Output: -
* Return: - 
*/


/*
* Function: static void EnableInterrupt(IRQn_Type IRQ_num)
* Description: enable interrupt.
* Input: IRQ_num - type of interrupt
* Output: -
* Return: - 
*/
static void EnableInterrupt(IRQn_Type IRQ_num)
{
	switch(IRQ_num)
	{
		case CT_IRQn: 			INTIOCON |= 0x01;		break;
		case TIMER0_IRQn: 		INTTRCON |= 0x01;		break;
		case NVM0_IRQn: 		INTMEMCON |= 0x01;		break;
		case ALGORITHM_IRQn: 	INTALGCON |= 0xffffffff;	break;
		case APP_IRQn: 			INTSWICON |= 0xffffffff;	break;
		case TIMER1_IRQn:		INTTRCON |= 0x02;		break;
		case TIMER2_IRQn:		INTTRCON |= 0x04;		break;
		case SPI_IRQn: 			INTIOCON |= 0x04;		break;
		case GPIO_IRQn:			INTIOCON |= 0x08;		break;
		case RF_IRQn:			INTIOCON |= 0x10;		break;
		default: break;
	}
}

/*
* Function: static void DisableInterrupt(IRQn_Type IRQ_num)
* Description: diaable interrupt.
* Input: IRQ_num - type of interrupt
* Output: -
* Return: - 
*/
static void DisableInterrupt(IRQn_Type IRQ_num)
{
	switch(IRQ_num)
	{
		case CT_IRQn: 			INTIOCON &= ~0x01;		break;
		case TIMER0_IRQn: 		INTTRCON &= ~0x01;		break;
		case NVM0_IRQn: 		INTMEMCON &= ~0x01;		break;
		case ALGORITHM_IRQn: 	INTALGCON &= ~0xffffffff;	break;
		case APP_IRQn: 			INTSWICON &= ~0xffffffff;	break;
		case TIMER1_IRQn:		INTTRCON &= ~0x02;		break;
		case TIMER2_IRQn:		INTTRCON &= ~0x04;		break;
		case SPI_IRQn: 			INTIOCON &= ~0x04;		break;
		case GPIO_IRQn:			INTIOCON &= ~0x08;		break;
		case RF_IRQn:			INTIOCON &= ~0x10;		break;
		default: break;
	}
}

/*
* Function: void CPUWaitForModuleFinish(IRQn_Type IRQ_num)
* Description: wait for the module finish, choose event mode or interrupt mode.
* Input: IRQ_num - type of interrupt
* Output: -
* Return: - 
*/
void CPUWaitForModuleFinish(IRQn_Type IRQ_num)
{
	u32 bakISER;	
	bakISER= NVIC->ISER[0];		 //backup the interrput enable flag	
	NVIC->ICER[0] = 0xFFFFFF;	 //disable all interrupt

	//interrupt mode
	EnableInterrupt(IRQ_num);
	NVIC_ClearPendingIRQ(IRQ_num);
	NVIC_EnableIRQ(IRQ_num);
	__SEV();
	__WFI();
	NVIC_DisableIRQ(IRQ_num);	
	DisableInterrupt(IRQ_num);	
	NVIC->ISER[0] = bakISER;	  //set interrupt enable flag to the backup value
}
