/*
 *  Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: SPI.c
 *  SCFID: 
 *  Feature: SPI process of PICC
 *  Version: V0.1
 * 
 *  History: 
 *    2017-09-10 by GaoY
 *      1. Original version 0.1
 * */
#include "halglobal.h"


uint8_t  SPIMasterEn;

/**
  *@brief  SPI master init.
  *@param  spiMode:SPI mode select,SQI SDI or normal mode.
  *@param  clkMode:Define clock phase plarity. 
  *@retval  None.           
  */
#define S7816_SIO_GPIO4_MASK ( 0x001c0 )
void SPI_MasterInit(uint8_t speed, uint8_t clkMode)
{
  u32 iosel;
  SPIMasterEn = 1;

  CLKIOCON  |= 0x04;       /* Enable SPI clock */
  SPIMSK    |= 0x01;       /* Mask BOVER interrupt */
  SPIDMAMSK  = 0x03;       /* Mask BOVER OVER DMA interrupt */  
  SPICON2    = 0x00;          /* Clear first */
  SPICON2    = (((speed & 0x07) << 2) | clkMode);      
  SPICON2   |= 0x000000C0;   /* Master mode,enable*/
  iosel      = IOSEL0;
  IOSEL0     = (iosel & S7816_SIO_GPIO4_MASK) | 0x48200; // MISO,MOSI,GPIO1,CLK,XXX
//  IOSEL0 =0x48200;        /* SPI mode*/
}
/**
  *@brief  SPI slave init.
  *@param  clkMode:Define clock phase plarity. 
  *@retval  None.           
  */
void SPI_SlaveInit(uint8_t clkMode)
{
  u32 iosel;
  SPIMasterEn = 0;
  CLKIOCON |= 0x04;          /* Enable SPI clock */
  iosel   = IOSEL0;
  IOSEL0  = (iosel & S7816_SIO_GPIO4_MASK) | 0x49200; // MISO,MOSI,SSN,CLK, XXX
//  IOSEL0 =0x49200;            /*SPI mode*/
  SPIMSK |= 0x01;          /* Mask BOVER interrupt */
  SPIDMAMSK = 0x03;          /* Mask BOVER OVER DMA interrupt */      
//  SPIRESUMEMSK &=  Bit0_Dis;        /* Enable slave mode ssn wakeup function */
  SPICON2 = (0x80|clkMode);        /* Slave mode,enable,clkMode defines clock��phase ��plarity */
}

/**
  *@brief  SPI receive one byte.
  *@param  None. 
  *@retval  None.
  */
uint8_t SPI_RxByte(void)
{
  if(SPIMasterEn)
  {
    SPIDAT = 0xFF;          /* As master,send data 0xFF to start clock. */
  }
  while(!(SPISTS&0x01));      /* Wait BOVER be set, receive over. */
  return SPIDAT;            /* Return data and clear flag. */
}

/**
  *@brief  SPI transmit one byte.
  *@param  data:Data to transmit. 
  *@retval  None.
  */
void SPI_TxByte(uint8_t data)
{
  SPIDAT = data;             /* Send data */
  while(!(SPISTS&0x01));      /* Wait BOVER be set, transmit over. */ 
  data = SPIDAT;            /* Clear flag. */
}
#ifdef SPIDMAEN
/**
  *@brief  SPI DMA receive,working in half-duplex.
  *@param  buf:Receive data buffer. 
  *@param  len:Data length.
  *@retval  None.
  */
void  SPI_DmaRx(uint8_t * buf,uint16_t len)
{
  SPIDMALEN  = len;            /* Length of data to be received. */
  SPIRADR = (uint32_t)(buf);   
  SPIDMACON &= 0xfffffffd;        /* Receive only. */  
  SPIDMACON |= 0x04;
  SPIDMACON |= 0x01;        /* Start DMA receive. */
  while(!(SPIDMASTS & 0x01));     /* Waiting for DMA operation accomplish. */
  SPIDMASTS |= 0x01;        /* Clear flag. */
}

/**
  *@brief  SPI DMA transmit,working in half-duplex.
  *@param  buf:Transmit data buffer. 
  *@param  len:Data length.
  *@retval  None.
  */
void SPI_DmaTx(uint8_t * buf,uint16_t len)
{
  SPIDMALEN  = len;          /* Length of data to be transmitted. */
  SPITADR = (uint32_t)(buf);
  SPIDMACON &= 0xfffffffb;         /* Transmit only. */      
  SPIDMACON |= 0x02;
  SPIDMACON |= 0x01;        /* Start DMA transmit. */
  while(!(SPIDMASTS & 0x01));      /* Waiting for DMA operation accomplish. */
  SPIDMASTS |= 0x01;        /* Clear flag. */
}

/**
  *@brief  SPI DMA transfer.
  *@param  bufTx:Transmit data buffer. 
  *@param  bufRx:Receive data buffer. 
  *@param  len:Data length.
  *@retval  None.
  */
void SPI_DmaTxRx(uint8_t * bufTx,uint8_t * bufRx,uint16_t len)
{
  SPIDMALEN  = len;          /* Length of data to be transmitted. */
  SPITADR = (uint32_t)(bufTx);
  SPIRADR = (uint32_t)(bufRx);
  SPIDMACON |= 0x04;        
  SPIDMACON |= 0x02;
  SPIDMACON |= 0x01;        /* Start DMA transmit. */
  while(!(SPIDMASTS & 0x01));      /* Waiting for DMA operation accomplish. */
  SPIDMASTS |= 0x01;        /* Clear flag. */
}
#endif

/**
  *@brief  SPI receive multi bytes.
  *@param  buf:Receive data buffer. 
  *@param  len:Data length.
  *@retval  None.
  */
void SPI_RxNBytes(uint8_t * buf,uint16_t len)
{     
#ifdef SPIDMAEN
  SPI_DmaRx(buf,len);
#else
  uint16_t  i;
    
  for(i=0;i<len;i++)
  {  
    *(buf+i) = SPI_RxByte();
  }
  
#endif
}

/**
  *@brief  SPI transmit multi bytes.
  *@param  buf:Transmit data buffer. 
  *@param  len:Data length.
  *@retval  None.
  */
void SPI_TxNBytes(uint8_t * buf,uint16_t len)
{   
#ifdef SPIDMAEN  
    SPI_DmaTx(buf,len);
#else
  uint16_t  i;

  for(i=0;i<len;i++)
  {  
    SPI_TxByte(*(buf+i));
  }
  
#endif
}

void SPI_IRQHandler(void)
{
  if(SPIDMASTS&0x01)
  {
    SPIDMASTS = 0xff;  
  }
}


/**
  * @}
  */

/******************* (C) COPYRIGHT 2017 TMC company *****END OF FILE****/





