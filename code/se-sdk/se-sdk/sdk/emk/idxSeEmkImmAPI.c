/** ***************************************************************************
 * \file   idxSeEmkImmApi.c
 * \brief  Implementation of SE Enroll/Match Kit API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2013-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "idxSeEmkAPI.h"
#include "idxSerialInterface.h" // For idxCom
#include "bio-ctrl.h" // For idxMcuComms
#include "idxBaseDefines.h"

#include "idxBioLed.h"

uint8_t *pTest;
uint8_t errPoint;
	
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM ( enrolls & match API ) 
* info struct contains : { bOrdinal, bOptions, data[CL]} CL = command length(bytes)
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
typedef struct { const uint8_t *info; const uint8_t csize;} cmd_t; 

static const uint8_t initc[]          = {0x00,0x00,0x02};
static const uint8_t initcl[]         = {0x00,0x00,0x03};
static const uint8_t uninit[]         = {0x01,0x00};
static const uint8_t deleteRecord[]   = {0x11,0x00,0xff,0xff};
static const uint8_t prepareImage[]   = {0x02,0x00,0x03};
static const uint8_t getTemplate[]    = {0x05,0x00,0x00};
static const uint8_t mergeTemplate[]  = {0x06,0x00,0x03,0x00};
static const uint8_t matchTemplate[]  = {0x07,0x00,0x06,0x00,0x00};
static const uint8_t enrollcmd[]      = {0x17,0x00,0x03,0x03,0x01,0x58,0x01,0x01,0x00,0x59,0x00};
#ifdef SLE78
static const uint8_t storeTemplate[]  = {0x10,0x00,0x01,0x01,0x58,0x01,0x01,0x00,0x59,0x00};
static const uint8_t setParam20[]     = {0x09,0x00,0x20,0x00,0x00}; //   - Switch off BoD (set to 0x00)
static const uint8_t setParam23[]     = {0x09,0x00,0x23,0xd2,0x80}; //  - Change Power Probe mask 2 to 0x80D2
static const uint8_t setParam07[]     = {0x09,0x00,0x07,0x10,0x27}; //  - Set timeout to 10 second 0x2710
static const uint8_t LedBlinkOff[]    = {0x09,0x00,0x87,0x00,0x00};
static const uint8_t LedLastTouch[]   = {0x09,0x00,0x87,0x01,0x00};
static const uint8_t LedTouch[]       = {0x09,0x00,0x87,0x00,0x00};
#endif
//static const uint8_t LedBlinkOn[]     = {0x09,0x00,0x87,0x10,0x88};
static const uint8_t LedBlinkOn[]     = {0x09,0x00,0xb7,0x10,0x88};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Send Commands 
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
static uint16_t sendCmds(cmd_t cmds[], uint8_t numCmds){
  uint8_t i;
  uint16_t wCode;
  for ( i = 0; i < numCmds; i++ )
  { 
		uint8_t bOrdinal = cmds[i].info[0];   
    uint8_t bOptions = cmds[i].info[1];   
		uint8_t csize    = cmds[i].csize - 2;
		const uint8_t *cdata   = csize ? &cmds[i].info[2] : NULL ;
		
		uint16_t data_size = csize;
		memcpy(pDataBuf, cdata, csize);

		wCode = idxMcuComms(bOrdinal, bOptions, &data_size, idxBuffer );  
		if ( wCode != IDX_ISO_SUCCESS ) return wCode;
	}
	return IDX_ISO_SUCCESS;
}	

uint16_t idxSeEmkInit()
{
	errPoint = 0xff;
	return IDX_ISO_SUCCESS;
}

void idxSeEmkGetUnitVersion(idx_UnitFuzeVersion_t* pEmkVersion){
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM (enroll) 2 cmds entry poin
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

uint16_t idxSeEmkMcuEnroll(){
  
  uint16_t wCode;
  cmd_t cmds[] = {
    {initc,     sizeof(initc)}, 
    {enrollcmd, sizeof(enrollcmd)}};

  idxSetConnectionMode(1);
	wCode = sendCmds(cmds, sizeof(cmds)/sizeof(cmds[0]));
	return wCode; 
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM (verify) 4 cmds entry point
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
uint16_t idxSeEmkImmMatch(){
  
  uint16_t wCode;
  cmd_t cmds[] = {
//    {initcl,        sizeof(initcl)},
    {prepareImage , sizeof(prepareImage)},
    {getTemplate,   sizeof(getTemplate)},
    {matchTemplate, sizeof(matchTemplate)}};

  idxSetConnectionMode(1);
	wCode = sendCmds(cmds, sizeof(cmds)/sizeof(cmds[0]));
	return wCode; 
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * IMM (enroll) SE sends multiple cmds + LED control
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define NUM_FINGERS  ( 2 )
#define NUM_TOUCHES  ( 2 )
uint16_t idxSeEmkImmEnroll(){
  
  uint16_t i,j;
  uint16_t wCode;
  cmd_t cmdsProlog[] = {
    {initcl,        sizeof(initcl)}, 
    {deleteRecord , sizeof(deleteRecord)},
	};
  cmd_t cmdsBody[] = {
    {LedBlinkOn,    sizeof(LedBlinkOn)},
    {prepareImage , sizeof(prepareImage)},
    {getTemplate,   sizeof(getTemplate)},
    {mergeTemplate, sizeof(mergeTemplate)},
	};
  cmd_t cmdsPostlog[] = {
		{uninit, sizeof(uninit)},
//    {storeTemplate, sizeof(storeTemplate)}
  };

  wCode = sendCmds(cmdsProlog,sizeof(cmdsProlog)/sizeof(cmdsProlog[0]));
  if ( wCode != IDX_ISO_SUCCESS ) return wCode;

  wCode = idxBioLedSetMode(IDX_CARD_LED_START, idxBuffer);
  if ( wCode != IDX_ISO_SUCCESS ) return wCode;
	
	for ( i = 0; i < NUM_FINGERS; i++ )
  {
		for ( j = 0; j < NUM_TOUCHES; j++ ){
		
			wCode = sendCmds(cmdsBody, sizeof(cmdsBody)/sizeof(cmdsBody[0]));
			if ( wCode != IDX_ISO_SUCCESS ) return wCode;
		}
  }
  wCode = sendCmds(cmdsPostlog,sizeof(cmdsPostlog)/sizeof(cmdsPostlog[0]));
  if ( wCode != IDX_ISO_SUCCESS ) return wCode;

  return IDX_ISO_SUCCESS;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Sleeve-IMM (enroll) cmds test entry poin - SLE78
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
uint16_t idxSeEmkImmSleeveEnroll(){return IDX_ISO_SUCCESS;}
#ifdef SLE78
#define NUM_TOUCHES  ( 6 )
uint16_t sleeveImmEnrol(void){
  
  uint16_t i,j,k;
  uint16_t wCode;
  idx_CommandHeader *pCmdHdr;
  cmd_t cmds[] = {
    {deleteRecord , sizeof(deleteRecord)},
    {setParam20,    sizeof(setParam20)},  
    {setParam23,    sizeof(setParam23)},  
    {setParam07,    sizeof(setParam07)},  
    {prepareImage , sizeof(prepareImage)},
    {getTemplate,   sizeof(getTemplate)},
    {mergeTemplate, sizeof(mergeTemplate)},
    {storeTemplate, sizeof(storeTemplate)}
  };

  idxCom.state->mode = MODE_SLEEVE;  // SLEEVE mode
  configTimerB();
  for(k = 0 ; k < NUM_TOUCHES ; k++){
    uint8_t startIdx = k ? 1 : 0;
    uint8_t lastIdx  = (k == NUM_TOUCHES - 1 ) ? sizeof(cmds)/sizeof(cmds[0]) : sizeof(cmds)/sizeof(cmds[0]) - 1 ;
    for(j = startIdx ; j < lastIdx ; ){
      
      pCmdHdr  = idxCom.state->CommandHeader;

        // Build Command Header
      pCmdHdr->wSize    = sizeof(idx_CommandHeader) + cmds[j].csize - 2;
      pCmdHdr->bOrdinal = cmds[j].info[0]; // bOrdinal
      pCmdHdr->bOptions = cmds[j].info[1]; // bOptions
      for(i = 0; i < cmds[j].csize - 2 ; i++){
        idxCom.state->CommandData[i] = cmds[j].info[i+2];
      }
	  // TODO: update this function to use idxMcuComms(), instead of SendCommand(), in order to use antenna power adjustments
      idxCom.SendCommand(idxCom.state->CommandData);
      wCode = RD_U16(&idxCom.state->ReplyHeader->wCode);
      if(wCode != IDX_ISO_SUCCESS){
        if(wCode == 0x6748){  // timeout case ==> send uninit command and exit
          pCmdHdr->wSize    = sizeof(idx_CommandHeader);
          pCmdHdr->bOrdinal = 0x01; // bOrdinal = uninit
          pCmdHdr->bOptions = 0x00; // bOptions
          idxCom.SendCommand(idxCom.state->CommandData);
          return RD_U16(&idxCom.state->ReplyHeader->wCode);
        }
        if(wCode != 0x6a83)  return wCode;
      }
      else j++;
    }
    blinkCtrl |= 8*4;
    while( blinkCtrl & 0xfc);
    blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch) 
  }
  StopTimer();  
  return IDX_ISO_SUCCESS;
}
#endif

#if 0
/*****************************************************************************
 * IDM (dummy) API calls 
 *
 ******************************************************************************/
uint16_t idxSeEmkSingleEnroll(idx_SeEmkConfig_t *pConfig,
                              uint8_t  touch,
                              uint8_t  finger)
{  return IDX_ISO_SUCCESS ; }

uint16_t idxSeEmkSleeveEnroll(idx_SeEmkConfig_t *pConfig)
{  return IDX_ISO_SUCCESS ; }

uint16_t idxSeEmkEnroll( idx_SeEmkConfig_t *pConfig )
{  return IDX_ISO_SUCCESS ; }

uint16_t idxSeEmkMatch(idx_SeEmkConfig_t *pConfig,
                       uint8_t  imgsrc,
                       uint16_t mwfTimeBudget)
{  return IDX_ISO_SUCCESS ; }

uint16_t idxSeEmkDeleteFinger(idx_SeEmkConfig_t *pConfig,
                              uint8_t finger,
                              int8_t mode)
{  return(IDX_ISO_SUCCESS);}
#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
