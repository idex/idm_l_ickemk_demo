#include <stdint.h>

#include "..\pb_hmoc\inc\pb_hybrid_moc.h"
#include "..\pb_hmoc\inc\templates_0x1102.h"
#include "..\pb_hmoc\inc\idex_template1_0x1102.h"

#define NCONT   3	
#define throwRuntimeException(e)  while(1)
#define assert(x) if ((x)==0) { while(0); }
#define GPIOPB_HMOC_TEST  (1<<4)

static const uint8_t* const CONTAINERS[NCONT] = { CONTAINER_CFG, CONTAINER_ENRL_1,CONTAINER_ENRL_2};
PB_WORKAREA WAREA; 
hmoc_config hmoc_cfg;	

extern unsigned int TMC_GPIO_Read(void);
extern void TMC_GPIO_Write(unsigned int data);
/**
 * Handle HMOC calls.
 */
uint8_t call(uint8_t fn)
{
	uint8_t *metadata = WAREA.work8 + sizeof(WAREA.param);
	uint8_t res = 0;
  uint8_t decision = metadata[0];
  uint8_t index;
  uint16_t index_score;

  if (fn == FN0_GETMETA0 || fn == FN1_GETMETA1) 
		{
		uint8_t metasize = 0;
    uint8_t ncont = fn;  // fn=0, no containers

		/* @param contp     Container pointers, there should be ncontp container pointers.
		 * @param ncontp    Number of container pointers.
		 * @param resp      Response buffer.
		 * @param resp_size Length of metadata respons buffer. at least 13 bytes 
		 * @param resp_len  Length of metadata response output. */

#if CALL_PB_HMOC_LIB
			res = pb_hmoc_getmeta(CONTAINERS, ncont, metadata, 16, &metasize);
#endif
		if (res) 
		{
			throwRuntimeException(res);
    }
    memset(&WAREA.param, 0, sizeof(WAREA.param));
    WAREA.param.returnValue       = metasize;
    WAREA.param.returnArray1_addr = metadata;
    WAREA.param.returnArray1_len  = metasize;
      
		return(0);

    } 
		else if (fn == FN2_MATCH || fn == FN3_MATCH_SCORE) 
		{
			uint16_t score;
      uint16_t* scorep = (fn == FN3_MATCH_SCORE) ? &score : 0;

			/* @param work        Work buffer, needed to complete the operation.
			 * @param work_size   The size of the work buffer.
			 * @param ver_offs    The offset to the verification template which 
														should be stored inside the work area upon entry
			 * @param decision    The outcome of the match, decision is made
			 * @param score       Optional request matching score, normally set this to null
			 * @param index       Optional request best matching subtemplate index.
			 * @param index_score Optional request best matching subtemplate score.*/
 #if CALL_PB_HMOC_LIB
      res = pb_hmoc_match(CONTAINERS, NCONT,
                            WAREA.work, sizeof(WAREA.work),
                            sizeof(WAREA.param), // ver_offs
                            &decision, scorep,
                            &index, &index_score);
#endif
      if (res) 
			{
				throwRuntimeException(res);
      }
      memset(&WAREA.param, 0, sizeof(WAREA.param));
      if (fn == FN2_MATCH) WAREA.param.returnValue = decision;
      else                 WAREA.param.returnValue = score;
      WAREA.param.returnArray1_addr = metadata;
      WAREA.param.returnArray1_len  = 3;
      WAREA.param.returnArray1_addr[0] = index;
      WAREA.param.returnArray1_addr[1] = index_score >> 8;
      WAREA.param.returnArray1_addr[2] = index_score;
      
			return(0);

    } 
		else if (fn == FN4_VERIFY) 
		{
      index = metadata[1];
			/* @param work        Work buffer, 
			 * @param work_size   The size of the work buffer.
			 * @param ver_offs    The offset to the verification template which 
														should be stored inside the work area upon entry
			 * @param decision    The finger code (container code) of the matching
			 *                    finger. This is both an input and output parameter.
			 * @param index       Subtemplate index.
			 * @param index_score Recalculated subtemplate score. */
#if CALL_PB_HMOC_LIB
			res = pb_hmoc_verify(CONTAINERS, NCONT,
                             WAREA.work, sizeof(WAREA.work),
                             sizeof(WAREA.param)+2, // ver_offs
                             &decision, index, &index_score);
#endif
      if (res) 
			{
				throwRuntimeException(res);
      }
      memset(&WAREA.param, 0, sizeof(WAREA.param));
      WAREA.param.returnValue = decision;
      WAREA.param.returnArray1_addr = metadata;
      WAREA.param.returnArray1_len  = 2;
      WAREA.param.returnArray1_addr[0] = index_score >> 8;
      WAREA.param.returnArray1_addr[1] = index_score;
      
			return(0);

    } 
		else if (fn == FN5_UPDATE) 
		{
      uint8_t* header;
      uint8_t* footer;
      uint16_t header_size;
      uint16_t footer_size;
      const uint8_t* header_dest;
      const uint8_t* footer_dest;

			/* @param contp       Container pointers.
			 * @param ncont       Number of container pointers.
			 * @param work        Work buffer, 
			 * @param work_size   The size of the work buffer.
			 * @param ver_offs    The offset to the verification template which
			 *                    should be stored inside the work area upon entry.
			 * @param decision    The finger code (container code) of the matching
			 *                    finger.
			 * @param header      Updated container data to replace existing data. 
			 * @param header_size Number of bytes in header buffer.
			 * @param header_dest Address in the finger container where the
			 *                    header data should be written.
			 * @param footer      Additional container data to append.
			 * @param footer_size Number of bytesn in footer buffer.
			 * @param footer_dest Address in the finger containers where the
			 *                    footer data should be written.*/
 #if CALL_PB_HMOC_LIB
      res = pb_hmoc_update(CONTAINERS, NCONT,
                             WAREA.work, sizeof(WAREA.work),
                             sizeof(WAREA.param)+1, // ver_offs
                             decision,
                             &header, &header_size, &header_dest,
                             &footer, &footer_size, &footer_dest);
#endif
			if (res) 
			{
				throwRuntimeException(res);
      }

      memset(&WAREA.param, 0, sizeof(WAREA.param));		
      WAREA.param.returnArray1_len  = 8;
        
      WAREA.param.returnValue = 0;
      WAREA.param.returnArray1_addr = metadata;
      WAREA.param.returnArray1_addr[0] = footer_size >> 8;
      WAREA.param.returnArray1_addr[1] = footer_size;
      WAREA.param.returnArray1_addr[2] = (unsigned long)footer_dest >> 8;
      WAREA.param.returnArray1_addr[3] = (unsigned long)footer_dest;
      WAREA.param.returnArray1_addr[4] = header_size >> 8;
      WAREA.param.returnArray1_addr[5] = header_size;
      WAREA.param.returnArray1_addr[6] = (unsigned long)header_dest >> 8;
      WAREA.param.returnArray1_addr[7] = (unsigned long)header_dest;
      
			return(0);
    } 
		else 
		{
      throwRuntimeException(PBI_EPARAMETER);
    }
    //return res; // not reached
}
#define SET_TEST_GPIO 	TMC_GPIO_Write((TMC_GPIO_Read() | 1));		
#define RESET_TEST_GPIO TMC_GPIO_Write((TMC_GPIO_Read() & ~1));		
/**
 * idex_toggle_test_pin
 *
 * @param  
 *
 * @brief  
 **/

void idex_toggle_test_pin(void)
{
	if(( TMC_GPIO_Read() & 1) == 0)	
		SET_TEST_GPIO
	else
		RESET_TEST_GPIO	
}

/**
 * idex_pb_hmoc_verify
 *
 * @param  
 *
 * @brief  
 **/
void idex_pb_hmoc_verify(hmoc_config* pcfg_hmoc)
{
    uint8_t ret = 0;

    // Test get metdata with container
		SET_TEST_GPIO  
		ret = call(pcfg_hmoc->bgetMeta? FN1_GETMETA1: FN0_GETMETA0);
		RESET_TEST_GPIO

		assert(ret == 0);
		assert(WAREA.param.returnValue == 13);            // Response length
		assert(WAREA.param.returnArray1_len == 13);
		assert(WAREA.param.returnArray1_addr[0] == 0xC0); // tag
		assert(WAREA.param.returnArray1_addr[1] ==   11); // len
		assert(WAREA.param.returnArray1_addr[3] == ((LIB_MAGIC >> 8) & 0xff )); // LIB_MAGIC MSB
		assert(WAREA.param.returnArray1_addr[4] == ((LIB_MAGIC >> 0) & 0xff )); // LIB_MAGIC LSB
		assert(WAREA.param.returnArray2_len == 0);
		if(!pcfg_hmoc->bgetMeta)	
		{
			assert(WAREA.param.returnArray1_addr[2] == 0x00); // cont id 0 inactive
		}
		else
		{
			assert(WAREA.param.returnArray1_addr[2] == 0x80); // cont id 0, active
			// Depends highly on conf container testdata...
			assert(WAREA.param.returnArray1_addr[5] == 0x00); // Num containers MSB
			assert(WAREA.param.returnArray1_addr[6] == 0x02); // 2              LSB
			assert(WAREA.param.returnArray1_addr[7] == 0x01); // Metadata size  MSB
			assert(WAREA.param.returnArray1_addr[8] == 0xD0); // 464            LSB
			assert(WAREA.param.returnArray1_addr[9] == 0xff); // Finger size    MSB
			assert(WAREA.param.returnArray1_addr[10]== 0xf0); // 4080           LSB
			assert(WAREA.param.returnArray1_addr[11]== 0x00); // Act fingers    MSB
			assert(WAREA.param.returnArray1_addr[12]== 0x04); // 0b0000 0100    LSB
		}
    // Test match 
    memcpy( WAREA.work8+sizeof(WAREA.param), pcfg_hmoc->pVerifyTmpl, pcfg_hmoc->wSize);
		SET_TEST_GPIO
		ret = call(FN2_MATCH);
		RESET_TEST_GPIO
    assert(ret == 0);
    assert(WAREA.param.returnArray1_len == 3);
    assert(WAREA.param.returnArray2_len == 0);

    // verify for a genuine match
    WAREA.work8[sizeof(WAREA.param)+1] = WAREA.param.returnArray1_addr[0]; // Index
    WAREA.work8[sizeof(WAREA.param)+0] = WAREA.param.returnValue; // Decision
    memmove(WAREA.work8+sizeof(WAREA.param)+2, WAREA.work8+WORK_SIZE-pcfg_hmoc->wSize, pcfg_hmoc->wSize);
    SET_TEST_GPIO
		assert(call(FN4_VERIFY) == 0);
		RESET_TEST_GPIO
    assert(WAREA.param.returnValue > 0);  // Expect a match with testdata ...
}

/**
 * idex_pb_hmoc_verify
 *
 * @param  
 *
 * @brief  
 **/
void idex_pb_hmoc_match(hmoc_config* pcfg_hmoc)
{
    uint8_t ret = 0;

    // Test get metdata with container
		SET_TEST_GPIO  
		ret = call(pcfg_hmoc->bgetMeta? FN1_GETMETA1: FN0_GETMETA0);
		RESET_TEST_GPIO

		assert(ret == 0);
		assert(WAREA.param.returnValue == 13);            // Response length
		assert(WAREA.param.returnArray1_len == 13);
		assert(WAREA.param.returnArray1_addr[0] == 0xC0); // tag
		assert(WAREA.param.returnArray1_addr[1] ==   11); // len
		assert(WAREA.param.returnArray1_addr[3] == ((LIB_MAGIC >> 8) & 0xff )); // LIB_MAGIC MSB
		assert(WAREA.param.returnArray1_addr[4] == ((LIB_MAGIC >> 0) & 0xff )); // LIB_MAGIC LSB
		assert(WAREA.param.returnArray2_len == 0);
		if(!pcfg_hmoc->bgetMeta)	
		{
			assert(WAREA.param.returnArray1_addr[2] == 0x00); // cont id 0 inactive
		}
		else
		{
			assert(WAREA.param.returnArray1_addr[2] == 0x80); // cont id 0, active
			// Depends highly on conf container testdata...
			assert(WAREA.param.returnArray1_addr[5] == 0x00); // Num containers MSB
			assert(WAREA.param.returnArray1_addr[6] == 0x02); // 2              LSB
			assert(WAREA.param.returnArray1_addr[7] == 0x01); // Metadata size  MSB
			assert(WAREA.param.returnArray1_addr[8] == 0xD0); // 464            LSB
			assert(WAREA.param.returnArray1_addr[9] == 0xff); // Finger size    MSB
			assert(WAREA.param.returnArray1_addr[10]== 0xf0); // 4080           LSB
			assert(WAREA.param.returnArray1_addr[11]== 0x00); // Act fingers    MSB
			assert(WAREA.param.returnArray1_addr[12]== 0x04); // 0b0000 0100    LSB
		}
    // Test match without score
    memcpy( WAREA.work8+sizeof(WAREA.param), pcfg_hmoc->pVerifyTmpl, pcfg_hmoc->wSize);
		SET_TEST_GPIO
		ret = call(pcfg_hmoc->bgetScore? FN3_MATCH_SCORE: FN2_MATCH);
		RESET_TEST_GPIO
    assert(ret == 0);
    assert(WAREA.param.returnArray1_len == 3);
    assert(WAREA.param.returnArray2_len == 0);
}

/**
 * idex_pb_hmoc_func
 *
 * @param  
 *
 * @brief  
 **/
 
unsigned char* idex_pb_hmoc_func(unsigned char fn)
{
	hmoc_config *pcfg_hmoc = &hmoc_cfg;	
	uint8_t *res = WAREA.work8 + sizeof(WAREA.param)-6;

	switch(fn)
	{		
		case 0: 		// 0 - Match Template 1
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 0;
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[0].Data;
			pcfg_hmoc->wSize = verification_containers[0].Size;
			idex_pb_hmoc_match((hmoc_config*)pcfg_hmoc);
			break;

		case 1:			// 1 - Match with score (Template 1)
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 1;
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[0].Data;
			pcfg_hmoc->wSize = verification_containers[0].Size;
			idex_pb_hmoc_match((hmoc_config*)pcfg_hmoc);
			break;

		case 2:			// 2 - Verify Template 1
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 0;
			memset(pcfg_hmoc->pVerifyTmpl,0,sizeof(hmoc_config));
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[0].Data;
			pcfg_hmoc->wSize = verification_containers[0].Size;
			idex_pb_hmoc_verify((hmoc_config*)pcfg_hmoc);
			break;

		case 3:			// 3 - Match Template 2
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 0;
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[1].Data;
			pcfg_hmoc->wSize = verification_containers[1].Size;
			idex_pb_hmoc_match((hmoc_config*)pcfg_hmoc);
			break;

		case 4:			// 4 - Match with score (Template 2)
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 1;
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[1].Data;
			pcfg_hmoc->wSize = verification_containers[1].Size;
			idex_pb_hmoc_match((hmoc_config*)pcfg_hmoc);
			break;
		
		case 5:			// 5 - Verify Template 2
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 0;
			memset(pcfg_hmoc->pVerifyTmpl,0,sizeof(hmoc_config));
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[1].Data;
			pcfg_hmoc->wSize = verification_containers[1].Size;
			idex_pb_hmoc_verify((hmoc_config*)pcfg_hmoc);
			break;

		case 6:			// 6 - Match imposter 
			pcfg_hmoc->bgetMeta = 0;
			pcfg_hmoc->bgetScore = 0;
			pcfg_hmoc->pVerifyTmpl = (unsigned char*)verification_containers[2].Data;
			pcfg_hmoc->wSize = verification_containers[2].Size;
			idex_pb_hmoc_match((hmoc_config*)pcfg_hmoc);
			break;

		
		default:
				break;
	}

	return res;
}
