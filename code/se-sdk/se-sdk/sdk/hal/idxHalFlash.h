/** ***************************************************************************
 * \file   idxHalFlash.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         Flash programming. If a new platform is to be supported
 *         then it is expected that the HAL APIs are implemented for that
 *         platform in order to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_FLASH_H_
#define _IDX_HAL_FLASH_H_

#include <stdint.h>
#include "idxIsoErrors.h"

/**
* @brief Function to perform erase check on a contiguous block of data to Flash
*
* @param[in] pAddr is a pointer to the start of the Flash area to perform check
* @param[in] bytesToCheck indicates the number of bytes to be checked
*
* @return IDX_ISO_SUCCESS if provided area is erased,
*         or another error code if not successful or is not erased
*/                          
uint16_t idxHalFlashEraseCheck(uint8_t *pAddr, 
                               uint16_t bytesToCheck);

/**
* @brief Function for erasing a contiguous block of data to Flash
*
* @param[in] pAddr is a pointer to the start of the Flash area to be erased
* @param[in] bytesToErase indicates the number of bytes to erase from to Flash
*
* @return IDX_ISO_SUCCESS if successful,
*         or another error code if not successful
*/                          
uint16_t idxHalFlashErase(uint8_t *pAddr, 
                          uint32_t bytesToErase);
/**
* @brief Function for writing a contiguous block of data to Flash
*
* @param[in] pAddr is a pointer to the start of the Flash area to be written
* @param[in] pData is a pointer to the data to be written to Flash
* @param[in] bytesToWrite indicates the number of bytes to write to Flash
*
* @return IDX_ISO_SUCCESS if successful,
*         or another error code if not successful
*/
uint16_t idxHalFlashWrite(uint8_t *pAddr, 
                          const uint8_t *pData, 
                          uint16_t bytesToWrite);


#endif /* _IDX_HAL_FLASH_H_ */
