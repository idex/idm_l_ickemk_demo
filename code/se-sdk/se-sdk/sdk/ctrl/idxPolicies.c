/******************************************************************************
* \file  idxPolicies.c
* \brief code for controling policies
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2020 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "idxPolicies.h"
#include "idxSeBioAPI.h"
#include "idxHalFlash.h"
#include "idxHalSystem.h"

#define POLICY_OFFSET(NAME) (((char *)&(((struct idxBioPolicies_s *)0)->NAME))-((char *)0))
#define POLICY_SIZE(NAME)   (sizeof(((struct idxBioPolicies_s *)0)->NAME))
#define POLICY_DESCR(NAME)  {POLICY_OFFSET(NAME), POLICY_SIZE(NAME)}

struct idxPolicyDescr_s
{
	uint16_t offset;
	uint8_t size;
};

const static struct idxPolicyDescr_s PolicyDescr[] =
{
	POLICY_DESCR(unused),
	POLICY_DESCR(max_fingers),
	POLICY_DESCR(max_touches),
	POLICY_DESCR(sensorinit),
	POLICY_DESCR(additional)
};

#define POLICY_ACCESS(TYPE, OFFSET) (*((TYPE *)(((char *)&idxHalSystemPolicies) + OFFSET)))
#define POLICY_ACCESS_PTR(OFFSET) (((uint8_t *)&idxHalSystemPolicies) + OFFSET)

#define NUM_POLICIES (sizeof(PolicyDescr) / sizeof(struct idxPolicyDescr_s))

static uint32_t read_policy(unsigned int policyID, uint8_t* policySize, uint16_t* nRet)
{
	uint32_t res = 0;
	const struct idxPolicyDescr_s *pd;
	if (nRet)
		*nRet = IDX_ISO_SUCCESS;

	if (policyID >= NUM_POLICIES)
	{
		if (nRet)
			*nRet = IDX_ISO_ERR_BAD_PARAMS;
		return res;
	}

	pd = &PolicyDescr[policyID];

	if (policySize)
		*policySize = pd->size;

	switch(pd->size)
	{
		case 1: res = POLICY_ACCESS(uint8_t, pd->offset); break;
		case 2: res = POLICY_ACCESS(uint16_t, pd->offset); break;
		//case 4: res = POLICY_ACCESS(uint32_t, pd->offset); break;
		default: {if (nRet) *nRet = IDX_ISO_ERR_BAD_PARAMS;}
	}

	return res;
}

static uint16_t write_policy(unsigned int policyID, uint32_t policyValToWrite)
{
	uint16_t ret;
	uint8_t* addr = NULL;
	const struct idxPolicyDescr_s *pd;
	uint32_t writeData = 0;

	if (policyID >= NUM_POLICIES) return IDX_ISO_ERR_BAD_PARAMS;

	pd = &PolicyDescr[policyID];

	switch(pd->size)
	{
		case 1: *((uint8_t*)&writeData) = (uint8_t)policyValToWrite; break;
		case 2: *((uint16_t*)&writeData) = (uint16_t)policyValToWrite; break;
		//case 4: *((uint32_t*)&writeData) = (uint32_t)policyValToWrite; break;
		default: {return IDX_ISO_ERR_BAD_PARAMS;}
	}

	// get address of policy in flash
	addr = POLICY_ACCESS_PTR(pd->offset);

	// write policy value to flash
	ret = idxHalFlashWrite(addr, (uint8_t*)&writeData, pd->size);

	return ret;
}

// set/get policies
uint16_t idxPolicy(const uint8_t* dat, uint8_t apdulc, // input data
				   uint32_t* policyValResponse, uint8_t* policySizeResponse) // output data
{
	//apdulc value:
	//0x01 Simply read the indexed policy
	//0x02 Read the indexed policy and write the one-byte data
	//0x03 Read the indexed policy and write the two-byte data

	uint16_t ret;
	uint16_t nRet;
	uint32_t policyVal;
	uint16_t policyVal16_swap;
	//uint32_t policyVal32_swap;
	uint8_t policySize;
	uint32_t policyValToWrite;
	unsigned int numBytes, i;
	const uint8_t* p;

	*policySizeResponse = 0;
	ret = IDX_ISO_SUCCESS;

	// get policy value from flash
	policyVal = read_policy(dat[0], &policySize, &nRet);
	if (nRet != IDX_ISO_SUCCESS)
	{ // error
		*policySizeResponse = 0;
		return ret;
	}

	if (policySize == 1)
	{
		*((uint8_t*)policyValResponse) = (uint8_t)policyVal;
		*policySizeResponse = sizeof(uint8_t);
	}
	else if (policySize == 2)
	{
		WR_U16(&policyVal16_swap, (uint16_t)policyVal);
		*((uint16_t*)policyValResponse) = (uint16_t)policyVal16_swap;
		*policySizeResponse = sizeof(uint16_t);
	}
	//else if (policySize == 4)
	//{
	//	WR_U32(&policyVal32_swap, (uint32_t)policyVal);
	//	*((uint32_t*)policyValResponse) = (uint32_t)policyVal32_swap;
	//	*policySizeResponse = sizeof(uint32_t);
	//}
	else
	{ // error
		*policySizeResponse = 0;
		return IDX_ISO_ERR_BAD_PARAMS;
	}

	// set policy value to flash, if write policy request(apdulc > 1)
	if (apdulc > 1)
	{ // write policy
		// inside a condition that (apdulc > 1) so a write is needed...
		// apdulc value:
		// 0x02 - write the one-byte data
		// 0x03 - write the two-bytes data
		// 0x05 - write the four-bytes data

		if (apdulc > 3/*5*/)
		{ // error
			*policySizeResponse = 0;
			return IDX_ISO_ERR_BAD_PARAMS;
		}

		p = &dat[1];
		numBytes = apdulc-1;
		policyValToWrite = 0;
		for (i=0; i<numBytes; i++)
		{
			policyValToWrite |= (((uint32_t)(*p++)) << (8 * i));
		}

		// write policy value to flash
		if ((ret = write_policy(dat[0], policyValToWrite)) != IDX_ISO_SUCCESS)
		{ // error
			*policySizeResponse = 0;
			return ret;
		}
	}

	return ret;
}
