/** ***************************************************************************
 * \file   idxUartSerialInterface.c
 * \brief  Communication over UART-based IDEX serial interface.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2012-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#define HOST_IF_BUFFER_SIZE 256

#include <stdint.h>
#include <stdbool.h>

#include <string.h> // For memcpy

#include "idxHalGpio.h"
#include "idxHalUart.h"
#include "idxHalSystem.h"

#include "idxBaseErrors.h"
#include "idxSeBioDrvAPI.h"

#include "idxSerialInterface.h"
#include "_idxSerialInterface.h"

#include "idxSeAPDUInterface.h" // To get IDX_APDU_CMD_DELETERECORD

#define UNUSED(p) if(p){}

static bool initialized; // Set false in idxSerialInterfaceInit()
static uint8_t mode = MODE_UNINITIALIZED;
static bool erase;

//--------------------------------------------------------------------
void doWtxCallbackNow(void);

uint16_t idxBioDrvComms(const idxBuffer_t *pCommsBuffer)
{
  uint16_t wCode;
  enum idxBioDrvRet_e ret;
  uint8_t *hostifBuffer = &pCommsBuffer->pBuffer[0];

  uint16_t wSize = RD_U16(&hostifBuffer[0]); 
  uint8_t bOrdinal = hostifBuffer[2];


  if (!initialized) {

    // Delay calling idxBioDrvInit() until a command needs to be sent
    // so that the WTX handler is interposed in the correct (CL or CB)
    // WTX interrupt handler.

    idxBioDrvInit();
    initialized = true;
  }
  
  erase = (bOrdinal == IDX_APDU_CMD_DELETERECORD);

  // While debugging, send a WTX before each command so as to be reasonably sure
  // that the data transfers aren't interrupted by WTXs
  doWtxCallbackNow();

  // Reorganize the command header into the format expected by idxBioDrvSend()
  
  hostifBuffer[0] = 0x84;
  hostifBuffer[1] = 0x00;
  // Leave bOrdinal in hostifBuffer[2]
  // Leave bOptions in hostifBuffer[3]

#if 1

  // Moved to the idxIso7816HostIf2WireHal layer
  //idxHalSystemDivertPowerToMCU();
  
  ret = idxBioDrvSend(HOST_IF_BUFFER_SIZE, hostifBuffer);
  if (IDX_BIO_DRV_OK == ret) { 
    ret = idxBioDrvReceive(HOST_IF_BUFFER_SIZE, hostifBuffer);
  }

  if (IDX_BIO_DRV_OK == ret) {
    // Reorganize the response header from the format returned by
    // idxBioDrvReceive().  Note that wSize and wCode are only decoded from
    // buffer if reception succeeded.
    
    // XXXXXXXXXXXXX Also note the documentation of idxBioDrvReceive()
    // doesn't agree with the current implementation - I think it's the
    // documentation that's wrong - in that wSize is returned in
    // bytes 4 and 5, and the wCode in bytes 2 and 3 - matching the
    // format of data passed into idxBioDrvSend() XXXXXXXXXXXXXXXXX
    
    wCode = RD_U16(&hostifBuffer[2]);
    wSize = RD_U16(&hostifBuffer[4]);
    ret = IDX_ISO_SUCCESS;
  } else {
    wCode = IDX_ISO_ERR_COMM;
    wSize = 0;
    ret = IDX_ISO_ERR_COMM;
  }
#else
  // For doing some testing on the simulator...
  hostifBuffer[0] = 0x42; // Two unused bytes
  hostifBuffer[1] = 0x43;
  hostifBuffer[2] = 0x00; // wCode
  hostifBuffer[3] = 0x90;
  hostifBuffer[4] = 0x04; // wSize
  hostifBuffer[5] = 0x00;
  
  wCode = RD_U16(&hostifBuffer[2]);
  wSize = RD_U16(&hostifBuffer[4]);
  ret = IDX_ISO_SUCCESS;
#endif
  
#if (RESPONSE_CRC_KLUDGE)
  // Kludge things to look like what comes back on the SLE78
  hostifBuffer[4] = hostifBuffer[0]; // Put back the CRC
  hostifBuffer[5] = hostifBuffer[1];
  hostifBuffer[0] = wSize & 255;
  hostifBuffer[1] = wSize >> 8;
  hostifBuffer[2] = wCode & 255;
  hostifBuffer[3] = wCode >> 8;
#endif

  // Moved to the idxIso7816HostIf2WireHal layer
  //idxHalSystemRestorePowerSharing();

  return ret;
}

void idxSetConnectionMode( uint8_t newMode )
{
  mode = newMode;
}

uint8_t idxGetConnectionMode()
{
  return mode;
}

// Kludge for now...
bool idxGetErase()
{
  return erase;
}

void idxSerialInterfaceInit()
{
  initialized = false;
  mode = MODE_UNINITIALIZED;
  erase = false;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

