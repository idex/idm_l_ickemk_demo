
#include "halglobal.h"
#include "stdint.h"

#define ALG_OK ((uint16_t)0xAAAA)
#define ALG_ERROR ((uint16_t)0x5555)

typedef struct{
#if 0
	uint8_t Input[512];
 	uint8_t KEY_P[256];
 	uint8_t KEY_Q[256];
 	uint8_t KEY_DP[256];
 	uint8_t KEY_DQ[256];
 	uint8_t KEY_QINV[256];
 	uint8_t KEY_N[512];
 	uint8_t KEY_D[512];
 	uint8_t KEY_E[512];
#else	
	uint8_t Input[2];
	uint8_t KEY_P[6];
	uint8_t KEY_Q[6];
	uint8_t KEY_DP[6];
	uint8_t KEY_DQ[6];
	uint8_t KEY_QINV[6];
	uint8_t KEY_N[2];
	uint8_t KEY_D[2];
	uint8_t KEY_E[2];
#endif
} RSAEnter; 
RSAEnter RSAdata __attribute__((at(0x20000291)));

RSAPrivKey_t	RSA_PrivKey;
RSAPubKey_t		RSA_PubKey;
RSAPrivKeyCrt_t RSA_PrivKeyCrt;
extern  unsigned char SM2BUF[];
#define TempDataBuf SM2BUF  //2K

static uint16_t usDataLen = 0;
#define	Bit7_En		0x00000080//0x1<<7
#define SM2_PubKey  RSAdata.KEY_P
#define SM2_PriKey 	RSAdata.KEY_Q
#define SM2_ID		RSAdata.KEY_DP

unsigned char EccDataBuf1[0x200];
#define EccDataBuf2 RSAdata.Input 
#define		EccPubKey   	RSAdata.KEY_N
#define		EccPrivKey		RSAdata.KEY_D
#define		EccP			RSAdata.KEY_P
#define		EccN			RSAdata.KEY_Q
#define		EccA			RSAdata.KEY_DP
#define		EccB		   	RSAdata.KEY_DQ
#define		EccGx			RSAdata.KEY_QINV
#define		EccGy			RSAdata.KEY_E 
BigNum_t ECC_PubKey={EccPubKey,0},ECC_PrivKey={EccPrivKey,0},ECC_P={EccP,0},ECC_N={EccN,0},
ECC_A={EccA,0},ECC_B={EccB,0},ECC_Gx={EccGx,0},ECC_Gy={EccGy,0},ECC_DataBuf1={EccDataBuf1,0}, ECC_DataBuf2={EccDataBuf2, 0};
ECC_Domain_t ECC_DOMAIN = {&ECC_P,&ECC_N,&ECC_A,&ECC_B,&ECC_Gx,&ECC_Gy};
BigNum_t RSADAT = {RSAdata.Input, 0};
BigNum_t	pID;

SHA1_Context_t SHA1_Context;
SHA256_Context_t SHA256_Context;
SM3_Context_t SM3_Context;
//#define Bit7_En  0x80
//SM2 keyExchange
#define		SM2_DA			RSAdata.KEY_D
#define		SM2_RDA			RSAdata.KEY_P
#define		SM2_RPA			RSAdata.KEY_Q
#define		SM2_ZA			RSAdata.KEY_DP
#define		SM2_PB		   	RSAdata.KEY_DQ
#define		SM2_RPB			RSAdata.KEY_QINV
#define		SM2_ZB			RSAdata.KEY_E
#define		SM2_KA			TempDataBuf
#define		SM2_S1			EccDataBuf1
#define		SM2_SA			EccDataBuf2		 	 

u16 SM2Input( uint8_t *inputdata,uint16_t len)
{
	uint16_t rtn = 0x9000;
	uint8_t mode = (uint8_t)(*inputdata);
		
	_HalRamCopy(TempDataBuf+usDataLen,inputdata+1,len);
	usDataLen += len;
	if((mode & Bit7_En) == 0) 			/* Send all data at once or send the last block */
	{	
		/* SM2 pubkey */
		if((mode&0x0F) == 0x03) 		
		{
			if(usDataLen == 0x40)
			{
				_HalRamCopy(SM2_PubKey,TempDataBuf,usDataLen);
			}
			else
			{
				return 0x6c00;
			}		
		}
		/* SM2 prikey */
		else if((mode&0x0F) == 0x04)	
		{
			if(usDataLen == 0x20)
			{
				_HalRamCopy(SM2_PriKey,TempDataBuf,usDataLen);
			}
			else
			{
				return 0x6c00;
			}								
		}
		/*data*/
		else if ((mode&0x0F) == 0x00)
		{
			if ((PA2 != 0x00)&&((PA2 != 0x01)))
			{
				_HalRamCopy(ECC_DataBuf1.p,TempDataBuf,usDataLen);
			}
			ECC_DataBuf1.length = usDataLen;	
		}
		else if ((mode&0x0F) == 0x01)
		{
			if ((PA2 != 0x00)&&((PA2 != 0x01)))
			{
				_HalRamCopy(ECC_DataBuf2.p,TempDataBuf,usDataLen);
			}
			ECC_DataBuf2.length = usDataLen;	
		}
		/*ID*/
		else if ((mode&0x0F) == 0x02)
		{
			_HalRamCopy(SM2_ID,TempDataBuf,usDataLen);
			pID.p = SM2_ID;
			pID.length = usDataLen;	
		}
		else if ((mode&0x0F) == 0x05) //SM2_DA
		{
			_HalRamCopy(SM2_DA,TempDataBuf,usDataLen);
		}
		else if ((mode&0x0F) == 0x06) //SM2_RDA
		{
			_HalRamCopy(SM2_RDA,TempDataBuf,usDataLen);
		}
		else if ((mode&0x0F) == 0x07) //SM2_RPA
		{
			_HalRamCopy(SM2_RPA,TempDataBuf,usDataLen);
		}
		else if ((mode&0x0F) == 0x08) //SM2_ZA
		{
			_HalRamCopy(SM2_ZA,TempDataBuf,usDataLen);
		}
		else if ((mode&0x0F) == 0x09) //SM2_PB
		{
			_HalRamCopy(SM2_PB,TempDataBuf,usDataLen);
		}
		else if ((mode&0x0F) == 0x0a) //SM2_RPB
		{
			_HalRamCopy(SM2_RPB,TempDataBuf,usDataLen);
		}
		else if ((mode&0x0F) == 0x0b) //SM2_ZB
		{
			_HalRamCopy(SM2_ZB,TempDataBuf,usDataLen);
		}		
		else
		{
			return 0x6985;	
		}
		usDataLen = 0;
	}
	return rtn;
}

u16 RSAInput(uint8_t *inputdata,uint16_t len)
{
	u16 rtn = 0x9000;
	uint8_t mode = (uint8_t)(*inputdata);

	_HalRamCopy(TempDataBuf + usDataLen, inputdata+1, len);
	usDataLen += len;
	if((mode & Bit7_En) == 0) 			/* Send all data at once or send the last block */
	{		
		if((mode&0x0F) == 0x03) 		/* RSA-N */
		{
			_HalRamCopy(RSAdata.KEY_N, TempDataBuf, usDataLen);
			RSA_PubKey.N.p = RSAdata.KEY_N;
			RSA_PubKey.N.length = usDataLen;
			RSA_PrivKey.N.p = RSAdata.KEY_N;
			RSA_PrivKey.N.length = usDataLen;	
		}
		else if((mode&0x0F) == 0x04)	/* RSA-QINV	*/
		{
			_HalRamCopy(RSAdata.KEY_QINV, TempDataBuf, usDataLen);
			RSA_PrivKeyCrt.QInv.p = RSAdata.KEY_QINV;
			RSA_PrivKeyCrt.QInv.length = usDataLen;	
		}
		else if((mode&0x0F) == 0x05)	/* RSA-P */
		{
			_HalRamCopy(RSAdata.KEY_P, TempDataBuf, usDataLen);
			RSA_PrivKeyCrt.P.p = RSAdata.KEY_P;
			RSA_PrivKeyCrt.P.length = usDataLen;	
		}
		else if((mode&0x0F) == 0x06)	/* RSA-Q */
		{
			_HalRamCopy(RSAdata.KEY_Q, TempDataBuf, usDataLen);
			RSA_PrivKeyCrt.Q.p = RSAdata.KEY_Q;
			RSA_PrivKeyCrt.Q.length = usDataLen;
		}
		else if((mode&0x0F) == 0x07)	/* RSA-DP */
		{
			_HalRamCopy(RSAdata.KEY_DP, TempDataBuf, usDataLen);
			RSA_PrivKeyCrt.DP.p = RSAdata.KEY_DP;
			RSA_PrivKeyCrt.DP.length = usDataLen;
		}
		else if((mode&0x0F) == 0x08)	/* RSA-DQ */
		{
			_HalRamCopy(RSAdata.KEY_DQ, TempDataBuf, usDataLen);
			RSA_PrivKeyCrt.DQ.p = RSAdata.KEY_DQ;
			RSA_PrivKeyCrt.DQ.length = usDataLen;
		}
		else if((mode&0x0F) == 0x09)	/* RSA-D */
		{
			_HalRamCopy(RSAdata.KEY_D, TempDataBuf, usDataLen);
			RSA_PrivKey.D.p = RSAdata.KEY_D;
			RSA_PrivKey.D.length = usDataLen;	
		}
		else if((mode&0x0F) == 0x02)	/* RSA-E */
		{
			_HalRamCopy(RSAdata.KEY_E, TempDataBuf, usDataLen);
			RSA_PubKey.E.p = RSAdata.KEY_E;
			RSA_PubKey.E.length = usDataLen;
		}
		else if((mode&0x0F) == 0x00)
		{
			_HalRamCopy(RSAdata.Input, TempDataBuf, usDataLen);
			RSADAT.length = usDataLen;	
		}

		else
		{				
			return 0x6a00;
		}
		usDataLen = 0;	
	}
	return rtn;
}
u16 ECCInput(uint8_t *inputdata,uint16_t len)
{
	u16 rtn = 0x9000;
	uint8_t mode = (uint8_t)(*inputdata);
	_HalRamCopy(TempDataBuf+usDataLen,inputdata+1,len);
	usDataLen += len;	
	if((mode & Bit7_En) == 0) 				/* Send all data at once or send the last block */
	{
		/* ECC pubkey */
		if((mode&0x0F) == 0x03) 		
		{ 
			_HalRamCopy(ECC_PubKey.p,TempDataBuf,usDataLen);
			ECC_PubKey.length = usDataLen;
		}
		/* ECC prikey */
		else if((mode&0x0F) == 0x04)	
		{
			_HalRamCopy(ECC_PrivKey.p,TempDataBuf,usDataLen);
			ECC_PrivKey.length = usDataLen;		
		}
		/* ECC P */
		else if((mode&0x0F) == 0x05)
		{
			_HalRamCopy(ECC_P.p,TempDataBuf,usDataLen);
			ECC_P.length = usDataLen;
		}
		/* ECC N */
		else if((mode&0x0F) == 0x06)
		{
			_HalRamCopy(ECC_N.p,TempDataBuf,usDataLen);
			ECC_N.length = usDataLen;
		}
		/* ECC A */
		else if((mode&0x0F) == 0x07)
		{
			_HalRamCopy(ECC_A.p,TempDataBuf,usDataLen);
			ECC_A.length = usDataLen;
		}
		/* ECC B */
		else if((mode&0x0F) == 0x08)
		{
			_HalRamCopy(ECC_B.p,TempDataBuf,usDataLen);
			ECC_B.length = usDataLen;
		}
		/* ECC Gx */
		else if((mode&0x0F) == 0x09)
		{
			_HalRamCopy(ECC_Gx.p,TempDataBuf,usDataLen);
			ECC_Gx.length = usDataLen;
		}
		/* ECC Gy */
		else if((mode&0x0F) == 0x0a)
		{
			_HalRamCopy(ECC_Gy.p,TempDataBuf,usDataLen);
			ECC_Gy.length = usDataLen;
		}
		else if((mode&0x0F) == 0x00)
		{
			_HalRamCopy(ECC_DataBuf1.p,TempDataBuf,usDataLen);
			ECC_DataBuf1.length = usDataLen;	
		}
		/*Stor data, signature or  point*/
		else if((mode&0x0F) == 0x01)
		{
			_HalRamCopy(ECC_DataBuf2.p,TempDataBuf,usDataLen);
			ECC_DataBuf2.length = usDataLen;	
		}
		else
		{
			rtn = 0x6a00;
		}
		usDataLen = 0;						
	}
	return rtn;

}

u16 SM2CaculAndResult(uint8_t oper, uint8_t *inputdata,uint8_t *outputdata)
{
	uint8_t mode = (u8 )(*inputdata);
	u8 len = (u8 )(*(inputdata+1));
//	static BigNum_t	pID;
	BigNum_t inputBuf,outputBuf;
	SM2KeyExchangeParam_t sm2KeyExParam;
	SM2KeyExchangeOut_t sm2KeyExOut;
	
	u16 rtn = 0x9000;
	if(mode&0x40)
	{
		/*enc*/
		if(oper == 0x00)
		{
			inputBuf.p = TempDataBuf;
			inputBuf.length = ECC_DataBuf1.length;
			outputBuf.p = TempDataBuf;
				
			if(tmcSM2Encrypt(&outputBuf, &inputBuf, SM2_PubKey) != ALG_OK)
			{
				return 0x6504;
			}		
		}
		/*dec*/
		else if(oper  == 0x01)
		{
			inputBuf.p = TempDataBuf;
			inputBuf.length = ECC_DataBuf1.length;
			outputBuf.p = TempDataBuf;
			
			if(tmcSM2Decrypt(&outputBuf, &inputBuf, SM2_PriKey) != ALG_OK)
			{
				return 0x6504;
			}		
		}
		/*sign*/ 
		else if(oper  == 0x02)
		{
			if(tmcSM2Sign(EccDataBuf2, SM2_PriKey, EccDataBuf1) != ALG_OK)
			{
				return 0x6504;
			}		
		}
		/*verify*/
		else if(oper  == 0x03)
		{
			if(tmcSM2Verify(SM2_PubKey, EccDataBuf1, EccDataBuf2) != ALG_OK)
			{
				return 0x6504;
			}		
		}
		/*PointAdd*/ 
		else if(oper  == 0x04)
		{

		}
		/*PointMulti*/
		else if(oper  == 0x05)
		{

		}
		/* Genarate key */   
		else if(oper  == 0x06)
		{				
			u8 type = 0;
			if (mode &0x20)
			{
				type = 0x01;
			}
			while((tmcSM2KeyPairGen(SM2_PubKey, SM2_PriKey,type) != ALG_OK));
			rtn = ((PkeSM2Sec_PtChk((uint8_t *)SM2_PubKey) == ALG_OK?0x9000:0x6504));		
		}
		/*KeyExchange*/
		else if(oper  == 0x07)
		{
			sm2KeyExParam.pbThisPriKey = (uint8_t *)SM2_DA;
			sm2KeyExParam.pbThisTempPriKey = (uint8_t *)SM2_RDA;
			sm2KeyExParam.pbThisTempPubKey = (uint8_t *)SM2_RPA;
			sm2KeyExParam.pbThisZ = (uint8_t *)SM2_ZA;
			sm2KeyExParam.pbOtherPubKey = (uint8_t *)SM2_PB;
			sm2KeyExParam.pbOtherTempPubKey = (uint8_t *)SM2_RPB;
			sm2KeyExParam.pbOtherZ = (uint8_t *)SM2_ZB;
			if (!(mode&0x20))
			{
				sm2KeyExParam.isSender = 1;
			}
			else
			{
				sm2KeyExParam.isSender = 0;
			}
			sm2KeyExOut.pbSymKey = SM2_KA;
			sm2KeyExOut.pbThisVerifyData = SM2_S1;
			sm2KeyExOut.pbOtherVerifyData = SM2_SA;		
			if(tmcSM2KeyExchange(&sm2KeyExOut,&sm2KeyExParam,0x10) != ALG_OK)
			{
				return 0x6504;
			}					
		}
		/*SetUserMessage*/ //
		else if(oper  == 0x08)
		{
			inputBuf.p = EccDataBuf1;
			inputBuf.length = ECC_DataBuf1.length;
		
			if(tmcSM2GetHashData(EccDataBuf2, SM2_PubKey, &pID, &inputBuf) != ALG_OK)
			{
				return 0x6504;
			}		
		}
		/*ZGen*/
		else if(oper  == 0x09)
		{
			pID.length = pID.length;
			pID.p = pID.p;
				
			if(tmcSM2ZGen(EccDataBuf2,SM2_PubKey, &pID) != ALG_OK)
			{
				return 0x6504;
			}		
		}
	}
	if(oper  == 0x06 )
	{
		/* SM2 pubkey */
		if((mode&0x0F) == 0x03) 		
		{
			_HalRamCopy(outputdata,SM2_PubKey+usDataLen,len);	
		}
		/* SM2 prikey */
		else if((mode&0x0F) == 0x04)
		{
			_HalRamCopy(outputdata,SM2_PriKey+usDataLen,len);	
		}
		else
		{
			return 0x6a00;
		}						
	}
	else if (oper  == 0x07)
	{
		if ((mode&0x0F) == 0x0c)
		{
			_HalRamCopy(outputdata,SM2_KA+usDataLen,len);	
		}
		else if ((mode&0x0F) == 0x0d)
		{
			_HalRamCopy(outputdata,SM2_S1+usDataLen,len);
		}
		else if ((mode&0x0F) == 0x0e)
		{
			_HalRamCopy(outputdata,SM2_SA+usDataLen,len);
		}
	}
	else if ((oper == 0x00) || (oper == 0x01)) //enc  dec
	{
		_HalRamCopy(outputdata, TempDataBuf + usDataLen, len);
		usDataLen += len;		
	}
	else
	{
		_HalRamCopy(outputdata, EccDataBuf2 + usDataLen, len);
		usDataLen += len;
	}
	if((mode & Bit7_En) == 0) 			/* Send all data at once or send the last block */
	{
		usDataLen = 0;							
	}
	return rtn;	
}

u16 RSACaculAndResu1t(uint8_t oper, uint8_t *inputdata, uint8_t * outputdata)
{
	uint16_t usBitLenKey;
	uint8_t mode = (uint8_t)(*inputdata);
	BigNum_t outputBuf,inputBuf;
	RSAKeyGenOut_t rsaGenOut;
	u16 rtn = 0x9000;
	uint8_t len = (u8 )(*(inputdata+1));
	u8	mask[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	
	if(mode & 0x40)				//	Calculate
	{
		if(oper == 0x00)		//	Encrypt	
		{
			inputBuf.p = RSAdata.Input;
			inputBuf.length = RSADAT.length;
			outputBuf.p = RSAdata.Input;
			if(tmcRSAEncrypt(&outputBuf, &inputBuf, &RSA_PubKey) != ALG_OK)
			{
				rtn = 0x6504;
			}
			RSADAT.length = outputBuf.length;		
		}
		else if(oper == 0x01)	//	Decrypt
		{
			inputBuf.p = RSAdata.Input;
			inputBuf.length = RSADAT.length;
			outputBuf.p = RSAdata.Input;
			if(tmcRSADecrypt(&outputBuf, &inputBuf, &RSA_PrivKey)!= ALG_OK)			
			{ 
			   	rtn = 0x6504;
			}
			RSADAT.length = outputBuf.length;					
		}
		else if(oper == 0x11)	//	Decrypt CRT
		{
			BigNum_t MK;
			MK.p = mask;
			MK.length = 8;
			inputBuf.p = RSAdata.Input;
			inputBuf.length = RSADAT.length;
			outputBuf.p = RSAdata.Input;
			if(tmcRSADecryptCrt_SU(&outputBuf, &inputBuf, &RSA_PrivKeyCrt, &MK, &(RSA_PubKey.E)) != ALG_OK)
			{ 
				rtn = 0x6504;
			}			
			RSADAT.length = outputBuf.length;		
		}
		else if(oper == 0x03)	//	Generate Key Pair
		{
			usBitLenKey = inputdata[2]*256 + inputdata[3];
	
			while(ALG_OK != tmcRSAKeyPairGen(&rsaGenOut, &RSA_PubKey.E, usBitLenKey, 2))
			{;}
				
			RSA_PrivKey.N.p = RSAdata.KEY_N;
			RSA_PubKey.N.p = RSAdata.KEY_N;				
			_HalRamCopy(RSAdata.KEY_N, rsaGenOut.PriKey.N.p, rsaGenOut.PriKey.N.length);	//	Read key pair N					
			RSA_PrivKey.N.length = rsaGenOut.PriKey.N.length;
			RSA_PubKey.N.length = rsaGenOut.PriKey.N.length;
				
			RSA_PrivKey.D.p = RSAdata.KEY_D;
			_HalRamCopy(RSAdata.KEY_D, rsaGenOut.PriKey.D.p, rsaGenOut.PriKey.N.length);	//	Read key pair D											
			RSA_PrivKey.D.length = rsaGenOut.PriKey.N.length;
				
			RSA_PrivKeyCrt.P.p = RSAdata.KEY_P;
			_HalRamCopy(RSAdata.KEY_P, rsaGenOut.PriCrtKey.P.p, (rsaGenOut.PriKey.N.length)>>1);	//	Read key pair P								
			RSA_PrivKeyCrt.P.length = rsaGenOut.PriKey.N.length>>1;
				
			RSA_PrivKeyCrt.Q.p = RSAdata.KEY_Q;
			_HalRamCopy(RSAdata.KEY_Q, rsaGenOut.PriCrtKey.Q.p, (rsaGenOut.PriKey.N.length)>>1);	//	Read key pair Q							
			RSA_PrivKeyCrt.Q.length = rsaGenOut.PriKey.N.length>>1;
				
			RSA_PrivKeyCrt.DP.p = RSAdata.KEY_DP;
			_HalRamCopy(RSAdata.KEY_DP, rsaGenOut.PriCrtKey.DP.p, (rsaGenOut.PriKey.N.length)>>1);	//	Read key pair DP					
			RSA_PrivKeyCrt.DP.length = rsaGenOut.PriKey.N.length>>1;
			
			RSA_PrivKeyCrt.DQ.p = RSAdata.KEY_DQ;
			_HalRamCopy(RSAdata.KEY_DQ, rsaGenOut.PriCrtKey.DQ.p, (rsaGenOut.PriKey.N.length)>>1);	//	Read key pair DQ			
			RSA_PrivKeyCrt.DQ.length = rsaGenOut.PriKey.N.length>>1;
			
		    RSA_PrivKeyCrt.QInv.p = RSAdata.KEY_QINV;
			_HalRamCopy(RSAdata.KEY_QINV, rsaGenOut.PriCrtKey.QInv.p, (rsaGenOut.PriKey.N.length)>>1);	//	Read key pair QInv		
			RSA_PrivKeyCrt.QInv.length = rsaGenOut.PriKey.N.length>>1;
		}
		else if(oper == 0x02)	//	Get E
		{
			BigNum_t MK;
			
			MK.p = mask;
			MK.length = 8;
			outputBuf.p = RSAdata.KEY_E;
			if(ALG_OK != tmcRSACrtGetE(&outputBuf,  &(RSA_PrivKeyCrt.P), &(RSA_PrivKeyCrt.Q), &(RSA_PrivKeyCrt.DP), &(RSA_PrivKeyCrt.DQ),&MK))
			{ 
				rtn = 0x6504;
			}
			RSA_PubKey.E.p = RSAdata.KEY_E;
			RSA_PubKey.E.length = outputBuf.length;		
		}			 
	}
	
	if((oper == 0x00) || (oper == 0x01)|| (oper == 0x11) || (oper == 0x21))
	{
		if(usDataLen == RSADAT.length)
		{
			rtn = 0x6985;
		}
		else
		{
			_HalRamCopy(outputdata, RSAdata.Input + usDataLen, len);
			usDataLen += len;
			if((mode & Bit7_En) == 0) 								/* Send all data at once or send the last block */	
				usDataLen = 0;
		} 			
	}
	else if(oper == 0x03)
	{
		len = (u8 )(*(inputdata+1));
		if((mode&0x0F) == 0x03) 		/* RSA-N */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_N + usDataLen, len);
		}
		else if((mode&0x0F) == 0x04)	/* RSA-QINV */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_QINV + usDataLen, len);
		}
		else if((mode&0x0F) == 0x05)	/* RSA-P */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_P + usDataLen, len);
		}
		else if((mode&0x0F) == 0x06)	/* RSA-Q */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_Q + usDataLen, len);
		}
		else if((mode&0x0F) == 0x07)	/* RSA-DP */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_DP + usDataLen, len);
		}
		else if((mode&0x0F) == 0x08)	/* RSA-DQ  */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_DQ + usDataLen, len);
		}
		else if((mode&0x0F) == 0x09)	/* RSA-D */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_D + usDataLen,len);
		}
		else if((mode&0x0F) == 0x02)	/* RSA-E */
		{
			_HalRamCopy(outputdata, RSAdata.KEY_E + usDataLen, len);
		}
		usDataLen += len;
		if((mode & Bit7_En) == 0) 		/* Send all data at once or send the last block */
			usDataLen = 0;
	}
	else if(oper == 0x02)
	{
		_HalRamCopy(outputdata, RSAdata.KEY_E + usDataLen, len);
		usDataLen += len;
		if((mode & Bit7_En) == 0)
		{ 								/* Send all data at once or send the last block */	
			usDataLen = 0;
		} 		
	}
	return rtn;
}

u16 ECCCaculAndResu1t(uint8_t oper, uint8_t *inputdata,uint8_t *outputdata)
{
	u16 rtn = 0x9000;
	BigNum_t outputBuf,inputBuf;
	uint8_t mode = (uint8_t)(*inputdata);
	uint8_t len = (uint8_t )(*(inputdata+1));
	if(mode&0x40)//cacul
	{
		if(oper == 0x00) //enc
		{
			//
		}
		else if(oper == 0x01) //dec
		{
		
		}
		else if(oper == 0x02) //sign
		{
			inputBuf.p = EccDataBuf1;
			inputBuf.length = ECC_DataBuf1.length;
			outputBuf.p = EccDataBuf2;		
			if(tmcECCSign(&outputBuf, &inputBuf, &ECC_PrivKey, &ECC_DOMAIN) != ALG_OK)
			{			
				rtn = 0x6504;
			}
			ECC_DataBuf2.length = outputBuf.length;		
		}
		else if(oper == 0x03) //verify
		{
			inputBuf.p = EccDataBuf1;
			inputBuf.length = ECC_DataBuf1.length;
		
			if(tmcECCVerify(&inputBuf, &ECC_PubKey, &ECC_DataBuf2, &ECC_DOMAIN) != ALG_OK)
			{			
				rtn =  0x6504;
			}
	
		}
		else if(oper == 0x04) //PointAdd
		{
			inputBuf.p = EccDataBuf2;
			inputBuf.length = ECC_DataBuf2.length;
			outputBuf.p = EccDataBuf2;
			if(tmcECCPointAdd(&outputBuf, &ECC_DataBuf1, &inputBuf, &ECC_DOMAIN) != ALG_OK)
			{				
				rtn =  0x6504;
			}
			ECC_DataBuf2.length = outputBuf.length;		
		}
		else if(oper == 0x05) //PointMulti
		{
			inputBuf.p = EccDataBuf2;
			inputBuf.length = ECC_DataBuf2.length;
			outputBuf.p = EccDataBuf2;				
			if(tmcECCPointMul(&outputBuf, &ECC_DataBuf1, &inputBuf, &ECC_DOMAIN) != ALG_OK)
			{				
				rtn =  0x6504;
			}
			ECC_DataBuf2.length = outputBuf.length;
		}
		else if(oper == 0x06) //KeyGen
		{
			u8 type = 0;
			if (mode &0x20)
			{
				type = 0x01;
			}
			/* Genarate key,input prikey,output pubkey */		
			while((tmcECCKeyGen(&ECC_PubKey,&ECC_PrivKey,&ECC_DOMAIN,type) != ALG_OK));
		}
		else if(oper == 0x07) //KeyExchange
		{
		
		}
		else if(oper == 0x08) //SetUserMessage
		{
		
		}
		else if(oper == 0x09) //ParamCheck
		{
			if(tmcECCParamCheck(&ECC_DOMAIN)!= ALG_OK)
			{			
				rtn =  0x6504;	
			}				
		}			
	}
	if ((oper == 0x02)||(oper == 0x03) || (oper == 0x04) || (oper ==0x05))
	{
		if (usDataLen == ECC_DataBuf2.length )
		{
			rtn = 0x6985; 
		}
		else
		{
			_HalRamCopy(outputdata, EccDataBuf2 + usDataLen, len);
			usDataLen += len;
			if((mode & Bit7_En) == 0)
			{ 								/* Send all data at once or send the last block */	
				usDataLen = 0;
			}
		} 							
	}
	else if(oper== 0x06)
	{
		if((mode&0x0f) == 0x03)
		{
			_HalRamCopy(outputdata,ECC_PubKey.p + usDataLen,len);			
		}
		else if((mode&0x0f) == 0x04)
		{
			_HalRamCopy(outputdata,ECC_PrivKey.p + usDataLen,len);	
		}
		else
		{
			return 0x6985;	
		}
		usDataLen += len;
		/* Send data once time */
		if((mode & Bit7_En) == 0)
		{ 
			usDataLen = 0;		
		}
	}

	return rtn;
}

u16 SM3InitFunc(void)
{
	u16 rtn = 0x9000;
	tmcSM3Init(&SM3_Context);
	return rtn;
}

u16 SHA1InitFunc(void)
{
	u16 rtn = 0x9000;
	tmcSHA1Init(&SHA1_Context);
	return rtn;
}

u16 SHA256InitFunc(void)
{
	u16 rtn = 0x9000;
	tmcSHA256Init(&SHA256_Context);
	return rtn;
}

u16 SM3Input(uint8_t *inputdata, uint8_t len)
{	
	u16 rtn = 0x9000;
	tmcSM3Update(&SM3_Context,inputdata,len);
	return rtn;	
}

u16 SHA1Input(uint8_t *inputdata, uint8_t len)
{	
	u16 rtn = 0x9000;
	tmcSHA1Update(&SHA1_Context,inputdata,len);
	return rtn;	
}

u16 SHA256Input(uint8_t *inputdata, uint8_t len)
{	
	u16 rtn = 0x9000;
	tmcSHA256Update(&SHA256_Context,inputdata,len);
	return rtn;	
}

u16 SM3FinalFunc(uint8_t *inputdata, uint8_t *outputdata, uint8_t len)
{
	u16 rtn = 0x9000;
	tmcSM3Update(&SM3_Context,inputdata,len);
	if(tmcSM3Final(outputdata,&SM3_Context) != ALG_OK)
	{
		rtn =  0x6504;
	}
	return rtn;		
}

u16 SHA1FinalFunc(uint8_t *inputdata, uint8_t *outputdata, uint8_t len)
{
	u16 rtn = 0x9000;
	tmcSHA1Update(&SHA1_Context,inputdata,len);
	if(tmcSHA1Final(outputdata,&SHA1_Context) != ALG_OK)
	{
		rtn =  0x6504;
	}
	return rtn;		
}

u16 SHA256FinalFunc(uint8_t *inputdata, uint8_t *outputdata, uint8_t len)
{
	u16 rtn = 0x9000;
	tmcSHA256Update(&SHA256_Context,inputdata,len);
	if(tmcSHA256Final(outputdata,&SHA256_Context) != ALG_OK)
	{
		rtn =  0x6504;
	}
	return rtn;		
}


