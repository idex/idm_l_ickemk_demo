
;/*****************************************************************************
; * @file:    startup_MPS_CM0.s
; * @purpose: CMSIS Cortex-M0 Core Device Startup File 
; *           for the ARM 'Microcontroller Prototyping System' 
; * @version: V1.0
; * @date:    19. Aug. 2009
; *------- <<< Use Configuration Wizard in Context Menu >>> ------------------
; *
; * Copyright (C) 2008-2009 ARM Limited. All rights reserved.
; * ARM Limited (ARM) is supplying this software for use with Cortex-M0 
; * processor based microcontrollers.  This file can be freely distributed 
; * within development tools that are supporting such ARM based processors. 
; *
; * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
; * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
; * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
; * ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
; * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
; *
; *****************************************************************************/

; <h> Stack Configuration
;   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>
Stack_Size      EQU     0x00000600

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp;	EQU		0x20001600

; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>
Heap_Size       EQU     0x00000000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit

                PRESERVE8
                THUMB

; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors

__Vectors       DCD     __initial_sp              ; Top of Stack
                DCD     Reset_Handler             ; Reset Handler
                DCD     NMI_IRQHandler            ; NMI Handler
                DCD     HardFault_IRQHandler      ; Hard Fault Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     SVC_Handler               ; SVCall Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     PendSV_Handler            ; PendSV Handler
                DCD     SysTick_Handler           ; SysTick Handler

                ; External Interrupts
                DCD     CT_IRQHandler      		;0: ct
                DCD     TIMER0_IRQHandler		;1: timer0           
                DCD     NVM0_IRQHandler         ;2: NVM0 
                DCD     ALGORITHM_IRQHandler    ;3: algorithm
                DCD     SECURITY_IRQHandler     ;4: security 
				DCD     0			 			;5: Reserved          
                DCD     APP_IRQHandler			;6: app
                DCD     TIMER1_IRQHandler       ;7: timer1
                DCD     TIMER2_IRQHandler       ;8: timer2
				DCD     0			  			;9: Reserved
				DCD     0			  			;10: Reserved
				DCD     0			  			;11: Reserved
				DCD     0			  			;12: Reserved
				DCD     0			  			;13: Reserved
				DCD     SPI_IRQHandler 			;14: SPI
				DCD     GPIO_IRQHandler 		;15: Reserved
				DCD     0			  			;16: Reserved
                DCD     RF_IRQHandler         	;17: rf
                DCD     0 						;18: I2C
                DCD     0           			;19: Reserved
                DCD     0                       ;20: Reserved
                DCD     0                       ;21: Reserved
                DCD     0                       ;22: Reserved
                DCD     0                       ;23: Reserved
                DCD     0                       ;24: Reserved
                DCD     0                       ;25: Reserved
                DCD     0                       ;26: Reserved
                DCD     0                       ;27: Reserved
                DCD     0               		;28: Reserved
                DCD     0                       ;29: Reserved
                DCD     0          				;30: Reserved
                DCD     0            			;31: Reserved

                AREA    |.text|, CODE, READONLY

; Reset Handler
Reset_Handler   PROC
                EXPORT  Reset_Handler             [WEAK]
                IMPORT  __main
				LDR     R0, =__main
                BX      R0
                ENDP

; Dummy Exception Handlers (infinite loops which can be modified)
NMI_IRQHandler     		PROC
                	EXPORT  NMI_IRQHandler          [WEAK]
                	B       .
                	ENDP
HardFault_IRQHandler   	PROC
                	EXPORT  HardFault_IRQHandler    [WEAK]
                	B       .
                	ENDP
SVC_Handler     		PROC
                	EXPORT  SVC_Handler             [WEAK]
                	B       .
                	ENDP
PendSV_Handler  		PROC
                	EXPORT  PendSV_Handler          [WEAK]
                	B       .
                	ENDP
SysTick_Handler 		PROC
                	EXPORT  SysTick_Handler         [WEAK]
                	B       .
                	ENDP
CT_IRQHandler      		PROC
					EXPORT	CT_IRQHandler      		[WEAK]
					B		.
					ENDP	
TIMER0_IRQHandler		PROC
					EXPORT	TIMER0_IRQHandler		[WEAK]
					B		.
					ENDP	
NVM0_IRQHandler     	PROC
					EXPORT	NVM0_IRQHandler     	[WEAK]
					B		.
					ENDP	
ALGORITHM_IRQHandler	PROC
					EXPORT	ALGORITHM_IRQHandler	[WEAK]
					B		.
					ENDP	
SECURITY_IRQHandler 	PROC
					EXPORT	SECURITY_IRQHandler 	[WEAK]
					B		.
					ENDP	
APP_IRQHandler			PROC
					EXPORT	APP_IRQHandler			[WEAK]
					B		.
					ENDP	
TIMER1_IRQHandler   	PROC
					EXPORT	TIMER1_IRQHandler   	[WEAK]
					B		.
					ENDP	
TIMER2_IRQHandler   	PROC
					EXPORT	TIMER2_IRQHandler   	[WEAK]
					B		.
					ENDP
SPI_IRQHandler       	PROC
					EXPORT	SPI_IRQHandler       	[WEAK]
					B		.
					ENDP		
GPIO_IRQHandler       	PROC
					EXPORT	GPIO_IRQHandler       	[WEAK]
					B		.
					ENDP	
RF_IRQHandler       	PROC
					EXPORT	RF_IRQHandler       	[WEAK]
					B		.
					ENDP	
                ALIGN

; User Initial Stack & Heap
                IF      :DEF:__MICROLIB
                
                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit
                
                ELSE
                
                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap
__user_initial_stackheap

                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR

                ALIGN

                ENDIF

                END

