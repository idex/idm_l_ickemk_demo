/** ***************************************************************************
 * \file   idxHalTimer.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         Timers. If a new platform is to be supported
 *         then it is expected that the HAL APIs are implemented for that
 *         platform in order to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *
 * This provides an interface to timing functions that are required by
 * the IDEX SDK.
 *
 * There are essentially three capabilities that are provided:
 *
 * 1) The ability to delay for a period of time.
 * 2) The ability to call a call-back procedure, either once
 *    or periodically.
 * 3) The ability to start a timer and get regular timestamps.
 *
 * Of these, the last two may be implemented using a shared hardware timer,
 * so only one of these capabilities is guaranteed to work at a time and
 * IDEX code will not attempt to use both of these capabilities at the
 * same time as one-another.
 *
 * The ability to delay for a period may be used alongside (one of)
 * the other capabilities - on SEs with sufficient hardware timers it
 * will have its own timer so that it will have no impact on the other
 * timing functions. (On an SE that does not have more than one available
 * timer then the delay capablity might be implemented so as to share a timer,
 * and this may lead to inaccuracies in the other timing functions.)
 *
 * The calls to cause a delay - idxHalTimerDelayMsec() and
 * idxHalTimerDelayUsec() - are not called by IDEX code in situations where
 * the timer completion may occur during NVM programming, so that an
 * implementation that requires an interrupt service routine to be called
 * at this time need not be concerned to ensure they may run successfully
 * during NVM programming. However calls to idxHalTimerTimeOut() and
 * idxHalTimerFreeRun() cause a timer to run that may complete a timeout
 * period during NVM programming.
 * 
 *****************************************************************************/
 
#ifndef _IDX_HAL_TIMER_H_
#define _IDX_HAL_TIMER_H_

#include <stdint.h>
#include <stdbool.h>

typedef void idxHalTimerCallback(void);

typedef enum idxHalTimerRepeat {
  IDX_HAL_TIMER_ONCE = 0x11,
  IDX_HAL_TIMER_REPEAT
} idxHalTimerRepeat;

// Initialize the Timer HAL module
void idxHalTimerInit(void);

// Delay for the requested number of milliseconds
void idxHalTimerDelayMsec(uint16_t msec);

// Delay for the requested number of microseconds
void idxHalTimerDelayUsec(uint16_t usec);

// Clear the timestamp value to zero and start the timer
void idxHalTimerFreeRun(void);

// Return the current timestamp value - number of milliseconds
// since idxHalTimerFreeRun() called.
uint16_t idxHalTimerGetTicks(void);

// Stop the timer
void idxHalTimerStopRun(void);

// Call cb() after msec milliseconds.
// If repeat == IDX_HAL_TIMER_ONCE, then stop the timer
// If repeat == IDX_HAL_TIMER_REPEAT, call again after each msec period
//   until idxHalTimerStopRun() is called.
void idxHalTimerTimeOut(uint16_t msec, idxHalTimerRepeat repeat,
  idxHalTimerCallback *cb);

//////////////////////////////////////////////////////////////////
// Temporarily located here pending updating some SEs to the IDEX
// timer HAL
extern bool idxHalTimerEnableTimeout;
//////////////////////////////////////////////////////////////////

#endif /* _IDX_HAL_TIMER_H_ */
