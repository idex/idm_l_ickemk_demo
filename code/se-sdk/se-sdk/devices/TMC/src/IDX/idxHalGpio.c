/** ***************************************************************************
 * \file   idxHalGpio.c
 * \brief  Implementation of GPIO control HAL API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#include <string.h>
#include "halglobal.h"
#include "idxHalGpio.h"

/**********************************************************************************
 *  HAL - Implementation
 *
 ***********************************************************************************/

/* defines
****************************************************************/
#define S7816_SIO_GPIO4_MASK    ( 0x001c0 )

// GPIO pin definitions/
#define IDEX_GPIO0              0x01	
#define IDEX_GPIO1              0x02	
#define IDEX_GPIO2              0x04
#define IDEX_GPIO3              0x08
#define IDEX_GPIO4              0x10

#define IDEX_SPI_CLK            IDEX_GPIO0	
#define IDEX_SE_HSO             IDEX_GPIO0	
#define IDEX_SPI_MOSI           IDEX_GPIO2		
#define IDEX_SE_HSI             IDEX_GPIO1	
#define IDEX_SPI_MISO           IDEX_GPIO3
#define IDEX_ISO_IO             IDEX_GPIO4	

#define DEMO_SET_BIT(x,y)       (x |= (0x01UL <<y))
#define DEMO_CLR_BIT(x,y)       (x &= (~(0x01UL <<y)))
#define DEMO_CHECK_BIT(x,y)     (x & (0x01UL <<y))

#define TIMEOUT_COUNT			3000
#define TIMEOUT_LIMIT			5000		// Don NOT modify

/* variables
****************************************************************/
static uint8_t isFirstInit;

static idxHalGpioListenerFuncType callback = (idxHalGpioListenerFuncType)NULL;

// forward declaration
typedef void TMC_Gpio_Callback_Type(void *param);
void TMC_Gpio_Register_Callback(TMC_Gpio_Callback_Type *cb);

// External definitions for HS Timeout 
extern volatile bool idxHalT1TimeoutFlag;

//////////////////////////////////
// Internal functions
//////////////////////////////////
/************************************************* 
  Function:     TMC_GPIO_Write(unsigned int data)
  Description:  output data by writting GPIO interface    
  Input: 
  Output:
  Return:
*************************************************/ 
void TMC_GPIO_Write(unsigned int data)
{
    GPIODAT = data;
}

/************************************************* 
  Function:     TMC_GPIO_Read(void)
  Description:  input data by reading GPIO interface    
  Input: 
  Output:
  Return:
*************************************************/ 
unsigned int TMC_GPIO_Read(void)
{
    return GPIODAT;
}
/************************************************* 
  Function:     TMC_GPIO_SetOPDir
  Description:  set GPIO Direction
  Input: 
  Output:
  Return:
*************************************************/ 
void TMC_GPIO_SetOPDir(unsigned char dir)
{
    GPIODIR &= ~dir;
}
/************************************************* 
  Function:     TMC_GPIO_SetIPDir
  Description:  set GPIO Direction
  Input: 
  Output:
  Return:
*************************************************/ 
void TMC_GPIO_SetIPDir(unsigned char dir)
{
    GPIODIR |= dir;
}

/************************************************* 
  Function: HBHSConfig_RDY
  Description: Configure RDY interrupt
  Input: none
  Return: none
************************************************/
static void HBHSConfig_RDY()
{
  CLKIOCON |= 0x08;               // Enable GPIO clock
  IOSEL0 &= ~(0x07 << 12);        // Switch IO1 to GPIO mode

  GPIOCFG0 &= ~IDEX_GPIO1;        // Edge-triggered interrupts
  GPIOCFG1 |= IDEX_GPIO1;         // Rising-Edge

  TMC_GPIO_SetIPDir(IDEX_GPIO1);  // Set RDY Direction to IP

  GPIORESUMEEN &= ~IDEX_GPIO1;    // Disable resume function
  GPIORESUMESTS |= IDEX_GPIO1;    // Write "1" to clear by Software
  GPIOINTEN &= ~IDEX_GPIO1;       // Disable GPIO intr
  GPIOINTSTS |= IDEX_GPIO1;       // Write "1" to clear by Software

  GPIORESUMEEN |= IDEX_GPIO1;     // Enable resume function 
  GPIOINTEN |= IDEX_GPIO1;        // Enable GPIO1 interrupt
  INTIOCON |= 0x08;               // Enable GPIO interrupt

  NVIC_ClearPendingIRQ(GPIO_IRQn);
  NVIC_EnableIRQ(GPIO_IRQn);
}

static void TMC_Gpio_Int_Disable()
{
	GPIORESUMEEN &= ~IDEX_GPIO1; // Disable resume function
	GPIORESUMESTS |= IDEX_GPIO1;	// Write "1" to clear by Software
	GPIOINTEN &= ~IDEX_GPIO1;		// Disable GPIO intr
	GPIOINTSTS |= IDEX_GPIO1;		// Write "1" to clear by Software
}

static void TMC_Gpio_Int_Enable()
{
	GPIORESUMEEN |= IDEX_GPIO1; 	// Enable resume function again
	GPIOINTEN |= IDEX_GPIO1;			// Enable the GPIO interrupt again
}

static void idxHalGpioCallback(void *param) {
	TMC_Gpio_Int_Disable();
	if (callback)
		callback();
	TMC_Gpio_Int_Enable();
}
//////////////////////////////////

//////////////////////////////////
// Interface functions
//////////////////////////////////
void idxHalGpioInit(void) {
	isFirstInit = 1;
}

void idxHalGpioInitHandshake() {
  uint32_t originalClkSysSel = CLKSYSSEL;
	uint32_t iosel = IOSEL0;

	DEMO_SET_BIT(CLKIOCON,3);      // Enable GPIO clock
	IOSEL0 = iosel & S7816_SIO_GPIO4_MASK;  // GPIO[0..3] mode, keep SIO func   

	TMC_GPIO_SetOPDir( IDEX_SE_HSO );  // HSO direction = O/P

	GPIOINTEN &= ~IDEX_SE_HSI;         // Disable RDY pin specific interrupt
	INTIOCON &= ~0x08;            // Disable global GPIO interrupt

  // Since the handshake is often performed by polling, take care of power
  // by switching to a very low CPU frequency
  CLKSYSSEL = 0x000000F0; // 4.5MHZ

	if(isFirstInit)
	{
		volatile uint32_t cnt = 1200;
		while(cnt--);
		isFirstInit = 0;
	}
	/* Polling based handshaking */
	// Step 1: Set SE_HSO goes LOW
	TMC_GPIO_Write((TMC_GPIO_Read() & ~IDEX_SE_HSO));        

	// Step 2: Wait for RDY to go LOW
	while((( TMC_GPIO_Read() & IDEX_SE_HSI) == IDEX_SE_HSI));

  CLKSYSSEL = originalClkSysSel; // Restore original clock speed

	IOPULLSEL0 |= 0x7f;            // Enable weak pull up
	IOPULLSEL2 = 0;                 // disable strong pull ups
	TMC_GPIO_SetOPDir( IDEX_SPI_MOSI );// MOSI direction to OP  
}

void idxHalGpioSet(idxHalGpioPinType pin, idxHalGpioStateType state) {
	if (pin == IDX_HAL_GPIO_HSI) {
		if (state == IDX_HAL_GPIO_LOW) {
			TMC_GPIO_Write(TMC_GPIO_Read() & ~IDEX_SE_HSI);
		}
		else {
			TMC_GPIO_Write(TMC_GPIO_Read() | IDEX_SE_HSI);
		}
	}
	else if (pin == IDX_HAL_GPIO_HSO) {
		IOSEL0 &= ~(0x07 << 9);     // Set GPIO1 pin to GPIO mode
		GPIODIR &= ~IDEX_SPI_CLK;    // Select direction to OP    
		if (state == IDX_HAL_GPIO_LOW) {
			TMC_GPIO_Write(TMC_GPIO_Read() & ~IDEX_SE_HSO);
		}
		else {
			TMC_GPIO_Write(TMC_GPIO_Read() | IDEX_SE_HSO);
		}
	} else if (pin == IDX_HAL_GPIO_ISO_IO) {
		switch (state) {
			case IDX_HAL_GPIO_LOW:
				TMC_GPIO_Write(TMC_GPIO_Read() & ~IDEX_ISO_IO);
				TMC_GPIO_SetOPDir(IDEX_ISO_IO);
				break;
			case IDX_HAL_GPIO_HIGH:
				TMC_GPIO_Write(TMC_GPIO_Read() | IDEX_ISO_IO);
				TMC_GPIO_SetOPDir(IDEX_ISO_IO);
				break;
			case IDX_HAL_GPIO_FLOAT:
				TMC_GPIO_SetIPDir(IDEX_ISO_IO);
				break;
			default: { /* Do nothing */ }
		}
	}
}

idxHalGpioStateType idxHalGpioGet(idxHalGpioPinType pin) {
	idxHalGpioStateType r = IDX_HAL_GPIO_IGNORE;
	if (pin == IDX_HAL_GPIO_HSI) {
		r = (TMC_GPIO_Read() & IDEX_SE_HSI) ? IDX_HAL_GPIO_HIGH : IDX_HAL_GPIO_LOW;
	}
	else if (pin == IDX_HAL_GPIO_HSO) {
		r = (TMC_GPIO_Read() & IDEX_SE_HSO) ? IDX_HAL_GPIO_HIGH : IDX_HAL_GPIO_LOW;
	}
	return r;
}

void idxHalGpioSetIRQ(idxHalGpioPinType pin, idxHalGpioIrqEdgeType edge) {
  // Set the priority to be lower than the WTX timer interrupt
  uint32_t timerPriority = NVIC_GetPriority(TIMER0_IRQn);
  uint32_t gpioPriority = timerPriority + 1;
  if (gpioPriority > 3) {
    // Cortex-M0 only has two bits for priority.
    gpioPriority = 3;
  }
  NVIC_SetPriority(GPIO_IRQn, gpioPriority);
  HBHSConfig_RDY();
}

void idxHalGpioConfigure(idxHalGpioPinType pin,
  idxHalGpioConfigType cfg, idxHalGpioStateType state)
{
	if (pin == IDX_HAL_GPIO_HSO) {
		
		if (cfg == IDX_HAL_GPIO_HARDWARE) {
			IOSEL0 = (IOSEL0 & ~(0x7 << 9)) | (0x1 << 9); // Revert to SPI CLK mode
		} else if (cfg == IDX_HAL_GPIO_SWIO) {
			IOSEL0 = (IOSEL0 & ~(0x7 << 9)) | (0x0 << 9); // Set GPIO1 pin to GPIO mode
		}
		
	} else if (pin == IDX_HAL_GPIO_ISO_IO) {
		
		if (cfg == IDX_HAL_GPIO_SWIO) {
			
			CLKIOCON   |= ( 1 << 3);        // GPIO CLK EN
			IOPULLSEL0 &= ~(1 << 2);				// Disable weak pull up
			IOPULLSEL2 &= ~(1 << 2);	     	// Disable strong pull ups
			
			idxHalGpioSet(pin, state);      // Set initial state

			IOSEL0      = (IOSEL0 & ~(0x7 << 6)) | (0x2 << 6); // S7816_SIO_FUNCSEL ==> GPIO4  
		}
		
	}
}

void idxHalGpioRegisterListener(idxHalGpioListenerFuncType listener) {
	callback = listener;
	(void)TMC_Gpio_Register_Callback(idxHalGpioCallback);
}

//////////////////////////////////
