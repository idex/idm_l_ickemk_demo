/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: democmd_EMV.h
 * * SCFID: 
 * * Feature: It is the header file for democmd_EMV.c
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-08-27 by Tom
 * *     1. Original version 0.1
 * */

#ifndef	__DEMOCMD_EMV_H__
#define	__DEMOCMD_EMV_H__
#pragma pack(push)
#pragma pack(1)

void DemoEmvInit(void);
void APDU_SelectADF(void);
void APDU_GPO(void);
void APDU_ReadRecord(void);
void APDU_GetData(void);
void APDU_VerifyPin(void);
void APDU_GAC(void);

#endif
