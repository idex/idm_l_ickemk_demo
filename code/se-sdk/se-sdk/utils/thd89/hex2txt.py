'''
Created on 4 Nov 2019

@author: jl241
'''

import sys, getopt
import re

arguments = len(sys.argv) - 1
if arguments == 0:
   print ('usage: hex2txt.py -i <inputfile> -o <outputfile>')
   sys.exit() 

try:
    opts, args = getopt.getopt(sys.argv[1:],"hi:o:")
except getopt.GetoptError:
    print ('usage: hex2txt.py -i <inputfile> -o <outputfile>')
    sys.exit(2)

inputfile = ''
outputfile = ''

for opt, arg in opts:
    if opt == '-h':
        print ('usage: hex2txt.py -i <inputfile> -o <outputfile>')
        sys.exit()
    elif opt in ("-i"):
        inputfile = arg
    elif opt in ("-o"):
        outputfile = arg

#crc('123456789')
#crcb(0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39)

POLYNOMIAL = 0x1021
PRESET = 0
def _initial(c):
    crc = 0
    c = c << 8
    for _j in range(8):
        if (crc ^ c) & 0x8000:
            crc = (crc << 1) ^ POLYNOMIAL
        else:
            crc = crc << 1
        c = c << 1
    return crc

_tab = [ _initial(i) for i in range(256) ]

def _update_crc(crc, c):
    cc = 0xff & c

    tmp = (crc >> 8) ^ cc
    crc = (crc << 8) ^ _tab[tmp & 0xff]
    crc = crc & 0xffff
#    print ( crc )

    return crc

#def crc(str):
#    crc = PRESET
#    for c in str:
#        crc = _update_crc(crc, ord(c))
#    return crc

def crcb(*i):
    crc = PRESET
    for c in i:
        crc = _update_crc(crc, c)
    return crc

def crc(s):
    crc = PRESET
    ll  = [int(s[2*i:2*i+2],16) for i in range(len(s)//2)]  # Xmodem
    #print( ['{:02x}'.format(x) for x in ll] )
    lll = [int('{:08b}'.format(n)[::-1], 2) for n in ll]   # Kermit
    #print( ['{:02x}'.format(x) for x in lll] )
    #exit(0)
    for x in lll:
        crc = _update_crc(crc, x)
    #return crc   # Xmodem  
    return int('{:016b}'.format(crc)[::-1], 2)  # Kermit

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ------- Tests
#
#x = '06000090'
#y = (0x06, 0x00, 0x00, 0x90)
#print '{:04x}'.format(crc(x))
#exit(0)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ------- MAIN Program starts here
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PAGE_SIZE = 1024   # 512 bytes = 1024 chars
APDU_SIZE = 256    # 128 bytes =  256 chars
out = []           # keeps hex data
pre = []           # keeps prefix info
txt = []           # apdu-s
tst = {}           #
crclog = {}        # CRC info
paglog = {}        # Page log

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Globals
n = 0
dataseg = 0
baseaddr = 0;
f = open(inputfile,"r")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main loop through HEX file
# Parsing groups(1..5): length[1], offset[2], type[1], parity[1], data[N]  
# Types: '00'=data, '01'=EOF, '02''03'=Intel(80x86), '04'=Ext Lin Addr, '05'Start Lin Addr    

for line in f:
    n = n + 1     
#    print( line )
    m = re.search(r":(\S{2})(\S{4})(\S{2})((\S*)\S\S)", line)
#    if(m): #print( m.group(5) )
    if not m : break
    if m.group(3) == '00' :
        #~~~~~~~~~~~~~~~ Data ==> APDU '58'
        if dataseg == 0: 
            dataseg = 1  # start of data segment 
            doffset = int(m.group(2),16);
        out.append(m.group(5))
    else :
        if dataseg == 1 :
            dataseg = 0  # end of data segment
            dd = ''.join(out)
            out=[]
            #print( 'dd=',dd )
            #~~~~~~~~~~~~~ Fill the beginning of page with 0xff bytes
            first = doffset % 0x200
            if first != 0 :
                ds = ''.join([first*'ff',dd])
                doffset = doffset - first
            else: ds = dd
            #~~~~~~~~~~~~~ Fill the end of page with 0xff bytes
            last = len(ds) % PAGE_SIZE
            if last != 0 :
                ddd = ''.join([ds,(PAGE_SIZE-last)*'f'])
            else : ddd = ds            
            
            d1 = [ddd[i:i+APDU_SIZE] for i in range(0,len(ddd),APDU_SIZE)]      # Python3.7
#            d1 = [ddd[i:i+APDU_SIZE] for i in xrange(0,len(ddd),APDU_SIZE)]    # Python2.7
            for i in range(len(d1)) : 
                #print( d1[i])
                t1 = ['0058','{:04x}'.format(doffset+128*i),'80',d1[i]]
                l1 = ''.join(t1)
                #print( ll )
                txt.append(l1)
            d2 = [ddd[i:i+PAGE_SIZE] for i in range(0,len(ddd),PAGE_SIZE)]      # Python3.7
#            d2 = [ddd[i:i+PAGE_SIZE] for i in xrange(0,len(ddd),PAGE_SIZE)]    # Python2.7
            cc = ['{:02x}'.format(crc(x) % 256) + '{:02x}'.format(crc(x) >> 8) for x in d2]
            crclog[ baseaddr +'{:04x}'.format(doffset)] = cc
            #for x in d2 : print( '{:04x}'.format(crc(x)) ) 
            paglog[ baseaddr +'{:04x}'.format(doffset)] = '{:02x}'.format(len(d2)) 
        if m.group(3) == '04' :
            #~~~~~~~~~~~~~~~ Extended Linear Address ==> APDU '50'
            if baseaddr == m.group(5) : continue 
            baseaddr = m.group(5)
            tt = ['0050',m.group(5),'00']
            ll = ''.join(tt)
            txt.append(ll)
        elif m.group(3) == '05' :
            print( 'hex-code 05' )
        elif m.group(3) == '01' :
            #~~~~~~~~~~~~~~~~~~~  End of file APDU='F0' - add CRC values APDU '30'
            prevelem = '0000'
#            for elem,val in sorted( crclog.iteritems()) :   # Python2.7
            for elem,val in sorted( crclog.items()) :        # Python3.7
                if prevelem != elem[:4] :
                    txt.append('0050' + elem[:4]+'00')
                    prevelem = elem[:4]
                #print( 'len=',len(val) ) 
                if len(val) > 64 :
                    l1 = ''.join(val[:64])
                    txt.append('0030'+elem[4:]+'{:02x}'.format(64 * 2 + 2) +'0200'+l1)
                    l2 = ''.join(val[64:])
                    txt.append('0030'+'{:04x}'.format(int(elem[4:],16)+0x8000) +'{:02x}'.format(2*(len(val)-64) + 2)+'0200'+l2)
                else :
                    l3 = ''.join(val[:])
                    txt.append('0030'+elem[4:]+'{:02x}'.format(2*len(val) + 2)+'0200'+l3)   
            #print(paglog)
            #~~~~~~~~~~~~~~~~~~~ Erase part: Erased pages are derived dynamically.
            prev = ''
            for elem,val in sorted( paglog.items()) :        # Python3.7
                if(tst == {}) : 
                    tst[elem] = val
                    prev = elem
                else :
                    if prev[:4] == elem[:4] and int(prev[4:],16)+0x200 == int(elem[4:],16):
                        tst[prev] = '{:02x}'.format(int(tst[prev],16) + int(val,16))
                    else : 
                        tst[elem] = val
                        prev = elem
            for elem,val in sorted( tst.items()) :        # Python3.7
                pre.append('0050' + elem[:4]+'00')
                pre.append('002E' + elem[4:]+'0200'+val)

            txt.append('00f0000000')
            txt.append('#')
f.close()

f = open(outputfile,"wt")
for x in pre : f.write( x +'\n')   
for x in txt : 
    #print( x )
    f.write( x +'\n')   
f.close() 
   
#for i in range(100) : print( txt[i] )
#print( crclog )

