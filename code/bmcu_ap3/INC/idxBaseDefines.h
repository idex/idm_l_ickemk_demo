/*************************************************************************************
* \file  idxBaseDefines.h
* \brief header for Idex's global defines
* \author IDEX ASA
* \version 0.0.0
**************************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __IDX_BASE_DEFINES_H__
#define __IDX_BASE_DEFINES_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "idxBaseErrors.h"
#include "idxBaseTypes.h"

#ifdef WIN32
#pragma pack(push)
#pragma pack(1)
#endif

/* Log messages ID definitions */
enum LOG_MSG_ID
{
	MSG_ID_RESULT,
	MSG_ID_TRACE,
	MSG_ID_ERROR,
	MSG_ID_WARNING,
	MSG_ID_INFO,
	MSG_ID_DEBUG,
	MSG_ID_USER = 1000, /* starting ID for user defined message types */
};

/* ID of (void*) result */
enum LOG_RESULT_ID
{
	RESULT_ID_NONE,
	RESULT_ID_TEST_STATUS,        /* see LOG_TEST_STATUS_RESULT for status values */
	RESULT_ID_TIME,               /* pointer to uint64 (us) */
	RESULT_ID_STRING,             /* pointer to string */
	RESULT_ID_UINT64,             /* pointer to uint64 */
	RESULT_ID_INT64,              /* pointer to int64 */
	RESULT_ID_UINT32,             /* uint32 value */
	RESULT_ID_INT32,              /* int32 value */
	RESULT_ID_UINT16,             /* uint16 value */
	RESULT_ID_INT16,              /* int16 value */
	RESULT_ID_UINT8,              /* uint8 value */
	RESULT_ID_INT8,               /* int8 value */
	RESULT_ID_USER_STRUCT = 1000, /* starting ID for user defined structure ponters */
};

/* ID for TEST_STATUS results values */
enum LOG_TEST_STATUS_RESULT
{
	RESULT_STAT_PASS,
	RESULT_STAT_SUCCESS,
	RESULT_STAT_SKIPPED,
	RESULT_STAT_FAIL,
	RESULT_STAT_DONE,
};

enum
{
	UNKNOWN_SENSOR_TYPE = -1,
	SWIPE_SENSOR_TYPE_0 = 0,
	SWIPE_SENSOR_TYPE_1,
	CARDINAL_SENSOR_TYPE,
	CARDINALP_SENSOR_TYPE,
	EAGLE_SENSOR_TYPE,
	SFL_SENSOR_TYPE,
	EAGLE_SENSOR_8x8,
	EAGLE_SENSOR_9x9,
	EAGLE_SENSOR_4x9,
	KESTREL_SENSOR_9x9,
	AURORA_SENSOR_9x9,
	POLARIS_SENSOR,
	URSA_SENSOR,
} ;

typedef struct PACKED_ATTRIBUTE __idx_ReturnCode
{
	unsigned short module;
	short idxRet;
} idx_ReturnCode_t;

#if 0 && defined(DEBUG)
#include "Debug.h"

__inline bool idx_status(const char *func, idx_ReturnCode_t err)
{
	DEBUG_MSG(ZONE_SERVICE, ("%s returns moduleId %d, idxRet %04x\n", func, err.module, err.idxRet));
	return (err.idxRet == IDX_SUCCESS);
}

#else

#define IDX_SUCCEEDED( func, err ) (err.idxRet == IDX_SUCCESS)

#endif

typedef struct InitDataEntry_t
{
	char iface[64];
	void* handle;
} InitDataEntry;

typedef struct InitDataContainer_t
{
	uint32_t size;
	uint32_t id; 
	InitDataEntry* data;
} InitDataContainer;

typedef struct PACKED_ATTRIBUTE __idx_ImageData
{
	int size;
	unsigned char *data;
} idx_ImageData_t;

typedef struct PACKED_ATTRIBUTE DebugTraceInfo_t
{
	const char * log_folder_name;
	const char * application_name;
	char **argv;
	int argc;
} DebugTraceInfo, *DebugTraceInfo_ptr;

typedef PrePACKED struct S_PACKED __idx_basic_ImageInfo
{
	uint16_t width;
	uint16_t height;
	uint16_t DPI;
	uint16_t depth;

#define WHITE_RIDGE (255)
#define BLACK_RIDGE (0)
	uint8_t  ridgeColor;

#define IMG_ORIENT_WEST_MIRROR  (0)
#define IMG_ORIENT_NORTH_MIRROR (1)
#define IMG_ORIENT_EAST_MIRROR  (2)
#define IMG_ORIENT_SOUTH_MIRROR (3)
#define IMG_ORIENT_WEST         (4)
#define IMG_ORIENT_NORTH        (5)
#define IMG_ORIENT_EAST         (6)
#define IMG_ORIENT_SOUTH        (7)
	uint8_t  orientation_info;

	uint8_t  padding[6];   /* padding to 8-bytes */
} POST_PACKED idx_basic_ImageInfo_t;

typedef struct PACKED_ATTRIBUTE __idx_ImageInfo
{
	unsigned char *data;
	idx_basic_ImageInfo_t info;
} idx_ImageInfo_t;

//////////////////////////////////////////////////////
//
// idx_UnitVersion_t - modules version info
//
typedef struct PACKED_ATTRIBUTE __idx_UnitVersion
{
	unsigned char version ;
	unsigned char revision ;
	unsigned short build ;
} idx_UnitVersion_t ;

#ifdef WIN32
#pragma pack(pop)
#endif

//////////////////////////////////////////////
// Macros for supporting versioning fir staic libraries:
//ADD_VERSION to generate Version TAG:  (e.g - extern const volatile char* compatibility_lib_version = { "ver_modulename:x.x.x" };)
//USE_LIBRARY_VERSION - for using in application to avoid removing version as unused variable by linker
//
// sample usage:
// 1. in compatibility library
//         #include "idxVersion.h"
//         #include "idxBaseDefines.h"
//         ADD_VERSION( compatibility );
// 2. in target application which use compatibility
//         #include "idxBaseDefines.h"
//         USE_LIBRARY_VERSION( compatibility );
//
#define TEXTIFY(x) #x
#define TOSTR(x) TEXTIFY(x)
#ifdef __cplusplus
#define ADD_VERSION(module) \
		extern "C" const volatile char* module##_lib_version ; \
		const volatile char* module##_lib_version USED_ATTRIBUTE = { "IdxVer_" TEXTIFY(module) ":" TOSTR(IDX_VERSION) "." TOSTR(IDX_REVISION) "." TOSTR(IDX_BUILD) }
#define USE_LIBRARY_VERSION(module) \
		extern "C" const volatile char* module##_lib_version; \
		const char* module##_getversion( void ) { return (const char *)module##_lib_version; }
#define ADD_FUZEID(module) \
		extern "C" const volatile char* module##_lib_fuzeid ; \
		const volatile char* module##_lib_fuzeid USED_ATTRIBUTE = { "IDEX" TEXTIFY(module) IDX_FUZE_ID }
#else
#define ADD_VERSION(module) \
		extern const volatile char* module##_lib_version ; \
		const volatile char* module##_lib_version USED_ATTRIBUTE = { "IdxVer_" TEXTIFY(module) ":" TOSTR(IDX_VERSION) "." TOSTR(IDX_REVISION) "." TOSTR(IDX_BUILD) }
#define USE_LIBRARY_VERSION(module) \
		extern const volatile char* module##_lib_version; \
		const char* module##_getversion( void ) { return (const char *)module##_lib_version; }
#define ADD_FUZEID(module) \
		extern const volatile char* module##_lib_fuzeid ; \
		const volatile char* module##_lib_fuzeid USED_ATTRIBUTE = { "IDEX" TEXTIFY(module) IDX_FUZE_ID }
#endif

#endif//__IDX_BASE_DEFINES_H__
