/*
 * * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: ct.h
 * * SCFID: 
 * * Feature: header file for t=0
 * * Version: V0.1
 * *
 * * History: 
 * *   2012-01-03 by Luqian
 * *     1. Original version 0.1
 * */
#ifndef CT_DEF_H
#define CT_DEF_H

#define IS_CT_POW_POS	1
#define IS_CT_RX		2
#define IS_CT_TX		4
#define IS_CT_RST_POS	8
#define IS_CT_ERROR		0x10
#define IS_CT_FIFO		0x20

#define CLR_CT_POW_POS	0xFE
#define CLR_CT_RX		0xFD
#define CLR_CT_TX		0xFB
#define CLR_CT_RST_POS	0xF7
#define CLR_CT_ERROR	0xEF
#define CLR_CT_FIFO		0xDF

//flow control macro
#define RS_ATR	0x01
#define RS_PPS	0x02

#pragma pack(push)
#pragma pack(1)
typedef struct
{
	u8 volatile flow;
	u8 volatile event;
	u16 wStatusBk;
	u16 wRemainLen;
	u16 wINSTimer;
	u8 *g_commBuf;
	u8 pbBuf[255];	//case4��backup upper lavel data
} ST_CT;
#pragma pack(pop)

extern ST_CT g_stCT;

#define CTSoftInit(pbCTBuf) {g_stCT.g_commBuf = pbCTBuf;}
#define CTINSTimerInit(wTimer) {g_stCT.wINSTimer = wTimer;}
#define CT_GetFrameBuffer() (g_stCT.g_commBuf)

#define COMMAND_HEAD_LEN 5

u8 CTinit(void);
u8 SendATR(void);
u8 GetResponse (void);
u8 CTRxCmdHeader(void);
void CTRxCmdBody(void);
u8 CTTxResponse(ST_COMCMD *pstCMD);
void CTtx(u8 *pbSendBuf, u16 wLen);

#endif
