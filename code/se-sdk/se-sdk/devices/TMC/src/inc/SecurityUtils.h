#ifndef __UTILS__
#define __UTILS__

/*
* Function: void SUStopChip(void)
* Description: Chip stop.
* Input: none
* Output: none
* Return: none
* Other:
*/
void SUStopChip (void);

void seEncryptData(u8 *MK, u8 *dest, u8 *src, u16 len);

void seDecryptData(u8 *MK, u8 *TK, u8 *dest, u8 *src, u16 len);

/*
* Function: void GetRand(u16 len, u8 * dataout)
* Description: Get random data.
* Input:  wLen - Random data length.
* Output: pbDataOut - Output random data.
* Return: -
* Other:
*/
typedef unsigned char (*pSUGenRandomNum)( u16 len, u8 *pBuf );
#define CryptoSU_ROMADDR			0x0400B000
#define tmcGenRandomNum_SU			((pSUGenRandomNum)(*(unsigned long*)(CryptoSU_ROMADDR + 0x08)))
#define SUGenRandomNumOLTIRQ SUGenRandomNum

/*
* Function: void CrytoSULibVersion(unsigned char *version)
* Description: Get CryptoLib Version.
* Input: -
* Output: version
* Return: none
* Other:
*/
void CrytoSULibVersion(unsigned char *version);

#endif
