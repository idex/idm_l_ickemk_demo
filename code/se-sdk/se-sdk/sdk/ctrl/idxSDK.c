/** ***************************************************************************
 * \file   idxSDK.c
 * \brief  IDEX SDK initialization.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

#include "idxSDK.h"

#include "idxHalGpio.h"
#include "idxHalTimer.h"
#include "idxHalSystem.h"

#include "idxSerialinterface.h"
#include "idxSeEmkAPI.h"
#include "bio-ctrl.h"
#include "crypto.h"

void idxSdkInit()
{
  // Initialize the HAL modules that are always required
  idxHalGpioInit();
  idxHalTimerInit();
  // Finally initialize the System HAL, which will call the
  // Init() functions of any HAL modules required by this
  // IDEX system, but not by all (for example SPI, UART).
  idxHalSystemInit();
  
  idxSerialInterfaceInit();
  (void) idxSeEmkInit();
  idxBioCtrlInit();
  idxCryptoInit();
}
