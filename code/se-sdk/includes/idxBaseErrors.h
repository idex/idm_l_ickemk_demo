/*************************************************************************************
* \file  idxBaseErrors.h
* \brief header for IDEX's error defines
* \author IDEX ASA
* \version 0.0.0
**************************************************************************************/
/**************************************************************************************
* Copyright 2013-2019 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __IDX_BASE_ERRORS_H__
#define __IDX_BASE_ERRORS_H__
//
/// Module IDs.
//
#define SW_COMMON_MODULE_ID     1 // Common module.
#define SW_ICK_MODULE_ID        2 // image capture kit module.
#define SW_EMK_MODULE_ID        3 // enroll match kit module.
#define SW_SS_MODULE_ID         4 // Sensor Stack module.
#define SW_IPL_MODULE_ID        5 // Image Processing Library module.
#define SW_SD_MODULE_ID         6 // Sensor Driver module.
#define SW_BIST_MODULE_ID       7 // Sensor Driver module.
//
/// Error codes.
//
// Common error codes (range 0x0000 - 0x0FFF).
#define IDX_SUCCESS                     0x0000  // SUCCESS.
#define IDX_ERR_UNKNOWN                 0x0001  // UNKNOWN ERROR.
#define IDX_ERR_STATE                   0x0002  // Conditions of use not satisfied.
#define IDX_ERR_BAD_PARAMETERS          0x0003  // Bad parameters.
#define IDX_ERR_NOT_SUPPORTED           0x0004  // Function not supported.
#define IDX_ERR_BAD_COMMAND             0x0005  // Command is not valid or not implemented.
#define IDX_ERR_COMM                    0x0006  // Communication problem (EIO).
#define IDX_ERR_NO_MEMORY               0x0007  // Cannot allocate enough memory.
#define IDX_ERR_NOT_INITIALIZED         0x0008  // Function is called before module being initialized.
#define IDX_ERR_NOT_FOUND               0x0009  // The requested item was not found.
#define IDX_ERR_FILE_OPEN_FAILED        0x000a  // Unable to open a file.
#define IDX_ERR_FILE_READ_FAILED        0x000b  // Unable to read from file.
#define IDX_ERR_FILE_WRITE_FAILED       0x000c  // Unable to write to a file.
#define IDX_ERR_IT                      0x000d  // Interrupt problem (ETIME).
#define IDX_ERR_TOO_SMALL               0x000e  // Image too small.
#define IDX_ERR_BAD_QUALITY             0x000f  // Quality is too bad.
#define IDX_ERR_ALLOWED                 0x0010  // Command is not allowed.
#define IDX_ERR_SIZE                    0x0011  // Invalid command size.
#define IDX_ERR_NO_SPACE                0x0012  // Not enough space left on device.
#define IDX_ERR_VALUE_RANGE             0x0013  // Parameter/Value out of range
// image capture kit error codes (range 0x2000 - 0x2FFF).
#define IDX_ERR_TIMEOUT                 0x2000  // User timeout
#define IDX_ERR_ABORTED                 0x2001  // Acquisition aborted
#define IDX_ERR_CALIB                   0x2002  // Wrong calibration data
#define IDX_ERR_NO_WATERMARK            0x2003  // Image is not watermarked
// enroll match kit error codes (range 0x3000 - 0x3FFF).
#define IDX_ERR_ENROLL_IMG_REJECTED     0x3000  // Rejected to enroll image.
#define IDX_ERR_ENROLL_MAX_CAPACITY     0x3001  // Reached to the max enroll images.
#define IDX_ERR_NO_MATCH                0x3002  // Image does not match with template.
#define IDX_ERR_TOO_MUCH_OVERLAP        0x3003  // Too much overlap.
#define IDX_ERR_NEED_MORE_IMAGES        0x3004  // Need more images.
#define IDX_ERR_NOT_ALIGNED             0x3005  // Not properly aligned
#define IDX_ERR_BAD_IMAGE               0x3006  // Image quality/formatting bad

#include "idxIsoErrors.h"

#endif//__IDX_BASE_ERRORS_H__
