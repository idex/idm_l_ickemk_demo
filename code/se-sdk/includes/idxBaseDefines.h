/*************************************************************************************
* \file  idxBaseDefines.h
* \brief header for Idex's global defines
* \author IDEX ASA
* \version 0.0.0
**************************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __IDX_BASE_DEFINES_H__
#define __IDX_BASE_DEFINES_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "idxBaseErrors.h"
#include "idxBaseTypes.h"

#ifdef WIN32
#pragma pack(push)
#pragma pack(1)
#endif

/* 
   parameter's value (-1) means that for requested parameter, need to use 
   default value specified by device/module which own that parameter 
*/
#define USE_DEFAULT_SENSOR_TIMEOUT (-1)

/* Log messages callback definitions */
typedef void( *TestLibMessageCallback )( void* ctx, int messageId, const char *command, const char *tag, void* result, int resultId, char *description );
typedef void( *DebugMessageCallback )( int messageId, char *message );

/* Log messages ID definitions */
enum LOG_MSG_ID
{
	MSG_ID_RESULT,
	MSG_ID_TRACE,
	MSG_ID_ERROR,
	MSG_ID_WARNING,
	MSG_ID_INFO,
	MSG_ID_DEBUG,
	MSG_ID_USER = 1000, /* starting ID for user defined message types */
};

/* ID of (void*) result */
enum LOG_RESULT_ID
{
	RESULT_ID_NONE,
	RESULT_ID_TEST_STATUS,        /* see LOG_TEST_STATUS_RESULT for status values */
	RESULT_ID_TIME,               /* pointer to uint64 (us) */
	RESULT_ID_STRING,             /* pointer to string */
	RESULT_ID_UINT64,             /* pointer to uint64 */
	RESULT_ID_INT64,              /* pointer to int64 */
	RESULT_ID_UINT32,             /* uint32 value */
	RESULT_ID_INT32,              /* int32 value */
	RESULT_ID_UINT16,             /* uint16 value */
	RESULT_ID_INT16,              /* int16 value */
	RESULT_ID_UINT8,              /* uint8 value */
	RESULT_ID_INT8,               /* int8 value */
	RESULT_ID_USER_STRUCT = 1000, /* starting ID for user defined structure ponters */
};

/* ID for TEST_STATUS results values */
enum LOG_TEST_STATUS_RESULT
{
	RESULT_STAT_PASS,
	RESULT_STAT_SUCCESS,
	RESULT_STAT_SKIPPED,
	RESULT_STAT_FAIL,
	RESULT_STAT_DONE,
};

enum
{
	UNKNOWN_SENSOR_TYPE = -1,
	SWIPE_SENSOR_TYPE_0 = 0,
	SWIPE_SENSOR_TYPE_1,
	CARDINAL_SENSOR_TYPE,
	CARDINALP_SENSOR_TYPE,
	EAGLE_SENSOR_TYPE,
	SFL_SENSOR_TYPE,
	EAGLE_SENSOR_8x8,
	EAGLE_SENSOR_9x9,
	EAGLE_SENSOR_4x9,
	KESTREL_SENSOR_9x9,
	AURORA_SENSOR_9x9,
	POLARIS_SENSOR,
	URSA_SENSOR,
} ;

typedef struct PACKED_ATTRIBUTE __idx_ReturnCode
{
	unsigned short module;
	short idxRet;
} idx_ReturnCode_t;

#if 0 && defined(DEBUG)
#include "Debug.h"

__inline bool idx_status(const char *func, idx_ReturnCode_t err)
{
	DEBUG_MSG(ZONE_SERVICE, ("%s returns moduleId %d, idxRet %04x\n", func, err.module, err.idxRet));
	return (err.idxRet == IDX_SUCCESS);
}

#else

#define IDX_SUCCEEDED( func, err ) (err.idxRet == IDX_SUCCESS)

#endif

typedef struct InitDataEntry_t
{
	char iface[64];
	void* handle;
} InitDataEntry;

typedef struct InitDataContainer_t
{
	uint32_t size;
	uint32_t id; 
	InitDataEntry* buffer;
} InitDataContainer;

typedef struct PACKED_ATTRIBUTE __idx_ImageData
{
	int size;
	unsigned char *buffer;
} idx_ImageData_t;

typedef struct PACKED_ATTRIBUTE DebugTraceInfo_t
{
	const char * log_folder_name;
	const char * application_name;
	char **argv;
	int argc;
	DebugMessageCallback dbg_callback;
} DebugTraceInfo, *DebugTraceInfo_ptr;

typedef PrePACKED struct S_PACKED __idx_basic_ImageInfo
{
	uint16_t width;
	uint16_t height;
	uint16_t DPI;
	uint16_t depth;

#define WHITE_RIDGE (255)
#define BLACK_RIDGE (0)
	uint8_t  ridgeColor;

#define IMG_ORIENT_WEST_MIRROR  (0)
#define IMG_ORIENT_NORTH_MIRROR (1)
#define IMG_ORIENT_EAST_MIRROR  (2)
#define IMG_ORIENT_SOUTH_MIRROR (3)
#define IMG_ORIENT_WEST         (4)
#define IMG_ORIENT_NORTH        (5)
#define IMG_ORIENT_EAST         (6)
#define IMG_ORIENT_SOUTH        (7)
	uint8_t  orientation_info;

	uint8_t  padding[6];   /* padding to 8-bytes */
} POST_PACKED idx_basic_ImageInfo_t;

typedef struct PACKED_ATTRIBUTE __idx_ImageInfo
{
	unsigned char *buffer;
	idx_basic_ImageInfo_t info;
} idx_ImageInfo_t;

//////////////////////////////////////////////////////
//
// idx_UnitFuzeVersion_t - modules version info
//
typedef struct PACKED_ATTRIBUTE __idx_UnitFuzeVersion
{
	unsigned char version ;
	unsigned char revision ;
	unsigned short build ;  // build or shortFuzeID

#define IDEX_VERSION_DEFAULT_PADDING (0x3e3d) // "=>" to make it easy to find in binary by serching "=>IDEX"
	unsigned short pad ; // ( keep structure size as 32 bytes )

	char module_prefix_name[8] ; // 8 bytes module name prefix 
	char module_fuze_id[17] ;    // null terminated fuze id

#define VERSION_SCHEMA_GENERIC  (0) // major minor build
#define VERSION_SCHEMA_FUZE     (1) // major minor fuzeid moduleName
#define VERSION_SCHEMA_FUZE_WIN (2) // major minor fuzeid moduleName shortFuzeID
	char version_schema ;	
} idx_UnitFuzeVersion_t ;

/*
Recommendation for reporting version from idx_UnitFuzeVersion_t data:
Version of module may reported in 3 different formats:

1. by using format major.minor.fuzeid
printf("ICK Version %d.%d.%s\n", ickversion.version, ickversion.revision, ickversion.module_fuze_id);
--> ICK Version 1.0.190918-1dd-6646
--> FW : 4.11.190802-3e1-3693

2. by using full fuzed "modleid + fuzeid"
printf("fizeID %s\n", ickversion.module_prefix_name);
--> fizeID IDEXMICK190918-1dd-6646
--> FuzeID : IDEXFW  190802-3e1-3693

3. For components that do not have fuzeID (major.minor.build)
printf("FW %d.%d.%d\n", ver.version, ver.revision, ver.build);
--> FW : 3.11.1
*/

typedef struct PACKED_ATTRIBUTE __idx_UnitShortVersion
{
	unsigned char version ;
	unsigned char revision ;
	unsigned short build ;
} idx_UnitShortVersion_t ;

#ifdef WIN32
#pragma pack(pop)
#endif

//////////////////////////////////////////////
// Macros to inject fuze id into the binary.
// To find fuzeid in binary need to search "=>IDEX", and after that label, expected to see 4-bytes MODULE name and then FuzeID.
//
// sample code for using:
// 
// #include "idxVersion.h"
// #include "idxBaseDefines.h"
//
// ADD_FUZEID( MICK );
//
// ...
//
// strcpy( module_prefix_name, GET_FUZEID_STR( MICK ) ); //use fuzeID by GET_FUZEID_STR() to avoid removing it by linker.
//
//
//
#define TEXTIFY(x) #x
#define TOSTR(x) TEXTIFY(x)
#define LIBNAME_CONCAT(module) module ## _lib_fuzeid
#define LIBNAME_FUZEID(module) LIBNAME_CONCAT(module)
#define ADD_FUZEID(module) \
		const char LIBNAME_FUZEID(module)[] = { "=>" "IDEX" TEXTIFY(module) IDX_FUZE_ID }
#define GET_FUZEID_STR(module) \
		(const char *)&LIBNAME_FUZEID(module)[2]

#endif//__IDX_BASE_DEFINES_H__
