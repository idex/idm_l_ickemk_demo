/*
* Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
* All rights reserved.
*
* FileName: smlib.h
* SCFID: 
* Feature: SM1, SM2, SM3, SM4 and SSF33 algorithm
* Version: V0.1
*/
#ifndef __SM_LIB_H__
#define __SM_LIB_H__


/*
* Function: void cryptoSM1ECB(u8 * pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode)
* Description: SM1 calculation.
* Input:  pbKey - EK. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - 0 Encrypt, 1 Decrypt.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSM1ECB(u8 * pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode);
u8 SM1_Crypto_sw(u8 *EK, u8 *input, u8 *output,u8 encrypt, u8 *AK, u8 *SK, u8 CryptRound);

/*
* Function: void cryptoSM4ECB(u8 *pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode)
* Description: SM4 ECB calculation.
* Input:  pbKey - SM4 key. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
*                bit1: 0 Use new key, 1 Use last key.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSM4ECB(u8 *pbKey, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode);

/*
* Function: void cryptoSM4CBC(u8 *pbKey, u8 *pbIV, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode)
* Description: SM4 CBC calculation.
* Input:  pbKey - SM4 key. 16 bytes length.
*         pbIV - Initialization vector. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
*                bit1: 0 Use new key, 1 Use last key.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSM4CBC(u8 *pbKey, u8 *pbIV, u8 *pbDataInput, u8 *pbDataOutput, u8 bMode);

/*
* Function: void cryptoSSF33ECB(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode)
* Description: SSF33 calculation.
* Input:  pbKey - MK. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSSF33ECB(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode);

#endif
