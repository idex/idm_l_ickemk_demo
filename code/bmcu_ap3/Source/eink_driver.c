
#include "main.h"

uint8_t PIC_Orientation[4000];

extern unsigned char* dataImage;

extern void uSleep( uint32_t us );
extern void Sleep( uint32_t ms );
extern am_hal_gpio_pincfg_t g_AM_HAL_GPIO_INPUT_pullup_weak;

extern void apollo3_spi_epd_send(uint8_t data, uint8_t dc);
const uint8_t result_match[640]={
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	};
const uint8_t result_no_match[]={
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x3f,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xcf,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xf3,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xfc,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff

};

const uint8_t result_error[640]={
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
	};
const uint8_t eink_dc_option[54]={0,1,
					0,1,
					0,1,1,1,
					0,1,1,1,1,
					0,1,
					0,1,1,1,
					0,1,
					0,1,
					0,1,
					0,1,
					0,1,1,
					0,1,1,1,1,
					0,1,1,1,1,1,1,
					0,1,
					0,1,
					0,1,
					0,1,
					0,1,1,
					0
	};
const uint8_t eink_init_data[54]={0x74,0x54,
				0x7E,0x3B,
				0x01,0xF9,0x00,0x00,
				0x0C,0x8B,0x9C,0x96,0x0F,
				0x03,0x17,
				0x04,0x41,0x8e,0x32,
				0x3a,0x2b,
				0x3b,0x0b,
				0x3c,0xc0,
				0x11,0x01,
				0x44,0x00,0x0f,
				0x45,0xf9,0x00,0x00,0x00,
				0x37,0x00,0x00,0x00,0x00,0x00,0x80,
				0x18,0x80,
				0x21,0x00,
				0x22,0xf7,
				0x4e,0x00,
				0x4f,0xf9,0x00,
				0x24
	};

const am_hal_gpio_pincfg_t g_AM_HAL_GPIO_INPUT_NONE_PULLUP =
{
	3,//.uFuncSel		=
	0,//	ePowerSw
	AM_HAL_GPIO_PIN_PULLUP_NONE,//	ePullup
	0,//.eDriveStrength = 
	AM_HAL_GPIO_PIN_OUTCFG_DISABLE,//.eGPOutcfg  =
	AM_HAL_GPIO_PIN_INPUT_ENABLE,// eGPInput
	0,// eIntDir
	AM_HAL_GPIO_PIN_RDZERO_READPIN,// eGPRdZero
	0,//  uIOMnum
	0,// uNCE
	0,//eCEpol
	0// uRsvd22

};



int EPD_BUSY(void)
{
	int ret;
	ret=((GPIO->RDA & (1<<(EPD_BUSY_PIN&0x1f)))>0)? 1:0;
	return ret;
}


void EpdWaitBusyStatus()
{ 
	while(1)
  	{
    		uSleep(5);
    		if(EPD_BUSY() == 0)
                { 		 
                  break;
                }
  	}      
}

//am_hal_gpio_pinconfig(CS_pin,g_AM_HAL_GPIO_OUTPUT);
//am_hal_gpio_output_set(CS_pin);


void EPD_PORT_INITIAL(void)
{
//busy
am_hal_gpio_pinconfig(EPD_BUSY_PIN,g_AM_HAL_GPIO_INPUT_NONE_PULLUP);

//nres
am_hal_gpio_pinconfig(EPD_NRES_PIN,g_AM_HAL_GPIO_OUTPUT);
am_hal_gpio_output_set(EPD_NRES_PIN);

//ndc
am_hal_gpio_pinconfig(EPD_NDC_PIN,g_AM_HAL_GPIO_OUTPUT);
am_hal_gpio_output_set(EPD_NDC_PIN);

//ncs
am_hal_gpio_pinconfig(EPD_NCS_PIN,g_AM_HAL_GPIO_OUTPUT);
am_hal_gpio_output_set(EPD_NCS_PIN);



 EPD_CS_H;
 EPD_RST_H;

}


void eink_send_command(uint8_t data)
{
	apollo3_spi_epd_send(data,0);
}
void eink_send_data(uint8_t data)
{
	apollo3_spi_epd_send(data,1);

}

void eink_spi_header_transfer(uint8_t *data, int data_size, uint8_t *dc_flag)
{
	int i;
	uint8_t *p_data;
	uint8_t *p_dc;
	
	p_data=data;
	p_dc=dc_flag;

	for(i=0;i<data_size;i++)
	{
		apollo3_spi_epd_send(*p_data,*p_dc);
		p_dc++;
		p_data++;
	}
}


int eink_spi_image_data_transfer( unsigned char *buffer, int size )
{
	int ret = 0;
	int k;
//	uint8_t *tp;
	uint32_t *point;
	uint32_t temp_data,temp_datab,temp_datac,temp_datad;//,temp_datae;
	uint32_t count;

	EPD_DC_H;//data send
	
	k=0;
	point=(uint32_t *)&buffer[0];
	EPD_CS_L;

	while(k<size)
		{
		if(k<size)
			{
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			IOM0->FIFOPUSH=*point;
			point++;
			temp_datab=(32<=(size-k))? 32:(size-k);
			}
		IOM0->INTCLR|=AM_HAL_IOM_INT_ALL;
		temp_data=(0x00000001+(temp_datab<<8));
		IOM0->CMD=temp_data;
		while(0x00000000==(0x00000001&IOM0->INTSTAT));
		count=0;
		while(count<temp_datab)
			{
			temp_data=IOM0->FIFOPTR;
			temp_datac=((temp_datab-count)>=4)? 4:(temp_datab-count);
			if(((temp_data&0x00ff0000)>>16)>=temp_datac)
				{					
				temp_datad=IOM0->FIFOPOP;//read and clear fifo
				k+=temp_datac;
				count+=temp_datac;
				}
			}
		}
	IOM0->FIFOCTRL&=~IOM0_FIFOCTRL_FIFORSTN_Msk;
	uSleep(5);
	IOM0->FIFOLOC=0;
	IOM0->FIFOCTRL|=IOM0_FIFOCTRL_FIFORSTN_Msk;

	EPD_CS_H;
	return ret ;
}





void spi_swith_to_sensor(void)
{
	IOM0->CLKCFG	= 0x00000401;
	IOM0->MSPICFG	|= IOM0_MSPICFG_FULLDUP_Msk|IOM0_MSPICFG_SPHA_Msk|IOM0_MSPICFG_SPOL_Msk;

}


void spi_swith_to_eink(void)
{
	EPD_CS_H;
	EPD_RST_H;
	IOM0->CLKCFG 	= 0x00000d01;
	IOM0->MSPICFG	&= ~(IOM0_MSPICFG_SPHA_Msk|IOM0_MSPICFG_SPOL_Msk);
	IOM0->FIFOCTRL&=~IOM0_FIFOCTRL_FIFORSTN_Msk;
	uSleep(5);
	IOM0->FIFOLOC=0;
	IOM0->FIFOCTRL|=IOM0_FIFOCTRL_FIFORSTN_Msk;


}
void EpdReset()
{
  	EPD_CS_H;
  	EPD_DC_L;
  	EPD_SI_L;
  	EPD_SCK_L;  
  	EPD_RST_H;
  	EPD_RST_L;
  	Sleep(1);                
  	EPD_RST_H;
	uSleep(10);  
  	EpdWaitBusyStatus();
  	eink_send_command(0x12);   			//??RESET
  	EpdWaitBusyStatus();
}

//note: from deep sleep to wakeup, a reset should be issued.
void EpdEnterDeepSleep()  
{
  	eink_send_command(0x10);
	eink_send_data(0x01);
}

void fast_epdinit(void)
{

EpdReset(); 
//eink_OPM021B1_stm32_spi_init();

eink_spi_header_transfer((uint8_t *)eink_init_data,54,(uint8_t *)eink_dc_option);



}


void fps_image_to_eink(void)
{
uint8_t i;
uint8_t j;
uint8_t k;
uint8_t m;
uint8_t data;
int image_size=16641;
int eink_offset=3999;
uint8_t *p_image;
uint16_t offset=0;

p_image=(uint8_t *)&dataImage[0];



eink_offset=1935;
memset(PIC_Orientation+1936,0,2064);
k=0;
while(0<image_size)
{
	j=k&0x07;
	m=(uint8_t)(16-(k>>3));
	for(i=0;i<129;i++)
		{
		data=0;
		if(image_bias< *p_image++)
			{
			data=(1<<(j));
			}
		offset=eink_offset +(uint16_t) ((128-i)*16)+(m);
		//if(3999<offset){printf("size error,offset=%d,i=%d,m=%d\r\n",offset,i,m);}
		PIC_Orientation[offset]|=data;
		}

	image_size-=129;
	k++;
	if(k>128)break;
}


}

void fill_result_to_eink(uint8_t result)
{
	memset(PIC_Orientation,0xff,1936);

	switch (result)
	{
	case 1:
		memcpy(PIC_Orientation+512,result_match,640);
		
		break;
	case 0:
		memcpy(PIC_Orientation+512,result_no_match,640);
		break;
	default:
		memcpy(PIC_Orientation+512,result_error,640);
		break;
	}
}










void EpdDisplayPic(unsigned char num)
{


	spi_swith_to_eink();

	fast_epdinit();

	switch(num)
		{
		case PIC_WHITE:
			{
				memset(PIC_Orientation,0xff,4000);
			}
			break;
		case PIC_BLACK:
			{
				memset(PIC_Orientation,0,4000);
			}
			break;
		default:
			{
			//memset(PIC_Orientation,0xff,4000);
			//memset(PIC_Orientation,0,2000);
				}break;
		}
	eink_spi_image_data_transfer(PIC_Orientation,4000);
  	eink_send_command(0x20);
  	EpdWaitBusyStatus();
	EpdEnterDeepSleep();
	spi_swith_to_sensor();
}


void switch_clk_mosi_to_gpio(void)
{
	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_SCK,g_AM_HAL_GPIO_OUTPUT);
	am_hal_gpio_output_clear(AM_BSP_GPIO_IOM0_SCK);
	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_MOSI,g_AM_HAL_GPIO_INPUT_pullup_weak);

}

void switch_clk_mosi_to_SPI(void)
{
	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_SCK,  g_AM_BSP_GPIO_IOM0_SCK);

	am_hal_gpio_pinconfig(AM_BSP_GPIO_IOM0_MOSI, g_AM_BSP_GPIO_IOM0_MOSI);

}





uint8_t eink_read_command(void)
{
	uint8_t data;
	uint8_t i;
	switch_clk_mosi_to_gpio();
	EPD_SCK_L;
	uSleep(5);
	EPD_DC_H;
	EPD_CS_L;
	uSleep(5);
	data=0;
	for(i=0;i<8;i++)
		{
		EPD_SCK_L;
		uSleep(5);
		data=(data<<1);
		if(GPIO_PIN_RESET==(GPIO->RDA & (1<<EPD_NSDA_PIN)))
			{
			data&=0xfe;
			}
		else
			{
			data|=0x01;
			}
		EPD_SCK_H;
		uSleep(5);

		}
	uSleep(5);
	EPD_SCK_L;

	EPD_CS_H;
	switch_clk_mosi_to_SPI();

	return data;
}


void EpdWait_onoff_Status_wf0154(void)
{ 
	uint16_t timecounter=0;
	uint8_t busy;
	do
	{
		eink_send_command(0x71);
		busy=eink_read_command();
		busy=(busy&0x06);
		Sleep(10);
		timecounter++;
		if(500<timecounter)
			{
			printf("timeout.\r\n");
			break;
			}
	}while(0x04!=busy);
	Sleep(200);
}



void EpdWaitBusyStatus_wf0154(void)
{ 
	uint16_t timecounter=0;
	uint8_t busy;
	do
	{
		eink_send_command(0x71);
		busy=eink_read_command();
		busy=!(busy&0x01);
		Sleep(10);
		timecounter++;
		if(500<timecounter)
			{
			printf("timeout.\r\n");
			break;
			}
	}while(busy);
	Sleep(200);
}


void EpdResetWF0154(void)
{
  	EPD_CS_H;
  	EPD_DC_L;
  	EPD_SI_L;
  	EPD_SCK_L;  
  	EPD_RST_H;
  	EPD_RST_L;
  	Sleep(100);                
  	EPD_RST_H;
	Sleep(100);  
	EPD_RST_L;
	Sleep(100);				
	EPD_RST_H;
	Sleep(100);  
	EPD_RST_L;
	Sleep(100);				
	EPD_RST_H;
	Sleep(100);  

  	EpdWaitBusyStatus_wf0154();

}


void EpdDisplayPic_wf0154_test(unsigned char num)
{

	spi_swith_to_eink();




	
		EpdResetWF0154(); 

		eink_send_command(0x00);
		eink_send_data(0x1f);


		memset(PIC_Orientation,0xff,2888);
		eink_send_command(0x10);
		eink_spi_image_data_transfer(PIC_Orientation,2888);
		memset(PIC_Orientation,0x55,2888);
		eink_send_command(0x13);
		eink_spi_image_data_transfer(PIC_Orientation,2888);





		
		eink_send_command(0x06);
		eink_send_data(0x1f);
		eink_send_data(0x1f);
		eink_send_data(0x1f);


		eink_send_command(0x61);
		eink_send_data(0x98);
		eink_send_data(0x00);
		eink_send_data(0x98);


		//eink_send_command(0x60);
		//eink_send_data(0x11);

		//eink_send_command(0x30);
		//eink_send_data(0x3e);

	
		eink_send_command(0x04);
		EpdWait_onoff_Status_wf0154();



  	eink_send_command(0x12);
	//Sleep(5000);
	EpdWaitBusyStatus_wf0154();

	eink_send_command(0x50);
	eink_send_data(0xf7);

  	eink_send_command(0x02);
	//Sleep(5000);
	EpdWaitBusyStatus_wf0154();

	eink_send_command(0x07);
	eink_send_data(0xa5);

spi_swith_to_sensor();

}





