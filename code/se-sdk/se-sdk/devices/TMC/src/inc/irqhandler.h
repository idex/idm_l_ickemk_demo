/*
 * * Copyright (c) 2014, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: irqhandler.h
 * * SCFID: 
 * * Feature: function list for irahandler
 * * Version: V0.1
 * *
 * * History: 
 * *   2014-11-18
 * *     1. Original version 0.1
 * */

#ifndef IRQHANDER_DEF_H
#define IRQHANDER_DEF_H

extern u8 volatile g_finish;
void CPUWaitForModuleFinish(IRQn_Type IRQ_num);

#define 	EVENT_NOTIFY_MODE 	0
#define 	INT_NOTIFY_MODE 	1
#define		QUERY_NOTIFY_MODE 	2

#include <stdbool.h>
#endif
