/*
* Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
* All rights reserved.
*
* FileName: THD89_Hal.h
* SCFID:
* Feature: 
* Version: V0.1
*/
#ifndef __THD89_HAL_H__
#define __THD89_HAL_H__

#include "type.h"

typedef enum {
  NOCMD = 0,
  CTCMD,
  RFCMD,
  BATCMD  
}CMDTYPE;

typedef struct {
	u8 bPipeNum;	  //RxCmdHead
	u8 bFlagIn;		  //RxCmdHead
	u8 header[5];	  //RxCmdHead
	u16 wStatusWord;  //IoSetStatus
	u16 wStatusBk;	  //_IoSetOriStatus
	u16 wOutLen;	// RxCmdHead
	u8 *pbBuf;		//RxCmdHead_Init
}ST_COMCMD;

/*
 * The APDU buffer size must match the value APDU_BUFFER_LENGTH
 * defined in com.sun.javacard.impl.Constants.
 */
#define APDU_BUFFER_SIZE	261

// information field size for ICC i.e Maximum Incoming BlockSize in T=1
#define IFSC 1
// information field size for IFD i.e Maximum Outgoing BlockSize in T=1
#define IFSD 258

// Protocol Constants definition
// These constants must be in sync with the values
// specified in the class javacard.framework.APDU
#define PROTOCOL_T0                0x00
#define PROTOCOL_T1                0x01
#define PROTOCOL_TCL               0x91

#define HAL_OK	0x00L
#define HAL_UPDATE_COM_CFG_ERR	0xFFFFFFFFL

extern ST_COMCMD g_stCMD;

//To abtain APDU to target address
#define CLA g_stCMD.header[0]
#define INS g_stCMD.header[1]
#define PA1 g_stCMD.header[2]
#define PA2 g_stCMD.header[3]
#define PA3 g_stCMD.header[4]
#define LC g_stCMD.header[4]
#define CMM_DAT ((u8 *)(g_stCMD.pbBuf+5))

#define IS_CT (IOSTS0 & 0x10)
#define IS_RF !(IOSTS0 & 0x10)

#define _IoGetAPDUDat(dest) {if(dest != g_stCMD.pbBuf) _HalRamCopy(dest, g_stCMD.pbBuf, LC+5);}

void ioInit(u8* pbCL4Buf, u8* pbCTBuf);

/*
* Function: u8 ioRxCmdHead(u8* pbBuf);
* Description: Receive command header
* Input:  pbBuf - Set the 5 commend haeder to this buffer
* Output: - 
* Return: 1: receive APDU
          0: none, continue to call the interface waits to receive APDU 
*/
u8 ioRxCmdHead(u8* pbBuf);

/*
* Function: u16 ioRxCmdBody(u8 *pbBuf);
* Description: Receive command body
* Input:  pbBuf - Set the 5 commend haeder and data to this buffer
* Output: - 
* Return: - le ngth of APDU
*/
u16 ioRxCmdBody(u8 *pbBuf);

/*
* Function: void _halInitAllSecurity(void);
* Description: inital security 
* Input: -
* Output: -
* Return: -
*/		
void _halInitAllSecurity(void);

void initRam1(void);


void BackupPC(void);

/*
* Function: void ioSendData(u8 *pbDat,u16 uslen);
* Description: Copy the data into the transmit buffer
*              This function can be called repeatedly
*              Data will be send to the transmit buffer accordiing to the order 
* Input:  pbDat - data to send
*         uslen - length of data
* Output: - 
* Return: - 
*/
void ioSendData(u8 *pbDat,u16 uslen);

/*
* Function: void ioSendData(u8 *pbDat,u16 uslen);
* Description: Send the content in the transmit buffer
* Input: -
* Output: - 
* Return: - 
*/
void ioTxResponse(void);

//Set response status word
#define IoSetStatus(sw) {g_stCMD.wStatusWord = (u16)sw;}
#define ioSetStatus(sw) {g_stCMD.wStatusWord = (u16)sw;}

/*
*set response data buffer pointer, if the pointer is always in a place of the function, just call once.
*If share the buffer with the APDU received, better to return data form DAT and reserve instead of cover the header
*/
#define _IoSetResp(pbAddr) {g_stCMD.pbBuf = pbAddr;}

//Get the length of response data
#define _IoGetOutLen() (g_stCMD.wOutLen)

//Set the number of response physical channel, if it is the same with APDU, there is no need to call the interface.
#define _IoSetPipeNum(num) {g_stCMD.bPipeNum = num;}

// Get the APDU received
#define _IoGetBuffer() (g_stCMD.pbBuf)
// Set original status word
#define _IoSetOriStatus(sw) {g_stCMD.wStatusBk = sw;}

/* copy */
void _HalRamCopy(u8 *dest, u8 *src, u16 len);
void _HalRamSet(u8 *dest, u8 value, u16 len);

/*
* Function: void DMAMemcopyTrans( u8 *pbDest, u8 *pbSrc, u16 wLen);
* Description: Data copy and do big to little transfer aat the same time,
*              This is used for algorithm module 
* Input:  pbDest - Destination address for data copy
*         pbSrc - Source address for data copy
*         wLen - Data length
* Output: - 
* Return: - 
*/
void DMAMemcopyTrans(u8 *pbDest, u8 *pbSrc, u16 wLen);

#define DMAMemcopy(pbDest, pbSrc, wLen) _HalRamCopy(pbDest, pbSrc, wLen)

//Set CPU clock of dealing with APDU at the first APDU
void _halCLKInit(void);

/*
* Function: void SetCPUCLK(u8 bMode)
* Description: Set CPU clock frequence.
* Input:  bMode - 00: ocsclk
*		  		  01: tsclk
*        		  02: rfclk(13.56M)
* Output: - 
* Return: -
*/
void SetCPUCLK(u8 bMode);

__asm unsigned long disableIRQ(void);
__asm void restoreIRQ(unsigned long backup);

#endif
