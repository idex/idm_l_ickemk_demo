#ifndef __PKE__H__
#define __PKE__H__

//STATUS RsaOperation(uint8_t mode,uint8_t oper,uint8_t * inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sm2Operation(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS EccOperation(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sha1Operation(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sha256Operation(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sm3Operation(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//
//STATUS RsaOperationResult(uint8_t mode,uint8_t oper,uint8_t * inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sm2OperationResult(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS EccOperationResult(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sha1OperationResult(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sha256OperationResult(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//STATUS Sm3OperationResult(uint8_t mode,uint8_t oper,uint8_t *inputdata,uint8_t *outputdata,uint16_t len);
//void   DataXOR(uint32_t *inputdata,uint32_t xordata,uint16_t len);
u16 SM2Input( uint8_t *inputdata,uint16_t len);
u16 RSAInput( uint8_t *inputdata,uint16_t len);
u16 ECCInput( uint8_t *inputdata,uint16_t len);
u16 SM2CaculAndResult(uint8_t oper, uint8_t *inputdata,uint8_t *outputdata);
u16 RSACaculAndResu1t(uint8_t oper, uint8_t *inputdata,uint8_t *outputdata);
u16 ECCCaculAndResu1t(uint8_t oper, uint8_t *inputdata,uint8_t *outputdata);

u16 SM3InitFunc(void);
u16 SHA1InitFunc(void);
u16 SHA256InitFunc(void);
u16 SM3Input(uint8_t *inputdata, uint8_t len);
u16 SHA1Input(uint8_t *inputdata, uint8_t len);
u16 SHA256Input(uint8_t *inputdata, uint8_t len);

u16 SM3FinalFunc(uint8_t *inputdata, uint8_t *outputdata, uint8_t len);
u16 SHA1FinalFunc(uint8_t *inputdata, uint8_t *outputdata, uint8_t len);
u16 SHA256FinalFunc(uint8_t *inputdata, uint8_t *outputdata, uint8_t len);

#endif
