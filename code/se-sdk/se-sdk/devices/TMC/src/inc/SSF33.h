/*
* Function: void cryptoSSF33ECB(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode)
* Description: SSF33 calculation.
* Input:  pbKey - MK. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
* Output: pbDataOutput - Output result. 16 bytes length.
* Return: -
* Other:
*/
void cryptoSSF33ECB(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode);