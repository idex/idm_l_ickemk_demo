/** ***************************************************************************
 * \file   idxBioPolicies.i
 * \brief  header for set default policies
 * \author IDEX ASA
 *
 * \copyright \parblock
 * \Copyright 2020, IDEX ASA. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX ASA is the owner of this software and all intellectual property rights
 * in and to the software. The software may only be used together with IDEX
 * fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * ASA has no obligation to support this software, and the software is provided
 * "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
 * not to be liable for any damages, any relief, or for any claim by any third
 * party, arising from use of this software.
 * \endparblock
 *****************************************************************************/

0, /* unused(hidden policy entry) */

/** \brief Determine the max number of fingers to enroll */
2, /* max_fingers */

/** \brief Determine the max number of enrolled touches per finger
 *
 * \note This policy over-writes biometric subsystem parameter
 * \a TOUCHES_PER_ENROLL at the start of on-card enrollment.
 */
6, /* max_touches */

/** \brief idxBioInitialize() should initialize the sensor.
 *
 * Determines whether idxBioInitialize() should initialize the sensor, or
 * whether this should be delayed until a call that needs the sensor to
 * be operational.
 */
1, /* sensorinit(default is true(should init)) */

260/* additional(for debug purpose) */
