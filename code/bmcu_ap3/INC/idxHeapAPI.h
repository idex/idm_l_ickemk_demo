/****************************************************************************
* \file  idxHeapAPI.h
* \author IDEX ASA
*****************************************************************************/
/**************************************************************************************
* Copyright 2013-2018 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
**************************************************************************************/

#ifndef __IDXHEAPAPI_H__
#define __IDXHEAPAPI_H__

#include "idxBaseTypes.h"
#include "idxBaseDefines.h"

#ifdef  __cplusplus
extern "C" {
#endif

typedef void*( *fn_SENSOR_Malloc )( size_t );
typedef void( *fn_SENSOR_Free )( void* );

typedef struct SensorHeapCallbacks_t
{
	fn_SENSOR_Malloc    fn_Malloc;
	/*fn_WeakMalloc can be used when heap is divided for some cases and special malloc is needed for some allocations 
	 * For standard releases this function can be same as fn_Malloc
	 */
	fn_SENSOR_Malloc    fn_WeakMalloc;
	fn_SENSOR_Free      fn_Free;
	/*fn_WeakFree is special free, which will be used when memory is allocated using fn_WeakMalloc 
	 * For standard releases this function can be same as fn_Free
	 */
	fn_SENSOR_Free      fn_WeakFree;
} SensorHeapCallbacks, *SensorHeapCallbacks_ptr;


#ifdef  __cplusplus
}
#endif

#endif // __IDXHEAPAPI_H__
