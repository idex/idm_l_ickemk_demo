/** ***************************************************************************
 * \file   idxHalAlgo.c
 * \brief  Implementation of Algo HAL API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#include <stdlib.h>
#include <stdint.h>
#include "idxHalAlgo.h"
#include "idxIsoErrors.h"

#include "halglobal.h"

typedef unsigned char (*pSUGenRandomNum)( uint16_t len, uint8_t *pBuf );
#define CryptoSU_ROMADDR   0x0400B000
#define tmcGenRandomNum_SU ((pSUGenRandomNum)(*(unsigned long*)(CryptoSU_ROMADDR + 0x08)))

void idxHalAlgoInit(void)
{
}

uint16_t idxHalAlgoCRC16(const uint8_t *pData, uint32_t dataLen, uint16_t* pCrcData)
{
  uint16_t ret = IDX_ISO_SUCCESS;
  if((!pData) || (!pCrcData)) 
    return IDX_ISO_ERR_BAD_PARAMS;

  *pCrcData = 0;
  CRC16(((uint8_t *) pCrcData),((uint8_t *) pData), dataLen);
  *pCrcData = (((*pCrcData & 0xff00) >> 8) | ((*pCrcData & 0x00ff) << 8));

	return ret;
}

uint16_t idxHalAlgoGenRandNamber(uint16_t dataLen, uint8_t *pOutData){
	return (uint16_t)tmcGenRandomNum_SU(dataLen,pOutData);
}

uint16_t idxHalAlgoAES128encrypt(const uint8_t *pData, uint8_t *pKey, uint8_t *pIV, uint8_t *pOutDat)
{
  uint8_t ret,i;
  uint8_t iv[]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  
  uint8_t MK[] = {0,0,0,0,0,0,0,0,0};
  uint8_t TK[] = {0,0,0,0,0,0,0,0,0};
  uint8_t bin[16];
  uint32_t bmode = 0xa | (0x0a << 4) | (0x22 << 8) | (5 << 16) | (5 << 21 ) | (5 << 24);

  if((!pData) || (!pKey) || (!pOutDat))
      return IDX_ISO_ERR_BAD_PARAMS; 

  if(!pIV) pIV = iv;

  if(pIV != NULL)  for(i = 0 ; i < 16 ; i++) bin[i] = pData[i] ^ pIV[i];
  else            for(i = 0 ; i < 16 ; i++) bin[i] = pData[i];

  ret = tmcAES_ECB_Mask(pKey, bin, MK, TK, pOutDat, bmode);
  return (ret == 0xaa ? IDX_ISO_SUCCESS : IDX_ISO_ERR_UNKNOWN);
}

uint16_t idxHalAlgoAES128decrypt(const uint8_t *pData, uint8_t *pKey, uint8_t *pIV, uint8_t *pOutDat)
{
  uint8_t ret,i;
  uint8_t iv[]  = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  uint8_t MK[] = {0,0,0,0,0,0,0,0,0};
  uint8_t TK[] = {0,0,0,0,0,0,0,0,0};
  uint32_t bmode = 0x05 | (0x0a << 4) | (0x22 << 8) | (5 << 16) | (5 << 21 ) | (5 << 24);

  if((!pData) || (!pKey) || (!pOutDat))
      return IDX_ISO_ERR_BAD_PARAMS;   

  if(!pIV) pIV = iv;      

  ret = tmcAES_ECB_Mask(pKey, (uint8_t *)pData, MK, TK, pOutDat, bmode);
  if (ret != 0xaa ) return IDX_ISO_ERR_UNKNOWN;
  if(pIV != NULL)  for(i = 0 ; i < 16 ; i++) pOutDat[i] ^= pIV[i];

  return  IDX_ISO_SUCCESS;
}

