/*
 * * Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 * * All rights reserved.
 * *
 * * FileName: ct.c
 * * SCFID: 
 * * Feature: head file of api of communication 
 * * Version: V0.1
 * *
 * * History: 
 * *   2011-12-31 by Luqian
 * *     1. Original version 0.1
 * * 	 2. modified version 0.2
 * */
#ifndef COM_CFG_H

/* The DEMO_VERSION is 12 bytes length.
 * Demo version format: 0x89 + Card Version + Greek Letter + Main Version  + Date
*/
#define DEMO_VERSION 0x89, 0x01, 0xA0, 0x10, 0x20, 0x18, 0x05, 0x04

#define COM_CFG_H
	#define PROTOCOL_SUPPORT 0
	#define CT_CWT 1
	#define CT_EGT	3
	#define CT_TRY	4
	#define CT_BGT	5
	#define CT_ATR_PROTOCOL 6
	#define CT_ATR_HISTORY 20
	#define CT_AUTO_CFG_INFO 36
	#define CL3A_ATQA 37
	#define CL3A_ATQA0 37
	#define CL3A_ATQA1 38
	#define CL3A_SAK 39
	#define CL3A_SAK0 39
	#define CL3A_SAK1 40
	#define CL3A_SAK2 41
	#define CL3A_CASA 42
	#define CL3A_UID 43
	#define CL3A_ATS_PROTOCOLINFO 47
	#define CL3A_ATS_HISTORY 52	
	#define M1_DATA_ADDR 68
	#define CLKSYSSELOFFS5 72
	#define CLKSYSSELOFFS6 73
	#define CLKSYSSELOFFS7 74
	#define CLKSYSSELOFFS8 75
	#define CLKSYSSELOFFS9 76
	#define CLKSYSSELOFFS10 77
	#define CLKSYSSELOFFS11 78
	#define CLKSYSSELOFFS12 79
	#define CPU_CT_CLK 80
	#define PKE_RF_CLK 81
	#define PKE_CT_CLK 82 
	#define M1_MODE    83
	#define CPU_RF_CLK 84
	#define IS_CPUCLK_SPEC 85
	#define IS_SDPASS 86
	#define CPU_RATS_CLK 87
	#define NVM_CMP_BEFORE_WRITE 88
	#define HISTORY_SN_LEN 89
	#define HISTORY_SN_RFA_LEN 90
	#define RF_TYPA_FDT	91
	#define RFPOW_LEVEL	92
	#define RFPOW_DIV_PKE 93
	#define RFPOW_DIV_CPU 94
	#define MINFS 95
	
	#define COM_CFG_SIZE 100
	
	#define T0SUPPORT 0x01
	#define ASUPPORT 0x02
	#define BSUPPORT 0x04
	#define ABSUPPORT 0x06
	#define M1SUPPORT	0x08
	
	#define AUTO_GETRESPONSE 0x04
	#define AUTO_CT_60	0x02
	#define AUTO_CT_3B 0x01
	
	#define ATR_MAX_HIS_LEN 0x0F
	#define ATR_MAX_PROC_LEN 0x0F
	#define ATS_MAX_PROC_LEN 0x04
	#define ATS_MAX_HIS_LEN 0x0F

	extern unsigned char const g_pbComCfg[];

	void ComCfgInit(void);
#endif
