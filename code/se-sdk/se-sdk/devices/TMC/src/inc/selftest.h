/**
  ******************************************************************************
  * @file    THD89/selftest.h
  * @author  GaoY
  * @version V1.0
  * @date    16/05/2017
  * @brief   
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, TMC SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 TMC</center></h2>
  */

typedef void (* pselfcheck)(unsigned long testmsk, unsigned long eeAddr, unsigned short sectorNum, unsigned long *testresult);
typedef unsigned char (*pSelfCrcCheck)(unsigned long DestAddr, unsigned short blockSize, unsigned short blockNum, unsigned char* wRomCRCData);


#define SELFTESTBASEADDR		0x04009000


/*
* Function: unsigned char selfcheck(unsigned long DestAddr, unsigned short blockSize, unsigned short blockNum, unsigned char* wRomCRCData);
* Description: CRC Self Check
* Input:  DestAddr - CRC start address.
*         blockSize - size of each block.
*         blockNum - number of blocks.
*         wRomCRCData - data to be compared
* Output:  pbDataOutput - Output result. 4 bytes length..
* Return: 
* Other:
*/
#define selfcheck		((pselfcheck)(*(unsigned long *)(SELFTESTBASEADDR + 0x00)))

/*
* Function: void SelfCrcCheck(u8 * pbKey, u8 *pbDataInput, u8 * pbDataOutput, u8 bMode)
* Description: SSF33 calculation.
* Input:  pbKey - MK. 16 bytes length.
*         pbDataInput - Input data. 16 bytes length.
*         mode - bit0: 0 Encrypt, 1 Decrypt.
* Output: -.
* Return: 1 - difference
*         0 - same
* Other:
*/
#define SelfCrcCheck		((pSelfCrcCheck)(*(unsigned long *)(SELFTESTBASEADDR + 0x04)))
