/** ***************************************************************************
 * \file   idxHalTimer.c
 * \brief  Implementation of Timer HAL API.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#include <stdlib.h>

#include "halglobal.h"
#include "idxHalTimer.h"

#define UNUSED(p) if(p){}

/*******************************************************
 * Timer0/1/2 common definitions
 *  
 */  
#define CLKSEL_INTERNAL   ( 0 )
#define CLKSEL_ISO7816    ( 1 )
#define CLKSEL_ISO14443   ( 7 )
#define ONE_TIME_MODE     ( 0x00 )
#define NON_STOP_MODE     ( 0x10 )
#define OSC_CLK_HZ        ( 72000000 )
#define RF_CLK_HZ         ( 3390000 )
#define T_OV              ( 1 )

bool idxHalTimerEnableTimeout;
volatile bool idxHalT1TimeoutFlag;

//////////////////////////////////
// Internal functions
//////////////////////////////////
static volatile bool t2to = false;
static idxHalTimerCallback *t2cb  = NULL; // Timer 2 callback
static idxHalTimerRepeat t2repeat = IDX_HAL_TIMER_REPEAT;

/*******************************************************
 * Timer-1 IRQ Handler ==> Hand Shake timeout
 *  
 */ 
void t1IrqHandler(void)
{
	TIMER1STSCLR = 0x01;	//Clear overflow
	TIMER1CONCLR = 0x01;	//Stop timer1
  	INTTRCON &= ~0x02;  //Disable interrupt
	idxHalT1TimeoutFlag	= true;
}

/*******************************************************
 * Timer-2 IRQ Handler ==> LED blinkControl
 *  
 */ 
void t2IrqHandler(void)
{
  // reset overflow-bit
  TIMER2STSCLR = 0x01;  //Clear overflow
  TIMER2CONCLR = 0x01;  //Stop timer-2
  INTTRCON   &= ~0x04;  //Disable interrupt

  if(t2cb != NULL) t2cb();

  if(t2repeat == IDX_HAL_TIMER_ONCE){
    t2to = true;
  }
  else{
    // start Timer2
    TIMER2CONSET = 0x0D;   // 001101b Count during SLEEP, overflow cleard by SW and start 
    INTTRCON    |= 0x04;  //Enable interrupt
  }
}

/****************************************************************************
 *  
 * Timer2 Configuration for enrolment on Sleeve
 *
 * Now this configuration is done via HAL API
 *****************************************************************************/
  

/****************************************************************************
 *  
 * Timer2 Configuration for time measurement during verify phase
 *
 * Master counter is 32 bit SW counter, the timer tick is 28.44 usec so the 
 *           max time is 2^32 * 28.44 usec = 122167 seconds,
 * Timestamp counter tick duration is 16 times Master counter tick duration 
 *           so 16 bit wide means 16*2^16*28.44 usec = 29.8 seconds
 * Normalise to msec  T = ticks * 28.4444 = ticks * 455 / 16
 *****************************************************************************/
static uint32_t masterCnt;
static uint16_t currCnt;
static void startTim2FreeRun(void){
  masterCnt    = 0;
  currCnt      = 0;

  // We assume that the system clk is 72MHz(full power)so 1 usec = 72 cycles  
  CLKSYSSEL = 0;
  
  CLKTRCON    |= 0x04;      // Enable Timer2 clk
  CLKTR2SEL    = (7<<4) | (0 << 0);  // 0 = osc-clk=72Mhz ;  CLK = Fsrc/ ( (DIV=7) + 1 ) = 9 Mhz
  TIMER2CONCLR = 0xFF;   //Clear TIMER2CON flags
  TIMER2RLD    = 0;     // free running
  TIMER2DIV    = 255;   // 
  TIMER2CONSET = 0x1D;   // 011101b Free running ; Count during SLEEP ; overflow cleard by SW ; start 
}

static void stopTim2(void){
  t2cb         = NULL;
  t2repeat     = IDX_HAL_TIMER_REPEAT;
  TIMER2STSCLR = 0x01;  //Clear overflow
  TIMER2CONCLR = 0x3f;   //Clear TIMER2CON flags
  INTTRCON   &= ~0x04;  //Disable interrupt
}

static uint16_t GetTim2Timestamp(void){
  uint16_t cnt = 100; // initial loop counter
  uint32_t tmp;
  uint32_t t2Control = TIMER2CONRD;
  t2Control |= 0x20;  // T2_RTN = 1  for reading counter
  TIMER2CONSET = t2Control;
  
  // avoid infinite loop
  while(cnt-- && (t2Control & 0x20)){
    t2Control = TIMER2CONRD;
  }
  
  cnt = (uint16_t)TIMER2VAL;
  if(cnt < currCnt) masterCnt += (0x10000 + cnt - currCnt);
  else masterCnt += cnt - currCnt;
  currCnt = cnt;  
  // tmp = masterCnt * 233/256/32  ; 233 =0xe9
  tmp = ((masterCnt << 7)  + (masterCnt << 6 ) +(masterCnt << 5 ) + (masterCnt << 3 ) + masterCnt ) >> 13;

  return (uint16_t) tmp; 
}
/*
 * END Timer2 Configuration for time measurement
 *****************************************************************************/


//////////////////////////////////////////////////
// HAL API - Interface functions ( use Timer-2 )
//
//  idxHalTimerInit
//  idxHalTimerFreeRun
//  idxHalTimerGetTicks
//  idxHalTimerStopRun
//  idxHalTimerDelayUsec
//  idxHalTimerDelayMsec
//  idxHalTimerTimeOut
//
//////////////////////////////////////////////////
void idxHalTimerInit()
{
}

void idxHalTimerFreeRun(){
  startTim2FreeRun();
}

uint16_t idxHalTimerGetTicks(){
  return GetTim2Timestamp();
}

void idxHalTimerStopRun(){
  stopTim2();
}



void mcu_execute_usleep(uint32_t us)
{
	uint32_t i;
	uint32_t j,k;
	j=CLKSYSSEL;
	j=(j>>3)&0x000000ff;
	for(i=0;i<us;i++)
		{
		for(k=0;k<j;k++)
			{
			__asm__ ("nop");	
			}

		}

}



/***************************************************************************
 *idxHalTimerDelayUsec(usec)  delay usec microseconds. max delay = 1280 usec
 *-------------------------------------------------------------------------- 
 * The tick value for 3.39Mhz is 0.295 usec (we need 3.39 ticks for one usec) 
 * The closest integer ratio for this is 17/5 = 3.4 = ( 3 + 0.4 )
 * 0.4 ~ 51/128; 51 = 32 + 16 + 2 + 1 
 * The overload is about 30..40 instructions ( ~0.5 usec @72MHz ) so RF clk 
 * works for usec > 10 ( with < 5% error ).
 * For usec < 10 RF clk is too coarse so sys clk is used (assuming 72MHz)
 **************************************************************************/
void idxHalTimerDelayUsec(uint16_t usec) { 
  extern void DelayAsmLoop(uint32_t R0); 
  uint32_t ticks;
  uint32_t clk = CLKSYSSEL;
  // We assume that the system clk is 72MHz(full power)so 1 usec = 72 cycles  
  CLKSYSSEL = 0;
  if( usec < 20){    
    // cnt = 14 * usec ; avoid multiplication which could take up to 32 cycles      
    volatile uint16_t cnt;    
    cnt = (--usec << 3);// the following part is compensated by call overload [+ (usec << 2) + (usec << 1)];
    
    // asm loop takes 5 cycles (sub1,cmp1,bne3) delay = 14*5*usec cycles    
    if(cnt)DelayAsmLoop((uint32_t)cnt); // this takes 5 cycles (sub1,cmp1,bne3) 
    CLKSYSSEL = clk;
    return;
  }

  //72 clock is 1us. usec should multiple 72, 72 =64+8;
	ticks= ((usec<<6)+(usec<<3));
	if(0x0000ffff<ticks)
		{
		ticks=0;
		}
	else
		{
		ticks=0x0000ffff-ticks;
		}


  CLKTRCON    |= 0x04;     //Enable Timer2 clk
  CLKTR2SEL    = (0 << 4) | ( CLKSEL_INTERNAL << 0);  // fclk = Fsrc/( DIV+1) internal clock is 72mhz
  TIMER2CONCLR = 0xFF;     //Clear TIMER2CON flags    
  TIMER2RLD    = ticks;//-ticks;//TIMER2RLD is 16 bit long.0xffff is 0.94ms,
  TIMER2DIV    = 0;        // fclk /= (TIMERDIV + 1)
  TIMER2CONSET = 0x1D;     //Count during SLEEP, overflow cleard by SW and start 
  // loop for OVF bit
  while(!(TIMER2STSRD & T_OV));  
  TIMER2STSCLR = 0x01;   // clear Status register
  TIMER2CONCLR = 0x3f;   //Clear TIMER2CON flags  
  CLKSYSSEL = clk;
}

// delay in milliseconds
void idxHalTimerDelayMsec(uint16_t msec){
  stopTim2();
  t2to = false;
  idxHalTimerTimeOut(msec, IDX_HAL_TIMER_ONCE, NULL);
  while(!t2to){
    __WFI();
  }
}

/**********************************************************************
 Call cb() after msec milliseconds.
 If repeat == IDX_HAL_TIMER_ONCE, then stop the timer
 If repeat == IDX_HAL_TIMER_REPEAT, call again after each msec period
   until idxHalTimerStopRun() is called.
 Note: Fcount = Fsel / (CLKDIV+1) / (TIMER2DIV+1) ; CKDIV = 0-7, TDIV=0-255   
   Fsel = SysClk mode Tmax =  1850 msec ( internal, not accurate)
   Fsel = RFclk  mode Tmax = 39592 msec ( external, accurate)
***********************************************************************/
#define T2_SYS_DIV_FACTOR ( 7 )
#define T2_RF_DIV_FACTOR  ( 7 )
#define T2_DIV_FACTOR     ( 255 )
#define T2_SYS_LIMIT      (  1850 )     // 2^16 * 2^8 / 72000 * 8 )
#define T2_RF_LIMIT       ( 39500 )     // 2^16 * 2^8 /  3390 * 8 )
void idxHalTimerTimeOut(uint16_t msec, idxHalTimerRepeat repeat, idxHalTimerCallback *cb)
{
  extern CMDTYPE g_cmd;
  u32 rld = 0x0000FFFF;
  u8  div = T2_RF_DIV_FACTOR;
  u8  clk = CLKSEL_ISO14443;  

  if(msec == 0) return;

  if(g_cmd == RFCMD){ // If RF enable use RF clock
    if (msec > T2_RF_LIMIT){
      msec = T2_RF_LIMIT;
    }
    rld -= ((u32)msec * 3390) >> 11;   // ticks/msec = 3390 / 256 / 8
  }
  else if((g_cmd == CTCMD) || (g_cmd == BATCMD)){
    div = T2_SYS_DIV_FACTOR;  //use SYS clock
    clk = CLKSEL_INTERNAL;
    if (msec > T2_SYS_LIMIT){
      msec = T2_SYS_LIMIT;
    }
    rld -= ( (u32)msec * 1125 ) >> 5;  // ticks/msec = 72000 / 256 / 8 = 1125 / 32
  }
  else{
    // we shouldn't be here
    return;
  }
  
  t2repeat = repeat;
  t2cb = cb;

  // Set the priority to be lower than the WTX timer interrupt
  {
    uint32_t timer0Priority = NVIC_GetPriority(TIMER0_IRQn);
    uint32_t timer2Priority = timer0Priority + 1;
    if (timer2Priority > 3) {
      // Cortex-M0 only has two bits for priority.
      timer2Priority = 3;
    }
    NVIC_SetPriority(TIMER2_IRQn, timer2Priority);    //Timer1 priority level lower than timer0
  }
  NVIC_ClearPendingIRQ(TIMER2_IRQn);
  NVIC_EnableIRQ(TIMER2_IRQn);
    
  CLKTRCON    |= 0x04;     //Enable Timer2 clk
  CLKTR2SEL    = (div << 4) | (clk << 0);  // 0 = osc-clk=72Mhz ;  CLK = Fsrc/ ( (DIV=7) + 1 ) = 9 Mhz
  TIMER2CONCLR = 0xFF;     //Clear TIMER2CON flags    
  TIMER2RLD    = rld ;     // 1/9 Mhz * 256 = 28.44us so 35 ticks give 1ms.
  TIMER2DIV    = T2_DIV_FACTOR;
  TIMER2CONSET = 0x0D;     //Count during SLEEP, overflow cleard by SW and start 
  INTTRCON    |= 0x04;     //Enable interrupt    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  end of file ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
