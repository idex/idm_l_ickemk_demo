/** ***************************************************************************
 * \file   idxHalSpi.h
 * \brief  Header for Hardware Abstraction Layer (HAL) functions related to
 *         SPI data transfers. If a new platform is to be supported then it is
 *         expected that the HAL APIs are implemented for that platform in order
 *         to satisy the calls from the biometric SE-SDK.
 * \author IDEX Biometrics ASA
 *
 * \copyright \parblock
 * \Copyright IDEX Biometrics ASA 2019-2020. All rights reserved.
 * http://www.idexbiometrics.com
 *
 * IDEX Biometrics ASA is the owner of this software and all intellectual
 * property rights in and to the software. The software may only be used
 * together with IDEX fingerprint sensors, unless otherwise permitted by IDEX
 * Biometrics ASA in writing.
 *
 * This copyright notice must not be altered or removed from the software.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
 * Biometrics ASA has no obligation to support this software, and the software 
 * is provided "AS IS", with no express or implied warranties of any kind, and
 * IDEX Biometrics ASA is not to be liable for any damages, any relief, or for
 * any claim by any third party, arising from use of this software.
 * \endparblock
 *****************************************************************************/
#ifndef _IDX_HAL_SPI_H_
#define _IDX_HAL_SPI_H_

#include <stdint.h>

#include "idxSeSdkTypes.h"

/**
 * @defgroup idxHalSpi SPI HAL module
 *
 * The idxHalSpi module provides an API for SPI communication
 * @{
 */

/** \brief Select speed for SPI operation.
 */
typedef enum idxHalSpiSpeedType {
    IDX_HAL_SPI_SPEED_LOW, //!< Low speed operation - used before initialization
    IDX_HAL_SPI_SPEED_HIGH //!< High speed operation - after power probing
} idxHalSpiSpeedType;

/** \brief Initialize the SPI HAL module at startup.
*/
void idxHalSpiInit(void);

/** \brief Configure SPI hardware ready for data transfer.
*/
void idxHalSpiConfigure(void);

/** \brief Transmit data over SPI
*
* @param[in]  *pBuf    Data to transmit.
* @param[in]  len      Length of data to transmit.
*/
void idxHalSpiTx(const uint8_t *pBuf, uint16_t len);

/** \brief Receive data over SPI
*
* @param[out] *pBuf    Received data.
* @param[in]  len      Length of data to receive.
*/
void idxHalSpiRx(uint8_t *pBuf, uint16_t len);

/** \brief Set SPI clock speed.
*/
void idxHalSpiSetSpeed(idxHalSpiSpeedType speed);

/** @}*/
#endif /* _IDX_HAL_SPI_H_ */
