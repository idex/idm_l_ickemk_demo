/*
 *  Copyright (c) 2012, Beijing Tongfang Microelectroics Co., Ltd.
 *  All rights reserved.
 * 
 *  FileName: hal.c
 *  SCFID: HAL
 *  Feature: hal api
 *  Version: V0.1
 * 
 *  History: 
 *    2011-12-31 by Luqian
 *      1. Original version 0.1
 * */
#ifndef __HALINTER__
#define __HALINTER__

//SM1
#define SM1_OVER	(SM1STS & 0x01)
//SM4
#define SM4ENCTRANS 	0x01
#define SM4CBC		 	0x02
#define SM4ENC		 	0x04
#define SM4REDOINIT	 	0x08
#define SM4HAVEKEY	 	0x10
#define SM4HAVEIV	 	0x20
#define SM4HAVEDATA	 	0x40
//SSF33
#define SSF33_OVER	(SSF33STS & 0x01)

extern u8 volatile g_testSystick;
extern u8 volatile g_testWDT;

//============================================================
//EXTERNAL function declarations
#define RAMLoadByte(pbAddr) (*((u8*)(pbAddr)))
#define RAMLoadWord(pbAddr) ((u16)( ((*((u8*)(pbAddr+1)))<<8) | (*((u8*)(pbAddr))) ))
#define RAMLoad3Byte(pbAddr) ((u32)( ((*((u8*)(pbAddr+2)))<<16) | ((*((u8*)(pbAddr+1)))<<8) | ((*((u8*)(pbAddr)))) ))
#define RAMLoadDWord(pbAddr) ((u32)( ((*((u8*)(pbAddr+3)))<<24) | ((*((u8*)(pbAddr+2)))<<16) | ((*((u8*)(pbAddr+1)))<<8) | ((*((u8*)(pbAddr)))) ))

#define RAMWriteByte(pbAddr, bValue)  {*((u8*)(pbAddr)) = (u8)bValue;}
#define RAMWriteWord(pbAddr, wValue)  { \
										  *((u8*)(pbAddr+1)) = (u8)(wValue>>8); \
										  *((u8*)(pbAddr)) = (u8)(wValue&0xFF); \
									  }
#define RAMWrite3Byte(pbAddr, dwValue) { \
										  *((u8*)(pbAddr+2)) = (u8)(dwValue>>16); \
										  *((u8*)(pbAddr+1)) = (u8)(dwValue>>8); \
										  *((u8*)(pbAddr)) = (u8)(dwValue&0xFF); \
									   }
#define RAMWriteDWord(pbAddr, dwValue) { \
										  *((u8*)(pbAddr+3)) = (u8)(dwValue>>24); \
										  *((u8*)(pbAddr+2)) = (u8)(dwValue>>16); \
										  *((u8*)(pbAddr+1)) = (u8)(dwValue>>8); \
										  *((u8*)(pbAddr)) = (u8)(dwValue&0xFF); \
									   }

#define RamStoreWord( pbAddr, wValue ) { \
										  *((u8*)(pbAddr)) = (u8)(wValue>>8); \
										  *((u8*)(pbAddr+1)) = (u8)(wValue&0xFF); \
									  }
#define RamStoreDword(pbAddr, dwValue) { \
										  *((u8*)(pbAddr)) = (u8)(dwValue>>24); \
										  *((u8*)(pbAddr+1)) = (u8)(dwValue>>16); \
										  *((u8*)(pbAddr+2)) = (u8)(dwValue>>8); \
										  *((u8*)(pbAddr+3)) = (u8)(dwValue&0xFF); \
									   }
#define RamLoadWord(pbAddr)			   ((u16)( ((*((u8*)(pbAddr)))<<8) | (*((u8*)(pbAddr+1))) ))
#define RamLoadDword(pbAddr)		   ((u32)( ((*((u8*)(pbAddr)))<<24) | ((*((u8*)(pbAddr+1)))<<16) | ((*((u8*)(pbAddr+2)))<<8) | ((*((u8*)(pbAddr+3)))) ))
#define B2L(x) (*(x+3)*0x1000000 + *(x+2)*0x10000 + *(x+1)*0x100 + *(x))
//1122---0x2211
#define B2S(x) (*(x+1)*0x100 + *(x))
//11223344---0x11223344
#define B2LT(x) (*(x)*0x1000000 + *(x+1)*0x10000 + *(x+2)*0x100 + *(x+3))
//1122---0x1122
#define B2ST(x) (*(x)*0x100+*(x+1))

//typedef
typedef enum IRQn
{
/******  Cortex-M0 Processor Exceptions Numbers ***************************************************/
  NonMaskableInt_IRQn         	= -14,    /*!< 2 Non Maskable Interrupt                             */
  HardFault_IRQn	      		= -13,    /*!< 3 Cortex-M0 Hard Fault Interrupt                     */
  SVCall_IRQn                 	= -5,     /*!< 11 Cortex-M0 SV Call Interrupt                       */
  PendSV_IRQn                 	= -2,     /*!< 14 Cortex-M0 Pend SV Interrupt                       */
  SysTick_IRQn                	= -1,     /*!< 15 Cortex-M0 System Tick Interrupt                   */

/******  CM0IKMCU Cortex-M0 specific Interrupt Numbers ********************************************/
	CT_IRQn					=0,
	TIMER0_IRQn				=1,
	NVM0_IRQn				=2,
	ALGORITHM_IRQn			=3,
	SECURITY_IRQn			=4,
	DMA_IRQn				=5,
	APP_IRQn				=6,
	TIMER1_IRQn				=7,
	TIMER2_IRQn				=8,
	TIMER3_IRQn				=9,
	NVM1_IRQn				=10,
	NVM2_IRQn				=11,
	NVM3_IRQn				=12,
	SWP_IRQn				=13,
	SPI_IRQn				=14,
	GPIO_IRQn				=15,
	USB_IRQn				=16,
	RF_IRQn					=17,
	CM0IKMCU_IRQ18_IRQn		= 18,
	CM0IKMCU_IRQ19_IRQn		= 19,
	CM0IKMCU_IRQ20_IRQn		= 20,
	CM0IKMCU_IRQ21_IRQn		= 21,
	CM0IKMCU_IRQ22_IRQn		= 22,
	CM0IKMCU_IRQ23_IRQn		= 23,
	CM0IKMCU_IRQ24_IRQn		= 24,
	CM0IKMCU_IRQ25_IRQn		= 25,
	CM0IKMCU_IRQ26_IRQn		= 26,
	CM0IKMCU_IRQ27_IRQn		= 27,
	CM0IKMCU_IRQ28_IRQn		= 28,
	CM0IKMCU_IRQ29_IRQn		= 29,
	CM0IKMCU_IRQ30_IRQn		= 30,
	CM0IKMCU_IRQ31_IRQn		= 31
} IRQn_Type;

/*
* Function: u8 DMAMemcmp(unsigned char *pbDest, unsigned char *pbSrc, unsigned short wLen)
* Description: Data compare.
* Input:  pbDest - destination address to compare
*		  pbSrc  - source address
*         mode - data length
* Output: - 
* Return: 0: Data is the same
*		  1: Data is difference 
*/
u8 DMAMemcmp(unsigned char *pbDest, unsigned char *pbSrc, unsigned short wLen);

#endif
