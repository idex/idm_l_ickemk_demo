/******************************************************************************
* \file  idxBioLed.c
* \brief code for controling leds
* \author IDEX ASA
* \version 0.0.1
*******************************************************************************
* Copyright
* 2013-2019 IDEX ASA. All Rights Reserved.www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
*
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is
* provided  "AS IS", with no express or implied warranties of any
* kind, and IDEX ASA is not to be liable for any damages, any relief, or for
* any claim by any third party, arising from use of this software.
******************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "idxBioLed.h"

#include "bio-ctrl.h"
#include "idxSeEmkDefines.h"

#include "idxHalTimer.h"
#include "idxHalGpio.h"

#ifdef TH89
#include "halglobal.h"      // for __WFI  definition
#endif

#define UNUSED(p) if(p){}

#if defined(TH89)
// TODO: Legacy routine to be replaced.
void idxHalTimerPeriodicRun(uint16_t msec, idxHalTimerCallback *cb);
#endif

#if defined(SLE78)
static volatile uint8_t blinkCtrl = 0;
#else
volatile uint8_t blinkCtrl = 0;
#endif

/**
* @brief LED Control Function Declarations
*
* @return IDX_ISO_SUCCESS if successful,
*         or another error code (taken from wCode in reply header) if not successful
*/

#define SETPARAMETER_CMD_DATA_SIZE (3)
static uint16_t idxLedStart(const idxBuffer_t *pCommsBuffer)
{
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = 0x10; pDataBuf[2] = 0x88;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xa0; }
  
  return wCode;
}

static uint16_t idxLedStop(const idxBuffer_t *pCommsBuffer)
{
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb8; }	
  
  return wCode;
}

static uint16_t idxLed2ndFinger(const idxBuffer_t *pCommsBuffer)
{
  // 2nd Finger - LED control here
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = 0x80; pDataBuf[2] = 0x88;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb6; return wCode; }	
  //sleepCnt(20000);
  idxHalTimerDelayMsec(2000);
  data_size = SETPARAMETER_CMD_DATA_SIZE;
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb7; }
  
  return wCode;
}

static uint16_t idxLedTouch( uint8_t LedColor, const idxBuffer_t *pCommsBuffer )
{
  // Touch - LED control here
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = LedColor; pDataBuf[2] = 0;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb4; return wCode; }	
  //sleepCnt(10000);
  idxHalTimerDelayMsec(1000);
  data_size = SETPARAMETER_CMD_DATA_SIZE;
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb5; }
  
  return wCode;
}

static uint16_t idxLedLastTouch(const idxBuffer_t *pCommsBuffer)
{
  // Last Touch - LED control here
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = 0x01; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer); 
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb1; return wCode; }	
  //sleepCnt(5000);
  idxHalTimerDelayMsec(500);
  data_size = SETPARAMETER_CMD_DATA_SIZE;
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb2; }

  return wCode;
}

static uint16_t idxLedBlink(const idxBuffer_t *pCommsBuffer)
{
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = 0x10; pDataBuf[2] = 0x88;	
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xb0; }

  return wCode;
}

uint16_t ledOnOff(uint8_t LedColor, const idxBuffer_t *pCommsBuffer)
{
  uint16_t wCode;
  uint16_t duration = 500;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;  
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = LedColor; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xba; return wCode; }	
  idxHalTimerDelayMsec(duration);
  data_size = SETPARAMETER_CMD_DATA_SIZE;  
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) { ctrlErr = 0xbb; }
  
  return wCode;
}

#ifdef SLE78

static void SleeveLEDOff()
{
  idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_FLOAT);
}

////////////////////////////////////////////////////////////////////////////////
// Callback for timer associated with LEDs
static void sleeveLedTimerCallback()
{
  if( blinkCtrl & 0xfc){
    blinkCtrl -= 4;
    blinkCtrl |= 1;
  }
  if(blinkCtrl & 1){ 
    blinkCtrl &= ~1;
    if( blinkCtrl & 2){
      idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_HIGH);
    }
    else{
      idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_LOW);
    }
  }
  else{
    blinkCtrl |= 1;
    idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_FLOAT);
  }
}
#endif // SLE78

#if defined(TH89)

static void led1on(void){
  idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_HIGH);
}  

static void led2on(void){
  idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_LOW);
}  

static void ledsOff(void){
  idxHalGpioSet(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_FLOAT);
}

void SleeveLEDOff( void )
{
	ledsOff();
}

static void sleeveLedTimerCallback(void)
{    
  if( blinkCtrl & 0xfc){
    blinkCtrl -= 4;
    blinkCtrl |= 1;
  }
  if(blinkCtrl & 1){ 
    blinkCtrl &= ~1;
    if( blinkCtrl & 2){
      led1on();
    }
    else{
      led2on();
    }
  }
  else{
    blinkCtrl |= 1;
    ledsOff();
  }
}

#endif

/**********************************************************************************************
* Turn on First finger indicator LED and start flashing the first touch indicator LED on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlStart(const idxBuffer_t *pCommsBuffer)
{
//-------------------------- SLE78 ----------------------------------  
#ifdef SLE78
#ifdef CARD_LEDS
  idxLedStart(pCommsBuffer);    
#else // CARD_LEDS
  UNUSED(pCommsBuffer);
#endif
  // Call sleeveLedTimerCallback() every 250ms
  idxHalTimerTimeOut(250, IDX_HAL_TIMER_REPEAT, sleeveLedTimerCallback);
  return IDX_ISO_SUCCESS;
//-------------------------- TH89 ----------------------------------    
#elif defined(TH89)
  {
	idxHalGpioConfigure(IDX_HAL_GPIO_ISO_IO, IDX_HAL_GPIO_SWIO, IDX_HAL_GPIO_HIGH);
  idxHalTimerTimeOut(250,IDX_HAL_TIMER_REPEAT,sleeveLedTimerCallback); // configureTimer2(0);
  return IDX_ISO_SUCCESS;
  }
//-------------------------- ??? ----------------------------------  
#else
  UNUSED(pCommsBuffer);		
  return IDX_ISO_ERR_NOT_SUPPTD;
#endif
}

/**
* Stop flashing all the LEDs on sleeve.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlStop(void)
{
//-------------------------- SLE78 ----------------------------------  
#if defined(SLE78)
#ifdef CARD_LEDS
  idxLedStop();
#endif // CARD_LEDS
  idxHalTimerStopRun();
    SleeveLEDOff();
    return IDX_ISO_SUCCESS;
//-------------------------- TH89 ----------------------------------    
#elif defined(TH89)
  SleeveLEDOff();
  idxHalTimerStopRun();
  return IDX_ISO_SUCCESS;
#else
//-------------------------- ??? ----------------------------------    
    return IDX_ISO_ERR_NOT_SUPPTD;
#endif
}

/**
* Flash finger indicator LED after enrollment finished and turn on 3 touch indicator LEDs.
* first finger finished, flashing first finger indicator LED.
* sencond finger finished, flashing both of the two finger indicator LEDs.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlEnrollFinished()
{
//-------------------------- SLE78 ----------------------------------   
#ifdef SLE78
	return IDX_ISO_SUCCESS;
//-------------------------- SLCx2 ----------------------------------  
#elif defined(SLCx2)
	return IDX_ISO_SUCCESS;
//-------------------------- TH89 ----------------------------------      
#elif defined(TH89)
	return IDX_ISO_SUCCESS;  
#else
//-------------------------- ??? ----------------------------------  
	return IDX_ISO_ERR_NOT_SUPPTD;
#endif
}

/**
* Turn on the second finger indicator LED on sleeve after first finger enrolled.
* Start flashing first touch indicator LED.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlNextFinger(const idxBuffer_t *pCommsBuffer)
{
//-------------------------- SLE78 ----------------------------------  
#ifdef SLE78
  uint32_t i;

#ifdef CARD_LEDS
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = 0x80; pDataBuf[2] = 0x88;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) return wCode;
#else //CARD_LEDS
  UNUSED(pCommsBuffer);
#endif
  
  for ( i = 0; i < 16; i++ )
  {
    blinkCtrl |= 1*4; // Set counter to 1
    while( blinkCtrl & 0xfc); // Test counter to see if it is 0
    blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch)
  }
#ifdef CARD_LEDS
  data_size = SETPARAMETER_CMD_DATA_SIZE;
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0x00;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) return wCode;
  idxLedBlink();
#endif //  CARD_LEDS
  
  return IDX_ISO_SUCCESS;
//-------------------------- TH89 ----------------------------------      
#elif defined(TH89)
  {
    uint32_t i;
    for ( i = 0; i < 16; i++ )
	{
      blinkCtrl |= 1*4; // Set counter to 1
      while( blinkCtrl & 0xfc)__WFI(); // Test counter to see if it is 0
      blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch)
    }
    return IDX_ISO_SUCCESS;
  }
#else
//-------------------------- ??? ----------------------------------  
  UNUSED(pCommsBuffer);		
  return IDX_ISO_ERR_NOT_SUPPTD;  
#endif
}

/**
* Turn off all touch indicator LED first then flash the next touch indicator LED on sleeve after each touch.
*
* @return      success or error code.
*/
static uint16_t sleeveLEDCtrlNextTouch(const idxBuffer_t *pCommsBuffer)
{
//-------------------------- SLE78 ----------------------------------  
#ifdef SLE78
#ifdef CARD_LEDS
  uint16_t wCode;
  uint16_t data_size = SETPARAMETER_CMD_DATA_SIZE;
  uint8_t *pDataBuf = &pCommsBuffer->pBuffer[HDR_SIZE];
  pDataBuf[0] = 135; pDataBuf[1] = LED_COLOR_GREEN; pDataBuf[2] = 0;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) return wCode;
#else // CARD_LEDS
  UNUSED(pCommsBuffer);	
#endif  
  blinkCtrl |= 8*4; // Set counter to 8
  while( blinkCtrl & 0xfc); // Test counter to see if it is 0
  blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch)
  
#ifdef CARD_LEDS
  data_size = SETPARAMETER_CMD_DATA_SIZE;
  pDataBuf[0] = 135; pDataBuf[1] = 0x00; pDataBuf[2] = 0;
  wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer);
  if( IDX_ISO_SUCCESS != wCode) return wCode;
  {
    data_size = SETPARAMETER_CMD_DATA_SIZE;
	pDataBuf[0] = 135; pDataBuf[1] = 0x10; pDataBuf[2] = 0x88;
    wCode = idxMcuComms(SETPARAMETER_CMD, 0x00, &data_size, pCommsBuffer); 
    if( IDX_ISO_SUCCESS != wCode) return wCode;
  }
#endif // CARD_LEDS
  return IDX_ISO_SUCCESS;	
#elif defined(SLCx2)
  UNUSED(pCommsBuffer);		
  return IDX_ISO_SUCCESS;
//-------------------------- TH89 ----------------------------------    
#elif defined(TH89)
  {
    blinkCtrl |= 8*4; // Set counter to 8
	while( blinkCtrl & 0xfc)__WFI(); // Test counter to see if it is 0
	blinkCtrl ^= 2;  // toggle 2nd bit (i.e. new touch)
	return IDX_ISO_SUCCESS;	
  } 
//-------------------------- ??? ----------------------------------  
#else	
  UNUSED(pCommsBuffer);		
  return IDX_ISO_ERR_NOT_SUPPTD;
#endif
}

/**
* Function for controlling LEDs.
*
* @param[in]  state          Enrollment state machine.
*
* @param[in]  pCommsBuffer   Pointer to a structure giving the size and location
*                            of the communication buffer. Any command data is
*                            passed in this buffer (starting at
*                            pCommsBuffer->pBuffer[IDX_BIO_BUFFER_DATA_OFFSET])
*                            and, if pRespDataBuf is NULL, any response data is
*                            returned in the same location.
*
* @return      success or error code.
*/
uint16_t idxBioLedSetMode( idxLedModeStateType state, const idxBuffer_t *pCommsBuffer )
{
	switch( state )
	{
  case IDX_CARD_LED_START:
    return idxLedStart(pCommsBuffer);
	case IDX_SLEEVE_LED_START:
		return sleeveLEDCtrlStart(pCommsBuffer);
  case IDX_CARD_LED_NEXT_FINGER:
    return idxLed2ndFinger(pCommsBuffer);
	case IDX_SLEEVE_LED_NEXT_FINGER:
		return sleeveLEDCtrlNextFinger(pCommsBuffer);
  case IDX_CARD_LED_TOUCH_SUCCESS:
    return idxLedTouch(LED_COLOR_GREEN, pCommsBuffer);
	case IDX_SLEEVE_LED_NEXT_TOUCH:
		return sleeveLEDCtrlNextTouch(pCommsBuffer);
  case IDX_CARD_LED_TOUCH_FAIL:
    return idxLedTouch(LED_COLOR_RED, pCommsBuffer);
  case IDX_CARD_LED_LAST_TOUCH:
    return idxLedLastTouch(pCommsBuffer);
  case IDX_SLEEVE_LED_ENROLL_FINISHED:
		return sleeveLEDCtrlEnrollFinished();
  case IDX_CARD_LED_STOP:
    return idxLedStop(pCommsBuffer);
	case IDX_SLEEVE_LED_STOP:
		return sleeveLEDCtrlStop();
  case IDX_CARD_LED_BLINK:
    return idxLedBlink(pCommsBuffer);
	case IDX_MATCH_STATE_SUCCESS:
    return ledOnOff(LED_COLOR_GREEN, pCommsBuffer);
	case IDX_MATCH_STATE_FAIL:
    return ledOnOff(LED_COLOR_RED, pCommsBuffer);
	default: break;
	}
	
  return IDX_ISO_SUCCESS;
}
