/******************************************************************************
* @file  idxIck.h
* @brief header for Image capture kit
* @author IDEX ASA
* @version 0.0.0
*******************************************************************************
* Copyright 2013-2017 IDEX ASA. All Rights Reserved. www.idexbiometrics.com
*
* IDEX ASA is the owner of this software and all intellectual property rights
* in and to the software. The software may only be used together with IDEX
* fingerprint sensors, unless otherwise permitted by IDEX ASA in writing.
* 
* This copyright notice must not be altered or removed from the software.
*
* DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: unless otherwise agreed, IDEX
* ASA has no obligation to support this software, and the software is provided
* "AS IS", with no express or implied warranties of any kind, and IDEX ASA is
* not to be liable for any damages, any relief, or for any claim by any third
* party, arising from use of this software.
*  
* Image capture and processing logic is defined and controlled by IDEX ASA in
* order to maximize FAR/FRR performance.
******************************************************************************/

#ifndef __IMAGE_CAPTURE_H__
#define __IMAGE_CAPTURE_H__

#include <inttypes.h>
#include <string.h>

#include "idxBaseTypes.h"
#include "idxIckDefines.h"

#include "idxHeapAPI.h"
#include "idxSensorDriverAPI.h"

#ifdef  __cplusplus
extern "C" {
#endif

typedef struct ICKConfigParams_t
{
	uint32_t params[3];
	void*	env;
	void*	pSecCtx;
} ICKConfigParams;

enum INIT_ICK_CONFIG_ID
{
	INIT_ICK_CONFIG_ID_FLAGS = 0,
	INIT_ICK_CONFIG_ID_BASELINE = 0,  //keep this for compatibility
	INIT_ICK_CONFIG_ID_BITDEPTH,
	INIT_ICK_CONFIG_ID_IPFLAGS
};

enum CONFIG_FLAGS
{
	CONFIG_FLAG_NONE = 0,
	DONT_RECREATE_BASELINE = 0, //keep this for compatibility
	RECREATE_BASELINE = 1,
	CONFIG_FLAG_RAW_MODE = 2
};

#define PREPIMG_FLAG_MASK		0x3F

#define PREPIMG_NON_BLOCKING	0x80	// this is to avoid prepare image waiting for the image, in case it is handled by the app outside of the functions
#define PREPIMG_BLOCKING		0x00

#define PREPIMG_PACKED			0x40	// this is to prepare image and keep image in packed form - for later get it by using GETIMAGE_COMPRESSED.
#define PREPIMG_UNPACKED		0x00

enum FINGER_PRESENCE_OPTIONS
{
	FINGER_PRESENCE_STATE_NOINFO,
	FINGER_PRESENCE_STATE_OK,
	FINGER_PRESENCE_STATE_EARLY_REMOVED,
	FINGER_PRESENCE_STATE_SLOW_TOUCH,
	FINGER_PRESENCE_STATE_MOVED_DURING_SCAN,
	FINGER_PRESENCE_STATE_NO_CENTROID,
	FINGER_PRESENCE_STATE_OFFSET_PROBLEM,
};

typedef struct PACKED_ATTRIBUTE __idx_ImageFingerProperties
{
	short imagePercentCovered;
	short imageSignalAveLevel;
	short imageSignalLevel;
	short fingerPresencePercentCovered;
	short fingerPresenceStatus;
	unsigned char imageFingerPatternAfterScan;
	unsigned char imageFingerPatternBeforeScan;
} idx_ImageFingerProperties_t;

/* Tunnel commands IDs - can be send by idxIckTunnelData()*/
enum ICK_SEC_TUNNEL_CMD_ID
{
	ICK_SEC_TUNNEL_CMD_ID_REQ_ID = 0,         // Secure Command Request Identity
	ICK_SEC_TUNNEL_CMD_ID_REQ_CHAIN_BY_IDX,   // Secure Command Request Chain By Index
	ICK_SEC_TUNNEL_CMD_ID_START_PAIRING,      // Secure Command Start Pairing
	ICK_SEC_TUNNEL_CMD_ID_NEW_SESS_KEY,       // Secure Command New Session Key
	ICK_SEC_TUNNEL_CMD_ID_UNDEFINED           // 
};

/**
* @brief Function used to initialize the sensor
*
*
* @param[in] SensorDriverCallbacks *drivercallbacks: The driver callbacks needs to be sent using this structure
* @param[in] SensorHeapCallbacks *heapCallbacks: the heap callbacks  needs to be sent using this structure
* @param[in] ICKConfigParams *ickConfig: Can contain some paramters that could be used to initialize
*
* @return idx_ReturnCode_t result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckInit( const SensorDriverCallbacks* driverCallbacks, const SensorHeapCallbacks* heapCallbacks, const ICKConfigParams* ickConfig );

/**
* @brief Function used to Uninitialize the sensor
*
*
* 
*
*
* @return idx_ReturnCode_t result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckUninitialize(void);

/**
* @brief Function used to prepare the image in the sensor
*
*
* @param[in] void *pServiceCallback  A service pointer if needed. Can be used to receive callbacks
* @param[in] int timeout_ms  Timeout in milliseconds. the function waits for the duration of timeout to detect a finger.
* @param[in] unsigned char scanFlags  Different scanning modes that could be used to fetch the image. Default is PREPIMG_FINGERDETECT
*
* @return int result returns Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckPrepareImage(void *pServiceCallback, int timeout_ms, PREP_IMAG_OPTIONS scanFlags);

/**
* @brief Function used to transfer the image that is scanned from sensor to host. 
*
* @return int result returns Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckTransferImage( void );

/**
* @brief Function used to process the image. The image data is given out as, N rows by M columns, 
*
*
* @param[out] int *buffer_size  Size of the image that is provided
* @param[out] unsigned char **dataGetImage  The pointer where the image is located
*
* @return int result returns Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckProcessImage( int *buffer_size, unsigned char **dataGetImage );

/**
* @brief Function used to get the value of a parameter/configuration
*
* @param[in] unsigned char param_id  parameter id of the parameter whose value needs to be fetched
* @param[out] unsigned short *value  Value of the parameter.
*
* @return int result returns Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckGetParameter( unsigned char param_id, unsigned short *value );

/**
* @brief Function used to set the value of a parameter/configuration
*
*
* @param[in] unsigned char param_id  parameter id of the parameter whose value needs to be set
* @param[in] unsigned short value  Value of the parameter.
*
* @return int result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckSetParameter(unsigned char param_id, unsigned short value);

/**
* @brief Function used to abort preparing an image
*
*
* @return int result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckAbort(void);

/**
* @brief Functions used to suspend/resume fingerprint operations
*
*
* @return int result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckSuspend(void );
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckResume(void );

/**
* @brief Function used to send data direct to the Sensor - will bypass all security
* options (required for messages in section 3.1 to 3.4 above)
*
* @param[in] unsigned short tunnelInDataSz: Size of data to be sent to sensor in
* bytes
* @param[in] unsigned char* tunnelInData: Data to be sent to sensor
* @param[in/out] unsigned short *tunnelOutDataSz: Size of data buffer to receive data
* from sensor on input. Size of data received on output. If buffer is not big enough
* to accept received data, on output this will indicate the required size of the
* buffer and an error code will be returned.
* @param[out] unsigned char* tunnelOutData: Data received from the sensor
*
* @return idx_ReturnCode_t result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckTunnelData( unsigned short tunnelInDataSz, unsigned char *tunnelInData, unsigned short *tunnelOutDataSz, unsigned char *tunnelOutData );

/**
* @brief Function to get the width of the image provided by the sensor
*
*
* @return int sizeX returns width of the image expected from the sensor
*/
CFUNC_DECLSPEC int CDECL_DECLSPEC idxIckGetSizeX(void );

/**
* @brief Function to get the length of the image provided by the sensor
*
*
* @return int sizeY returns height of the image expected from the sensor
*/
CFUNC_DECLSPEC int CDECL_DECLSPEC idxIckGetSizeY( void);

/**
* @brief Function to get the dpi of the image provided by the sensor
*
*
* @return int DPI returns the dpi of the image expected from the sensor
*/
CFUNC_DECLSPEC int CDECL_DECLSPEC idxIckGetSensorDPI(void);

/**
* @brief Function to get the depth of the image provided by the sensor
*
*
* @return int depth returns the depth of the image expected from the sensor
*/
CFUNC_DECLSPEC int CDECL_DECLSPEC idxIckGetImageBitDepth(void);

/**
* @brief Function used to get the version
*
* @param[out] idx_UnitVersion_t *device_version  Firmware version informaton.
* @param[out] idx_UnitVersion_t *stack_version  Sensor stack SW version informaton.
* @param[out] char **ppFirmwareFuzeIdStr  The pointer where the FuzeID is located
*
* @return int result returns Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckGetSensorVersion( idx_UnitVersion_t *device_version, idx_UnitVersion_t *stack_version, char **ppFirmwareFuzeIdStr );

/**
* @brief Function used to get the ick version
*
* @param[out] idx_UnitVersion_t *ick_version  Get the ICK version informaton.
*
* @return void
*/
CFUNC_DECLSPEC void CDECL_DECLSPEC idxIckGetUnitVersion( idx_UnitVersion_t *ick_version );

/**
* @brief Function used to load sensor firmware binary into RAM or program to flash/OTP
*
* @param[in] FWIMG_TYPE type - type of FW.
* @param[in] uint32_t dest_addr - RAM address or -1 for storing into flash/otp.
* @param[in] uint8_t *buffer - buffer for binary data.
* @param[in] uint32_t buffer_size - size of buffer (may be a partial data for incremental update).
* @param[in] uint32_t image_size - size of total fw binary.
* @param[in] uint32_t image_offset - offset for provided partial binary data.
* @param[in] uint32_t image_crc - crc of total fw binary.
*
* @return idx_ReturnCode_t result Returns error number and idx_ret
*/
CFUNC_DECLSPEC idx_ReturnCode_t CDECL_DECLSPEC idxIckLoadFirmware( FWIMG_TYPE type, uint32_t dest_addr, uint8_t *buffer, uint32_t buffer_size, uint32_t image_size, uint32_t image_offset, uint32_t image_crc );

/**
* @brief This single function will be used to call different functions with different parameters
*
* @param[in] int command_id command id IFID_*
*
* @param[in] void *inStruct input structure to transfer parameters and data
* @param[in] void *outStruct out structure to return data after performing the function
*
* @return int result returns success(0) or error (-1)
*/

CFUNC_DECLSPEC int CDECL_DECLSPEC idxIckInternalFuncs( int command_id, void* inStruct, void* outStruct );

//////////////////////////////////////////////////////
//
// IFID_* - set of internal functions list - for idxIckInternalFuncs(...)
//
#define IFID_NONE                           (0)
#define IFID_CalibrateSensor                (1)  
#define IFID_OpenShortTest                  (2)  
#define IFID_IsAsyncPrepareImageComplete    (3)  
#define IFID_GetCalibrationData             (4)
#define IFID_GetComponentVersion            (5)
#define IFID_GetHuuid						(6)
#define IFID_FingerDetect					(7)
#define IFID_IsSupportedFWOnSensor			(8)
#define IFID_SendRawSensorCommand			(9)
#define IFID_SendRawSensorCommandDeferred	(10)

typedef struct 
{
	unsigned char min_level;
	unsigned char max_level;
} idx_OpenShortData_t;

typedef struct
{
	void*		pIn;		//input arguments if any, or NULL
	uint16_t	in_size;	//size of input data
	uint8_t		command_id; //command id
	uint8_t		security_status; //security status 0 - clear 3 - encrpted and signed 
} idx_RawCmdInSecData_t;

typedef struct
{
	void*		pOut;		//output arguments if any, or NULL
	uint16_t	out_size;	//size of expected output data
	uint8_t		security_status; //actual security status from received response
} idx_RawCmdOutSecData_t;

/* example using IFID_CalibrateSensor:
--------------------------------------

idxIckInternalFuncs( IFID_CalibrateSensor, ( void* ) forceRecal, ( void* ) storeCalibration );

Parameters:
bool forceRecal;
	true  - recalibrate sensor regardless of whether it is calibrated or not
	false - calibrate only if sensor is not calibrated
bool storeCalibration;
	true  - store calibration data into OTP/flash
	false - do not store calibration block into OTP/flash

Function operation based on combination of parameters:
 forceRecal  storeCalibration
	false       false            - calibrate only when sensor is not calibrated (this is the operation that idxIckInitialize() used to perform)
	false       true             - calibrate only when sensor is not calibrated and then store to OTP/flash
	true        false            - force recalibration of sensor
	true        true             - force recalibration of sensor and then store to OTP/flash

Return values:
	0                            - Success
	-EALREADY                    - Operation already performed or not required
	Other                        - Error

example using IFID_GetCalibrationData:
--------------------------------------

	int calibration_data_size = 0;
	unsigned char *calibration_data_ptr = NULL;
	int status = idxIckInternalFuncs( IFID_GetCalibrationData, ( void* ) &calibration_data_ptr, ( void* ) &calibration_data_size );

	Return values:
	0                           - Success 
	-EINVAL						- Wrong arguments
	Other                       - Error

example using IFID_OpenShortTest:
--------------------------------------

	int errorCount = 0;
	idx_OpenShortData_t data;
	data.min_level = 50;
	data.max_level = 200;
	int status = idxIckInternalFuncs( IFID_OpenShortTest, ( void* ) &data, ( void* ) &errorCount );

	Return values:
	0                           - Success (for open-short test results please see "errorCount"
	-EBADF						- Sensor is not calibrated
	-EINVAL						- Wrong arguments
	Other                       - Error

example using IFID_GetHuuid:
--------------------------------------

	huuid_t huuid;
	char huuidStr[37];
	int status = idxIckInternalFuncs(IFID_GetHuuid, &huuidStr, &huuid);

	Return values:
	0                           - Success
	-ENOENT						- Sensor context is null.
	-EINVAL						- Wrong arguments
	Other                       - Error

example using IFID_FingerDetect:
--------------------------------------
bool wait, bool on_off
idxIckInternalFuncs( IFID_FingerDetect, ( int* ) timeout, ( void* ) on_off );

Parameters:
int timeout;
	>0  - wait till requested finger presence state or return current finger info after timeout
	0 - return immediately the current finger presense info (for polling)
	-1 - use default timeout
bool on_off;
	true  - check for finger_on state
	false - check for finger_off state

Return values:
	0                     - finger off
	1                     - finger on
	-ENOENT               - Sensor context is null.

possible scenario for using
Instead of doing:
	idxIckPrepareImage(... PREPIMG_FINGERDETECT);
	idxIckTransferImage();
	idxIckProcessImage(...);
could be:
	idxIckInternalFuncs( IFID_FingerDetect, &timeout, true ); // Wait for Finger On
	idxIckPrepareImage(... PREPIMG_FINGERDETECT ); // Will insert settling time
	idxIckTransferImage();
	idxIckInternalFuncs( IFID_FingerDetect, &timeout, false ); // Optional - wait for Finger Off before next scan
	idxIckProcessImage(...);
or
Instead of doing:
	idxIckPrepareImage(... PREPIMG_NEWFINGERDETECT);
	idxIckTransferImage();
	idxIckProcessImage(...);
could be:
	idxIckInternalFuncs( IFID_FingerDetect, &timeout, false ); // Wait for Finger Off
	idxIckInternalFuncs( IFID_FingerDetect, &timeout, true ); // Wait for Finger On
	Sleep(SETTLING_TIME_DELAY); // insert settling time 
	idxIckPrepareImage(... PREPIMG_LIVE); // Will collect image immediately
	idxIckTransferImage();
	idxIckProcessImage(...);

example using IFID_SendRawSensorCommand:
--------------------------------------
note: IFID_SendRawSensorCommandDeferred uses the same parameters as IFID_SendRawSensorCommand.
      IFID_SendRawSensorCommandDeferred should be used for asynchronous FW's commands(for example WFF command).

idxIckInternalFuncs( IFID_SendRawSensorCommand, ( void* ) inData, ( void* ) outData );

Parameters:
inData   [in]  - pointer to idx_RawCmdInSecData_t.
outData	 [out] - pointer to idx_RawCmdOutSecData_t. if NULL - do not need to retrieve output data from response packet

Return values:
0                           - Success
-ENOENT						- Sensor context is null.
-EINVAL						- Wrong arguments
Other                       - Error

idx_RawCmdInSecData_t inData;
inData.pIn = &collectBaselineInputData[0];
inData.in_size = 20;
inData.command_id = 0xE8; //ICP_CMD_ID_TEST_COLLECT_BACKGRND

int status = idxIckInternalFuncs(IFID_SendRawSensorCommand, &inData, NULL);

*/

#ifdef  __cplusplus
}
#endif

#endif //__IMAGE_CAPTURE_H__

